<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return redirect('/');
});

Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return redirect('/');
});


Route::get('/', 'Admin\LoginControllers@login');
Route::get('/admin-panel' , 'Admin\HomeController@index');
Route::get('/resetpassword', 'Admin\LoginControllers@resetpassword');
Route::post('/kirimpassword', 'Admin\LoginControllers@kirimPasswordBaru');

Route::get('/admin-panel/login' , 'Admin\LoginControllers@login');
Route::post('/admin-panel/login' , array('as' => 'Login', 'uses' => 'Admin\LoginControllers@loginverify'));

Route::get('/logout', 'Admin\LoginControllers@logout');

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware','prefix' => 'admin-panel'], function()
{

    //HOME----------------------------------------------------------------------------------------------------------
    Route::get('/dashboard', 'Admin\HomeController@dashboard');
    Route::get('/rekap', 'Admin\HomeController@rekap_barang');
    Route::get('/menuviews', 'Admin\WebController@menuviews');
    

    //ROLE----------------------------------------------------------------------------------------------------------
    Route::get('/role' , 'Admin\WebController@role');
    Route::post('/data_role' , 'Admin\WebController@data_role');
    Route::post('/tambahroleaksi' , array('as' => 'Tambahrole', 'uses' => 'Admin\WebController@tambahroleaksi'));
    Route::get('/editroleview/{id}' , 'Admin\WebController@editroleview');
    Route::post('/editroleaksi' , 'Admin\WebController@editroleaksi');
    Route::post('/deleterole' , 'Admin\WebController@roledelete');

    Route::get('/editroleakses/{id}' , 'Admin\WebController@editroleakses');
    Route::get('/editroleaksesaksi' , 'Admin\WebController@editroleaksesaksi');
    Route::post('/data_roleakses/{roleid}' , 'Admin\WebController@data_roleakses');

    //MENU MANAGEMENT-------------------------------------------------------------------------------------------------
    Route::get('/menutitle' , 'Admin\WebController@menutitle');
    Route::post('/data_menutitle' , 'Admin\WebController@data_menutitle');
    Route::post('/tambahmenutitleaksi' , array('as' => 'Tambahmenutitle', 'uses' => 'Admin\WebController@tambahmenutitleaksi'));
    Route::get('/editmenutitleview/{id}' , 'Admin\WebController@editmenutitleview');
    Route::post('/editmenutitleaksi' , 'Admin\WebController@editmenutitleaksi');
    Route::post('/deletemenutitle' , 'Admin\WebController@menutitledelete');

    Route::get('/menu' , 'Admin\WebController@menu');
    Route::post('/data_menu' , 'Admin\WebController@data_menu');
    Route::post('/tambahmenuaksi' , array('as' => 'Tambahmenu', 'uses' => 'Admin\WebController@tambahmenuaksi'));
    Route::get('/editmenuview/{id}' , 'Admin\WebController@editmenuview');
    Route::post('/editmenuaksi' , 'Admin\WebController@editmenuaksi');
    Route::post('/deletemenu' , 'Admin\WebController@menudelete');

    Route::get('/menu/submenu' , 'Admin\WebController@submenu');
    Route::post('/menu/data_submenu' , 'Admin\WebController@data_submenu');
    Route::post('/menu/tambahsubmenuaksi' , array('as' => 'Tambahsubmenu', 'uses' => 'Admin\WebController@tambahsubmenuaksi'));
    Route::get('/menu/editsubmenuview/{id}' , 'Admin\WebController@editsubmenuview');
    Route::post('/menu/editsubmenuaksi' , 'Admin\WebController@editsubmenuaksi');
    Route::post('/menu/deletesubmenu' , 'Admin\WebController@submenudelete');

    Route::post('/menu/aktifsubmenu' , 'Admin\WebController@aktifsubmenu');
    Route::post('/menu/nonaktifsubmenu' , 'Admin\WebController@nonaktifsubmenu');

    //KONFIGURASI 
    //MANAJEMEN USER---------------------------------------------------------------------------------------------------
    Route::get('/manajement/user', 'Admin\HomeController@ManajemenUser');
    Route::post('/manajemen/data_user', 'Admin\HomeController@DataUser');
    Route::get('/manajemen/edituserview/{id}', 'Admin\HomeController@EditUserView');
    Route::get('/manajemen/editpasswordview/{id}', 'Admin\HomeController@EditPasswordView');
    Route::get('/manajemen/view_image/user/{id}', 'Admin\HomeController@UserImageView');
    Route::get('/manajemen/tambahuserview', 'Admin\HomeController@TambahUserView');
    Route::get('/manajemen/view/user/{id}', 'Admin\HomeController@UserView');
    Route::post('/manajemen/tambahuseraksi', array('as' => 'Tambahuser', 'uses' => 'Admin\HomeController@TambahUserAksi'));
    Route::post('/manajemen/edituseraksi/{id}', array('as' => 'Edituser', 'uses' => 'Admin\HomeController@EditUserAksi'));
    Route::post('/manajemen/editpasswordaksi/{id}', array('as' => 'Editpassword', 'uses' => 'Admin\HomeController@EditPasswordAksi'));
    Route::post('/manajemen/deleteuser', 'Admin\HomeController@UserDelete');

    //USER PROFILE
    Route::get('/user/profile', 'Admin\HomeController@UserProfile');
    Route::post('/user/editmyprofile/{id}', array('as' => 'Editmyprofile', 'uses' => 'Admin\HomeController@EditMyProfile'));
    Route::post('/user/changemypassword/{id}', array('as' => 'Changemypassword', 'uses' => 'Admin\HomeController@ChangeMyPassword'));
    ///APLIAKSI
    Route::get('/aplikasi' , 'Admin\WebController@Aplikasi');
    Route::post('/updateaplikasiaksi/{id}' , 'Admin\WebController@UpdateAplikasiAksi');



    //METODE BAYAR----------------------------------------------------------------------------------------------------
    Route::get('/metodebayar' , 'Admin\BarangController@metodeBayar');
    Route::post('/data_metodebayar' , 'Admin\BarangController@dataMetodeBayar');
    Route::post('/tambahmetodebayaraksi' , array('as' => 'Tambahmetodebayar', 'uses' => 'Admin\BarangController@tambahmetodebayaraksi'));
    Route::get('/editmetodebayarview/{id}' , 'Admin\BarangController@editmetodebayarview');
    Route::post('/editmetodebayaraksi/{id}' , 'Admin\BarangController@editmetodebayaraksi');
    Route::post('/deletemetodebayar' , 'Admin\BarangController@metodebayardelete');

    //TOKO----------------------------------------------------------------------------------------------------------
    Route::get('/toko' , 'Admin\TokoController@Toko');
    Route::post('/data_toko' , 'Admin\TokoController@DataToko');
    Route::get('/tambahtokoview' , 'Admin\TokoController@TambahTokoView');
    Route::post('/tambahtokoaksi' , array('as' => 'Tambahtoko', 'uses' => 'Admin\TokoController@TambahTokoAksi'));
    Route::get('/edittokoview/{id}' , 'Admin\TokoController@EditTokoView');
    Route::get('/view_image/toko/{id}' , 'Admin\TokoController@TokoImageView');
    Route::get('/view/toko/{id}' , 'Admin\TokoController@TokoView');
    Route::post('/edittokoaksi/{id}' , array('as' => 'Edittoko', 'uses' => 'Admin\TokoController@EditTokoAksi'));
    Route::post('/deletetoko' , 'Admin\TokoController@TokoDelete');

    Route::get('/aktiftoko/{id}' , 'Admin\TokoController@TokoAktif');
    Route::post('/aktiftokoaksi' , 'Admin\TokoController@TokoAktifAksi');

    Route::get('/nonaktiftoko/{id}' , 'Admin\TokoController@TokoNonAktif');
    Route::post('/nonaktiftokoaksi' , 'Admin\TokoController@TokoNonAktifAksi');

    Route::get('/aktifulangtoko/{id}' , 'Admin\TokoController@TokoAktifUlang');
    Route::post('/aktifulangtokoaksi' , 'Admin\TokoController@TokoAktifUlangAksi');

    Route::post('/toko/data_usertoko' , 'Admin\TokoController@DataUserTokoDetaildata');


    ///GET DATA GLOBAL-------------------------------------------------------------------------------------------------
    Route::get('/gettokoglobal' , 'Admin\TokoController@GetTokoGlobal'); ////select2



    //USER TOKO----------------------------------------------------------------------------------------------------------
    Route::get('/usertoko' , 'Admin\TokoController@UserToko');
    Route::post('/data_usertoko' , 'Admin\TokoController@DataUserToko');
    Route::get('/tambahusertokoview' , 'Admin\TokoController@TambahUserTokoView');
    Route::post('/tambahusertokoaksi' , array('as' => 'Tambahusertoko', 'uses' => 'Admin\TokoController@TambahUserTokoAksi'));
    Route::get('/editusertokoview/{id}' , 'Admin\TokoController@EditUserTokoView');
    Route::get('/view_image/usertoko/{id}' , 'Admin\TokoController@UserTokoImageView');
    Route::get('/view/usertoko/{id}' , 'Admin\TokoController@UserTokoView');
    Route::post('/editusertokoaksi/{id}' , array('as' => 'Editusertoko', 'uses' => 'Admin\TokoController@EditUserTokoAksi'));
    Route::post('/deleteusertoko' , 'Admin\TokoController@UserTokoDelete');
    Route::get('/usertoko/edituserview/{id}', 'Admin\TokoController@EditUserTokoPwView');
    Route::post('/usertoko/editpasswordaksi/{id}', array('as' => 'Editpassword', 'uses' => 'Admin\TokoController@EditUserTokoPwAksi'));

    ////PENJUALAN TOKO-----------------
    Route::get('/penjualantoko', 'Admin\TokoController@PenjualanToko');
    Route::post('/penjualantoko/data', 'Admin\TokoController@DataPenjualanToko');
    Route::get('/penjualantoko/data/detail/{id}' , 'Admin\TokoController@PenjualanTokoDetail');
    Route::post('/penjualantoko/data/detail/aksi', 'Admin\TokoController@PenjualanTokoDetailAksi');


    ////PRODUK TOKO-----------------
    Route::get('/produktoko', 'Admin\TokoController@ProdukToko');
    Route::post('/produktoko/data', 'Admin\TokoController@DataProdukToko');
    Route::get('/produktoko/view_image/toko/{id}' , 'Admin\TokoController@ProdukTokoImageView');


    ////MASA AKTIF TOKO-----------------
    Route::get('/masaaktif', 'Admin\MasaAktifController@MasaAktif');
    Route::post('/masaaktif/data', 'Admin\MasaAktifController@DataMasaAktif');

    ////TAGIHAN TOKO-----------------
    Route::get('/tagihan', 'Admin\TagihanController@Tagihan');
    Route::post('/tagihan/data', 'Admin\TagihanController@DataTagihan');
    Route::post('/tagihan/terimabayar', 'Admin\TagihanController@TagihanTerima');
    Route::post('/tagihan/tolakbayar', 'Admin\TagihanController@TagihanTolak');


    


    //SUPPLIER----------------------------------------------------------------------------------------------------------
    Route::get('/supplier' , 'Admin\SupplierController@Supplier');
    Route::post('/data_supplier' , 'Admin\SupplierController@DataSupplier');
    Route::get('/tambahsupplierview' , 'Admin\SupplierController@TambahSupplierView');
    Route::post('/tambahsupplieraksi' , array('as' => 'Tambahsupplier', 'uses' => 'Admin\SupplierController@TambahSupplierAksi'));
    Route::get('/editsupplierview/{id}' , 'Admin\SupplierController@EditSupplierView');
    Route::get('/view_image/supplier/{id}' , 'Admin\SupplierController@SupplierImageView');
    Route::get('/view/supplier/{id}' , 'Admin\SupplierController@SupplierView');
    Route::post('/editsupplieraksi/{id}' , array('as' => 'Editsupplier', 'uses' => 'Admin\SupplierController@EditSupplierAksi'));
    Route::post('/deletesupplier' , 'Admin\SupplierController@SupplierDelete');
    Route::post('/riwayat_pembelian_supplier' , 'Admin\SupplierController@RiwayatPembelianSupplierView');
    Route::post('/riwayat_return_supplier' , 'Admin\SupplierController@RiwayatReturnSupplierView');
    

    

    //LAPORAN-----------------------------------------------------------------------------------------------------
    ////HISTORY PEMBAYARAN TOKO-----------------
    Route::get('/history_pembayaran', 'Admin\LaporanController@HistPembayaran');
    Route::post('/history_pembayaran/data', 'Admin\LaporanController@DataHistPembayaran');

    ////HISTORY SEWA APP-----------------
    Route::get('/history_sewa', 'Admin\LaporanController@HistSewa');
    Route::post('/history_sewa/data', 'Admin\LaporanController@DataHistSewa');

    ////HISTORY PEMUTUSAN APP-----------------
    Route::get('/history_suspend', 'Admin\LaporanController@HistSuspend');
    Route::post('/history_suspend/data', 'Admin\LaporanController@DataHistSuspend');
    
});

Route::post('/get_reg' , 'Admin\HomeController@get_reg');
Route::post('/get_dist' , 'Admin\HomeController@get_dist');

Route::post('/get_reg_edit' , 'Admin\HomeController@get_reg_edit');
Route::post('/get_dist_edit' , 'Admin\HomeController@get_dist_edit');

Route::get('/antrian' , 'AntrianController@Antrian')->name('antrian');
Route::post('/antrian' , 'AntrianController@AntrianAdd')->name('antrian.add');
Route::get('/antrian/{id}' , 'AntrianController@AntrianPage')->name('antrian.page');
Route::post('/get_barberman' , 'AntrianController@get_barberman');

Route::get('/laporan/umum' , 'Api\LaporanApiController@LaporanUmum');
Route::get('/laporan/penjualan' , 'Api\LaporanApiController@LaporanPenjualan');
Route::get('/laporan/beban' , 'Api\LaporanApiController@LaporanBeban');
Route::get('/laporan/reversal' , 'Api\LaporanApiController@LaporanReversal');
Route::get('/laporan/stock' , 'Api\LaporanApiController@LaporanStock');