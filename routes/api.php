<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'kasir','namespace' => 'App\Http\Controllers\Api'], function($api) {

////////LOGIN (TBL_KASIR)-----------------------------
        $api->post('checkloginkasir', 'LoginApiController@Login');

        $api->post('loadversi', 'HomeApiController@loadversi');
        $api->post('loadkonten', 'HomeApiController@loadkonten');
        $api->post('pendapatan', 'HomeApiController@daily_report');

/////////TOKO---------------------------------------------
        $api->post('edit_store', 'HomeApiController@edit_store');
        $api->post('edit_store_logo', 'HomeApiController@edit_store_logo');

        
////////USER TOKO (TBL_KASIR)-----------------------------
        $api->post('datakasir', 'UserTokoApiController@datakasir');
        $api->post('tambahkasir', 'UserTokoApiController@tambahkasir');
        $api->post('editkasir', 'UserTokoApiController@editkasir');
        $api->post('hapuskasir', 'UserTokoApiController@hapuskasir');
        $api->post('gantipasskasir', 'UserTokoApiController@gantipasskasir');

////////DATA PEKERJA (TBL_KASIR)-----------------------------
        $api->post('datapekerja', 'UserTokoApiController@dataPekerja');
        $api->post('tambahpekerja', 'UserTokoApiController@tambahpekerja');

////////KATEGORI (TBL_JENIS)-----------------------------
        $api->post('kategori/datakategori', 'KategoriApiController@datakategori');
        $api->post('kategori/tambah_kategori', 'KategoriApiController@tambah_kategori');
        $api->post('kategori/edit_kategori', 'KategoriApiController@edit_kategori');
        $api->post('kategori/hapus_kategori', 'KategoriApiController@hapus_kategori');

////////KATEGORI BEBAN (tbl_kategoribeban)-----------------------------
        $api->post('kategori/datakategoribeban', 'KategoriApiController@datakategoribeban');
        $api->post('kategori/tambah_kategori_beban', 'KategoriApiController@tambah_kategori_beban');
        $api->post('kategori/edit_kategori_beban', 'KategoriApiController@edit_kategori_beban');
        $api->post('kategori/hapus_kategori_beban', 'KategoriApiController@hapus_kategori_beban');

////////DISKON
        $api->post('diskon/data_diskon', 'DiskonApiController@data_diskon');
        $api->post('diskon/tambah_diskon', 'DiskonApiController@tambah_diskon');
        $api->post('diskon/edit_diskon', 'DiskonApiController@edit_diskon');
        $api->post('diskon/hapus_diskon', 'DiskonApiController@hapus_diskon');

////////BEBAN (tbl_beban)-----------------------------
        $api->post('beban/databeban', 'BebanApiController@databeban');
        $api->post('beban/databeban_hariini', 'BebanApiController@databeban_hariini');
        $api->post('beban/tambah_beban', 'BebanApiController@tambah_beban');
        $api->post('beban/edit_beban', 'BebanApiController@edit_beban');
        $api->post('beban/hapus_beban', 'BebanApiController@hapus_beban');

////////PRODUK (tbl_barang)-----------------------------
        $api->post('produk/data_produk_all', 'ProdukApiController@data_produk_all');
        $api->post('produk/data_produk_bykategori', 'ProdukApiController@data_produk_bykategori');
        $api->post('produk/data_produk_detail', 'ProdukApiController@data_produk_detail');
        $api->post('produk/tambah_produk', 'ProdukApiController@tambah_produk');
        $api->post('produk/edit_produk', 'ProdukApiController@edit_produk');
        $api->post('produk/hapus_produk', 'ProdukApiController@hapus_produk');

////////ITEM PAKET
        $api->post('produk/data_produk_brgjasa', 'ItemPaketApiController@data_produk_brgjasa');
        $api->post('produk/data_itempaket', 'ItemPaketApiController@data_itempaket');
        //////BAGI HASIL BY PAKET
        $api->post('produk/tambah_itempaket_bypaket', 'ItemPaketApiController@tambah_itempaket_bypaket');
        $api->post('produk/edit_itempaket_bypaket', 'ItemPaketApiController@edit_itempaket_bypaket');
        $api->post('produk/hapus_itempaket_bypaket', 'ItemPaketApiController@hapus_itempaket_bypaket');
        //////BAGI HASIL BY ITEM
        $api->post('produk/tambah_itempaket_byitem', 'ItemPaketApiController@tambah_itempaket_byitem');
        $api->post('produk/edit_itempaket_byitem', 'ItemPaketApiController@edit_itempaket_byitem');

////////STOK -----------------------------
        $api->post('produk/tambah_stok_produk', 'StokApiController@tambah_stok_produk');
        $api->post('produk/getmodal_produk', 'StokApiController@getmodal_produk');
        $api->post('produk/edit_modal_produk', 'StokApiController@edit_modal_produk');
        $api->post('produk/penyesuaian_stok_produk', 'StokApiController@penyesuaian_stok_produk');

////////BAGI HASIL
        $api->post('produk/data_bagihasil_produk', 'BagiHasilApiController@data_bagihasil_produk');
        $api->post('produk/tambah_bagihasil_produk', 'BagiHasilApiController@tambah_bagihasil_produk');
        $api->post('produk/edit_bagihasil_produk', 'BagiHasilApiController@edit_bagihasil_produk');
        $api->post('produk/hapus_bagihasil_produk', 'BagiHasilApiController@hapus_bagihasil_produk');

////////KERANJANG
        $api->post('penjualan/data_keranjang', 'KeranjangApiController@data_keranjang');
        $api->post('penjualan/tambah_keranjang', 'KeranjangApiController@tambah_keranjang');
        $api->post('penjualan/hapus_item_keranjang', 'KeranjangApiController@hapus_item_keranjang');


////////PENJUALAN
        $api->post('penjualan/pembayaran', 'PenjualanApiController@pembayaran');
        $api->post('penjualan/riwayat_penjualan', 'PenjualanApiController@riwayat_penjualan');
        $api->post('penjualan/penjualan_detail', 'PenjualanApiController@penjualan_detail');
        $api->post('penjualan/reversal_data', 'PenjualanApiController@reversal_data');
        $api->post('penjualan/reversal_aksi', 'PenjualanApiController@reversal_aksi');

////////ANTRIAN
        $api->post('antrian_pending', 'AntrianApiController@antrian_pending');
        $api->post('antrian', 'AntrianApiController@antrian');
        $api->post('antrian_all', 'AntrianApiController@antrian_all');
        $api->post('antrian/detail', 'AntrianApiController@antrianDetail');
        $api->post('antrian/update', 'AntrianApiController@antrianUpdate');


////////LAPORAN
        $api->post('list_laporan', 'LaporanApiController@list_laporan');
        $api->post('/laporan/umum', 'LaporanApiController@LaporanUmum');
        $api->post('/laporan/penjualan', 'LaporanApiController@LaporanPenjualan');
        $api->post('/laporan/beban', 'LaporanApiController@LaporanBeban');
        $api->post('/laporan/reversal', 'LaporanApiController@LaporanReversal');
        $api->post('/laporan/stock', 'LaporanApiController@LaporanStock');

    }); 
}); 
