@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Pengaturan</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <!-- left menu section -->
                            <div class="col-md-3 mb-2 mb-md-0 pills-stacked">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                                            <i class="bx bx-cog"></i>
                                            <span>Umum</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                                            <i class="bx bx-lock"></i>
                                            <span>Ubah Password</span>
                                        </a>
                                    </li>
                                
                                </ul>
                            </div>
                            <!-- right content section -->
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                                <form class="validate-form" id="input-title" method="POST" 
                                                action="{{ url('/admin-panel/user/editmyprofile',encrypt($data->form->id)) }}" enctype="multipart/form-data" style="display: contents;">
                                                    <div class="media">
                                                        {{ csrf_field() }}
                                                        <a href="javascript: void(0);">
                                                            @if($data->form->image != "")
                                                                <img src="{{ asset($data->form->image)}}" class="rounded mr-75" id="image-img-tag" alt="profile image" height="64" width="64">
                                                            @else
                                                                <img src="{{ asset('placeholder.png')}}" class="rounded mr-75" id="image-img-tag" alt="profile image" height="64" width="64">
                                                            @endif
                                                        </a>
                                                        <div class="media-body mt-25">
                                                            <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                                                <label for="image-img" class="btn btn-sm btn-light-primary ml-50 mb-50 mb-sm-0">
                                                                    <span>Ganti Foto Profil</span>
                                                                    <input type="file" name="image" id="image-img" accept="image/*" hidden>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Username</label>
                                                                    <input type="text" class="form-control" placeholder="Username" value="{{ $data->form->username }}" name="username" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Nama</label>
                                                                    <input type="text" class="form-control" placeholder="Nama" value="{{ $data->form->name }}" name="nama" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>E-mail</label>
                                                                    <input type="email" class="form-control" placeholder="Email" value="{{ $data->form->email }}" name="email">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                            <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                                changes</button>
                                                            <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade " id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false"> 
                                                <form class="validate-form" id="input-password" method="POST" 
                                                action="{{ url('/admin-panel/user/changemypassword',encrypt($data->form->id)) }}" enctype="multipart/form-data" style="display: contents;">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Password Lama</label>
                                                                    <input type="password" class="form-control" placeholder="Password Lama" id="password_lama" name="password_lama">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Password Baru</label>
                                                                    <input type="password" class="form-control" placeholder="Password Baru" id="password" name="password">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Konfirmasi Password Baru</label>
                                                                    <input type="password" class="form-control" data-validation-match-match="password" placeholder="Password Baru" id="password_confirm" name="password_confirm">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                            <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                                changes</button>
                                                            <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


@endsection
@section('js-scripts')
<!-- BEGIN: Vendor JS-->
<script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>

<script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
<script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

<script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('admins/noty/noty.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
<!-- END: Theme JS-->


@endsection
@section('js-action')
<script type="text/javascript">
    $(function() {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 2000000) {
                    new Noty({
                        text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>'
                        , layout: 'topRight'
                        , type: 'error'
                    }).setTimeout(4000).show();
                    input.value = "";
                } else {
                    reader.onload = function(e) {
                        $('#image-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $("#image-img").change(function() {
            readURL(this);
        });


        function resetForm() {

            $('#idrole').val('');
            $('#roleadd').val('');
            $('#modal-backdrop-disable-role').modal('hide');
            $('#progressrole').hide();
            $('#input-role').removeClass('is-valid');
        }

        var table;

        $(document).on("mouseenter", ".popover-hover", function() {
            $(this).tooltip('show');
        }).on("mouseleave", ".popover-hover", function() {
            $(this).tooltip('hide');
        });

        $('#input-title').validate({
            rules: {
                nama: {
                    required: true
                }
                , username: {
                    required: true
                }
                , email: {
                    required: true
                }
                , password: {
                    required:true,
                    minlength:5
                }
                ,password_confirm : {
                    required: true,
                    minlength : 5,
                    equalTo : '[name="password"]'
                }
                ,password_lama:{
                    required: true
                }
            , }
            , errorPlacement: function(label, element) {
                if (element.is("select")) {
                    label.insertAfter(element.next());
                } else if (element.is("input:radio")) {
                    label.insertAfter($('#status_div'));
                } else if (element.is("input#picture_val")) {
                    label.insertAfter($('.file-input'));
                } else {
                    label.insertAfter(element)
                }

            },

        });

        $('#input-password').validate({
            rules: {
                password: {
                    required:true,
                    minlength:5
                }
                ,password_confirm : {
                    required: true,
                    minlength : 5,
                    equalTo : '[name="password"]'
                }
                ,password_lama:{
                    required: true
                }
            , }
            , errorPlacement: function(label, element) {
                if (element.is("select")) {
                    label.insertAfter(element.next());
                } else if (element.is("input:radio")) {
                    label.insertAfter($('#status_div'));
                } else if (element.is("input#picture_val")) {
                    label.insertAfter($('.file-input'));
                } else {
                    label.insertAfter(element)
                }

            },

        });

        $(document).on("click", ".view_photo", function() {
            var id = $(this).attr('a');

            $.get('{{ url("/admin-panel/view_image/user")}}' + '/' + id, function(data) {

                $('#image-show').attr('src', '{{asset("/")}}' + data.data.image);


                $('#modal-backdrop-disable-editakses').modal('show');

                $("#progress2").hide();
            })
        });

        $(document).on("click", "#addRole", function() {
            $('#modal-backdrop-disable-role').modal('show');
        });

        $('#input-role').validate({
            rules: {
                roleadd: {
                    required: true
                }
            }
            , errorPlacement: function(label, element) {
                if (element.is("select")) {
                    label.insertAfter(element.next());
                } else if (element.is("input:radio")) {
                    label.insertAfter($('#status_div'));
                } else if (element.is("input#picture_val")) {
                    label.insertAfter($('.file-input'));
                } else {
                    label.insertAfter(element)
                }

            }
            , submitHandler: function(form) {
                var formData = new FormData(form);
                $('#progress').width('0%');
                $('#progress').show();

                $.ajax({
                    type: "POST"
                    , url: "{{ url('admin-panel/manajemen/tambahroleglobal') }}"
                    , data: formData
                    , processData: false
                    , contentType: false
                    , xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete + '%');
                            }
                        }, false);

                        return xhr;
                    }
                    , error: function(e) {
                        new Noty({
                            type: 'error'
                            , text: 'Response error ' + e
                            , timeout: 4000
                        , }).show();
                    }
                    , success: function(result) {
                        if (result['status'] == 'success') {
                            new Noty({
                                type: 'success'
                                , text: result['msg']
                                , timeout: 4000
                                , layout: 'topRight',

                            }).show();

                            resetForm();
                            getRole();
                        } else if (result['status'] == 'fail') {
                            new Noty({
                                type: 'error'
                                , text: result['msg']
                                , timeout: 4000
                                , layout: 'topRight',

                            }).show();

                            $("#progress").hide();
                        }
                    }
                });
                return false;
            }

        });


    });

</script>
@endsection