@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Pengaturan</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    @if($data->halaman == 'input')
                    <form id="input-title" method="POST" action="{{ URL::Route('Tambahuser') }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Tambah {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                                <a href="#">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button
                                                            type="button" class="btn btn-icon btn-light-danger"><i
                                                                class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="username">Username<small
                                                                style="color: red;">*</small></label>
                                                        <input type="text" id="username" name="username" class="form-control"
                                                            placeholder="Username" autocomplete="off">

                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama">Nama Lengkap<small
                                                                style="color: red;">*</small></label>
                                                        <input type="text" id="nama" name="nama" class="form-control">
                                                        <!-- <select class="select2 form-control" id="nama" name="nama" style="width: 100%;">
                                                              <option ></option>
                                                        </select> -->
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email<small
                                                                style="color: red;">*</small></label>
                                                        <input type="email" id="email" name="email" class="form-control">

                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select class="select2 form-control" id="status" name="status"
                                                            style="width: 100%;">
                                                            <option></option>
                                                            <option value="0"
                                                                {{ old('status') == 0 ? 'selected' : '' }}>Nonaktif</option>
                                                            <option value="1"
                                                                {{ old('status') == 1 ? 'selected' : '' }} selected="">Aktif
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="role">Hak Akses <small
                                                                style="color: red;">*</small></label>
                                                        <div class="input-group">
                                                            <select class="select2 form-control" id="role" name="role"
                                                                style="width: 100%;">
                                                                <option></option>
                                                                @if($data->select->role)
                                                                @foreach($data->select->role as $role)
                                                                <option value="{{ $role->id }}"
                                                                    {{ old('role') == $role->id ? 'selected' : '' }}>
                                                                    {{ $role->role }}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="kode">Password<small
                                                                style="color: red;">*</small></label>
                                                        <input type="password" id="password" name="password" class="form-control"
                                                            placeholder="Password" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="kode">Konfirmasi Password<small
                                                                style="color: red;">*</small></label>
                                                        <input type="password" id="password_confirm" name="password_confirm" class="form-control"
                                                            placeholder="Konfirmasi Password" autocomplete="off">
                                                    </div>
                                                </div>
                                                {{--<div class="col-lg-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="ppn">user Aktif ?</label>
                                                    <fieldset>
                                                        <div class="checkbox checkbox-success checkbox-icon">
                                                            <input type="checkbox" id="checkboxIcon3">
                                                            <label for="checkboxIcon3"><i class="bx bx-x"></i>Success</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                </div>--}}

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </form>
                    @elseif($data->halaman == 'update')
                    <form id="input-title" method="POST"
                        action="{{ url('/admin-panel/manajemen/edituseraksi',encrypt($data->form->id)) }}"
                        enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                                <a href="#">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button
                                                            type="button" class="btn btn-icon btn-light-danger"><i
                                                                class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">

                                        <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="username">Username<small
                                                                style="color: red;">*</small></label>
                                                        <input type="text" id="username" name="username" class="form-control"
                                                            placeholder="Username" autocomplete="off"
                                                            value="{{ $data->form->username }}">

                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama">Nama Lengkap<small
                                                                style="color: red;">*</small></label>
                                                        <input type="text" id="nama" name="nama" class="form-control"
                                                            placeholder="Nama Lengkap" autocomplete="off"
                                                            value="{{ $data->form->name }}">
                                                        {{-- <select class="select2 form-control" id="nama" name="nama" style="width: 100%;">
                                                              <option value="{{ $data->form->name }}">{{ $data->form->name }}</option>
                                                        </select> --}}

                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email<small
                                                                style="color: red;">*</small></label>
                                                        <input type="email" id="email" name="email" class="form-control"
                                                            placeholder="Alamat Email" value="{{ $data->form->email }}">

                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select class="select2 form-control" id="status" name="status"
                                                            style="width: 100%;">
                                                            <option></option>
                                                            <option value="0"
                                                                {{ $data->form->is_active == 0 ? 'selected' : '' }}>
                                                                Nonaktif</option>
                                                            <option value="1"
                                                                {{ $data->form->is_active == 1 ? 'selected' : '' }}>Aktif
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="role">Hak Akses <small
                                                                style="color: red;">*</small></label>
                                                        <div class="input-group">
                                                            <select class="select2 form-control" id="role" name="role"
                                                                style="width: 100%;">
                                                                <option></option>
                                                                @if($data->select->role)
                                                                @foreach($data->select->role as $role)
                                                                <option value="{{ $role->id }}"
                                                                    {{ $data->form->role_id == $role->id ? 'selected' : '' }}>
                                                                    {{ $role->role }}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        @if($data->form->image != "")
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture"
                                            src="{{ asset($data->form->image)}}">
                                        @else
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture"
                                            src="{{ asset('placeholder.png')}}">
                                        @endif

                                        <div class="form-body">
                                            <br>
                                            <fieldset class="form-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="image"
                                                        id="image-img" accept="image/*" />
                                                    <label class="custom-file-label" for="image">Pilih foto</label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @elseif($data->halaman == 'password')
                    <form id="input-title" method="POST"
                        action="{{ url('/admin-panel/manajemen/editpasswordaksi', encrypt($data->form->id)) }}"
                        enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                                <a href="#">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button
                                                            type="button" class="btn btn-icon btn-light-danger"><i
                                                                class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">

                                        <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-md-12">
                                                    <div class="col-lg-4 col-md-4 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="kode">Password Baru<small
                                                                    style="color: red;">*</small></label>
                                                            <input type="password" id="password" name="password" class="form-control"
                                                                placeholder="Password Baru" autocomplete="off">
    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-lg-4 col-md-4 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="kode">Konfirmasi Password<small
                                                                    style="color: red;">*</small></label>
                                                            <input type="password" id="password_confirm" name="password_confirm" class="form-control"
                                                                placeholder="Konfirmasi Password" autocomplete="off">
    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/manajement/user') }}"><button type="button"
                                                            class="btn btn-icon btn-light-danger mr-1">
                                                            <i class="bx bx-left-arrow-circle"></i> BATAL
                                                        </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                        <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu"
                                                        class="progress-bar progress-bar-success progress-bar-striped active"
                                                        role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                                        aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @elseif($data->halaman == 'data')
                    <div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Data {{ $data->title_sub }} </h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                            <a href="#">
                                                <a href="{{ url('/admin-panel/manajemen/tambahuserview') }}"><button type="button"
                                                        class="btn btn-icon btn-outline-primary"><i
                                                            class="bx bxs-add-to-queue"></i> Tambah
                                                        {{ $data->title }}</button></a>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                            <tr>
                                                <th></th>
                                                <th align="center">NO</th>
                                                <th align="left">NAMA</th>
                                                <th align="left">USERNAME</th>
                                                <th align="left">EMAIL</th>
                                                <th align="center">STATUS</th>

                                            </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap;text-align: center;"></tbody>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150"
                        tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-info modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-dark white">
                                    <span class="modal-title" id="myModalLabel150">Foto User</span>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img id="image-show" class="img-fluid file-input" id="profile-picture"
                                        src="{{ asset('placeholder.png')}}">
                                </div>


                            </div>
                        </div>
                    </div>
                    @elseif($data->halaman == 'detail')
                    <div class="col-lg-9 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">Detail {{ $data->title }} - {{ $data->form->name }} </h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                            <a href="#">
                                                <a href="{{ url('/admin-panel/manajement/user') }}"><button type="button"
                                                        class="btn btn-icon btn-light-danger"><i
                                                            class="bx bx-left-arrow-circle"></i> Kembali</button></a>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs mb-2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-tab"
                                            data-toggle="tab" href="#account" aria-controls="account" role="tab"
                                            aria-selected="true">
                                            <i class="bx bx-info-circle mr-25" data-toggle="tooltip"
                                                data-placement="bottom" data-title="Detail Data User"></i><span
                                                class="d-none d-sm-block">Detail User</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fade show" id="account" aria-labelledby="account-tab"
                                        role="tabpanel">
                                        <!-- users edit media object start 
                                        <div class="media mb-2">
                                            <a class="mr-2" href="javascript:void(0);">
                                                <img src="../../../app-assets/images/portrait/small/avatar-s-26.jpg" alt="users avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Avatar</h4>
                                                <div class="col-12 px-0 d-flex">
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary mr-25">Change</a>
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-light-secondary">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                        users edit media object ends -->
                                        <!-- users edit account form start -->
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                                                <table class="table mb-0 table-hover table-striped">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th colspan="2" style="text-align: center;">Data User</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table mb-0 table-hover table-striped">

                                                        <tbody>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Nama</td>
                                                                <td style="padding: 1.15rem 1rem;">{{$data->form->name}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Username</td>
                                                                <td style="padding: 1.15rem 1rem;">
                                                                    {{$data->form->username}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Email
                                                                </td>
                                                                <td style="padding: 1.15rem 1rem;">
                                                                    {{$data->form->email}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table mb-0 table-hover table-striped">

                                                        <tbody>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Hak Akses
                                                                </td>
                                                                <td style="padding: 1.15rem 1rem;">
                                                                    {{$data->select->role->role}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Status</td>
                                                                <td>
                                                                    <?php
                                                                        if($data->form->is_active == 0){
                                                                            $sst  = '<div class="badge badge-pill badge-glow badge-danger">Nonaktif</div>';
                                                                        }elseif($data->form->is_active == 1){
                                                                            $sst  = '<div class="badge badge-pill badge-glow badge-success">Aktif</div>';
                                                                        }

                                                                        echo $sst;
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 1.15rem 1rem;">Dibuat</td>
                                                                <td style="padding: 1.15rem 1rem;">
                                                                    @if($data->form->created_at == "")
                                                                
                                                                    @else
                                                                        {{date("d-m-Y / H:i", strtotime($data->form->created_at))}}
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                <a href="{{ url('/admin-panel/manajement/user') }}"> <button type="button"
                                                        class="btn btn-icon btn-light-danger"><i
                                                            class="bx bx-left-arrow-circle"></i> Kemali</button></a>
                                            </div>
                                        </div>

                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card border-secondary ">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">Foto {{ $data->title }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                    @if($data->form->image != "")
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture"
                                        src="{{ asset($data->form->image)}}">
                                    @else
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture"
                                        src="{{ asset('placeholder.png')}}">
                                    @endif
                                    {{--<div class="form-body">
                                    <br>
                                      <fieldset class="form-group">
                                          <div class="custom-file">
                                              <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                              <label class="custom-file-label" for="image">Pilih foto</label>
                                          </div>
                                      </fieldset>
                                  </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </section>
        </div>
    </div>
</div>



@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
<script type="text/javascript">
    $(function() {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 2000000) {
                    new Noty({
                        text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>'
                        , layout: 'topRight'
                        , type: 'error'
                    }).setTimeout(4000).show();
                    input.value = "";
                } else {
                    reader.onload = function(e) {
                        $('#image-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $("#image-img").change(function() {
            readURL(this);
        });

         


        function resetForm() {

            $('#idrole').val('');
            $('#roleadd').val('');
            $('#modal-backdrop-disable-role').modal('hide');
            $('#progressrole').hide();
            $('#input-role').removeClass('is-valid');
        }

        var table;

        $(document).on("mouseenter", ".popover-hover", function() {
            $(this).tooltip('show');
        }).on("mouseleave", ".popover-hover", function() {
            $(this).tooltip('hide');
        });

        $('#role').select2({
            placeholder: "Pilih Role"
            , allowClear: true
            , "language": {
                "noResults": function() {
                    return "Tidak ada data ditemukan";
                }
                , errorLoading: function() {
                    return 'Hasilnya tidak dapat dimuat';
                }
                , inputTooShort: function(args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            }
        , });

        function getrole() {
            $("#role").select2({
                placeholder: "Pilih Role"
                , allowClear: true
                , delay: 250
                , "language": {
                    "noResults": function() {
                        return "Tidak ada data ditemukan";
                    }
                }
                , ajax: {
                    url: "{{ url('admin-panel/manajemen/getroleglobal') }}"
                    , dataType: 'json'
                    , type: "GET",

                    data: function(params) {
                        return {
                            q: params.term, // search term
                        };
                    }
                    , processResults: function(data) {
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    text: item.nama
                                    , id: item.id
                                }
                            })
                        };
                    }
                }
            });
        }

        $('#input-title').validate({
            rules: {
                nama: {
                    required: true
                }
                , role: {
                    required: true
                }
                , username: {
                    required: true
                }
                , email: {
                    required: true
                }
                , password: {
                    required:true,
                    minlength:5
                }
                ,password_confirm : {
                    minlength : 5,
                    equalTo : '[name="password"]'
                }
            , }
            , errorPlacement: function(label, element) {
                if (element.is("select")) {
                    label.insertAfter(element.next());
                } else if (element.is("input:radio")) {
                    label.insertAfter($('#status_div'));
                } else if (element.is("input#picture_val")) {
                    label.insertAfter($('.file-input'));
                } else {
                    label.insertAfter(element)
                }

            },

        });


        // begin first table
        table = $('#sponsor').DataTable({
            "bProcessing": true
            , "bServerSide": true
            , "autoWidth": false
            , "paginationType": "full_numbers"
            , "aLengthMenu": [
                [50, 100, 200]
                , [50, 100, 200]
            ]
            , "iDisplayLength": 50
            , "responsive": true
            , "ajax": {
                "url": "{{url('admin-panel/manajemen/data_user')}}"
                , "dataType": "json"
                , "type": "POST"
                , "data": {
                    _token: "{{csrf_token()}}"
                }
            },

            "language": {
                "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
            }
            , order: [
                [2, "asc"]
            ],

            columnDefs: [{
                    targets: 0
                    , width: '5%'
                    , orderable: false
                }
                , {
                    targets: 1
                    , width: '5%'
                    , orderable: false
                }
                , {
                    className: "text-left"
                    , "targets": [2]
                }
                , {
                    className: "text-left"
                    , "targets": [3]
                }
                , {
                    className: "text-left"
                    , "targets": [4]
                }
                , {
                    className: "text-center"
                    , "targets": [5]
                }
            ]
            , "columns": [{
                    data: null
                    , render: function(data, type, row) {

                        var editUrl = "{{ url('/admin-panel/manajemen/edituserview', 'url') }}";
                        editUrl = editUrl.replace('url', data['url']);

                        var detailUrl = "{{ url('/admin-panel/manajemen/view/user', 'url') }}";
                        detailUrl = detailUrl.replace('url', data['url']);

                        var editPasswordUrl = "{{ url('/admin-panel/manajemen/editpasswordview', 'url') }}";
                        editPasswordUrl = editPasswordUrl.replace('url', data['url']);

                        return '<div class="text">\n\
                          <a href="' + detailUrl + '"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                          <a href="' + editUrl + '"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit Data"><i class="bx bx-pencil"></i> </button></a>\n\
                          <a href="' + editPasswordUrl + '"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Ubah Password"><i class="bx bx-lock"></i> </button></a>\n\
                          <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="' + data['id'] + '" b="' + data['namabar'] + '" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                  </div>';

                    }
                }
                , {
                    "data": "no"
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                }
                , {
                    data: 'nama'
                }
                , {
                    data: 'username'
                }
                , {
                    data: 'email'
                }
                , {
                    data: 'status'
                },

            ]
        , });

        $(document).on('click', '.delete_btn', function() {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({

                text: 'Apakah user <strong>' + b + '</strong> akan dihapus?'
                , type: 'error'
                , buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function() {
                        n.close();
                        $.ajax({
                            type: 'POST'
                            , url: '{{ url("admin-panel/manajemen/deleteuser") }}'
                            , data: ({
                                id: id
                                , _token: '{{csrf_token()}}'
                            })
                            , success: function(result) {
                                if (result['status'] == 'success') {
                                    new Noty({
                                        type: 'warning'
                                        , layout: 'topRight'
                                        , text: result['msg'],

                                        timeout: 4000
                                    , }).show();

                                    table.draw();

                                } else {
                                    new Noty({
                                        type: 'error'
                                        , layout: 'topRight'
                                        , text: result['msg'],

                                        timeout: 4000
                                    , }).show();
                                }
                            }
                        });

                    }, {
                        id: 'button1'
                        , 'data-status': 'ok'
                    }),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function() {
                        n.close();
                    })
                ]
            }).show();

        });

        $(document).on("click", ".view_photo", function() {
            var id = $(this).attr('a');

            $.get('{{ url("/admin-panel/manajemen/view_image/user")}}' + '/' + id, function(data) {

                $('#image-show').attr('src', '{{asset("/")}}' + data.data.image);


                $('#modal-backdrop-disable-editakses').modal('show');

                $("#progress2").hide();
            })
        });

        $(document).on("click", "#addRole", function() {
            $('#modal-backdrop-disable-role').modal('show');
        });

        $('#input-role').validate({
            rules: {
                roleadd: {
                    required: true
                }
            }
            , errorPlacement: function(label, element) {
                if (element.is("select")) {
                    label.insertAfter(element.next());
                } else if (element.is("input:radio")) {
                    label.insertAfter($('#status_div'));
                } else if (element.is("input#picture_val")) {
                    label.insertAfter($('.file-input'));
                } else {
                    label.insertAfter(element)
                }

            }
            , submitHandler: function(form) {
                var formData = new FormData(form);
                $('#progress').width('0%');
                $('#progress').show();

                $.ajax({
                    type: "POST"
                    , url: "{{ url('admin-panel/manajemen/tambahroleglobal') }}"
                    , data: formData
                    , processData: false
                    , contentType: false
                    , xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete + '%');
                            }
                        }, false);

                        return xhr;
                    }
                    , error: function(e) {
                        new Noty({
                            type: 'error'
                            , text: 'Response error ' + e
                            , timeout: 4000
                        , }).show();
                    }
                    , success: function(result) {
                        if (result['status'] == 'success') {
                            new Noty({
                                type: 'success'
                                , text: result['msg']
                                , timeout: 4000
                                , layout: 'topRight',

                            }).show();

                            resetForm();
                            getRole();
                        } else if (result['status'] == 'fail') {
                            new Noty({
                                type: 'error'
                                , text: result['msg']
                                , timeout: 4000
                                , layout: 'topRight',

                            }).show();

                            $("#progress").hide();
                        }
                    }
                });
                return false;
            }

        });


    });

</script>
@endsection