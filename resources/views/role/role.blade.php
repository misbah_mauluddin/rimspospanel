@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <!-- <h5 class="content-header-title float-left pr-1 mb-0">Fixed Navbar</h5> -->
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> ADMIN</a>
                            </li>
                            <li class="breadcrumb-item active">ROLE AKSES MENU
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">ROLE AKSES MENU </h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                      <li class="ml-2">
                                        <a href="#">
                                          <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#modal-backdrop-disable" data-backdrop="static"><i class="bx bxs-add-to-queue"></i> Tambah</button>
                                        </a>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    
                                </div>
                                <div class="table-responsive">
                                  <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-dark">
					                        <tr>
					                            <th>NO</th>
									            <th>ROLE</th>
									            <th>AKSI</th>
					                            
					                        </tr>
					                    </thead>  
                                        <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">             

        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Tambah Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="input-sport" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                   	<div class="form-group">
                        <label>Role Name</label>
                        
                        <input type="text" id="judul" name="judul" class="form-control"  placeholder="Role Name" >
                        <div class="invalid-feedback">
			                <i class="bx bx-radio-circle"></i>
			              </div>
                  	</div>
                   <br>

                    <div class="progress progress-bar-primary mb-2">
                		<div id="progress" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                	</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable-edit" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">     
        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Edit Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="edit-sponsor" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idbr" id="idbr">
                    <div class="form-group">
                        <label>Role Name</label>
                        
                        <input type="text" id="juduledit" name="juduledit" class="form-control"  placeholder="Role Name" >
                       
                    </div>


                    <div class="progress progress-bar-primary mb-2">
                		<div id="progress2" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                	</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpaneditt">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">     
        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Edit Data Role Akses</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="edit-sponsor" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idbrakses" id="idbrakses">
                <div id="akses"></div>


                    <div class="progress progress-bar-primary mb-2">
                		<div id="progress3" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                	</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>

@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
    <script type="text/javascript">
  
        $(document).on("mouseenter",".popover-hover",function(){             
            $(this).tooltip('show');
        }).on("mouseleave",".popover-hover",function(){
            $(this).tooltip('hide');
        });


      var table;


      function resetForm() {
        $('#id').val('');
        $('#judul').val('');
        $('#modal-backdrop-disable').modal('hide');
        $('#progress').hide();
        $('#input-sport').removeClass( 'is-valid' );

         $('#idbr').val('');
         $('#idbrakses').val('');
        $('#juduleditd').val('');
        $('#menuedit').val('');
        $('#roleedit').val('');
        $('#modal-backdrop-disable-edit').modal('hide');
        $('#progress2').hide();
        $('#progress3').hide();
    }
    

    function loaddata(){
       table = $('#sponsor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "autoWidth": false, 
            "paginationType": "full_numbers",
            "aLengthMenu": [ [5, 20, 50, 100], [5, 20, 50, 100] ],
            "iDisplayLength": 5, 
            "responsive": true,
            "ajax":{
                "url": "{{url('admin-panel/data_role')}}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
           
            "language": {
                "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
            },
            
            columnDefs: [
                { orderable: false, targets: 0 },
            ],
            "columns": [
                { data:  'no'  },
                { data:  'role'  },
                  { data: null, render: function ( data, type, row ) {
				return '<div class="text">\n\
                            <button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-id="'+data['id']+'" value="'+data['id']+'" a="'+data['id']+'" data-toggle="tooltip modal" data-target="#modal-backdrop-disable-edit" data-backdrop="static" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button>\n\
                            <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['role']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                            <button  class="btn btn-icon btn-outline-danger popover-hover akses" data-id="'+data['id']+'" value="'+data['id']+'" a="'+data['id']+'" data-toggle="tooltip modal" data-target="#modal-backdrop-disable-edit" data-backdrop="static" data-placement="left" data-title="Access"><i class="bx bx-lock"></i> </button>\n\
						</div>';
			} }
                 
                 
            ],

            drawCallback : function() {
               processInfo(this.api().page.info());
           }
      
        });
    }


    function processInfo(info) {
        //console.log(info);
        // $("#totaldata").hide().html(info.recordsTotal).fadeIn();
        //do your stuff here
    } 

     $('#input-sport').validate({
            // Rules for form validation
            rules: {
                judul: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                judul: {
                    required: "Role Name Tidak Boleh Kosong"
                }
            },

            // Error Placement
            errorPlacement: function(error, element)
            {
                // error.insertAfter(element.parents("div").find(".invalid-feedback").append(error));
                // error.insertAfter(element);
                error.appendTo('.invalid-feedback');

            },

            //Submit the form
            submitHandler: function (form) {
                var formData = new FormData(form);
                $('#progress').width('0%');
                $('#progress').show();

                $.ajax({
                    type: "POST",
                    url: "{{ url('admin-panel/tambahroleaksi') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            table.draw();
                        }
                        else
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progress").hide();
                        }
                    }
                });
                return false;
            }
        });

    $(document).on("click", ".edit_modal", function (){

          var id = $(this).val();

        $.get('{{ url("/admin-panel/editroleview")}}' + '/' + id, function (data) {
            $('#idbr').val(data.data.id);
            $('#juduledit').val(data.data.role);
            
            $('#modal-backdrop-disable-edit').modal('show');

            $("#progress2").hide();
        })
    });

    $(document).on("click", ".akses", function (){

          var id = $(this).val();

        $.get('{{ url("/admin-panel/editroleakses")}}' + '/' + id, function (data) {
            $('#akses').html(data);
            
            $('#modal-backdrop-disable-editakses').modal('show');

            $("#progress3").hide();
        })
    });

    function redraww(id){
        $.get('{{ url("/admin-panel/editroleakses")}}' + '/' + id, function (data) {
            $('#akses').html(data);
        })
    }

    $(document).on("click", ".check-input", function (e){
            e.preventDefault();
            $('#progress3').width('0%');
            $('#progress3').show();

            const menuId = $(this).data('menu');
            const roleId = $(this).data('role');
            $.ajax({
                url: "{{ url('admin-panel/editroleaksesaksi') }}",
                type: 'GET',
                data: {
                    menuId: menuId,
                    roleId: roleId,
                    _token: "{{csrf_token()}}"
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('#progress3').width(percentComplete+'%');
                        }
                    }, false);

                    return xhr;
                },
                error: function (e) {
                    new Noty({
                        type: 'error',
                        text: 'Response error ' + e,
                        timeout: 4000,
                        layout: 'topRight',
                        
                    }).show();
                },
                success: function(result){
                    if(result['status'] == 'success')
                    {
                        new Noty({
                            type: 'success',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();

                        redraww(roleId);
                    }
                    else
                    {
                        new Noty({
                            type: 'warning',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();
                        redraww(roleId);
                    }
                }
            });

        });

    $(function() {
		 // $("#aa").hide();
		 $("#progress").hide();

		
        loaddata();
        $("#edit-sponsor").on('submit',(function(e) {
            e.preventDefault();
            $('#progress2').width('0%');
            $('#progress2').show();

            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                url: "{{ url('admin-panel/editroleaksi') }}",
                data: formData,
                processData: false,
                contentType: false,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('#progress').width(percentComplete+'%');
                        }
                    }, false);

                    return xhr;
                },
                error: function (e) {
                    new Noty({
                        type: 'error',
                        text: 'Response error ' + e,
                        timeout: 4000,
                        layout: 'topRight',
                        
                    }).show();
                },
                success: function(result){
                    if(result['status'] == 'success')
                    {
                        new Noty({
                            type: 'warning',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();

                        resetForm();
                        table.draw();
                    }
                    else
                    {
                        new Noty({
                            type: 'error',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();

                        $("#progress").hide();
                    }
                }

            });
        }));

        $(document).on('click', '.delete_btn', function () {
            var a = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
            	
                text: 'Apakah Role <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/deleterole') }}',
                            data: ({a:a, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

        });



       
    });
      
</script>
@endsection


