@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <!-- <h5 class="content-header-title float-left pr-1 mb-0">Fixed Navbar</h5> -->
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> ADMIN</a>
                            </li>
                            <li class="breadcrumb-item active">MENU MANAGEMENT
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    <div class="col-md-12 col-lg-5">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Title Menu </h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                      <li class="ml-2">
                                        <a href="#">
                                          <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#modal-backdrop-disable-titlemenuadd" data-backdrop="static"><i class="bx bxs-add-to-queue"></i> Tambah</button>
                                        </a>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                      <table id="titlemenu" class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="2%">NO</th>
                                                    <th width="50%">MENU</th>
                                                    <th width="50%">AKSI</th>
                                                    
                                                </tr>
                                            </thead>   
                                            <tbody style="text-align: center;"></tbody>                                       
                                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-7">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Menu Management </h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                      <li class="ml-2">
                                        <a href="#">
                                          <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#modal-backdrop-disable" data-backdrop="static"><i class="bx bxs-add-to-queue"></i> Tambah</button>
                                        </a>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                      <table id="sponsor" class="table table-bordered table-hover table-sm table-striped ">
                                            <thead >
                                                <tr>
                                                    <th width="2%">NO</th>
                                                    <th>TITLE MENU</th>
                                                    <th>NAMA MENU</th>
                                                    <th>ICON</th>
                                                    <th>AKSI</th>
                                                </tr>
                                            </thead>   
                                            <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- MODAL TITLE MENU -->
<div class="modal fade" id="modal-backdrop-disable-titlemenuadd" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-scrollable modal-lg">                    

        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Tambah Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="input-title" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idtitle" id="idtitle">
                    <div class="form-group">
                        <label>Title Menu Name</label>
                        
                        <input type="text" id="judultitle" name="judultitle" class="form-control"  placeholder="Role Name" >
                       
                    </div>

                    <div class="col-md-12">
                        <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpan">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<!-- MODAL TITLE MENU -->
<div class="modal fade" id="modal-backdrop-disable-titlemenuedit" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">   
        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">EDIT Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="edit-title" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idbrtitle" id="idbrtitle">
                    <div class="form-group">
                        <label>Title Menu Name</label>
                        
                        <input type="text" id="juduledittitle" name="juduledittitle" class="form-control"  placeholder="Role Name" >
                       
                    </div>

                    <div class="col-md-12">
                        <div id="progresstitlemenuedit" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpanedit">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">             

        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Tambah Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="input-sport" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Title Menu</label>
                        <select name="id_titile_menu" id="id_titile_menu" class="select2 form-control" >
                            <option id="hiddent" value="">----SILAHKAN PILIH TITLE MENU----</option>
                            @foreach($titlemenu as $men)
                                <option value="{{$men->id}}">{{$men->menu}}</option>
                            @endforeach
                        </select>
                       
                    </div>
                    <div class="form-group">
                        <label>Menu Name</label>
                        
                        <input type="text" id="judul" name="judul" class="form-control"  placeholder="Menu Name" >
                       
                    </div>

                    <div class="form-group">
                        <label>Icon</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="icon" name="icon" placeholder="Enter icon menu" aria-describedby="button-iconmenu">
                            <div class="input-group-append" id="button-iconmenu">
                                <button class="btn btn-primary pb-0" id="btnsearchliveicon" type="button" onclick="window.open('https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/html/ltr/vertical-menu-template/icons-livicons.html', '_blank')" style="display: block;">
                                    <i class="bx bx-search"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div id="progress" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable-edit" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">     
        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Edit Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="edit-sponsor" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idbr" id="idbr">
                    <div class="form-group">
                        <label>Title Menu</label>
                        <select name="id_titile_menuedit" id="id_titile_menuedit" class="form-control" data-live-search="true" >
                        </select>
                       
                    </div>
                    <div class="form-group">
                        <label>Menu Name</label>
                        
                        <input type="text" id="juduledit" name="juduledit" class="form-control"  placeholder="Role Name" >
                       
                    </div>

                    <div class="form-group">
                        <label>Icon</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="iconedit" name="iconedit" placeholder="Enter icon menu" aria-describedby="button-iconmenu">
                            <div class="input-group-append" id="button-iconmenu">
                                <button class="btn btn-primary pb-0" id="btnsearchliveicon" type="button" onclick="window.open('https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/html/ltr/vertical-menu-template/icons-livicons.html', '_blank')" style="display: block;">
                                    <i class="bx bx-search"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div id="progress2" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpaneditt">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->

    

@endsection
@section('js-action')
    <script type="text/javascript">
  
        $(document).on("mouseenter",".popover-hover",function(){             
            $(this).tooltip('show');
        }).on("mouseleave",".popover-hover",function(){
            $(this).tooltip('hide');
        });


      var table,table2;

      function resetForm() {
            $('#id').val('');
            $('#idtitle').val('');
            $('#judul').val('');
            $('#judultitle').val('');
            $('#id_titile_menu').val('');
            $('#icon').val('');
            $('#modal-backdrop-disable').modal('hide');
            $('#modal-backdrop-disable-titlemenuadd').modal('hide');
            $('#progress').hide();
            $('#progresstitlemenu').hide();

             $('#idbr').val('');
             $('#idbrtitle').val('');
            $('#juduledit').val('');
            $('#juduledittitle').val('');
            $('#id_titile_menuedit').val('');
            $('#iconedit').val('');
            $('#modal-backdrop-disable-edit').modal('hide');
            $('#modal-backdrop-disable-titlemenuedit').modal('hide');
            $('#progress2').hide();
            $('#progresstitlemenuedit').hide();
        }

      ////////////////////TITLE MENU
        function loaddatatitle(){
           table = $('#titlemenu').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "autoWidth": false, 
                "paging":   false,
                "ordering": false,
                "info":     false,
                "bFilter": false,
                "paginationType": "full_numbers",
                "aLengthMenu": [ [5, 20, 50, 100], [5, 20, 50, 100] ],
                "iDisplayLength": 5, 
                "ajax":{
                    "url": "{{url('/admin-panel/data_menutitle')}}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
               
                "language": {
                    "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
                },
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: 0 },
                ],
                "columns": [
                    {
                        "data": "no",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data:  'menu'  },
                      { data: null, render: function ( data, type, row ) {
                    return '<div class="text">\n\
                                <button  class="btn btn-icon btn-outline-primary mr-1 mb-1 popover-hover edit_modaltitle" data-id="'+data['id']+'" value="'+data['id']+'" a="'+data['id']+'" data-toggle="tooltip modal" data-target="#modal-backdrop-disable-edit" data-backdrop="static" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button>\n\
                                <button class="btn btn-icon btn-outline-danger mr-1 mb-1 popover-hover delete_btntitle" a="'+data['id']+'" b="'+data['menu']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                            </div>';
                } }
                     
                     
                ]
          
            });
            table.on( 'order.dt search.dt', function () {
                table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        }

        

        /////////////////////MENU
        function loaddata(){
           table2 = $('#sponsor').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "autoWidth": false, 
                "info":     false,
                "paginationType": "full_numbers",
                "aLengthMenu": [ [5, 20, 50, 100], [5, 20, 50, 100] ],
                "iDisplayLength": 5, 
                "ajax":{
                    "url": "{{url('/admin-panel/data_menu')}}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
               
                "language": {
                    "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
                },
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: 0 },
                ],
                "columns": [
                    {
                        "data": "no",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data:  'titlemenu'  },
                    { data:  'menu'  },
                    { data:  'icon'  },
                      { data: null, render: function ( data, type, row ) {
                    return '<button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-id="'+data['id']+'" value="'+data['id']+'" a="'+data['id']+'" data-toggle="tooltip modal" data-target="#modal-backdrop-disable-edit" data-backdrop="static" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button>\n\
                                <button class="btn btn-icon btn-outline-danger popover-hover delete_btn" a="'+data['id']+'" b="'+data['menu']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                            ';
                } }
                     
                     
                ]
          
            });
               table2.on( 'order.dt search.dt', function () {
                table2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        }

        ///////////////////TITLE MENU
         $('#input-title').validate({
                // Rules for form validation
                rules: {
                    judultitle: {
                        required: true
                    }
                },

                // Messages for form validation
                messages: {
                    judultitle: {
                        required: "Nama Menu Tidak Boleh Kosong"
                    }
                },

                // Error Placement
                errorPlacement: function(error, element)
                {
                    error.insertAfter(element.parent('div').addClass('has-error'));

                },

                //Submit the form
                submitHandler: function (form) {
                    var formData = new FormData(form);
                    $('#progresstitlemenu').width('0%');
                    $('#progresstitlemenu').show();

                    $.ajax({
                        type: "POST",
                        url: "{{ url('/admin-panel/tambahmenutitleaksi') }}",
                        data: formData,
                        processData: false,
                        contentType: false,
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            xhr.upload.addEventListener("progresstitlemenu", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('#progresstitlemenu').width(percentComplete+'%');
                                }
                            }, false);

                            return xhr;
                        },
                        error: function (e) {
                            new Noty({
                                type: 'error',
                                text: 'Response error ' + e,
                                timeout: 4000,
                            }).show();
                        },
                        success: function (result) {
                            if(result['status'] == 'success')
                            {
                                new Noty({
                                    type: 'success',
                                    text: result['msg'],
                                    timeout: 4000,
                                    layout: 'topRight',
                                    
                                }).show();

                                // resetForm();
                                // table.draw();
                                window.location.href = '{{url('/admin-panel/menu')}}';
                            }
                            else
                            {
                                new Noty({
                                    type: 'error',
                                    text: result['msg'],
                                    timeout: 4000,
                                    layout: 'topRight',
                                    
                                }).show();

                                $("#progresstitlemenu").hide();
                            }
                        }
                    });
                    return false;
                }
            });


        ///////////////////MENU
         $('#input-sport').validate({
                // Rules for form validation
                rules: {
                    judul: {
                        required: true
                    },
                    icon: {
                        required: true
                    }
                },

                // Messages for form validation
                messages: {
                    judul: {
                        required: "Nama Menu Tidak Boleh Kosong"
                    }
                },

                // Error Placement
                errorPlacement: function(error, element)
                {
                    error.insertAfter(element.parent('div').addClass('has-error'));

                },

                //Submit the form
                submitHandler: function (form) {
                    var formData = new FormData(form);
                    $('#progress').width('0%');
                    $('#progress').show();

                    $.ajax({
                        type: "POST",
                        url: "{{ url('/admin-panel/tambahmenuaksi') }}",
                        data: formData,
                        processData: false,
                        contentType: false,
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            xhr.upload.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('#progress').width(percentComplete+'%');
                                }
                            }, false);

                            return xhr;
                        },
                        error: function (e) {
                            new Noty({
                                type: 'error',
                                text: 'Response error ' + e,
                                timeout: 4000,
                            }).show();
                        },
                        success: function (result) {
                            if(result['status'] == 'success')
                            {
                                new Noty({
                                    type: 'success',
                                    text: result['msg'],
                                    timeout: 4000,
                                    layout: 'topRight',
                                    
                                }).show();

                                resetForm();
                                table2.draw();
                            }
                            else
                            {
                                new Noty({
                                    type: 'error',
                                    text: result['msg'],
                                    timeout: 4000,
                                    layout: 'topRight',
                                    
                                }).show();

                                $("#progress").hide();
                            }
                        }
                    });
                    return false;
                }
            });

         /////////////////TITLE MENU
         $(document).on("click", ".edit_modaltitle", function (){

              var id = $(this).val();

            $.get('{{ url("/admin-panel/editmenutitleview")}}' + '/' + id, function (data) {
                $('#idbrtitle').val(data.data.id);
                $('#juduledittitle').val(data.data.menu);
                
                $('#modal-backdrop-disable-titlemenuedit').modal('show');

                $("#progresstitlemenuedit").hide();
            })
        });

         /////////////////MENU
        $(document).on("click", ".edit_modal", function (){

              var id = $(this).val();

            $.get('{{ url("/admin-panel/editmenuview")}}' + '/' + id, function (data) {
                $('#idbr').val(data.data.id);
                $('#juduledit').val(data.data.menu);
                $('#iconedit').val(data.data.icon);
                $("#id_titile_menuedit").html('');
                $('#id_titile_menuedit').append(data.sbb);

                
                $('#modal-backdrop-disable-edit').modal('show');

                $("#progress2").hide();
            })
        });

        $(function() {

           
            $("#progress").hide();
            $("#progresstitlemenu").hide();

        
            loaddata();
            loaddatatitle();

           
            ////////////////////TITLE MENU
            $("#edit-title").on('submit',(function(e) {
                e.preventDefault();
                $('#progresstitlemenuedit').width('0%');
                $('#progresstitlemenuedit').show();

                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin-panel/editmenutitleaksi') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progresstitlemenuedit", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progresstitlemenuedit').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();
                    },
                    success: function(result){
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'warning',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            table.draw();
                        }
                        else
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progresstitlemenueditss").hide();
                        }
                    }

                });
            }));

            ////////////////////MENU
            $("#edit-sponsor").on('submit',(function(e) {
                e.preventDefault();
                $('#progress2').width('0%');
                $('#progress2').show();

                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin-panel/editmenuaksi') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();
                    },
                    success: function(result){
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'warning',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            table2.draw();
                        }
                        else
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progress").hide();
                        }
                    }

                });
            }));


            ///////////////////TITLE MENU
            $(document).on('click', '.delete_btntitle', function () {
                var a = $(this).attr('a');
                var b = $(this).attr('b');

                var n = new Noty({
                    
                    text: 'Apakah Title Menu <strong>' + b + '</strong> akan dihapus?',
                    type: 'error',
                    buttons: [
                        Noty.button('YA', 'btn btn-success glow', function () {
                            n.close();
                            $.ajax({
                                type: 'POST',
                                url: '{{ url('/admin-panel/deletemenutitle') }}',
                                data: ({a:a, _token:'{{csrf_token()}}'}),
                                success: function(result){
                                    if(result['status'] == 'success')
                                    {
                                        new Noty({
                                            type: 'warning',
                                            layout: 'topRight',
                                            text: result['msg'],
                                            timeout: 4000,
                                        }).show();

                                        table.draw();

                                    }
                                    else
                                    {
                                        new Noty({
                                            type: 'error',
                                            layout: 'topRight',
                                            text: result['msg'],
                                            
                                            timeout: 4000,
                                        }).show();
                                    }
                                }
                            });

                        }, {id: 'button1', 'data-status': 'ok'}),

                        Noty.button('BATAL', 'btn btn-danger glow', function () {
                            n.close();
                        })
                    ]
                }).show();

            });

            ///////////////////MENU
            $(document).on('click', '.delete_btn', function () {
                var a = $(this).attr('a');
                var b = $(this).attr('b');

                var n = new Noty({
                    
                    text: 'Apakah Menu <strong>' + b + '</strong> akan dihapus?',
                    type: 'error',
                    buttons: [
                        Noty.button('YA', 'btn btn-success glow', function () {
                            n.close();
                            $.ajax({
                                type: 'POST',
                                url: '{{ url('/admin-panel/deletemenu') }}',
                                data: ({a:a, _token:'{{csrf_token()}}'}),
                                success: function(result){
                                    if(result['status'] == 'success')
                                    {
                                        new Noty({
                                            type: 'warning',
                                            layout: 'topRight',
                                            text: result['msg'],
                                            
                                            timeout: 4000,
                                        }).show();

                                        table2.draw();

                                    }
                                    else
                                    {
                                        new Noty({
                                            type: 'error',
                                            layout: 'topRight',
                                            text: result['msg'],
                                            
                                            timeout: 4000,
                                        }).show();
                                    }
                                }
                            });

                        }, {id: 'button1', 'data-status': 'ok'}),

                        Noty.button('BATAL', 'btn btn-danger glow', function () {
                            n.close();
                        })
                    ]
                }).show();

            });

           
        });
$("#hiddent").hide();
</script>
@endsection


