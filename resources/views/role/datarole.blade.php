<table id="sponsor" class="table table-striped table-bordered"  style="width: max-content 100%;" >
    <thead>
        <tr>
            <th>NO</th>
            <th>MENU</th>
            <th>KET</th>
            <th>ACCESS</th>
            
        </tr>
    </thead>  
    <tbody>
    	<?php $no = 1; ?>
        @foreach($menuuu as $m)

        <?php
            $akses = DB::table('user_access_menu')->where('role_id',$role->id)->where('menu_id',$m->id)->first();

            if ($akses) {
                $cekk = "checked='checked'";
            }else{
                $cekk = "";
            }
        ?>
            <tr>
                <th scope="row"><?php echo $no++ ?></th>
                <td><?php echo $m->label;  ?></td>
                <td><?php echo $m->class;  ?></td>
                <td>
                    <div class="form-check">
                    	<input type="checkbox" class="check-input" <?php echo $cekk; ?> data-role="<?php echo $role->id ?>" data-menu="<?php echo $m->id ?>">
                    </div>

                </td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>NO</th>
            <th>MENU</th>
            <th>KET</th>
            <th>ACCESS</th>
        </tr>
    </tfoot>                                    
   
</table>