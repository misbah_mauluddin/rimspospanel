@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <!-- <h5 class="content-header-title float-left pr-1 mb-0">Fixed Navbar</h5> -->
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> ADMIN</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{url('admin-panel/menu')}}">MENU MANAGEMENT</a>
                            </li>
                            <li class="breadcrumb-item active">SUB MENU MANAGEMENT
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Sub Menu Management</h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                      <li class="ml-2">
                                        <a href="#">
                                          <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#modal-backdrop-disable" data-backdrop="static"><i class="bx bxs-add-to-queue"></i> Tambah</button>
                                        </a>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                      <table id="sponsor" class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>TITLE</th>
                                                    <th>MENU</th>
                                                    <th>URL</th>
                                                    <th>ICON</th>
                                                    <th>ACTIVE</th>
                                                    <th>AKSI</th>
                                                    
                                                </tr>
                                            </thead>  
                                            <tfoot>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>TITLE</th>
                                                    <th>MENU</th>
                                                    <th>URL</th>
                                                    <th>ICON</th>
                                                    <th>ACTIVE</th>
                                                    <th>AKSI</th>
                                                </tr>
                                            </tfoot>  
                                            
                                            <tbody style="text-align: center;">
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach($submenu as $sub)
                                                    @php
                                                        if($sub->menu_id != 1){
                                                            $menuuuu = $sub->menu;
                                                            
                                                        }else{
                                                            $menuuuu = 'Single Menu';
                                                        }

                                                        $iconn = '<button type="button" class="btn btn-icon rounded-circle btn-light-primary" style="padding-bottom: 12px;padding-left: 11px;"  data-container="body" data-toggle="tooltip" data-placement="left" data-title="icon : '.$sub->icon.'">
                                                            <div class="fonticon-wrap"><i class="livicon-evo" data-options="name: '.$sub->icon.'.svg; size: 20px; style: solid;"></i>
                                                            </div>
                                                        </button>';

                                                        if($sub->is_active != 1){
                                                            $je = "<button class='btn btn-icon btn-danger glow popover-hover activee btn-sm' a='$sub->id' b='$sub->title' data-container='body' data-toggle='tooltip' data-placement='left' data-title='Active Menu'><i class='bx bxs-x-circle'></i> Tidak Aktif</button>";
                                                            
                                                        }else{
                                                            $je = "<button class='btn btn-icon btn-primary glow popover-hover shutt btn-sm' a='$sub->id' b='$sub->title' data-container='body' data-toggle='tooltip' data-placement='left' data-title='Inactive Menu'><i class='bx bx-check-double'></i> Aktif</button>";
                                                            
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td align="center">{{$no++}}</td>
                                                        <td align="left">{{$sub->title}}</td>
                                                        <td align="left">{{$menuuuu}}</td>
                                                        <td align="left">{{$sub->url}}</td>
                                                        <td align="center"><?php echo $iconn; ?></td>
                                                        <td align="center"><?php echo $je; ?></td>
                                                        <td align="center">
                                                            <div class="text">
                                                                <button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-id="{{$sub->id}}" value="{{$sub->id}}" a="{{$sub->id}}" data-toggle="tooltip modal" data-target="#modal-backdrop-disable-edit" data-backdrop="static" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button>
                                                                <button class="btn btn-icon btn-outline-danger popover-hover  delete_btn" a="{{$sub->id}}" b="{{$sub->title}}" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>                                       
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">             

        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Tambah Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="input-sport" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Menu</label>
                        <select name="menu_id" id="menu_id" class="select2 form-control" >
                            <option id="hiddent" value="">----SILAHKAN PILIH MENU----</option>
                            @foreach($menuuu as $men)
                                <option value="{{$men->id}}">{{$men->menu}}</option>
                            @endforeach
                        </select>
                       
                    </div>
                    <div class="form-group">
                        <label>Title Name</label>
                        
                        <input type="text" id="title" name="title" class="form-control"  placeholder="Title Name" >
                       
                    </div>
                    <div class="form-group">
                        <label>Url Menu</label>
                        
                        <input type="text" id="url" name="url" class="form-control"  placeholder="Url Menu" >
                       
                    </div>

                    <div class="form-group">
                        <label>Icon</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="icon" name="icon" placeholder="Enter icon menu" aria-describedby="button-iconmenu">
                            <div class="input-group-append" id="button-iconmenu">
                                <button class="btn btn-primary pb-0" id="btnsearchliveicon" type="button" onclick="window.open('https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/html/ltr/vertical-menu-template/icons-livicons.html', '_blank')" style="display: block;">
                                    <i class="bx bx-search"></i>
                            </div>
                        </div>
                    </div>
                    <div class="app-checkbox success inline"> 
                        <label><input type="checkbox" name="is_active" value="1" checked="checked"> Active<span></span></label> 
                    </div>

                    <div class="col-md-12">
                        <div id="progress" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

<!-- MODAL MENU -->
<div class="modal fade" id="modal-backdrop-disable-edit" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-info modal-lg" role="document">     
        <div class="modal-content">
            <div class="modal-header bg-dark white">                        
                <span class="modal-title" id="myModalLabel150">Edit Data</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
               <form id="edit-sponsor" method="POST" action="" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="idbr" id="idbr">
                    <div class="form-group">
                        <label>Menu Menu</label>
                        <select name="menu_idedit" id="menu_idedit" class="form-control" data-live-search="true" >
                        </select>
                       
                    </div>
                    <div class="form-group">
                        <label>Title Name</label>
                        
                        <input type="text" id="titleedit" name="titleedit" class="form-control"  placeholder="Title Name" >
                       
                    </div>

                    <div class="form-group">
                        <label>Url Menu</label>
                        
                        <input type="text" id="urledit" name="urledit" class="form-control"  placeholder="Url Menu" >
                       
                    </div>

                    <div class="form-group">
                        <label>Icon</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="iconedit" name="iconedit" placeholder="Enter icon menu" aria-describedby="button-iconmenu">
                            <div class="input-group-append" id="button-iconmenu">
                                <button class="btn btn-primary pb-0" id="btnsearchliveicon" type="button" onclick="window.open('https://www.pixinvent.com/demo/frest-clean-bootstrap-admin-dashboard-template/html/ltr/vertical-menu-template/icons-livicons.html', '_blank')" style="display: block;">
                                    <i class="bx bx-search"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div id="progress2" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                          <i class="bx bx-x d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Batal</span>
                        </button>
                        <button type="submit" class="btn btn-dark ml-1" id="simpaneditt">Simpan</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>            
</div>
<!-- END MODAL BACKDROP DISABLE -->

@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->

    

@endsection
@section('js-action')
    <script type="text/javascript">
     $(document).on("mouseenter",".popover-hover",function(){             
            $(this).tooltip('show');
        }).on("mouseleave",".popover-hover",function(){
            $(this).tooltip('hide');
        });

    var table;
   
    function resetForm() {
        $('#id').val('');
        $('#menu_id').val('');
        $('#title').val('');
        $('#url').val('');
        $('#icon').val('');
        $('#modal-backdrop-disable').modal('hide');
        $('#progress').hide();

         $('#idbr').val('');
        $('#menu_idedit').val('');
        $('#titleedit').val('');
        $('#urledit').val('');
        $('#iconedit').val('');
        $('#modal-backdrop-disable-edit').modal('hide');
        $('#progress2').hide();
    }
    

    


    function processInfo(info) {
        //console.log(info);
        // $("#totaldata").hide().html(info.recordsTotal).fadeIn();
        //do your stuff here
    } 

     $('#input-sport').validate({
            // Rules for form validation
            rules: {
                menu: {
                    required: true
                },
                title: {
                    required: true
                },
                url: {
                    required: true
                },
                icon: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                menu: {
                    required: "Menu Wajib Di Pilih"
                },
                title: {
                    required: "Title Wajib Diisi"
                },
                url: {
                    required: "Url Wajib Diisi"
                },
                icon: {
                    required: "Icons Wajib Di Pilih"
                },
            },

            // Error Placement
            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent('div').addClass('has-error'));

            },

            //Submit the form
            submitHandler: function (form) {
                var formData = new FormData(form);
                $('#progress').width('0%');
                $('#progress').show();

                $.ajax({
                    type: "POST",
                    url: "{{ url('admin-panel/menu/tambahsubmenuaksi') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            // resetForm();
                            // table.draw();

                            window.location.href = '{{url('admin-panel/menu/submenu')}}';
                        }
                        else
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progress").hide();
                        }
                    }
                });
                return false;
            }
        });

    $(document).on("click", ".edit_modal", function (){

          var id = $(this).val();

        $.get('{{ url("admin-panel/menu/editsubmenuview")}}' + '/' + id, function (data) {
            $('#idbr').val(data.data.id);
            $("#menu_idedit").empty();
            $('#menu_idedit').append(data.sbb);

            $('#titleedit').val(data.data.title);
            $('#urledit').val(data.data.url);

            $("#iconedit").val(data.data.icon);
            
            $('#modal-backdrop-disable-edit').modal('show');

            $("#progress2").hide();
        })
    });


    $(function() {
         // $("#aa").hide();
         $("#progress").hide();

        
        // loaddata();
        $("#edit-sponsor").on('submit',(function(e) {
            e.preventDefault();
            $('#progress2').width('0%');
            $('#progress2').show();

            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                url: "{{ url('admin-panel/menu/editsubmenuaksi') }}",
                data: formData,
                processData: false,
                contentType: false,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('#progress').width(percentComplete+'%');
                        }
                    }, false);

                    return xhr;
                },
                error: function (e) {
                    new Noty({
                        type: 'error',
                        text: 'Response error ' + e,
                        timeout: 4000,
                        layout: 'topRight',
                        
                    }).show();
                },
                success: function(result){
                    if(result['status'] == 'success')
                    {
                        new Noty({
                            type: 'warning',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();

                        // resetForm();
                        // table.draw();
                        window.location.href = '{{url('admin-panel/menu/submenu')}}';
                    }
                    else
                    {
                        new Noty({
                            type: 'error',
                            text: result['msg'],
                            timeout: 4000,
                            layout: 'topRight',
                            
                        }).show();

                        $("#progress").hide();
                    }
                }

            });
        }));

        $(document).on('click', '.delete_btn', function () {
            var a = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
                
                text: 'Apakah Sub Menu <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/menu/deletesubmenu') }}',
                            data: ({a:a, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    // table.draw();
                                    window.location.href = '{{url('admin-panel/menu/submenu')}}';

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

        });


        $(document).on('click', '.activee', function () {
            var a = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
                
                text: 'Apakah Sub Menu <strong>' + b + '</strong> akan Di <strong>Aktifkan</strong> ?',
                type: 'info',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/menu/aktifsubmenu') }}',
                            data: ({a:a, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'success',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    // table.draw();
                                    window.location.href = '{{url('admin-panel/menu/submenu')}}';

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

        });

        $(document).on('click', '.shutt', function () {
            var a = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
                
                text: 'Apakah Sub Menu <strong>' + b + '</strong> akan Di <strong>Non Aktifkan</strong> ?',
                type: 'warning',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/menu/nonaktifsubmenu') }}',
                            data: ({a:a, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    // table.draw();
                                    window.location.href = '{{url('admin-panel/menu/submenu')}}';

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

        });

       
    });
    

    $('#hidden').hide();
</script>
@endsection


