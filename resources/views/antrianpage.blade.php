<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="RIMS CORP">
    <meta name="keywords" content="RIMS CORP">
    <meta name="author" content="RIMS CORP">
    <title>RIMSPOS | {{$data->title}}</title>
    <link rel="apple-touch-icon" href="{{ asset('admins/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('icorims.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/ui/prism.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/components.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/themes/semi-dark-layout.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/noty.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/themes/bootstrap-v3.css') }}">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/datetimepicker/datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/font-awesome/css/font-awesome.css') }}">
    <!-- END: Custom CSS-->
    <script type="text/javascript">
        ////for liveicon
        var base_path = "{{url('/admins')}}";
    </script>

    <style type="text/css">
        label.is-invalid, label.is-valid {
            color: #FF5B5C !important;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- login page start -->
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-6 col-11">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- left section-login -->
                                <div class="col-md-12 col-12 px-0">
                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1" style="text-align: center;display: contents;">
                                            <div class="card-title">
                                                <img src="{{ asset('LOGO.png') }}" width="100px" alt="" style="margin-bottom: 7px;">
                                                {{-- <h4 class="text-center mb-2"> SELAMAT DATANG <br> <small></small></h4> --}}
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            
                                            <div class="divider">
                                                <div class="divider-text text-uppercase text-muted"><small>Silahkan simpan data di bawah</small>
                                                </div>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table class="table mb-0 table-hover table-striped">
                                                                {{-- <thead class="thead-light">
                                                                    <tr>
                                                                      <th colspan="2" style="text-align: center;">Data Antrian</th>
                                                                    </tr>
                                                                </thead> --}}
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding: 1.15rem 1rem;" width="40%">Nomor Antrian</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->form->id}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 1.15rem 1rem;" width="40%">Nama Pemesan</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->form->nama}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 1.15rem 1rem;">No Handphone</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->form->no_hp}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 1.15rem 1rem;">email</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->form->email}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 1.15rem 1rem;">Toko</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->select->toko->nama_toko}}</td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td style="padding: 1.15rem 1rem;">Baberman</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{$data->select->pekerja->nama_kasir}}</td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td style="padding: 1.15rem 1rem;">Tanggal Pesanan</td>
                                                                        <td style="padding: 1.15rem 1rem;">{{date("d-m-Y, H:i", strtotime($data->form->tanggal))}} WIB</td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td style="padding: 1.15rem 1rem;">Status</td>
                                                                        <td style="padding: 1.15rem 1rem;">
                                                                          <?php
                                                                            if($data->form->status == 0){
                                                                                $sst  = '<div class="badge badge-pill badge-glow badge-warning">Proses Persetujuan</div>';
                                                                            }elseif($data->form->status == 1){
                                                                                $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Di setujui</div>';
                                                                            }elseif($data->form->status == 2){
                                                                                $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak disetujui</div>';
                                                                            }

                                                                            echo $sst;
                                                                          ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="text-center"><small class="mr-25">2022 &copy; RIMS POS</small><br><span><small>Powered by<i class="bx bxs-heart pink mx-50 font-small-3"></i><a class="text-uppercase" href="https://rims.co.id" target="_blank">RIMS CORP</a></small></span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- right section image -->
                                {{-- <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <img class="img-fluid" src="{{ asset('admins/app-assets/images/pages/register.png') }}" alt="branding logo">
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </section>
                <!-- login page ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <script src="{{ asset('admins/app-assets/datetimepicker/moment.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/datetimepicker/datetimepicker.js') }}"></script>
    <!-- END: Theme JS-->
</body>
<!-- END: Body-->

</html>