@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <!-- <h5 class="content-header-title float-left pr-1 mb-0">Fixed Navbar</h5> -->
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">DASHBOARD
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <div class="col-12">
              <div class="row">
                <div class="col-sm-3 col-12 dashboard-users-success">
                  <div class="card text-center">
                    <div class="card-body py-1">
                      <div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto mb-50">
                        <i class="bx bx-briefcase-alt font-medium-5"></i>
                      </div>
                      <div class="text-muted line-ellipsis">Total Customer</div>
                      <h3 class="mb-0" id="total_barang">{{$data->total_customer}}</h3>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3 col-12 dashboard-users-danger">
                  <div class="card text-center">
                    <div class="card-body py-1">
                      <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                        <i class="bx bx-briefcase-alt font-medium-5"></i>
                      </div>
                      <div class="text-muted line-ellipsis">Customer Lunas<br>(Bln Ini)</small></div>
                      <h3 class="mb-0" id="barang_ready">{{$data->total_customer_byr->ttl}}</h3>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3 col-12 dashboard-users-danger">
                  <div class="card text-center">
                    <div class="card-body py-1">
                      <div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto mb-50">
                        <i class="bx bx-briefcase-alt font-medium-5"></i>
                      </div>
                      <div class="text-muted line-ellipsis">Customer Blm Lunas<br>(Bln Ini)</div>
                      <h3 class="mb-0" id="barang_dalam_perbaikan">{{$data->total_customer_blmbyr->ttl}}</h3>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3 col-12 dashboard-users-danger">
                  <div class="card text-center">
                    <div class="card-body py-1">
                      <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                        <i class="bx bx-briefcase-alt font-medium-5"></i>
                      </div>
                      <div class="text-muted line-ellipsis">Customer Blm Aktif</div>
                      <h3 class="mb-0" id="barang_tidak_ready">{{$data->total_customer_blmaktif}}</h3>
                    </div>
                  </div>
                </div>
                {{--<div class="col-sm-3 col-12 dashboard-users-danger">
                  <div class="card text-center">
                    <div class="card-body py-1">
                      <div class="badge-circle badge-circle-lg badge-circle-light-secondary mx-auto mb-50">
                        <i class="bx bx-briefcase-alt font-medium-5"></i>
                      </div>
                      <div class="text-muted line-ellipsis">Total Customer</div>
                      <h3 class="mb-0" id="barang_ditiadakan"></h3>
                    </div>
                  </div>
                </div>--}}
                <!-- <div class="col-xl-4 col-md-6 timline-card">
                  <div class="card ">
                    <div class="card-header">
                      <h4 class="card-title">
                        Timeline
                      </h4>
                    </div>
                    <div class="card-body">
                        <ul class="timeline ps ps--active-y">
                            <li class="timeline-item timeline-icon-success active">
                              <div class="timeline-time">Mon 8:17pm</div>
                              <h6 class="timeline-title">Jonny Richie Commented</h6>
                              <p class="timeline-text">on <a href="JavaScript:void(0);">Project name</a></p>
                              <div class="timeline-content">
                                Story behind video game and lame is very creative
                              </div>
                            </li>
                        </ul>
                      <button class="btn btn-block btn-primary">View All Notifications</button>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>

            {{--<div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $data->title_sub }} </h4>
                    </div>
                    
                    <div class="card-body">
                        <div class="row">
                          <div class="col-sm-3 col-12 dashboard-users-success">
                            <div class="card text-center">
                              <div class="card-body py-1">
                                <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                  <i class="bx bx-mail-send font-medium-5"></i>
                                </div>
                                <div class="text-muted line-ellipsis">Proses PO</div>
                                <h3 class="mb-0" id="total_barang"></h3>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3 col-12 dashboard-users-danger">
                            <div class="card text-center">
                              <div class="card-body py-1">
                                <div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto mb-50">
                                  <i class="bx bx-mail-send font-medium-5"></i>
                                </div>
                                <div class="text-muted line-ellipsis">Disetujui</div>
                                <h3 class="mb-0" id="barang_ready"></h3>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3 col-12 dashboard-users-danger">
                            <div class="card text-center">
                              <div class="card-body py-1">
                                <div class="badge-circle badge-circle-lg badge-circle-light-secondary mx-auto mb-50">
                                  <i class="bx bx-mail-send font-medium-5"></i>
                                </div>
                                <div class="text-muted line-ellipsis">Tidak Disetujui</div>
                                <h3 class="mb-0" id="barang_dalam_perbaikan"></h3>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3 col-12 dashboard-users-danger">
                            <div class="card text-center">
                              <div class="card-body py-1">
                                <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                  <i class="bx bx-mail-send font-medium-5"></i>
                                </div>
                                <div class="text-muted line-ellipsis">Proses Persetujuan</div>
                                <h3 class="mb-0" id="barang_tidak_ready"></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9 col-md-6 col-lg-6">
                                <fieldset class="form-group">
                                    <select id="status" class="form-control" id="basicSelect">
                                        <option value="all">Semua Status</option>
                                        <option value="0">Proses Persetujuan</option>
                                        <option value="1">Disetujui</option>
                                        <option value="2">Tidak Disetujui</option>
                                        <option value="3">Revisi</option>
                                        <option value="4">Proses PO (Selesai)</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="table-responsive">
                          <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                <thead class="thead-light">
                                    <tr>
                                        <th></th>
                                        <th align="center">NO</th>
                                        <th align="left">TGL PENGAJUAN</th>
                                        <th align="left">NOMOR</th>
                                        <th align="left">DEVISI</th>
                                        <th align="left">DI MINTA OLEH</th>
                                        <th align="center">STATUS</th>
                                        
                                    </tr>
                                </thead>  
                                <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                               
                          </table>
                        </div>
                        
                    </div>
                </div>
            </div>--}}

        </div>
    </div>
</div>
@endsection

@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>

  <!--     <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> -->

    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <!-- END: Theme JS-->
@endsection

@section('js-action')
<script type="text/javascript">
    $(function () {
        var table,table_spbdetail,table_spbdetail_edit,table_spbdetail_all,
            status = $('#status');  

        $(document).on("mouseenter",".popover-hover",function(){             
            $(this).tooltip('show');
        }).on("mouseleave",".popover-hover",function(){
            $(this).tooltip('hide');
        });

    // begin first table
    function getDataSpb(status){
          table = $('#sponsor').DataTable({
              "bProcessing": true,
              "bServerSide": true,
              "autoWidth": false, 
              "paginationType": "full_numbers",
              "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
              "iDisplayLength": 50, 
              "responsive": true,
              "destroy": true,
              "ajax":{
                  "url": "{{url('admin-panel/data_spb')}}",
                  "dataType": "json",
                  "type": "POST",
                  "data":{ _token: "{{csrf_token()}}", status: status}
              },
             
              "language": {
                  "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
              },
              order: [[2, "desc"]],
              
              columnDefs: [
                  { 
                      targets: 0,
                      width: '5%',
                      orderable: false
                  },
                  {
                      targets: 1,
                      width: '5%',
                      orderable: false
                  },
                  { className: "text-left", "targets": [ 2 ] },
                  { className: "text-left", "targets": [ 3 ] },
                  { className: "text-left", "targets": [ 4 ] },
                  { className: "text-left", "targets": [ 5 ] },
                  { 
                    className: "text-center", 
                    "targets": [ 6 ],
                    width: '5%',
                    orderable: false 
                  }
              ],
              "columns": [
                  { data: null, render: function ( data, type, row ) {
    
                    var editUrl = "{{ url('/admin-panel/editspbview', 'url') }}";
                    editUrl = editUrl.replace('url', data['url']);
    
                    var detailUrl = "{{ url('/admin-panel/view/spb', 'url') }}";
                    detailUrl = detailUrl.replace('url', data['url']);
    
                    var VerifikasiUrl = "{{ url('/admin-panel/spb/verifikasi', 'url') }}";
                    VerifikasiUrl = VerifikasiUrl.replace('url', data['url']);
                    
                    var status = data['status_code'];
                        if(status == '0'){
                            return '<div class="text">\n\
                                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                      <a href="'+ editUrl +'"><button type="button" class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button><br>\n\
                                      <a href="'+ VerifikasiUrl +'"><button type="button" class="btn btn-icon btn-outline-info popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Lihat/Periksa Data"><i class="bx bx-check-double"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-success popover-hover print_spb" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                              </div>';
                        }else if(status == '1'){
                            return '<div class="text">\n\
                                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-success popover-hover print_spb" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                              </div>';
                        }else if(status == '2'){
                            return '<div class="text">\n\
                                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                                      <button class="btn btn-icon btn-outline-success popover-hover print_spb" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                              </div>';
                        }else if(status == '3'){
                            return '<div class="text">\n\
                                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                      <a href="'+ editUrl +'"><button type="button" class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button><br>\n\
                                      <a href="'+ VerifikasiUrl +'"><button type="button" class="btn btn-icon btn-outline-info popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Lihat/Periksa Data"><i class="bx bx-check-double"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-success popover-hover print_spb" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                              </div>';
                        }else if(status == '4'){
                            return '<div class="text">\n\
                                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                      <button class="btn btn-icon btn-outline-success popover-hover print_spb" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                              </div>';
                        }
                      
                    
                    } 
                  },
                  {
                      "data": "no",
                      render: function (data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  { data:  'tgl_pengajuan'  },
                  { data:  'nomor'  },
                  { data:  'devisi_nama'  },
                  { data:  'peminta_nama'  },
                  { data:  'status'  }
                  
              ],
          });
      }

      getDataSpb(status.val());

      status.on('change', function () {
        getDataSpb(status.val());
      });

      status.select2({
          placeholder: "Pilih Status",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });
    
    $(document).on('click', '.print_spb', function () {
            var id = $(this).attr('a');
            PopupCenter('{{ url('/admin-panel/spb/print') }}?id=' + id , 'SURAT PERMINTAAN BARANG' ,'1200', '800');
        });

    $(document).on('click', '.delete_btn', function () {
          var id = $(this).attr('a');
          var b = $(this).attr('b');

          var n = new Noty({
            
              text: 'Apakah SPB Dengan Nomor <strong>' + b + '</strong> akan dihapus?',
              type: 'error',
              buttons: [
                  Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                      n.close();
                      $.ajax({
                          type: 'POST',
                          url: '{{ url('admin-panel/deletespb') }}',
                          data: ({id:id, _token:'{{csrf_token()}}'}),
                          success: function(result){
                              if(result['status'] == 'success')
                              {
                                  new Noty({
                                      type: 'warning',
                                      layout: 'topRight',
                                      text: result['msg'],
                                      
                                      timeout: 4000,
                                  }).show();

                                  table.draw();

                              }
                              else
                              {
                                  new Noty({
                                      type: 'error',
                                      layout: 'topRight',
                                      text: result['msg'],
                                      
                                      timeout: 4000,
                                  }).show();
                              }
                          }
                      });

                  }, {id: 'button1', 'data-status': 'ok'}),

                  Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                      n.close();
                  })
              ]
          }).show();

    });
      
  });

</script>
@endsection