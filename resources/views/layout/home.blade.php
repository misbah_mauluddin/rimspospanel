<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="RIMS CORP">
    <meta name="keywords" content="RIMS CORP">
    <meta name="author" content="RIMS CORP">
    <title>RIMSPOS</title>
    <link rel="apple-touch-icon" href="{{ asset('admins/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('icorims.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/ui/prism.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/components.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/themes/semi-dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/noty.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/themes/bootstrap-v3.css') }}">

    <style type="text/css">
        .form-control.is-invalid,
        .bootstrap-select.is-invalid > button {
            border-color: #FF5B5C !important;
        }

        label.is-invalid,
        label.is-valid {
            color: #FF5B5C !important;
        }
        .form-control.is-valid,
        .bootstrap-select.is-valid > button {
            border-color: #1BC5BD !important;
        }
        label.is-valid {
            color: #1BC5BD !important;
        }

        .input-group > .select2-container--default {
          width: 50% !important;
          flex: 1 1 auto;
        }

        .input-group > .select2-container--default .select2-selection--single {
          height: 100%;
          line-height: inherit;
        }

    </style>
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    <script type="text/javascript">
        ////for liveicon
        var base_path = "{{url('/admins')}}";
    </script>

    
    <!-- END: Page JS-->
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    @include('layout.navigasi')

    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->
    
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; RIMS POS</span><span class="float-right d-sm-inline-block d-none">Powered by<i class="bx bxs-heart pink mx-50 font-small-3"></i><a class="text-uppercase" href="https://rims.co.id" target="_blank">RIMS CORP</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </footer>
    <!-- END: Footer-->


    @yield('js-scripts')
    

    <!-- BEGIN: Page JS-->
    
    <script type="text/javascript">
        Noty.overrideDefaults({
                layout: 'topRight',
                theme: 'bootstrap-v3',
                
            timeout: 4000
                
            });

        function PopupCenter(url, title, w, h) {  
            // Fixes dual-screen position                         Most browsers      Firefox  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
                      
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
                      
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
            var top = ((height / 2) - (h / 2)) + dualScreenTop;  
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
          
            // Puts focus on the newWindow  
            if (window.focus) {  
                newWindow.focus();  
            }  
        }  
    </script>

    @if(Session::has('success'))
        <script type="text/javascript">
            new Noty({
                text: '<strong>Success</strong> <br> <?php echo Session::get('success'); ?>',
                layout: 'topRight',
                type: 'success'
            }).setTimeout(4000).show();
        </script>
    @endif
    @if(Session::has('fail'))
        <script type="text/javascript">
            new Noty({
                text: "<strong>Fails</strong> <br> <?php echo Session::get('fail'); ?>",
                layout: 'topRight',
                type: 'warning'
            }).setTimeout(4000).show();
        </script>
    @endif
    @yield('js-action')

</body>
<!-- END: Body-->

</html>