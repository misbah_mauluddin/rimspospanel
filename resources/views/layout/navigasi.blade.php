
<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-light">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="javascript:void(0);"><i class="ficon bx bx-menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav bookmark-icons">
                       
                        <!-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="#" data-toggle="tooltip" data-placement="top" >RIMS POS </a></li> -->
                        <!-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="https://und.rims.co.id/simpeg_und" data-toggle="tooltip" data-placement="top" title="Sistem Informasi Kepegawaian">|  <i class="ficon bx bx-group"></i></a></li> -->
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>

                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name">
                                    <?php 
                                        $sesiddata = Session::get('idadmin');
                                        $dtadmin = DB::table('user')->where('id',$sesiddata)->first();
                                        $getrole = DB::table('user_role')->where('id',$dtadmin->role_id)->first();
                                        
                                        echo $dtadmin->name;
                                    ?>
                                </span>
                                <span class="user-status"><?php echo $getrole->role; ?></span>
                            </div>
                            <span>
                                @if($dtadmin->image != "")
                                <img class="round" src="{{ asset($dtadmin->image) }}" alt="avatar" height="40" width="40">
                                @else
                                <img class="round" src="{{ asset('admins/app-assets/images/portrait/small/avatar-s-11.jpg') }}" alt="avatar" height="40" width="40">
                                @endif
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ url('/admin-panel/user/profile') }}">
                                <i class="bx bx-user mr-50"></i> My Profile
                            </a>
                            <div class="dropdown-divider">
                                
                            </div>
                            <a class="dropdown-item" href="{{url('/logout')}}"><i class="bx bx-power-off mr-50"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <div class="brand-logo">
                        <?php
                            $imgdata = DB::table('aplikasi')->first();
                        ?>
                        @if($imgdata->image != "")
                            <img src="{{ asset($imgdata->image) }}" width="30px" alt="" style="margin-top: -10px;">
                        @else
                            <img src="{{ asset('placeholder.png') }}" width="30px" alt="" style="margin-top: -10px;">
                        @endif
                        
                    </div>
                    <h2 class="brand-text mb-0">
                        @if($imgdata->nama_aplikasi != "")
                            {{$imgdata->nama_aplikasi}}
                        @else
                            RIMS POST
                        @endif
                    </h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        
        <?php 
            $sesdata = session()->get('role_id');
            $getuser = DB::table('user')->where('role_id',$sesdata)->first();
             
        ?>
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">

        @if($public_menu)
        
            @foreach($public_menu as $menu)

            

            <?php $cekuserakses = DB::table('user_access_menu')->where('role_id',$getuser->role_id)->where('menu_id',$menu->id)->first(); ?>
                @if($cekuserakses)
                @if($menu->class == "titlemenu")
                    <li class=" navigation-header text-truncate"><span data-i18n="{{$menu->label}}">{{$menu->label}}</span></li>
                @else
                    <?php 
                    
                    $menucek = DB::table('admin_menu_items')->where('label',$page)->first(); 
                    if($menu->id == $menucek->id && $menu->class == "single"){
                        if(request()->is($menu->link)){
                            $cssgo = "active";
                        }else{
                            $cssgo = "";
                        }
                        
                    }elseif($menu->id == $menucek->id && $menu->class != "single"){
                        $cssgo = "open";
                    }else{
                        $cssgo = "";
                    }

                    ?>
                    @if($menu->is_active == '1')
                    <li class=" nav-item <?php echo $cssgo;  ?> ">
                        <a href="{{ url($menu->link) }}" target="{{$menu->target}}">
                            <i class="menu-livicon" data-icon="{{ $menu->icon }}"></i>
                            <span class="menu-title text-truncate" data-i18n="{{ $menu->label }}">{{ $menu->label }}</span>
                        </a>
                        @if( $menu->child )

                            @if($menu->class != "single") <ul class="menu-content"> @endif
                            @foreach( $menu->child as $child )
                                <?php $cekuseraksessub = DB::table('user_access_menu')->where('role_id',$getuser->role_id)->where('menu_id',$child->id)->first(); ?>
                                    @if($cekuseraksessub)
                                        @if($child->is_active == '1')
                                        <li class="{{ (request()->is($child->link) ? 'active' : ($child->label ==  $page ? 'active' : '' )) }}">
                                            <a href="{{ url($child->link) }}" target="{{$child->target}}">
                                                <i class="menu-livicon" data-icon="{{$child->icon}}" style="width: 1.6rem !important;"></i>
                                                <span class="menu-item text-truncate" data-i18n="{{$child->label}}">{{$child->label}}</span>
                                            </a>
                                            @if( $child->depth  )
                                            <?php $getsubmenu = DB::table('admin_menu_items')->where('depth','>','1')->where('parent',$child->id)->orderBy('sort','ASC')->get();  ?>
                                                @foreach($getsubmenu as $rowsubmenu)
                                                @if($rowsubmenu->is_active == '1')
                                                <ul class="menu-content">
                                                    <li class="{{ (request()->is($rowsubmenu->link) ? 'active' : ($rowsubmenu->label ==  $page ? 'active' : '' )) }}">
                                                        <a href="{{ url($rowsubmenu->link) }}" target="{{$rowsubmenu->target}}">
                                                            <i class="menu-livicon" data-icon="{{$rowsubmenu->icon}}" style="width: 1.6rem !important;"></i>
                                                            <span class="menu-item text-truncate" data-i18n="{{$rowsubmenu->label}}">{{$rowsubmenu->label}}</span>
                                                        </a>
                                                        @if( $rowsubmenu->depth > '1' )
                                                        <?php $getsubsubmenu = DB::table('admin_menu_items')->where('depth','>','2')->where('parent',$rowsubmenu->id)->orderBy('sort','ASC')->get();  ?>
                                                            @foreach($getsubsubmenu as $rowsubsubmenu)
                                                                <ul class="menu-content">
                                                                    <li class="{{ (request()->is($rowsubsubmenu->link)) ? 'active' : '' }}">
                                                                        <a href="{{ url($rowsubsubmenu->link) }}" target="{{$rowsubsubmenu->target}}">
                                                                            <i class="menu-livicon" data-icon="{{$rowsubsubmenu->icon}}" style="width: 1.6rem !important;"></i>
                                                                            <span class="menu-item text-truncate" data-i18n="{{$rowsubsubmenu->label}}">{{$rowsubsubmenu->label}}</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                                @endif
                                                @endforeach
                                            @else

                                            @endif
                                        </li>

                                        @endif
                                    @endif
                            @endforeach
                            @if($menu->class != "single") </ul> @endif
                        @endif
                    </li>
                    @endif
                @endif
                @endif
            @endforeach
        @endif
        </ul>
    </div>
</div>