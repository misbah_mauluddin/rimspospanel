@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> PENGATURAN</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">

                    <form id="input-title" method="POST" action="{{ url('/inv-site/updateaplikasiaksi',encrypt($data->form->id)) }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Identitas {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                                <!-- <a href="#">
                                                    <a href="{{ url('/inv-site/barang') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                                </a> -->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">

                                        <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama_aplikasi">Nama Aplikasi <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama_aplikasi" name="nama_aplikasi" class="form-control" placeholder="Nama Aplikasi" value="{{ $data->form->nama_aplikasi }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama">Nama Perusahaan <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama" name="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" value="{{ $data->form->nama_perusahaan }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="no_telepon">No Telepon <small style="color: red;">*</small></label>
                                                        <input type="number" id="no_telepon" name="no_telepon" class="form-control" placeholder="No Telepon" value="{{ $data->form->no_telepon }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="no_hp">No Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="no_hp" name="no_hp" class="form-control" placeholder="No Hp" value="{{ $data->form->no_hp }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email <small style="color: red;">*</small></label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="{{ $data->form->email }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="fax">Fax <small style="color: red;">*</small></label>
                                                        <input type="text" id="fax" name="fax" class="form-control" placeholder="Fax" value="{{ $data->form->fax }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="kodepos">Kode Pos <small style="color: red;">*</small></label>
                                                        <input type="number" id="kodepos" name="kodepos" class="form-control" placeholder="Kode Pos" value="{{ $data->form->kodepos }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat <small style="color: red;">*</small></label>
                                                        <textarea class="form-control" id="alamat" name="alamat" rows="4" placeholder="Alamat">{{ $data->form->alamat }}</textarea>

                                                    </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                                <div class="col-12 d-flex justify-content-end">
                                                    <!-- <a href="{{ url('/inv-site/barang') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                            <i class="bx bx-left-arrow-circle"></i> BATAL
                                                        </button></a> -->
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                        <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Logo {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        @if($data->form->image != "")
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset($data->form->image)}}">
                                        @else
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                        @endif
                                        <!-- <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('uploads/aplikasi/logo/'.$data->form->image)}}" alt="logo.png"> -->
                                        <div class="form-body">
                                            <br>
                                            <fieldset class="form-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*" />
                                                    <label class="custom-file-label" for="image">Pilih logo</label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </section>
        </div>
    </div>
</div>

@endsection
@section('js-scripts')
<!-- BEGIN: Vendor JS-->
<script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
<script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>

<script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
<script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

<script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('admins/noty/noty.min.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
<script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
<!-- END: Theme JS-->


@endsection

@section('js-action')
<script type="text/javascript">
    $(function() {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 2000000) {
                    new Noty({
                        text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>',
                        layout: 'topRight',
                        type: 'error'
                    }).setTimeout(4000).show();
                    input.value = "";
                } else {
                    reader.onload = function(e) {
                        $('#image-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $("#image-img").change(function() {
            readURL(this);
        });
    });
</script>
@endsection