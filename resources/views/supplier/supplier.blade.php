@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> </a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;"> {{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    @if($data->halaman == 'input')
                      <form id="input-title" method="POST" action="{{ URL::Route('Tambahsupplier') }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Tambah {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/supplier') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama">Nama supplier <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama supplier"   value="{{ old('nama') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="kontak_person">Kontak Person <small style="color: red;">*</small></label>
                                                        <input type="text" id="kontak_person" name="kontak_person" class="form-control" placeholder="Kontak Person"  value="{{ old('kontak_person') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat 1 <small style="color: red;">*</small></label>
                                                        <textarea class="form-control" id="alamat" name="alamat" rows="2" placeholder="ALamat ...">{{ old('alamat') }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat_dua">Alamat 2 <small style="color: #8e8e8e;">(opsional)</small></label>
                                                        <textarea class="form-control" id="alamat_dua" name="alamat_dua" rows="2" placeholder="Alamat kedua (opsional) ...">{{ old('alamat_dua') }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="regency_id">Kota<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="regency_id" name="regency_id" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->regency)
                                                                @foreach($data->select->regency as $regency)
                                                                    <option value="{{ $regency->id }}" {{ old('regency_id') == $regency->id ? 'selected' : '' }}>{{ $regency->name }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="district_id">Kecamatan <small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="district_id" name="district_id" style="width: 100%;">
                                                            <option></option>
                                                        </select>
                                                        <div class="spinner-grow text-primary" role="status" id="dep_dist_c" style="display: none; ">
                                                          <span class="sr-only">Loading...</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="telepon">Telepon <small style="color: red;">*</small></label>
                                                        <input type="number" id="telepon" name="telepon" class="form-control" placeholder="Telepon"  autocomplete="off" value="{{ old('telepon') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="hp">Nomer Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="hp" name="hp" class="form-control" placeholder="Harga Nomer Hp"  autocomplete="off" value="{{ old('hp') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email </label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email Aktif"  autocomplete="off" value="{{ old('email') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="website">Website </label>
                                                        <input type="text" id="website" name="website" class="form-control" placeholder="Website"  autocomplete="off" value="{{ old('website') }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="pos">Kode Pos </label>
                                                        <input type="text" id="pos" name="pos" class="form-control" placeholder="Kode Pos"  autocomplete="off" value="{{ old('pos') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="fax">Fax </label>
                                                        <input type="text" id="fax" name="fax" class="form-control" placeholder="Fax"  autocomplete="off" value="{{ old('fax') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="ket">Keterangan</label>
                                                        <textarea class="form-control" id="ket" name="ket" rows="2" placeholder="keterangan ..."></textarea>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Supplier aktif ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="1" checked="checked">
                                                                <label for="status1">Aktif</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="0">
                                                              <label for="status2">Tidak Aktif</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                                
                                                
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/supplier') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                      </form>  
                    @elseif($data->halaman == 'update')
                      <form id="input-title" method="POST" action="{{ url('/admin-panel/editsupplieraksi',encrypt($data->form->id)) }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/supplier') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama">Nama supplier <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama supplier"   value="{{ $data->form->nama }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="kontak_person">Kontak Person <small style="color: red;">*</small></label>
                                                        <input type="text" id="kontak_person" name="kontak_person" class="form-control" placeholder="Kontak Person"  value="{{ $data->form->kontak_person }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat 1 <small style="color: red;">*</small></label>
                                                        <textarea class="form-control" id="alamat" name="alamat" rows="2" placeholder="ALamat ...">{{ $data->form->alamat }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat_dua">Alamat 2 <small style="color: #8e8e8e;">(opsional)</small></label>
                                                        <textarea class="form-control" id="alamat_dua" name="alamat_dua" rows="2" placeholder="Alamat kedua (opsional) ...">{{ $data->form->alamat_dua }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="regency_id">Kota<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="regency_id" name="regency_id" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->regency)
                                                                @foreach($data->select->regency as $regency)
                                                                    <option value="{{ $regency->id }}" {{ $data->form->regency_id == $regency->id ? 'selected' : '' }}>{{ $regency->name }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="dep_dist">Kecamatan <small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="dep_dist" name="district_id" style="width: 100%;">
                                                            <option></option>
                                                        </select>
                                                        <div class="spinner-grow text-primary" role="status" id="dep_dist_c" style="display: none; ">
                                                          <span class="sr-only">Loading...</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="telepon">Telepon <small style="color: red;">*</small></label>
                                                        <input type="number" id="telepon" name="telepon" class="form-control" placeholder="Telepon"  autocomplete="off" value="{{ $data->form->telepon }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="hp">Nomer Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="hp" name="hp" class="form-control" placeholder="Harga Nomer Hp"  autocomplete="off" value="{{ $data->form->hp }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email </label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email Aktif"  autocomplete="off" value="{{ $data->form->email }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="website">Website </label>
                                                        <input type="text" id="website" name="website" class="form-control" placeholder="Website"  autocomplete="off" value="{{ $data->form->website }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="pos">Kode Pos </label>
                                                        <input type="text" id="pos" name="pos" class="form-control" placeholder="Kode Pos"  autocomplete="off" value="{{ $data->form->pos }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="fax">Fax </label>
                                                        <input type="text" id="fax" name="fax" class="form-control" placeholder="Fax"  autocomplete="off" value="{{ $data->form->fax }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="ket">Keterangan</label>
                                                        <textarea class="form-control" id="ket" name="ket" rows="2" placeholder="keterangan ..."></textarea>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Supplier aktif ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="1" {{ $data->form->status == 1 ? 'checked' : '' }}>
                                                                <label for="status1">Aktif</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="0" {{ $data->form->status == 0 ? 'checked' : '' }}>
                                                              <label for="status2">Tidak Aktif</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                                
                                                
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/supplier') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      @if($data->form->image != "")
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset($data->form->image)}}">
                                      @else
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      @endif
                                      
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                      </form>  
                    @elseif($data->halaman == 'data')
                      <div class="col-md-12 col-lg-12">
                          <div class="card">
                              <div class="card-header">
                                  <h4 class="card-title">Data {{ $data->title_sub }} </h4>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/tambahsupplierview') }}"><button type="button" class="btn btn-icon btn-outline-primary"><i class="bx bxs-add-to-queue"></i> Tambah {{ $data->title }}</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-body">
                                <h6>Status {{ $data->title_sub }} </h6>
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <select id="status" class="form-control" id="basicSelect">
                                                <option value="all">Semua Status</option>
                                                <option value="0">Tidak Aktif</option>
                                                <option value="1">Aktif</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-icon btn-outline-success print-laporan"><i class="bx bx-printer"></i> Print</button>
                                    </div>
                                </div>
                                  <div class="table-responsive">
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                          <thead class="thead-light">
                                              <tr>
                                                  <th></th>
                                                  <th align="center">NO</th>
                                                  <th align="left">NAMA</th>
                                                  <th align="left">ALAMAT</th>
                                                  <th align="left">KOTA</th>
                                                  <th align="left">TELEPON</th>
                                                  <th align="left">HP</th>
                                                  <th align="center">STATUS</th>
                                                  
                                              </tr>
                                          </thead>  
                                          <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                         
                                    </table>
                                  </div>
                                  
                              </div>
                          </div>
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-info modal-sm" role="document">     
                            <div class="modal-content">
                                <div class="modal-header bg-dark white">                        
                                    <span class="modal-title" id="myModalLabel150">Foto Supplier</span>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img id="image-show" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                </div>
                                
                                
                            </div>
                        </div>            
                      </div>
                    @elseif($data->halaman == 'detail')
                      <div class="col-lg-9 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">Detail {{ $data->title }} - {{ $data->form->nama }} </h4>
                                <div class="heading-elements">
                                  <ul class="list-inline mb-0 d-flex align-items-center">
                                    <li class="ml-2">
                                      <a href="#">
                                        <a href="{{ url('/admin-panel/supplier') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kembali</button></a>
                                      </a>
                                    </li>
                                  </ul>
                              </div>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs mb-2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                                            <i class="bx bx-info-circle mr-25" data-toggle="tooltip" data-placement="bottom" data-title="Detail Data supplier"></i><span class="d-none d-sm-block">Detail Supplier</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fade show" id="account" aria-labelledby="account-tab" role="tabpanel">
                                        <!-- users edit media object start 
                                        <div class="media mb-2">
                                            <a class="mr-2" href="javascript:void(0);">
                                                <img src="../../../app-assets/images/portrait/small/avatar-s-26.jpg" alt="users avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Avatar</h4>
                                                <div class="col-12 px-0 d-flex">
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary mr-25">Change</a>
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-light-secondary">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                        users edit media object ends -->
                                        <!-- users edit account form start -->
                                          <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <table class="table mb-0 table-hover table-striped">
                                                <thead class="thead-light">
                                                    <tr>
                                                      <th colspan="2" style="text-align: center;">Data Supplier</th>
                                                    </tr>
                                                </thead>
                                              </table>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;" width="40%">Nama</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nama}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Kontak Person</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->kontak_person}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Alamat</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->alamat}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Alamat Kedua</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->alamat_dua}}</td>
                                                        </tr>
                                                        <tr >
                                                            <td style="padding: 1.15rem 1rem;">Kota</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->select->regency->name}}</td>
                                                        </tr>
                                                        <tr >
                                                            <td style="padding: 1.15rem 1rem;">Kecamatan</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->select->district->name}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    <tbody>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;" width="40%">Telepon</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->telepon}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Nomer Hp</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->hp}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Email</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->email}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Website</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->website}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Kode Pos</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->pos}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Fax</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->fax}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Status</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              <?php
                                                                if($data->form->status == 0){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                                                                }elseif($data->form->status == 1){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                                                                }

                                                                echo $sst;
                                                              ?>
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Keterangan</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->ket}}</td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Di input</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              @if($data->form->created_at == "")
                                                                
                                                              @else
                                                                {{date("d-m-Y / H:i", strtotime($data->form->created_at))}}
                                                              @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>
                                            
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                               <a href="{{ url('/admin-panel/supplier') }}"> <button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kemali</button></a>
                                            </div>
                                          </div>
                                            
                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card border-secondary ">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">Foto {{ $data->title }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                  @if($data->form->image != "")
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset($data->form->image)}}">
                                  @else
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                  @endif
                                  {{--<div class="form-body">
                                    <br>
                                      <fieldset class="form-group">
                                          <div class="custom-file">
                                              <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                              <label class="custom-file-label" for="image">Pilih foto</label>
                                          </div>
                                      </fieldset>
                                  </div>--}}
                                </div> 
                            </div>
                        </div>
                      </div>
                    @endif
                </div>
            </section>
        </div>
    </div>
</div>


@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              if(input.files[0].size > 2000000){
                 new Noty({
                      text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>',
                      layout: 'topRight',
                      type: 'error'
                  }).setTimeout(4000).show();
                 input.value = "";
              }else{
                  reader.onload = function (e) {
                      $('#image-img-tag').attr('src', e.target.result);
                  }
                  reader.readAsDataURL(input.files[0]);
              }
          }
      }

      $("#image-img").change(function(){
          readURL(this);
      });
 

      var table, tableRiwayatReturn, tableRiwayatPembelian, status = $('#status');

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });

      $('#regency_id').select2({
          placeholder: "Pilih Kota",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });

      $('#district_id').select2({
          placeholder: "Pilih Kecamatan",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });

      $('#dep_dist').select2({
          placeholder: "Pilih Kecamatan",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });

    @if($data->halaman == 'input')
        function get_dist(a = 1171, b) {
            $.ajax({
                type: "POST",
                url: "{{url('/get_dist')}}",
                data:({a:a, _token:'{{csrf_token()}}'}),
                dataType: "JSON",
                beforeSend: function () {
                    $('#dep_dist_c').show();
                },
                complete: function () {
                    $('#dep_dist_c').fadeOut(1000);
                },
                success: function (result) {
                    if (result['status'] == 'SUCCESS')
                    {
                        var dist_data = result['data'];
                        // $('#dep_dist').html('');

                        $('#district_id').html('').trigger('change');
                        $('#district_id').append('<option value="" id="hidessasasd">Pilih Kecamatan</option>');
                        $.each(dist_data, function (index) {

                            $('#district_id').append('<option value="'+dist_data[index].a+'">'+dist_data[index].b+'</option>');
                            $('#district_id').trigger('change');
                        });

                        if(b != null)
                        {
                            $('#district_id').val(b).trigger('change');
                            $('#district_id').trigger('change');
                        }


                    }else if (result['status'] == 'FAILED')
                    {
                        // $('#district_id').html('');
                        $('#district_id').html('').trigger('change');
                        $('#district_id').append('<option value="" id="hidessasasd">Pilih Kecamatan</option>');
                        $('#district_id').trigger('change');
                    }
                }
            });
        }

        $('#regency_id').on("change", function(e){
            $('#district_id').val('');

            var a = $('#regency_id').val();
            get_dist(a);
        });

    @elseif($data->halaman == 'update')

        <?php 
          if($data->form->regency_id == ""){
              $goreg = "b = 1171";
              $url = url('/get_reg');
          }else{
              $goreg = "b = ".$data->form->regency_id;
              $url = url('/get_reg_edit');
          }
          
        ?>

        get_reg_edit();
        function get_reg_edit(a = 11, <?php echo $goreg; ?> ) {
            $.ajax({
                type: "POST",
                url: "<?php echo $url; ?>",
                data:({a:a,b:b, _token:'{{csrf_token()}}'}),
                dataType: "JSON",
                beforeSend: function () {
                    $('#dep_dist_c').show();
                },
                complete: function () {
                    $('#dep_dist_c').fadeOut(1000);
                },
                success: function (result) {
                    if (result['status'] == 'SUCCESS')
                    {
                        var dist_data = result['data'];
                        // $('#dep_dist').html('');

                        $('#regency_id').html('').trigger('change');
                        $('#regency_id').append('<option value="" id="hidessasasd">Pilih Kota</option>');
                        $.each(dist_data, function (index) {
                            var aa = dist_data[index].a;
                            var aaa = dist_data[index].aa;
                            if(aa = aaa){
                                var sell = "selected";
                            }
                            $('#regency_id').append('<option value="'+dist_data[index].a+'" '+sell+'>'+dist_data[index].b+'</option>');
                            $('#regency_id').trigger("refresh");
                        });

                        if(b != null)
                        {
                            $('#regency_id').val(b).trigger('change');
                            $('#regency_id').trigger('change');
                        }


                    }else if (result['status'] == 'FAILED')
                    {
                        // $('#regency_id').html('');
                        $('#regency_id').html('').trigger('change');
                        $('#regency_id').append('<option value="" id="hidessasasd">Pilih Kota</option>');
                        $('#regency_id').trigger('change');
                    }
                }
            });
        }

         $('#regency_id').on("change", function(e){
            $('#dep_dist').val('');

            var aa = $('#regency_id').val();
            get_distedit(aa);
        });


        <?php 
            if($data->form->regency_id == ""){
                $goregdis = "a = 1171";
                $urldis = url('/get_dist');
            }else{
                $goregdis = "a = ".$data->form->regency_id;
                $urldis = url('/get_dist_edit');
            }
            
            if($data->form->district_id == ""){
                $goregdis = "b";
            }else{
                $godis = "b = ".$data->form->district_id;
            }


        ?>
        function get_distedit(<?php echo $goregdis ?>, <?php echo $godis ?> ) {
            $.ajax({
                type: "POST",
                url: "<?php echo $urldis; ?>",
                data:({a:a,b:b, _token:'{{csrf_token()}}'}),
                dataType: "JSON",
                beforeSend: function () {
                    $('#dep_dist_c').show();
                },
                complete: function () {
                    $('#dep_dist_c').fadeOut(1000);
                },
                success: function (result) {
                    if (result['status'] == 'SUCCESS')
                    {
                        var dist_data = result['data'];
                        // $('#dep_dist').html('');

                        $('#dep_dist').html('').trigger("refresh");
                        $('#dep_dist').append('<option value="" id="hidessasasd">Pilih Kecamatan</option>');
                        $.each(dist_data, function (index) {
                            var aa = dist_data[index].a;
                            var aaa = dist_data[index].aa;
                            if(aa = aaa){
                                var sell = "selected";
                            }
                            $('#dep_dist').append('<option value="'+dist_data[index].a+'" '+sell+'>'+dist_data[index].b+'</option>');
                            $('#dep_dist').trigger("refresh");
                        });

                        if(b != null)
                        {
                            $('#dep_dist').val(b).trigger('change');
                            $('#dep_dist').trigger("refresh");
                        }


                    }else if (result['status'] == 'FAILED')
                    {
                        // $('#dep_dist').html('');
                        $('#dep_dist').html('').trigger("refresh");
                        $('#dep_dist').append('<option value="" id="hidessasasd">Pilih Kecamatan</option>');
                        $('#dep_dist').trigger("refresh");
                    }
                }
            });
        }

    @elseif($data->halaman == 'detail')
    
        tableRiwayatPembelian = $('#riwayat-pembelian').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "autoWidth": false, 
            "paginationType": "full_numbers",
            "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
            "iDisplayLength": 50, 
            "responsive": true,
            "ajax":{
                "url": "{{url('admin-panel/riwayat_pembelian_supplier')}}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", id:  <?php echo $data->form->id ?>}
            },
            
            "language": {
                "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
            },
            order: [[2, "desc"]],
            
            columnDefs: [
                { 
                    targets: 0,
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    targets: 1,
                    width: '20px',
                    orderable: false,
                    className: "text-center"
                },
                { className: "text-left", "targets": [ 2 ] },
                { className: "text-center", "targets": [ 3 ] },
                { className: "text-center", "targets": [ 4 ] },
                { className: "text-left", "targets": [ 5 ] },
                { className: "text-left", "targets": [ 6 ] },
                { className: "text-center", "targets": [ 7 ] }
            ],
            "columns": [
                { data: null, render: function ( data, type, row ) {

                    var editUrl = "{{ url('/admin-panel/editpoterimaview', 'id_po') }}";
                    editUrl = editUrl.replace('id_po', data['id_po']);

                    var detailUrl = "{{ url('/admin-panel/view/poterima', 'id_po') }}";
                    detailUrl = detailUrl.replace('id_po', data['id_po']);

                    var VerifikasiUrl = "{{ url('/admin-panel/poterima/verifikasi', 'id_po') }}";
                    VerifikasiUrl = VerifikasiUrl.replace('id_po', data['id_po']);

                    var status = data['status_code'];
                        if(status == '4'){
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                <a href="'+ editUrl +'"><button type="button" class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Terima Barang"><i class="bx bx-package"></i> </button></a><br>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }else if(status == '5'){
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a><br>\n\
                                <a href="'+ VerifikasiUrl +'"><button type="button" class="btn btn-icon btn-outline-info popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Verifikasi/Periksa Data"><i class="bx bx-check-double"></i> </button></a>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }else{
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }
                            
                    
                    } 
                },
                {
                    "data": "no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data:  'nomor'  },
                { data:  'tgl_po'  },
                { data:  'tgl_terima'  },
                { data:  'supplier'  },
                { data:  'penerima'  },
                { data:  'status'  }
                
            ],
        });
      
        tableRiwayatReturn = $('#riwayat-return').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "autoWidth": false, 
            "paginationType": "full_numbers",
            "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
            "iDisplayLength": 50, 
            "responsive": true,
            "ajax":{
                "url": "{{url('admin-panel/riwayat_return_barang')}}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", id: <?php echo $data->form->id ?>}
            },
            
            "language": {
                "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
            },
            order: [[2, "desc"]],
            
            columnDefs: [
                { 
                    targets: 0,
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    targets: 1,
                    width: '20px',
                    orderable: false,
                    className: "text-center"
                },
                { className: "text-left", "targets": [ 2 ] },
                  { className: "text-center", "targets": [ 3 ] },
                  { className: "text-center", "targets": [ 4 ] }
            ],
            "columns": [
                { data: null, render: function ( data, type, row ) {

                var detailUrl = "{{ url('/admin-panel/view/poreturn', 'id_po') }}";
                detailUrl = detailUrl.replace('id_po', data['id_po']);
               
                return '<div class="text">\n\
                      <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                      <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                </div>';
                        
                
                } 
              },
                {
                    "data": "no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data:  'nomor'  },
                  { data:  'supplier'  },
                  { data:  'penerima'  },
                  { data:  'ket'  },
                ],
        });

    @endif
      

      $('#input-title').validate({
          rules: {
              nama: {
                  required: true
              },
              kontak_person: {
                  required: true
              },
              alamat: {
                  required: true
              },
              regency_id: {
                  required: true
              },
              district_id: {
                  required: true
              },
              telepon: {
                  required: true
              },
              hp: {
                  required: true
              }
          },       
          errorPlacement: function(label, element)
          {
              if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

          },

      });

      getDataSupplier(status.val());

      // begin first table
      function getDataSupplier(status) {
        table = $('#sponsor').DataTable({
          "bProcessing": true,
          "bServerSide": true,
          "autoWidth": false, 
          "paginationType": "full_numbers",
          "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
          "iDisplayLength": 50, 
          "responsive": true,
          "destroy": true,
          "ajax":{
              "url": "{{url('admin-panel/data_supplier')}}",
              "dataType": "json",
              "type": "POST",
              "data":{ _token: "{{csrf_token()}}", status: status}
          },
         
          "language": {
              "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
          },
          order: [[2, "asc"]],
          
          columnDefs: [
              { 
                  targets: 0,
                  width: '5%',
                  orderable: false
              },
              {
                  targets: 1,
                  width: '5%',
                  orderable: false
              },
              { className: "text-left", "targets": [ 2 ] },
              { className: "text-left", "targets": [ 3 ] },
              { className: "text-left", "targets": [ 4 ] },
              { className: "text-left", "targets": [ 5 ] },
              { className: "text-left", "targets": [ 6 ] },
              { 
                className: "text-center", 
                "targets": [ 7 ],
                width: '5%',
                orderable: false 
              }
          ],
          "columns": [
              { data: null, render: function ( data, type, row ) {

                var editUrl = "{{ url('/admin-panel/editsupplierview', 'url') }}";
                editUrl = editUrl.replace('url', data['url']);

                var detailUrl = "{{ url('/admin-panel/view/supplier', 'url') }}";
                detailUrl = detailUrl.replace('url', data['url']);
                  
                  return '<div class="text">\n\
                          <a href="'+ detailUrl +'"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                          <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                          <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['namasup']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                  </div>';
                
                } 
              },
              {
                  "data": "no",
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { data:  'nama'  },
              { data:  'alamat'  },
              { data:  'namareg'  },
              { data:  'telepon'  },
              { data:  'hp'  },
              { data:  'status'  },
              
          ],
        });
      }

      $('#status').on('change', function () {
        getDataSupplier(status.val());
      });

      $('#status').select2({
          placeholder: "Pilih Status",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });


      $(document).on('click', '.print_po', function () {
            var id = $(this).attr('a');
            PopupCenter('{{ url('/admin-panel/po/print') }}?id=' + id , 'SURAT PERMINTAAN BARANG' ,'1200', '800');
      });



      $(document).on('click', '.delete_btn', function () {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
              
                text: 'Apakah supplier <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/deletesupplier') }}',
                            data: ({id:id, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

      });

      $(document).on("click", ".view_photo", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/view_image/supplier")}}' + '/' + id, function (data) {

              $('#image-show').attr('src', '{{asset("/")}}' + data.data.image);

              
              $('#modal-backdrop-disable-editakses').modal('show');

              $("#progress2").hide();
          })
      });

      

     $(document).on('click', '.print-laporan', function () {
        PopupCenter('{{ url('/admin-panel/data_supplier/print') }}?status=' + status.val() , 'LIST DATA SUPPLIER' ,'1200', '800');
      });


      
  });

    

  </script>
@endsection


