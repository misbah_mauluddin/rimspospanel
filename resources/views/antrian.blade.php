<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="RIMS CORP">
    <meta name="keywords" content="RIMS CORP">
    <meta name="author" content="RIMS CORP">
    <title>RIMSPOS | {{$data->title}}</title>
    <link rel="apple-touch-icon" href="{{ asset('admins/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('icorims.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/ui/prism.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/components.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/themes/semi-dark-layout.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/noty.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/themes/bootstrap-v3.css') }}">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/datetimepicker/datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/font-awesome/css/font-awesome.css') }}">
    <!-- END: Custom CSS-->
    <script type="text/javascript">
        ////for liveicon
        var base_path = "{{url('/admins')}}";
    </script>

    <style type="text/css">
        label.is-invalid, label.is-valid {
            color: #FF5B5C !important;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- login page start -->
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-8 col-11">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- left section-login -->
                                <div class="col-md-6 col-12 px-0">
                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1" style="text-align: center;display: contents;">
                                            <div class="card-title">
                                                <img src="{{ asset('LOGO.png') }}" width="100px" alt="" style="margin-bottom: 7px;">
                                                <h4 class="text-center mb-2"> SELAMAT DATANG <br> <small></small></h4>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            
                                            <div class="divider">
                                                <div class="divider-text text-uppercase text-muted"><small>Silahkan isi data di bawah</small>
                                                </div>
                                            </div>
                                            <form action="{{ route('antrian.add') }}" method="post" id="input-title">
                                                {{ csrf_field() }}
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                                <div class="form-group">
                                                                <label for="id_toko">Toko<small style="color: red;">*</small></label>
                                                                  <select class="select2 form-control" id="id_toko" name="id_toko" style="width: 100%;">
                                                                    <option></option>
                                                                    @if($data->select->toko)
                                                                        @foreach($data->select->toko as $toko)
                                                                            <option value="{{ $toko->id_toko }}">{{ $toko->nama_toko }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="id_pekerja">Baberman<small style="color: red;">*</small></label>
                                                                  <select class="select2 form-control" id="id_pekerja" name="id_pekerja" style="width: 100%;">
                                                                    <option></option>
                                                                </select>
                                                                <div class="spinner-grow text-primary" role="status" id="id_pekerja_c" style="display: none; ">
                                                                  <span class="sr-only">Loading...</span>
                                                                </div>
                                                            </div>
                                                        </div>    
                                                        
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="nama">Nama Pemesan </label>
                                                                <input type="text" id="nama" name="nama" class="form-control" placeholder="Masukkan Nama lengkap anda"   value="{{ old('nama') }}">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="no_hp">No Handphone </label>
                                                                <input type="number" id="no_hp" name="no_hp" class="form-control" placeholder="Masukkan No Hp anda"   value="{{ old('no_hp') }}">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="email">Email </label>
                                                                <input type="email" id="email" name="email" class="form-control" placeholder="Masukkan Email anda"   value="{{ old('email') }}">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="tanggal">Tanggal </label>
                                                                <input type="text" id="tanggal" name="tanggal" class="form-control" placeholder="Masukkan tanggal Pesanan anda"   value="{{ old('tanggal') }}">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <div class="progress progress-bar-primary mb-2">
                                                                <div id="progress" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;display: none;">100%</div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary glow w-100 position-relative">Kirim<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                                    </div>
                                                </div>
                                                
                                            </form>
                                            <hr>
                                            <div class="text-center"><small class="mr-25">2022 &copy; RIMS POS</small><br><span><small>Powered by<i class="bx bxs-heart pink mx-50 font-small-3"></i><a class="text-uppercase" href="https://rims.co.id" target="_blank">RIMS CORP</a></small></span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- right section image -->
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <img class="img-fluid" src="{{ asset('admins/app-assets/images/pages/register.png') }}" alt="branding logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- login page ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <script src="{{ asset('admins/app-assets/datetimepicker/moment.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/datetimepicker/datetimepicker.js') }}"></script>
    <!-- END: Theme JS-->

    <script type="text/javascript">
        Noty.overrideDefaults({
                layout: 'topRight',
                theme: 'bootstrap-v3',
                
            timeout: 4000
                
            });

        function PopupCenter(url, title, w, h) {  
            // Fixes dual-screen position                         Most browsers      Firefox  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
                      
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
                      
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
            var top = ((height / 2) - (h / 2)) + dualScreenTop;  
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
          
            // Puts focus on the newWindow  
            if (window.focus) {  
                newWindow.focus();  
            }  
        }  
    </script>

    <script type="text/javascript">
        $('#id_toko').select2({
            placeholder: "Pilih Toko ",
            allowClear: true,
            "language": {
                "noResults": function () {
                        return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                        return 'Hasilnya tidak dapat dimuat';
                }
            },
        });

        $('#id_pekerja').select2({
            placeholder: "Pilih Baberman / Pekerja ",
            allowClear: true,
            "language": {
                "noResults": function () {
                        return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                        return 'Hasilnya tidak dapat dimuat';
                }
            },
        });

        // $('#tanggal').pickadate({
        //     format: 'dd-mm-yyyy',
        //     formatSubmit: 'yyyy-mm-dd'
        // });

        $('#tanggal').datetimepicker({  
            format: 'DD-MM-YYYY HH:mm',
          icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
         });  

        $('#jam').pickatime({
          format: 'T!ime selected: HH:i a',
          formatLabel: 'HH:i a',
          formatSubmit: 'HH:i',
          hiddenPrefix: 'prefix__',
          hiddenSuffix: '__suffix'
        });

        function get_barberman(a = 1171, b) {
            $.ajax({
                type: "POST",
                url: "{{url('/get_barberman')}}",
                data:({a:a, _token:'{{csrf_token()}}'}),
                dataType: "JSON",
                beforeSend: function () {
                    $('#id_pekerja_c').show();
                },
                complete: function () {
                    $('#id_pekerja_c').fadeOut(1000);
                },
                success: function (result) {
                    if (result['status'] == 'SUCCESS')
                    {
                        var dist_data = result['data'];
                        // $('#dep_dist').html('');

                        $('#id_pekerja').html('').trigger('change');
                        $('#id_pekerja').append('<option value="" id="hidessasasd">Pilih Baberman</option>');
                        $.each(dist_data, function (index) {

                            $('#id_pekerja').append('<option value="'+dist_data[index].a+'">'+dist_data[index].b+'</option>');
                            $('#id_pekerja').trigger('change');
                        });

                        if(b != null)
                        {
                            $('#id_pekerja').val(b).trigger('change');
                            $('#id_pekerja').trigger('change');
                        }


                    }else if (result['status'] == 'FAILED')
                    {
                        // $('#id_pekerja').html('');
                        $('#id_pekerja').html('').trigger('change');
                        $('#id_pekerja').append('<option value="" id="hidessasasd">Pilih Baberman</option>');
                        $('#id_pekerja').trigger('change');
                    }
                }
            });
        }

        $('#id_toko').on("change", function(e){
            $('#id_pekerja').val('');

            var a = $('#id_toko').val();
            get_barberman(a);
        });

        $('#input-title').validate({
            rules: {
                  id_toko: {
                      required: true
                  },
                  id_pekerja: {
                      required: true
                  },
                  nama: {
                      required: true
                  },
                  no_hp: {
                      required: true,
                      digits: true,
                      min: 10,
                      
                  },
                  email: {
                      required: true
                  },
                  tanggal: {
                      required: true
                  }
            },       
            errorPlacement: function(label, element)
            {
                  if (element.is("select")) {
                            label.insertAfter(element.next());
                        } else if (element.is("input:radio")) {
                            label.insertAfter($('#status_div'));
                        } else if (element.is("input#picture_val")) {
                            label.insertAfter($('.file-input'));
                        }
                        else {
                            label.insertAfter(element)
                        }
            },
              //Submit the form
            submitHandler: function (form) {
                var formData = new FormData(form);
                $('#progress').width('0%');
                $('#progress').show();

                $.ajax({
                    type: "POST",
                    url: "{{ route('antrian.add') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progress').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 3000,
                                layout: 'topRight',
                                
                            }).show();
                            setTimeout(function(){ window.location = "{{url('/antrian')}}/"+result['pemesan']; }, 3000);
                        }
                        else
                        {
                            new Noty({
                                type: 'warning',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progress").hide();
                        }
                    }
                });
                return false;
            }

      });
    </script>
</body>
<!-- END: Body-->

</html>