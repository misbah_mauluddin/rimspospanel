@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <!-- <h5 class="content-header-title float-left pr-1 mb-0">Fixed Navbar</h5> -->
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> BARANG</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    @if($data->halaman == 'input')
                    <div class="col-md-12 col-lg-4">
                        <div class="card">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">Tambah {{ $data->title }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                    <form id="input-title" method="POST" action="{{ URL::Route('Tambahjenis') }}" enctype="multipart/form-data" >
                                      {{ csrf_field() }}
                                      
                                          <div class="form-group">
                                              <label>Nama <small style="color: red;">*Wajib Disi</small></label>
                                              
                                              <input type="text" id="jenis" name="jenis" class="form-control" value="{{ old('jenis') }}" placeholder="Nama jenis" >
                                              <div class="invalid-feedback">
                                                <i class="bx bx-radio-circle"></i>
                                              </div>
                                          </div>

                                          <div class="form-group" >
                                            <button type="submit" class="btn btn-icon btn-light-secondary glow float-right" data-dismiss="modal">
                                              <i class="bx bxs-save"></i> SIMPAN DATA
                                            </button>
                                          </div>

                                          <div class="col-md-12" style="display: none;">
                                              <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                          </div>
                                      </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    @elseif($data->halaman == 'update')
                    <div class="col-md-12 col-lg-4">
                        <div class="card">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">Edit {{ $data->title }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                    <form id="input-title" method="POST" action="{{ url('/inv-site/editjenisaksi',encrypt($data->form->id)) }}" enctype="multipart/form-data" >
                                      {{ csrf_field() }}
                                      
                                          <div class="form-group">
                                              <label>Nama <small style="color: red;">*Wajib Disi</small></label>
                                              
                                              <input type="text" id="jenis" name="jenis" class="form-control" value="{{ $data->form->nama }}" placeholder="Nama jenis" >
                                              <div class="invalid-feedback">
                                                <i class="bx bx-radio-circle"></i>
                                              </div>
                                          </div>

                                          <div class="form-group" >
                                            <button type="submit" class="btn btn-icon btn-light-secondary glow float-right" data-dismiss="modal">
                                              <i class="bx bxs-save"></i> SIMPAN DATA
                                            </button>
                                          </div>

                                          <div class="col-md-12" style="display: none;">
                                              <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                          </div>
                                      </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    @endif
                    
                    <div class="col-md-12 col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Data {{ $data->title_sub }} </h4>
                                <!-- <div class="heading-elements">
                                    <ul class="list-inline mb-0 d-flex align-items-center">
                                      <li class="ml-2">
                                        <a href="#">
                                          <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#modal-backdrop-disable" data-backdrop="static"><i class="bx bxs-add-to-queue"></i> Tambah</button>
                                        </a>
                                      </li>
                                    </ul>
                                </div> -->
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                  <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                            <tr>
                                                <th></th>
                                                <th>NO</th>
                                                <th>NAMA</th>
                                                
                                            </tr>
                                        </thead>  
                                        <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                       
                                  </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {
      var table  ;

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });

      $('#input-title').validate({
          ignore: [],
          // Rules for form validation
          rules: {
              jenis: {
                  required: true
              }
          },

          // Messages for form validation
          messages: {
              jenis: {
                  required: "Kolom ini tidak boleh kosong"
              }
          },
          

          // Error Placement
          errorPlacement: function(label, element)
              {
                    if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

                    
              },

      });


      // begin first table
      table = $('#sponsor').DataTable({
          "bProcessing": true,
          "bServerSide": true,
          "autoWidth": false, 
          // "paginationType": "full_numbers",
          "aLengthMenu": [ [10, 20, 50, 100], [10, 20, 50, 100] ],
          "iDisplayLength": 10, 
          "responsive": true,
          "ajax":{
              "url": "{{url('inv-site/data_jenis')}}",
              "dataType": "json",
              "type": "POST",
              "data":{ _token: "{{csrf_token()}}"}
          },
         
          "language": {
              "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
          },
          order: [[2, "asc"]],
          
          columnDefs: [
              { 
                  targets: 0,
                  width: '15%',
                  orderable: false
              },
              {
                  targets: 1,
                  width: '5%',
                  orderable: false
              },
          ],
          "columns": [
              { data: null, render: function ( data, type, row ) {

                var editUrl = "{{ url('/inv-site/editjenisview', 'url') }}";
                editUrl = editUrl.replace('url', data['url']);
                <?php if($data->halaman == 'update'){ ?>
                      return '<div class="text">\n\
                              <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a></div>';
                <?php }else{ ?>
                      return '<div class="text">\n\
                              <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                              <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['nama']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                      </div>';
                <?php } ?>
                } 
              },
              {
                  "data": "no",
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { data:  'nama'  },
              
          ],
      });

      $(document).on('click', '.delete_btn', function () {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
              
                text: 'Apakah jenis <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('inv-site/deletejenis') }}',
                            data: ({id:id, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

        });

     

      
  });

    

  </script>
@endsection


