@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Customer</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Bootstrap Select start -->
            <section class="bootstrap-select">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card border-secondary ">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">{{ $data->title_sub }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                  <div class="form-body">
                                    <div class="row">
                                       <div class="col-lg-8 col-md-8 col-xs-6">
                                            <div class="form-group">
                                                <label for="id_toko">Cari Toko <small style="color: red;">*</small></label>
                                                <select class="select2 form-control" id="id_toko" name="id_toko" style="width: 100%;">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-xs-6">
                                            <div class="form-group">
                                                <label for="status">Status Bayar<small style="color: red;">*</small></label>
                                                <select class="select2 form-control" id="status" name="status" style="width: 100%;">
                                                    <option value="all" selected>Semua Status</option>
                                                        <option value="0">Belum Bayar</option>
                                                        <option value="1">Lunas</option>
                                                        <option value="2">Cicil</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 border-bottom mb-1"></div>
                                    <div class="col-md-12">
                                        <button id="report-view" class="btn btn-icon-fixed btn-primary glow"><i class="bx bx-search"></i> Lihat Data</button>
                                        <button id="report-refresh" class="btn btn-icon-fixed btn-danger glow"><i class="bx bx-refresh"></i> Segarkan</button>
                                    </div>
                                      
                                  </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Bootstrap Select end -->
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    
                      <div class="col-md-12 col-lg-12" id="rowdata" style="display: none;">
                          <div class="card" id="detailharga">
                              <div class="card-header">
                                    <h4 class="card-title"> </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        
                                        <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                            <thead class="thead-light">
                                            <tr>
                                                <th align="center">NO</th>
                                                  <th align="left">NAMA</th>
                                                  <th align="left">ALAMAT</th>
                                                  <th align="left">JENIS</th>
                                                  <th align="left">MASA AKTIF</th>
                                                  <th align="center">STATUS</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                          </div>
                      </div>
                </div>
            </section>

            <div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-info modal-sm" role="document">     
                    <div class="modal-content">
                        <div class="modal-header bg-dark white">                        
                            <span class="modal-title" id="myModalLabel150">Foto Barang</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="image-show" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                        </div>
                        
                        
                    </div>
                </div>            
              </div>
          </div>
        </div>
    </div>
</div>

@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->

    <script src="{{ asset('admins/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {



      

      var table ,
            id_toko = $('#id_toko'),
            status = $('#status'),
            date1 = $('#date1'),
            date2 = $('#date2'),
            rowdata = $('#rowdata');

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });


       

        $("#id_toko").select2({
            placeholder: "Cari Toko",
            allowClear: true,
            quietMillis: 50,
            minimumInputLength: 1,
            delay: 250,
            "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
            ajax: {
                url: "{{ url('admin-panel/gettokoglobal') }}",
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                      q: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            // if (item.id_merek == null){
                            //     var mrk = 'Belum ada merek';
                            // }else{
                            //     var mrk = item.namamerek;
                            // }
                            return {

                                text: item.nama_toko + " - Jenis Usaha: " + item.nama_jenisusaha,
                                id: item.id_toko
                            }
                            // get_satuan(item.id);
                        })
                    };
                },
                cache: true
            }
        });

        function getDataproduktoko(id_toko,status)
          {
                table = $('#sponsor').DataTable({
                  "bProcessing": true,
                  "bServerSide": true,
                  "autoWidth": false, 
                  "paginationType": "full_numbers",
                  "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
                  "iDisplayLength": 50, 
                  "responsive": true,
                  "destroy":true,
                  "ajax":{
                      "url": "{{url('admin-panel/masaaktif/data')}}",
                      "dataType": "json",
                      "type": "POST",
                      "data":{ _token: "{{csrf_token()}}", id_toko: id_toko, status: status}
                  },
                 
                  "language": {
                      "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
                  },
                  order: [[0, "DESC"]],
                  
                  columnDefs: [
                      { 
                          targets: 0,
                          width: '5%',
                          orderable: false
                      },
                      {
                          targets: 1,
                          width: '5%',
                          orderable: false
                      },
                      { className: "text-left", "targets": [ 2 ] },
                      { className: "text-left", "targets": [ 3 ] },
                      { className: "text-center", "targets": [ 4 ] },
                      { className: "text-center", "targets": [ 5 ] },
                  ],
                  "columns": [
                      {
                          "data": "no",
                          render: function (data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      },
                      { data:  'nama'  },
                      { data:  'alamat'  },
                      { data:  'namajenis'  },
                      { data:  'tgl'  },
                      { data:  'status'  },
                      
                  ],
              });
          }

        

      $('#report-view').on('click', function () {
            if(id_toko.val() == ''){
                new Noty({
                    type: 'warning',
                    layout: 'topRight',
                    text: 'Silahkan Pilih Toko terlebih dahulu',
                    
                    timeout: 4000,
                }).show();

                $('#id_toko').focus();
            }else{
                rowdata.fadeIn();
                getDataproduktoko(id_toko.val(),status.val());
            }
          
      });

      $('#report-refresh').on('click', function () {
            rowdata.fadeOut();
            getDataproduktoko(id_toko.val(),status.val());
      });

      $(document).on("click", ".view_photo", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/produktoko/view_image/toko")}}' + '/' + id, function (data) {

              $('#image-show').attr('src', '{{asset("/")}}' + data.data.foto);

              
              $('#modal-backdrop-disable-editakses').modal('show');

              $("#progress2").hide();
          })
      });

      

      $(document).on('click', '.print-laporan', function () {
            PopupCenter('{{ url('/admin-panel/produktoko/print') }}?date1='+date1.val() + '&date2='+date2.val() , 'STATUS Penjualan Toko DALAM INVENTORI' ,'1200', '800');
        });


      
  });

    

  </script>
@endsection


