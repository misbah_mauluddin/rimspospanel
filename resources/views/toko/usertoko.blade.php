@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Customer</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{url('admin-panel/toko')}}"> Toko</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    @if($data->halaman == 'input')
                      <form id="input-title" method="POST" action="{{ URL::Route('Tambahusertoko') }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Tambah {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/usertoko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="id_toko">Pilih Toko<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_toko" name="id_toko" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->id_toko)
                                                                @foreach($data->select->id_toko as $id_toko)
                                                                    <option value="{{ $id_toko->id_toko }}" {{ old('id_toko') == $id_toko->id_toko ? 'selected' : '' }}>{{ $id_toko->nama_toko }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama_kasir">Nama Kasir <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama_kasir" name="nama_kasir" class="form-control" placeholder="Nama usertoko"   value="{{ old('nama_kasir') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12"></div>
                                                
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nomor_hp">No Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="nomor_hp" name="nomor_hp" class="form-control" placeholder="No hp"   value="{{ old('nomor_hp') }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="pass">Password <small style="color: red;">*</small></label>
                                                        <input type="password" id="pass" name="pass" class="form-control" placeholder="Password"  >
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                              
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Status Kasir ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="2" >
                                                                <label for="status1">Kasir</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="3" checked="checked">
                                                              <label for="status2">Admin</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-info radio-glow">
                                                              <input type="radio" id="status3" name="status" value="4" >
                                                              <label for="status3">Pekerja</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/usertoko') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>--}}
                      </form>  
                    @elseif($data->halaman == 'update')
                      <form id="input-title" method="POST" action="{{ url('/admin-panel/editusertokoaksi',encrypt($data->form->id_kasir)) }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/usertoko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="id_toko">Pilih Toko<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_toko" name="id_toko" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->id_toko)
                                                                @foreach($data->select->id_toko as $id_toko)
                                                                    <option value="{{ $id_toko->id_toko }}" {{ $data->form->id_toko == $id_toko->id_toko ? 'selected' : '' }}>{{ $id_toko->nama_toko }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama_kasir">Nama Kasir <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama_kasir" name="nama_kasir" class="form-control" placeholder="Nama usertoko"   value="{{ $data->form->nama_kasir }}">
                                                        
                                                    </div>
                                                </div>
                                                <!-- <div class="col-lg-4 col-md-4 col-xs-12"></div> -->
                                                
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nomor_hp">No Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="nomor_hp" name="nomor_hp" class="form-control" placeholder="No hp"   value="{{ $data->form->nomor_hp }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                              
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Status Kasir ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="2" {{ $data->form->status == 2 ? 'checked' : '' }}>
                                                                <label for="status1">Kasir</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="3" {{ $data->form->status == 3 ? 'checked' : '' }}>
                                                              <label for="status2">Admin</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-info radio-glow">
                                                              <input type="radio" id="status3" name="status" value="4" {{ $data->form->status == 4 ? 'checked' : '' }}>
                                                              <label for="status3">Pekerja</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                                
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/usertoko') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                      </form>  
                    @elseif($data->halaman == 'data')
                      <div class="col-md-12 col-lg-12">
                          <div class="card">
                              <div class="card-header">
                                  <h4 class="card-title">Data {{ $data->title_sub }} </h4>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/tambahusertokoview') }}"><button type="button" class="btn btn-icon btn-outline-primary"><i class="bx bxs-add-to-queue"></i> Tambah {{ $data->title }}</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-body">
                                <h6>Pilih Toko </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset class="form-group">
                                                        <select id="status" class="form-control" id="basicSelect">
                                                            <option value="all" selected>Semua Toko</option>
                                                            @if($data->select->toko)
                                                                @foreach($data->select->toko as $toko)
                                                                    <option value="{{ $toko->id_toko }}">{{ $toko->nama_toko }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    {{-- <button id="report-view" class="btn btn-info btn-icon-fixed btn-shadowed"><i class="bx bx-search"></i> Lihat Laporan</button> --}}
                                                    <button class="btn btn-icon btn-outline-success print-laporan"><i class="bx bx-printer"></i> Print</button>
                                                </div>
                                            </div>
                                  <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                          <thead class="thead-light">
                                              <tr>
                                                  <th></th>
                                                  <th align="center">NO</th>
                                                  <th align="left">NAMA KASIR</th>
                                                  <th align="left">NAMA TOKO</th>
                                                  <th align="left">JENIS</th>
                                                  <th align="left">NO HP</th>
                                                  <th align="center">STATUS</th>
                                                  
                                              </tr>
                                          </thead>  
                                          <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                         
                                    </table>
                                  </div>
                                  
                              </div>
                          </div>
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-info modal-sm" role="document">     
                            <div class="modal-content">
                                <div class="modal-header bg-dark white">                        
                                    <span class="modal-title" id="myModalLabel150">Foto Toko</span>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img id="image-show" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                </div>
                                
                                
                            </div>
                        </div>            
                      </div>
                    @elseif($data->halaman == 'detail')
                      <div class="col-lg-9 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">Detail {{ $data->title }} - {{ $data->form->nama_kasir }} </h4>
                                <div class="heading-elements">
                                  <ul class="list-inline mb-0 d-flex align-items-center">
                                    <li class="ml-2">
                                      <a href="#">
                                        <a href="{{ url('/admin-panel/usertoko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kembali</button></a>
                                      </a>
                                    </li>
                                  </ul>
                              </div>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs mb-2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                                            <i class="bx bx-info-circle mr-25" data-toggle="tooltip" data-placement="bottom" data-title="Detail Data usertoko"></i><span class="d-none d-sm-block">Detail User</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fade show" id="account" aria-labelledby="account-tab" role="tabpanel">
                                        <!-- users edit media object start 
                                        <div class="media mb-2">
                                            <a class="mr-2" href="javascript:void(0);">
                                                <img src="../../../app-assets/images/portrait/small/avatar-s-26.jpg" alt="users avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Avatar</h4>
                                                <div class="col-12 px-0 d-flex">
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary mr-25">Change</a>
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-light-secondary">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                        users edit media object ends -->
                                        <!-- users edit account form start -->
                                          <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <table class="table mb-0 table-hover table-striped">
                                                <thead class="thead-light">
                                                    <tr>
                                                      <th colspan="2" style="text-align: center;">Data User</th>
                                                    </tr>
                                                </thead>
                                              </table>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;" width="40%">Nama Kasir</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nama_kasir}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">No Hp</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nomor_hp}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Status</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              <?php
                                                                if($data->form->status == 2){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Kasir</div>';
                                                                }elseif($data->form->status == 3){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Admin</div>';
                                                                }elseif($data->form->status == 4){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Pekerja</div>';
                                                                }else{
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak Ada Status</div>';
                                                                }

                                                                echo $sst;
                                                              ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    <tbody>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Nama Toko</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nama_toko}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Jenis Usaha</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->select->jenis->nama_jenisusaha}}</td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>
                                            
                                        
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                               <a href="{{ url('/admin-panel/usertoko') }}"> <button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kemali</button></a>
                                            </div>
                                          </div>
                                            
                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    @elseif($data->halaman == 'password')
                    <form id="passedit" method="POST"
                        action="{{ url('/admin-panel/usertoko/editpasswordaksi', encrypt($data->form->id_kasir)) }}"
                        enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} {{ $data->form->nama_kasir }}</h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                                <a href="#">
                                                    <a href="{{ url('/admin-panel/usertoko') }}"><button
                                                            type="button" class="btn btn-icon btn-light-danger"><i
                                                                class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">

                                        <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-md-12">
                                                    <div class="col-lg-4 col-md-4 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="kode">Password Baru<small
                                                                    style="color: red;">*</small></label>
                                                            <input type="password" id="password" name="password" class="form-control"
                                                                placeholder="Password Baru" autocomplete="off">
    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-lg-4 col-md-4 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="kode">Konfirmasi Password<small
                                                                    style="color: red;">*</small></label>
                                                            <input type="password" id="password_confirm" name="password_confirm" class="form-control"
                                                                placeholder="Konfirmasi Password" autocomplete="off">
    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/usertoko') }}"><button type="button"
                                                            class="btn btn-icon btn-light-danger mr-1">
                                                            <i class="bx bx-left-arrow-circle"></i> BATAL
                                                        </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                        <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu"
                                                        class="progress-bar progress-bar-success progress-bar-striped active"
                                                        role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                                        aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </section>
        </div>
    </div>
</div>


@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              if(input.files[0].size > 2000000){
                 new Noty({
                      text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>',
                      layout: 'topRight',
                      type: 'error'
                  }).setTimeout(4000).show();
                 input.value = "";
              }else{
                  reader.onload = function (e) {
                      $('#image-img-tag').attr('src', e.target.result);
                  }
                  reader.readAsDataURL(input.files[0]);
              }
          }
      }

      $("#image-img").change(function(){
          readURL(this);
      });


      function resetForm() {
            $('#idmerek').val('');
            $('#merekadd').val('');
            $('#modal-backdrop-disable').modal('hide');
            $('#progressmerek').hide();
            $('#input-merek').removeClass( 'is-valid' );

            $('#idsatuan').val('');
            $('#satuanadd').val('');
            $('#modal-backdrop-disable-satuan').modal('hide');
            $('#progresssatuan').hide();
            $('#input-satuan').removeClass( 'is-valid' );

            $('#idjenis').val('');
            $('#jenisadd').val('');
            $('#modal-backdrop-disable-jenis').modal('hide');
            $('#progressjenis').hide();
            $('#input-jenis').removeClass( 'is-valid' );
      }

      var table,tableRiwayatReturn,tableRiwayatPembelian, status = $('#status');

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });

      $('#id_toko').select2({
          placeholder: "Pilih Toko",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });
      

      
      @if($data->halaman=='detail')
        tableRiwayatPembelian = $('#riwayat-pembelian').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "autoWidth": false, 
            "paginationType": "full_numbers",
            "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
            "iDisplayLength": 50, 
            "responsive": true,
            "ajax":{
                "url": "{{url('admin-panel/riwayat_pembelian')}}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", id: <?php echo $data->form->id ?>}
            },
            
            "language": {
                "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
            },
            order: [[2, "desc"]],
            
            columnDefs: [
                { 
                    targets: 0,
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    targets: 1,
                    width: '20px',
                    orderable: false,
                    className: "text-center"
                },
                { className: "text-left", "targets": [ 2 ] },
                { className: "text-center", "targets": [ 3 ] },
                { className: "text-center", "targets": [ 4 ] },
                { className: "text-left", "targets": [ 5 ] },
                { className: "text-left", "targets": [ 6 ] },
                { className: "text-center", "targets": [ 7 ] }
            ],
            "columns": [
                { data: null, render: function ( data, type, row ) {

                    var editUrl = "{{ url('/admin-panel/editpoterimaview', 'id_po') }}";
                    editUrl = editUrl.replace('id_po', data['id_po']);

                    var detailUrl = "{{ url('/admin-panel/view/poterima', 'id_po') }}";
                    detailUrl = detailUrl.replace('id_po', data['id_po']);

                    var VerifikasiUrl = "{{ url('/admin-panel/poterima/verifikasi', 'id_po') }}";
                    VerifikasiUrl = VerifikasiUrl.replace('id_po', data['id_po']);

                    var status = data['status_code'];
                        if(status == '4'){
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                <a href="'+ editUrl +'"><button type="button" class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Terima usertoko"><i class="bx bx-package"></i> </button></a><br>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }else if(status == '5'){
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a><br>\n\
                                <a href="'+ VerifikasiUrl +'"><button type="button" class="btn btn-icon btn-outline-info popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Verifikasi/Periksa Data"><i class="bx bx-check-double"></i> </button></a>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }else{
                            return '<div class="text">\n\
                                <a href="'+ detailUrl +'"><button type="button" class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                                <button class="btn btn-icon btn-outline-success popover-hover print_po_terima" a="'+data['id']+'" b="'+data['nomor']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Print"><i class="bx bx-printer"></i></button>\n\
                            </div>';
                        }
                            
                    
                    } 
                },
                {
                    "data": "no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data:  'nomor'  },
                { data:  'tgl_po'  },
                { data:  'tgl_terima'  },
                { data:  'supplier'  },
                { data:  'penerima'  },
                { data:  'status'  }
                ],
        });
      @endif
      @if($data->halaman=='password')
        $('#passedit').validate({
          rules: {
            password: {
                required:true,
            },
            password_confirm : {
                required:true,
                equalTo : password
            }
          },       
          errorPlacement: function(label, element)
          {
              if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

          },

      });
      @endif


      $('#input-title').validate({
          rules: {
              id_toko: {
                  required: true
              },
              nama_kasir: {
                  required: true
              },
              nomor_hp: {
                  required: true
              },
              pass: {
                  required: true
              },
              status: {
                  required: true
              }
          },       
          errorPlacement: function(label, element)
          {
              if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

          },

      });
      
      getDataTable(status.val());

      // begin first table
      function getDataTable(status) {
        table = $('#sponsor').DataTable({
          "bProcessing": true,
          "bServerSide": true,
          "autoWidth": false, 
          "paginationType": "full_numbers",
          "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
          "iDisplayLength": 50, 
          "responsive": true,
          "destroy":true,
          "ajax":{
              "url": "{{url('admin-panel/data_usertoko')}}",
              "dataType": "json",
              "type": "POST",
              "data":{ _token: "{{csrf_token()}}", status: status, date1: '', date2: ''}
          },
         
          "language": {
              "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
          },
          order: [[2, "ASC"]],
          
          columnDefs: [
              { 
                  targets: 0,
                  width: '5%',
                  orderable: false
              },
              {
                  targets: 1,
                  width: '5%',
                  orderable: false
              },
              { className: "text-left", "targets": [ 2 ] },
              { className: "text-left", "targets": [ 3 ] },
              { className: "text-center", "targets": [ 4 ] }
          ],
          "columns": [
              { data: null, render: function ( data, type, row ) {

                var editUrl = "{{ url('/admin-panel/editusertokoview', 'url') }}";
                editUrl = editUrl.replace('url', data['url']);

                var detailUrl = "{{ url('/admin-panel/view/usertoko', 'url') }}";
                detailUrl = detailUrl.replace('url', data['url']);

                var editPasswordUrl = "{{ url('/admin-panel/usertoko/edituserview', 'url') }}";
                editPasswordUrl = editPasswordUrl.replace('url', data['url']);
                
                
                return '<div class="text">\n\
                          <a href="'+ detailUrl +'"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                          <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                          <a href="' + editPasswordUrl + '"><button  class="btn btn-icon btn-outline-danger popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Ubah Password"><i class="bx bx-lock"></i> </button></a>\n\
                          <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                  </div>';
                
                } 
              },
              {
                  "data": "no",
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { data:  'nama'  },
              { data:  'namatoko'  },
              { data:  'namajenis'  },
              { data:  'nomor_hp'  },
              { data:  'status'  }
              
          ],
      });
      }
      
      $('#status').on('change', function () {
          getDataTable(status.val());
      });

      $('#status').select2({
          placeholder: "Pilih Status",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });

      $(document).on('click', '.print-laporan', function () {
            PopupCenter('{{ url('/admin-panel/statususertoko/print') }}?status=' + status.val() , 'STATUS usertoko DALAM INVENTORI' ,'1200', '800');
        });

      $(document).on('click', '.print_po', function () {
            var id = $(this).attr('a');
            PopupCenter('{{ url('/admin-panel/po/print') }}?id=' + id , 'SURAT PERMINTAAN usertoko' ,'1200', '800');
      });

      $(document).on('click', '.delete_btn', function () {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
              
                text: 'Apakah usertoko <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/deleteusertoko') }}',
                            data: ({id:id, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

      });

      $(document).on("click", ".view_photo", function (){
          var id = $(this).attr('a');
        var detailUrl = "{{ url('/admin-panel/view/toko', 'url') }}";
        detailUrl = detailUrl.replace('url', id);

        location.href = detailUrl;
          
      });
  });

    

  </script>
@endsection


