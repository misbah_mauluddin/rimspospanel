@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    {{--<h5 class="content-header-title">{{ $data->title }}</h5>--}}
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Customer</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    @if($data->halaman == 'input')
                      <form id="input-title" method="POST" action="{{ URL::Route('Tambahtoko') }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Tambah {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/toko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama_toko">Nama toko <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama_toko" name="nama_toko" class="form-control" placeholder="Nama Toko"   value="{{ old('nama_toko') }}">
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="jenis">Jenis Usaha<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="jenis" name="jenis" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->jenis)
                                                                @foreach($data->select->jenis as $jenis)
                                                                    <option value="{{ $jenis->id_jenisusaha }}" {{ old('jenis') == $jenis->id_jenisusaha ? 'selected' : '' }}>{{ $jenis->nama_jenisusaha }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nohp">No Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="nohp" name="nohp" class="form-control" placeholder="No hp"   value="{{ old('nohp') }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email </label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email"   value="{{ old('email') }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="id_harga_custom">Paket Harga<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_harga_custom" name="id_harga_custom" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->id_harga_custom)
                                                                @foreach($data->select->id_harga_custom as $id_harga_custom)
                                                                    <option value="{{ $id_harga_custom->id }}" {{ old('id_harga_custom') == $id_harga_custom->id ? 'selected' : '' }}>{{ $id_harga_custom->nama }} - {{ number_format($id_harga_custom->harga) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat <small style="color: red;">*</small></label>
                                                        <textarea class="form-control" id="alamat" name="alamat" rows="2" placeholder="Alamat ...">{{ old('alamat') }}</textarea>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                              
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Status toko ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="1" >
                                                                <label for="status1">Aktif</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="0" checked="checked">
                                                              <label for="status2">Tidak Aktif</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                                <div class="col-lg-4 col-md-4 col-xs-12" id="tgl_a" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="tgl_aktif">Tanggal Aktif <small style="color: red;">*</small></label>
                                                        <input type="text" id="tgl_aktif" name="tgl_aktif" class="form-control" placeholder="Pilih Tanggal"   >
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12" id="tgl_b" style="display: none;"> 
                                                    <div class="form-group">
                                                        <label for="tgl_berakhir">Tanggal Berakhir <small style="color: red;">*</small></label>
                                                        <input type="text" id="tgl_berakhir" name="tgl_berakhir" class="form-control" placeholder="Pilih Tanggal"   >
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12" id="status_b" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="status_byr">Status Bayar<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="status_byr" name="status_byr" style="width: 100%;">
                                                            <option></option>
                                                            <option value="0">Belum Bayar</option>
                                                            <option value="1">Lunas</option>
                                                            <option value="2">Cicil</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12" id="id_me" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="id_metodebyr">Metode Bayar<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_metodebyr" name="id_metodebyr" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->id_metodebyr)
                                                                @foreach($data->select->id_metodebyr as $id_metodebyr)
                                                                    <option value="{{ $id_metodebyr->id }}" {{ old('id_metodebyr') == $id_metodebyr->id ? 'selected' : '' }}>{{ $id_metodebyr->nama }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/toko') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                      </form>  
                    @elseif($data->halaman == 'update')
                      <form id="input-title" method="POST" action="{{ url('/admin-panel/edittokoaksi',encrypt($data->form->id_toko)) }}" enctype="multipart/form-data" style="display: contents;">
                        <div class="col-lg-9 col-md-12 col-xs-12">
                            <div class="card ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Edit {{ $data->title }} </h4>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/toko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Batal</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                        
                                          <div class="form-body">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nama_toko">Nama toko <small style="color: red;">*</small></label>
                                                        <input type="text" id="nama_toko" name="nama_toko" class="form-control" placeholder="Nama Toko"   value="{{ $data->form->nama_toko }}">
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="jenis">Jenis Usaha<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="jenis" name="jenis" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->jenis)
                                                                @foreach($data->select->jenis as $jenis)
                                                                    <option value="{{ $jenis->id_jenisusaha }}" {{ $data->form->id_jenisusaha == $jenis->id_jenisusaha ? 'selected' : '' }}>{{ $jenis->nama_jenisusaha }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="nohp">No Hp <small style="color: red;">*</small></label>
                                                        <input type="number" id="nohp" name="nohp" class="form-control" placeholder="No hp"   value="{{ $data->form->nohp }}">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="email">Email </label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email"   value="{{ $data->form->email }}">
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="id_harga_custom">Paket Harga<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_harga_custom" name="id_harga_custom" style="width: 100%;">
                                                            <option></option>
                                                            @if($data->select->id_harga_custom)
                                                                @foreach($data->select->id_harga_custom as $id_harga_custom)
                                                                    <option value="{{ $id_harga_custom->id }}" {{ $data->form->id_harga_custom == $id_harga_custom->id ? 'selected' : '' }}>{{ $id_harga_custom->nama }} - {{ number_format($id_harga_custom->harga) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-8 col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat <small style="color: red;">*</small></label>
                                                        <textarea class="form-control" id="alamat" name="alamat" rows="2" placeholder="Alamat ...">{{ $data->form->alamat }}</textarea>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 border-bottom mb-1"></div>

                                              
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                  <div class="form-group">
                                                    <label for="status">Status toko ?</label>
                                                    <ul class="list-unstyled mb-0">
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio radio-success radio-glow">
                                                                <input type="radio" id="status1" name="status" value="1" {{ $data->form->status == 1 ? 'checked' : '' }} disabled>
                                                                <label for="status1">Aktif</label>
                                                            </div>
                                                        </fieldset>
                                                      </li>
                                                      <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                          <div class="radio radio-danger radio-glow">
                                                              <input type="radio" id="status2" name="status" value="0" {{ $data->form->status == 0 ? 'checked' : '' }} disabled>
                                                              <label for="status2">Tidak Aktif</label>
                                                          </div>
                                                        </fieldset>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                                <div class="col-lg-4 col-md-4 col-xs-12" id="tgl_a">
                                                    <div class="form-group">
                                                        <label for="tgl_aktif">Tanggal Aktif <small style="color: red;">*</small></label>
                                                        <input type="text" id="tgl_aktif" name="tgl_aktif" class="form-control" placeholder="Pilih Tanggal"   value="{{$data->cektgl->tgl_aktif }}" disabled>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-xs-12" id="tgl_b">
                                                    <div class="form-group">
                                                        <label for="tgl_berakhir">Tanggal Berakhir <small style="color: red;">*</small></label>
                                                        <input type="text" id="tgl_berakhir" name="tgl_berakhir" class="form-control" placeholder="Pilih Tanggal"   value="{{ $data->cektgl->tgl_berakhir }}" disabled>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12" id="status_b">
                                                    <div class="form-group">
                                                        <label for="status_byr">Status Bayar<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="status_byr" name="status_byr" style="width: 100%;" disabled>
                                                            <option></option>
                                                            <option value="0" {{ $data->form->status_byr == "0" ? 'selected' : '' }}>Belum Bayar</option>
                                                            <option value="1" {{ $data->form->status_byr == "1" ? 'selected' : '' }}>Lunas</option>
                                                            <option value="2" {{ $data->form->status_byr == "2" ? 'selected' : '' }}>Cicil</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-xs-12" id="id_me">
                                                    <div class="form-group">
                                                        <label for="id_metodebyr">Metode Bayar<small style="color: red;">*</small></label>
                                                          <select class="select2 form-control" id="id_metodebyr" name="id_metodebyr" style="width: 100%;" disabled>
                                                            <option></option>
                                                            @if($data->select->id_metodebyr)
                                                                @foreach($data->select->id_metodebyr as $id_metodebyr)
                                                                    <option value="{{ $id_metodebyr->id }}" {{ $data->form->id_metodebyr == $id_metodebyr->id ? 'selected' : '' }}>{{ $id_metodebyr->nama }} - {{ number_format($id_harga_custom->harga) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                

                                                <div class="col-12 d-flex justify-content-end">
                                                    <a href="{{ url('/admin-panel/toko') }}"><button type="button" class="btn btn-icon btn-light-danger mr-1">
                                                      <i class="bx bx-left-arrow-circle"></i> BATAL
                                                    </button></a>
                                                    <button type="submit" class="btn btn-icon btn-light-secondary ">
                                                      <i class="bx bxs-save"></i> SIMPAN DATA
                                                    </button>
                                                </div>

                                                <div class="col-md-12" style="display: none;">
                                                    <div id="progresstitlemenu" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                                </div>  
                                            </div>
                                          </div>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12">
                            <div class="card border-secondary ">
                                <div class="card-header border-bottom ">
                                    <h4 class="card-title">Foto {{ $data->title }} </h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body mt-1 mb-2">
                                      @if($data->form->logo != "")
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset($data->form->logo)}}">
                                      @else
                                        <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                      @endif
                                      
                                      <div class="form-body">
                                        <br>
                                          <fieldset class="form-group">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="image" id="image-img" accept="image/*"/>
                                                  <label class="custom-file-label" for="image">Pilih foto</label>
                                              </div>
                                          </fieldset>
                                      </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                      </form>  
                    @elseif($data->halaman == 'data')
                      <div class="col-md-12 col-lg-12">
                          <div class="card">
                              <div class="card-header">
                                  <h4 class="card-title">Data {{ $data->title_sub }} </h4>
                                  <div class="heading-elements">
                                      <ul class="list-inline mb-0 d-flex align-items-center">
                                        <li class="ml-2">
                                          <a href="#">
                                            <a href="{{ url('/admin-panel/tambahtokoview') }}"><button type="button" class="btn btn-icon btn-outline-primary"><i class="bx bxs-add-to-queue"></i> Tambah {{ $data->title }}</button></a>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="card-body">
                                <h6>Status {{ $data->title_sub }} </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset class="form-group">
                                                        <select id="status" class="form-control" id="basicSelect">
                                                            <option value="all" selected>Semua Status</option>
                                                            <option value="0">Tidak Aktif</option>
                                                            <option value="1" >Aktif</option>
                                                            <option value="2">Proses Aktifasi</option>
                                                            <option value="3">Di Hapus</option>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    {{-- <button id="report-view" class="btn btn-info btn-icon-fixed btn-shadowed"><i class="bx bx-search"></i> Lihat Laporan</button> --}}
                                                    <button class="btn btn-icon btn-outline-success print-laporan"><i class="bx bx-printer"></i> Print</button>
                                                </div>
                                            </div>
                                  <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                          <thead class="thead-light">
                                              <tr>
                                                  <th></th>
                                                  <th align="center">NO</th>
                                                  <th align="left">NAMA</th>
                                                  <th align="left">ALAMAT</th>
                                                  <th align="left">JENIS</th>
                                                  <th align="left">MASA AKTIF</th>
                                                  <th align="center">STATUS BAYAR</th>
                                                  <th align="center">STATUS</th>
                                                  
                                              </tr>
                                          </thead>  
                                          <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                         
                                    </table>
                                  </div>
                                  
                              </div>
                          </div>
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-editakses" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-sm" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header bg-dark white">                        
                                        <span class="modal-title" id="myModalLabel150">Foto toko</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="image-show" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                    </div>
                                    
                                    
                                </div>
                            </div>            
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-Aktiftoko" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-lg" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header bg-dark white">                        
                                        <span class="modal-title" id="myModalLabel150">AktifKan Toko</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form id="aktifkantokoaksi" method="POST" action="#" enctype="multipart/form-data" style="display: contents;">
                                    <div class="modal-body">
                                        
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                        <div class="col-lg-12 border-bottom mb-1"></div>

                                                        <input type="hidden" name="id_tokoak" id="id_tokoak">      
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                          <div class="form-group">
                                                            <label for="status">Status toko ?</label>
                                                            <ul class="list-unstyled mb-0">
                                                              <li class="d-inline-block mr-2 mb-1">
                                                                <fieldset>
                                                                    <div class="radio radio-success radio-glow">
                                                                        <input type="radio" id="status1" name="status" value="1" checked="checked">
                                                                        <label for="status1">Aktif</label>
                                                                    </div>
                                                                </fieldset>
                                                              </li>
                                                              <li class="d-inline-block mr-2 mb-1">
                                                                <fieldset>
                                                                  <div class="radio radio-danger radio-glow">
                                                                      <input type="radio" id="status2" name="status" value="0" >
                                                                      <label for="status2">Tidak Aktif</label>
                                                                  </div>
                                                                </fieldset>
                                                              </li>
                                                            </ul>
                                                          </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-xs-12"></div>
                                                        <div class="col-lg-6 col-md-6 col-xs-12" id="tgl_a">
                                                            <div class="form-group">
                                                                <label for="tgl_aktif">Tanggal Aktif <small style="color: red;">*</small></label>
                                                                <input type="text" id="tgl_aktif" name="tgl_aktif" class="form-control" placeholder="Pilih Tanggal"   >
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-xs-12" id="tgl_b"> 
                                                            <div class="form-group">
                                                                <label for="tgl_berakhir">Tanggal Berakhir <small style="color: red;">*</small></label>
                                                                <input type="text" id="tgl_berakhir" name="tgl_berakhir" class="form-control" placeholder="Pilih Tanggal"   >
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 col-xs-12" id="status_b">
                                                            <div class="form-group">
                                                                <label for="status_byr">Status Bayar<small style="color: red;">*</small></label>
                                                                  <select class="select2 form-control" id="status_byr" name="status_byr" style="width: 100%;">
                                                                    <option></option>
                                                                    <option value="0">Belum Bayar</option>
                                                                    <option value="1">Lunas</option>
                                                                    <option value="2">Cicil</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6 col-xs-12" id="id_me">
                                                            <div class="form-group">
                                                                <label for="id_metodebyr">Metode Bayar<small style="color: red;">*</small></label>
                                                                  <select class="select2 form-control" id="id_metodebyr" name="id_metodebyr" style="width: 100%;">
                                                                    <option></option>
                                                                    @if($data->select->id_metodebyr)
                                                                        @foreach($data->select->id_metodebyr as $id_metodebyr)
                                                                            <option value="{{ $id_metodebyr->id }}" {{ old('id_metodebyr') == $id_metodebyr->id ? 'selected' : '' }}>{{ $id_metodebyr->nama }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="progress progress-bar-primary mb-2" style="display: none;">
                                              <div id="progresssatuan" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                            </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                          <i class="bx bx-x d-block d-sm-none"></i>
                                          <span class="d-none d-sm-block">Batal</span>
                                        </button>
                                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                                    </div>
                                    </form>
                                    
                                </div>
                            </div>            
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-NonAktiftoko" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-lg" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header bg-dark white">                        
                                        <span class="modal-title" id="myModalLabel150">Suspend/NonAktifkan Toko</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form id="nonaktifkantokoaksi" method="POST" action="#" enctype="multipart/form-data" style="display: contents;">
                                    <div class="modal-body">
                                        
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                        <div class="col-lg-12 border-bottom mb-1"></div>

                                                        <input type="hidden" name="id_tokononak" id="id_tokononak">      
                                                        
                                                        <div class="col-lg-12 col-md-12 col-xs-12"></div>
                                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="tgl_suspend">Tanggal Suspend <small style="color: red;">*</small></label>
                                                                <input type="text" id="tgl_suspend" name="tgl_suspend" class="form-control" placeholder="Pilih Tanggal"   >
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="ket">Alasan <small style="color: red;">*</small></label>
                                                                <textarea class="form-control" id="ket" name="ket" rows="2" placeholder="... ..."></textarea>
                                                                
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="progress progress-bar-primary mb-2" style="display: none;">
                                              <div id="progressnonak" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                            </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                          <i class="bx bx-x d-block d-sm-none"></i>
                                          <span class="d-none d-sm-block">Batal</span>
                                        </button>
                                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                                    </div>
                                    </form>
                                    
                                </div>
                            </div>            
                      </div>

                      <div class="modal fade" id="modal-backdrop-disable-AktifUlangtoko" aria-labelledby="myModalLabel150" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-lg" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header bg-dark white">                        
                                        <span class="modal-title" id="myModalLabel150">Suspend/NonAktifkan Toko</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form id="aktifulangkantokoaksi" method="POST" action="#" enctype="multipart/form-data" style="display: contents;">
                                    <div class="modal-body">
                                        
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                        <div class="col-lg-12 border-bottom mb-1"></div>

                                                        <input type="hidden" name="id_tokoakulang" id="id_tokoakulang">      
                                                        
                                                        <div class="col-lg-12 col-md-12 col-xs-12"></div>
                                                        
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="ket">Keterangan <small style="color: red;">*</small></label>
                                                                <textarea class="form-control" id="ket" name="ket" rows="2" placeholder="... ..."></textarea>
                                                                
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="progress progress-bar-primary mb-2" style="display: none;">
                                              <div id="progressakulang" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                                            </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                          <i class="bx bx-x d-block d-sm-none"></i>
                                          <span class="d-none d-sm-block">Batal</span>
                                        </button>
                                        <button type="submit" class="btn btn-dark ml-1" id="simpann">Simpan</button>
                                    </div>
                                    </form>
                                    
                                </div>
                            </div>            
                      </div>
                    @elseif($data->halaman == 'detail')
                      <div class="col-lg-9 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">Detail {{ $data->title }} - {{ $data->form->nama_toko }} </h4>
                                <div class="heading-elements">
                                  <ul class="list-inline mb-0 d-flex align-items-center">
                                    <li class="ml-2">
                                      <a href="#">
                                        <a href="{{ url('/admin-panel/toko') }}"><button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kembali</button></a>
                                      </a>
                                    </li>
                                  </ul>
                              </div>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs mb-2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                                            <i class="bx bx-info-circle mr-25" data-toggle="tooltip" data-placement="bottom" data-title="Detail Data toko"></i><span class="d-none d-sm-block">Detail toko</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information" aria-controls="information" role="tab" aria-selected="false">
                                            <i class="bx bx-user mr-25" data-toggle="tooltip" data-placement="bottom" data-title="User Toko"></i><span class="d-none d-sm-block">User Toko</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fade show" id="account" aria-labelledby="account-tab" role="tabpanel">
                                        <!-- users edit media object start 
                                        <div class="media mb-2">
                                            <a class="mr-2" href="javascript:void(0);">
                                                <img src="../../../app-assets/images/portrait/small/avatar-s-26.jpg" alt="users avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Avatar</h4>
                                                <div class="col-12 px-0 d-flex">
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary mr-25">Change</a>
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-light-secondary">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                        users edit media object ends -->
                                        <!-- users edit account form start -->
                                          <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <table class="table mb-0 table-hover table-striped">
                                                <thead class="thead-light">
                                                    <tr>
                                                      <th colspan="2" style="text-align: center;">Data toko</th>
                                                    </tr>
                                                </thead>
                                              </table>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;" width="40%">Nama Toko</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nama_toko}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Jenis Usaha</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->select->jenis->nama_jenisusaha}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">No Hp</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->nohp}}</td>
                                                        </tr>
                                                        <tr >
                                                            <td style="padding: 1.15rem 1rem;">Email</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->email}}</td>
                                                        </tr>
                                                        <tr >
                                                            <td style="padding: 1.15rem 1rem;">Alamat</td>
                                                            <td style="padding: 1.15rem 1rem;">{{$data->form->alamat}}</td>
                                                        </tr>
                                                        <tr >
                                                            <td style="padding: 1.15rem 1rem;">Paket Harga</td>
                                                            <td style="padding: 1.15rem 1rem;"><div class="badge badge-pill badge-glow badge-info">{{$data->select->id_harga_custom->nama}}</div> <br> Rp {{number_format($data->select->id_harga_custom->harga)}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    <tbody>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Sisa Waktu</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              <?php 

                                                                if($data->form->tgl_aktif == ""){
                                                                    $date1 = '0';
                                                                }else{
                                                                    $date1 = date("Y-m-d",strtotime($data->form->tgl_aktif));
                                                                }

                                                                if($data->form->tgl_berakhir == ""){
                                                                    $date2 = '0';
                                                                }else{
                                                                    $date2 = date("Y-m-d",strtotime($data->form->tgl_berakhir));
                                                                }


                                                                if($date1 == '0' && $date2 == '0'){
                                                                    $diffd = '0 Hari';
                                                                    $diffm = '0 Bln';
                                                                    $diff = '0 Thn';
                                                                }else{
                                                                    $date1 = date("Y-m-d");
                                                                    $date2 = $date2;

                                                                    $ts1 = strtotime($date1);
                                                                    $ts2 = strtotime($date2);

                                                                    $year1 = date('Y', $ts1);
                                                                    $year2 = date('Y', $ts2);

                                                                    $month1 = date('m', $ts1);
                                                                    $month2 = date('m', $ts2);

                                                                    $day1 = date('d', $ts1);
                                                                    $day2 = date('d', $ts2);

                                                                    $diff = (($year2 - $year1) * 12);
                                                                    $diffm = (($year2 - $year1) * 12) + ($month2 - $month1);
                                                                    $diffd = (($year2 - $year1) * 12)  + ($month2 - $month1) + ($day2 - $day1).' Hari';

                                                                    if($diffd < 0){
                                                                        $diffd = '0 Hari';
                                                                    }else{
                                                                        $diffd = (($year2 - $year1) * 12)  + ($day2 - $day1).' Hari ';
                                                                    }

                                                                    if($diffm < 0){
                                                                        $diffm = '0 Bln';
                                                                    }else{
                                                                        $diffm = (($year2 - $year1) * 12)  + ($month2 - $month1).' Bln ';
                                                                    }

                                                                    if($diff < 0){
                                                                        $diff = '0 Thn';
                                                                    }else{
                                                                        $diff = (($year2 - $year1) * 12) .' Thn ';
                                                                    }
                                                                }

                                                                echo $diffd.' '.$diffm.' '.$diff;

                                                              ?>
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Tgl Aktif</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              @if($data->form->tgl_aktif == "")
                                                                -
                                                              @else
                                                                {{date("d-m-Y", strtotime($data->form->tgl_aktif))}}
                                                              @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Tgl Berakhir</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              @if($data->form->tgl_berakhir == "")
                                                                -
                                                              @else
                                                                {{date("d-m-Y", strtotime($data->form->tgl_berakhir))}}
                                                              @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Status</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              <?php
                                                                if($data->form->status == 0){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                                                                }elseif($data->form->status == 1){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                                                                }elseif($data->form->status == 2){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Proses Aktifasi</div>';
                                                                }elseif($data->form->status == 3){
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Di Hapus</div>';
                                                                }else{
                                                                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                                                                }

                                                                echo $sst;
                                                              ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Status Bayar</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              <?php
                                                                if($data->form->status_byr == 0){
                                                                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-warning">Belum Bayar</div>';
                                                                }elseif($data->form->status_byr == 1){
                                                                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-secondary">Lunas</div>';
                                                                }elseif($data->form->status_byr == 2){
                                                                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-info">Cicil</div>';
                                                                }else{
                                                                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                                                                }

                                                                echo $sstbyr;
                                                              ?>
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="padding: 1.15rem 1rem;">Metode Bayar</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              @if($data->form->id_metodebyr == "")
                                                                
                                                              @else
                                                                {{$data->select->id_metodebyr->nama}}
                                                              @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 1.15rem 1rem;">Di Buat</td>
                                                            <td style="padding: 1.15rem 1rem;">
                                                              @if($data->form->tgl == "")
                                                                
                                                              @else
                                                                {{date("d-m-Y", strtotime($data->form->tgl))}}
                                                              @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>
                                            
                                        
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                               <a href="{{ url('/admin-panel/toko') }}"> <button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kemali</button></a>
                                            </div>
                                          </div>
                                            
                                        <!-- users edit account form ends -->
                                    </div>
                                    <div class="tab-pane fade show" id="information" aria-labelledby="information-tab" role="tabpanel">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table id="usertoko" class="table nowrap table-bordered table-hover table-striped">
                                                      <thead class="thead-light">
                                                          <tr>
                                                              <th></th>
                                                              <th align="center">NO</th>
                                                              <th align="left">NAMA KASIR</th>
                                                              <th align="left">JENIS</th>
                                                              <th align="left">NO HP</th>
                                                              <th align="center">STATUS</th>
                                                              
                                                          </tr>
                                                      </thead>  
                                                      <tbody style="white-space: nowrap;text-align: center;"></tbody>                                 
                                                     
                                                </table>
                                              </div>
                                            </div>

                                            <div class="col-lg-6 col-md-12 col-xs-12" style="padding-left: 0px;">
                                              <div class="table-responsive">
                                                <table class="table mb-0 table-hover table-striped">
                                                    
                                                </table>
                                              </div>
                                            </div>
                                            
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                               <a href="{{ url('/admin-panel/toko') }}"> <button type="button" class="btn btn-icon btn-light-danger"><i class="bx bx-left-arrow-circle"></i> Kemali</button></a>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-12 col-xs-12" style="padding-left: 0px;">
                        <div class="card border-secondary ">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">Foto {{ $data->title }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                  @if($data->form->logo != "")
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset($data->form->logo)}}">
                                  @else
                                    <img id="image-img-tag" class="img-fluid file-input" id="profile-picture" src="{{ asset('placeholder.png')}}">
                                  @endif
                                </div> 
                            </div>
                        </div>
                      </div>
                    @endif
                </div>
            </section>
        </div>
    </div>
</div>


@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              if(input.files[0].size > 2000000){
                 new Noty({
                      text: '<strong>Fails</strong> Ukuran Gambar Terlalu Besar. <b>Maximal Ukuran 2MB</b>',
                      layout: 'topRight',
                      type: 'error'
                  }).setTimeout(4000).show();
                 input.value = "";
              }else{
                  reader.onload = function (e) {
                      $('#image-img-tag').attr('src', e.target.result);
                  }
                  reader.readAsDataURL(input.files[0]);
              }
          }
      }

      $("#image-img").change(function(){
          readURL(this);
      });


      function resetForm() {
            $('#id_tokoak').val('');
            $('#tgl_aktif').val('');
            $('#tgl_berakhir').val('');
            $('#status_byr').val(null).trigger("change"); 
            $('#id_metodebyr').val(null).trigger("change"); 
            $('#modal-backdrop-Aktiftoko').modal('hide');
            $('#progresssatuan').hide();
            $('#aktifkantokoaksi').removeClass( 'is-valid' );

            $('#id_tokononak').val('');
            $('#tgl_suspend').val('');
            $('#modal-backdrop-NonAktiftoko').modal('hide');
            $('#progressnonak').hide();
            $('#nonaktifkantokoaksi').removeClass( 'is-valid' );

            $('#id_tokoakulang').val('');
            $('#tgl_suspend').val('');
            $('#modal-backdrop-AktifUlangtoko').modal('hide');
            $('#progressakulang').hide();
            $('#aktifulangkantokoaksi').removeClass( 'is-valid' );
      }

      var table,tableRiwayatReturn,tableRiwayatPembelian, status = $('#status');

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });

        $('#tgl_aktif').pickadate({
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

        $('#tgl_berakhir').pickadate({
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

        $('#tgl_suspend').pickadate({
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

      $('#jenis').select2({
          placeholder: "Pilih Jenis toko / Material",
          allowClear: true,
          "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
      });

      $('#id_harga_custom').select2({
          placeholder: "Pilih Jenis Harga",
          allowClear: true,
          "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
      });

      $('#id_metodebyr').select2({
          placeholder: "Pilih Metode Bayar",
          allowClear: true,
          "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
      });

      $('#status_byr').select2({
          placeholder: "Pilih Status Bayar",
          allowClear: true,
          "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
      });

        // $('#tgl_a').hide();
        // $('#tgl_b').hide();
        // $('#status_b').hide();
        // $('#id_me').hide();
        @if($data->halaman=='input')
            $('input:radio[name="status"]').change(function(){
                if ($(this).is(':checked') && $(this).val() == '1') {
                    $('#tgl_a').fadeIn();
                    $('#tgl_b').fadeIn();
                    $('#status_b').fadeIn();
                    $('#id_me').fadeIn();
                }else if ($(this).is(':checked') && $(this).val() == '0') {
                    $('#tgl_a').fadeOut();
                    $('#tgl_b').fadeOut();
                    $('#status_b').fadeOut();
                    $('#id_me').fadeOut();
                }
            });
        @else($data->halaman=='update')
            var ckbx = $("input[type='radio'][name='status']:checked").val();
            if (ckbx == '1') {
                $('#tgl_a').show();
                $('#tgl_b').show();
                $('#status_b').show();
                $('#id_me').show();

                $('input:radio[name="status"]').change(function(){
                    if ($(this).is(':checked') && $(this).val() == '1') {
                        $('#tgl_a').fadeIn();
                        $('#tgl_b').fadeIn();
                        $('#status_b').fadeIn();
                        $('#id_me').fadeIn();
                    }else if ($(this).is(':checked') && $(this).val() == '0') {
                        $('#tgl_a').fadeOut();
                        $('#tgl_b').fadeOut();
                        $('#status_b').fadeOut();
                        $('#id_me').fadeOut();
                    }
                });
            }else if (ckbx == '0') {
                $('#tgl_a').hide();
                $('#tgl_b').hide();
                $('#status_b').hide();
                $('#id_me').hide();

                $('input:radio[name="status"]').change(function(){
                    if ($(this).is(':checked') && $(this).val() == '1') {
                        $('#tgl_a').fadeIn();
                        $('#tgl_b').fadeIn();
                        $('#status_b').fadeIn();
                        $('#id_me').fadeIn();
                    }else if ($(this).is(':checked') && $(this).val() == '0') {
                        $('#tgl_a').fadeOut();
                        $('#tgl_b').fadeOut();
                        $('#status_b').fadeOut();
                        $('#id_me').fadeOut();
                    }
                });
            }
        @endif



      
      @if($data->halaman=='detail')
        tableRiwayatPembelian = $('#usertoko').DataTable({
          "bProcessing": true,
          "bServerSide": true,
          "autoWidth": false, 
          "paginationType": "full_numbers",
          "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
          "iDisplayLength": 50, 
          "responsive": true,
          "destroy":true,
          "ajax":{
              "url": "{{url('/admin-panel/toko/data_usertoko')}}",
              "dataType": "json",
              "type": "POST",
              "data":{ _token: "{{csrf_token()}}", id_toko: <?php echo $data->form->id_toko ?>}
          },
         
          "language": {
              "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
          },
          order: [[2, "ASC"]],
          
          columnDefs: [
              { 
                  targets: 0,
                  width: '5%',
                  orderable: false
              },
              {
                  targets: 1,
                  width: '5%',
                  orderable: false
              },
              { className: "text-left", "targets": [ 2 ] },
              { className: "text-left", "targets": [ 3 ] },
              { className: "text-center", "targets": [ 4 ] }
          ],
          "columns": [
              { data: null, render: function ( data, type, row ) {

                var editUrl = "{{ url('/admin-panel/editusertokoview', 'url') }}";
                editUrl = editUrl.replace('url', data['url']);

                var detailUrl = "{{ url('/admin-panel/view/usertoko', 'url') }}";
                detailUrl = detailUrl.replace('url', data['url']);

                var editPasswordUrl = "{{ url('/admin-panel/usertoko/edituserview', 'url') }}";
                editPasswordUrl = editPasswordUrl.replace('url', data['url']);
                
                
                return '<div class="text">\n\
                          <a href="'+ detailUrl +'"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                          <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                          <a href="' + editPasswordUrl + '"><button  class="btn btn-icon btn-outline-danger popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Ubah Password"><i class="bx bx-lock"></i> </button></a>\n\
                          <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button>\n\
                  </div>';
                
                } 
              },
              {
                  "data": "no",
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { data:  'nama'  },
              { data:  'namajenis'  },
              { data:  'nomor_hp'  },
              { data:  'status'  }
              
          ],
      });
      
      @endif

      $('#input-title').validate({
          rules: {
              nama_toko: {
                  required: true
              },
              jenis: {
                  required: true
              },
              nohp: {
                  required: true
              },
              alamat: {
                  required: true
              },
              status: {
                  required: true
              }
          },       
          errorPlacement: function(label, element)
          {
              if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

          },

      });
      
      getDataTable(status.val());

      // begin first table
      function getDataTable(status) {
        table = $('#sponsor').DataTable({
          "bProcessing": true,
          "bServerSide": true,
          "autoWidth": false, 
          "paginationType": "full_numbers",
          "aLengthMenu": [ [50, 100, 200], [50, 100, 200] ],
          "iDisplayLength": 50, 
          "responsive": true,
          "destroy":true,
          "ajax":{
              "url": "{{url('admin-panel/data_toko')}}",
              "dataType": "json",
              "type": "POST",
              "data":{ _token: "{{csrf_token()}}", status: status, date1: '', date2: ''}
          },
         
          "language": {
              "url": "{{ asset('admins/app-assets/js/datatables/language/Indonesia.json') }}"
          },
          order: [[0, "DESC"]],
          
          columnDefs: [
              { 
                  targets: 0,
                  width: '5%',
                  orderable: false
              },
              {
                  targets: 1,
                  width: '5%',
                  orderable: false
              },
              { className: "text-left", "targets": [ 2 ] },
              { className: "text-left", "targets": [ 3 ] },
              { className: "text-center", "targets": [ 4 ] }
          ],
          "columns": [
              { data: null, render: function ( data, type, row ) {

                var editUrl = "{{ url('/admin-panel/edittokoview', 'url') }}";
                editUrl = editUrl.replace('url', data['url']);

                var detailUrl = "{{ url('/admin-panel/view/toko', 'url') }}";
                detailUrl = detailUrl.replace('url', data['url']);
                
                var statusbar = data['statusbar'];
                var is_suspend = data['is_suspend'];

                if (is_suspend == '1') {
                    var btnsus = '<button class="btn btn-icon btn-outline-primary popover-hover aktifulang" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Aktifkan Kembali Toko"><i class="bx bx-broadcast"></i></button>';
                } else if (is_suspend == '0') {
                    var btnsus = '<button class="btn btn-icon btn-outline-info popover-hover aktif" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Aktifkan Toko"><i class="bx bx-check-double"></i></button>';
                }

                if (statusbar == '1') {
                        return '<div class="text">\n\
                              <a href="'+ detailUrl +'"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                              <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a><br>\n\
                              <button class="btn btn-icon btn-outline-danger popover-hover nonaktif" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Suspend Toko"><i class="bx bx-wifi-off"></i></button>\n\
                        </div>';
                } else {
                        return '<div class="text">\n\
                              <a href="'+ detailUrl +'"><button  class="btn btn-icon btn-outline-dark popover-hover detail" data-toggle="tooltip" data-placement="left" data-title="Detail Data"><i class="bx bx-file-find"></i> </button></a>\n\
                              <a href="'+ editUrl +'"><button  class="btn btn-icon btn-outline-primary popover-hover edit_modal" data-toggle="tooltip" data-placement="left" data-title="Edit"><i class="bx bx-pencil"></i> </button></a>\n\
                              <button class="btn btn-icon btn-outline-warning popover-hover delete_btn" a="'+data['id']+'" b="'+data['namabar']+'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Hapus"><i class="bx bx-trash"></i></button><br>'+btnsus+'\n\
                        </div>';
                }
                
                
                
                } 
              },
              {
                  "data": "no",
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              { data:  'nama'  },
              { data:  'alamat'  },
              { data:  'namajenis'  },
              { data:  'tgl'  },
              { data:  'status_byr'  },
              { data:  'status'  },
              
          ],
      });
      }
      
      $('#status').on('change', function () {
          getDataTable(status.val());
      });

      $('#status').select2({
          placeholder: "Pilih Status",
          allowClear: true,
          "language": {
              "noResults": function () {
                  return "Tidak ada data ditemukan";
              }
          }
      });

      

      $(document).on('click', '.delete_btn', function () {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
              
                text: 'Apakah toko <strong>' + b + '</strong> akan dihapus?',
                type: 'error',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/deletetoko') }}',
                            data: ({id:id, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'warning',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

      });

      $(document).on('click', '.aktifkembali', function () {
            var id = $(this).attr('a');
            var b = $(this).attr('b');

            var n = new Noty({
              
                text: 'Apakah toko <strong>' + b + '</strong> akan di <strong>Aktifkan</strong> kembali?',
                type: 'info',
                buttons: [
                    Noty.button('YA', 'btn btn-success glow btn-sm', function () {
                        n.close();
                        $.ajax({
                            type: 'POST',
                            url: '{{ url('admin-panel/aktiftoko') }}',
                            data: ({id:id, _token:'{{csrf_token()}}'}),
                            success: function(result){
                                if(result['status'] == 'success')
                                {
                                    new Noty({
                                        type: 'success',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();

                                    table.draw();

                                }
                                else
                                {
                                    new Noty({
                                        type: 'error',
                                        layout: 'topRight',
                                        text: result['msg'],
                                        
                                        timeout: 4000,
                                    }).show();
                                }
                            }
                        });

                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('BATAL', 'btn btn-danger glow btn-sm', function () {
                        n.close();
                    })
                ]
            }).show();

      });

      $(document).on("click", ".view_photo", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/view_image/toko")}}' + '/' + id, function (data) {

              $('#image-show').attr('src', '{{asset("/")}}' + data.data.logo);

              
              $('#modal-backdrop-disable-editakses').modal('show');

              $("#progress2").hide();
          })
      });

      $(document).on("click", ".aktif", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/aktiftoko")}}' + '/' + id, function (data) {
              
              $('#modal-backdrop-disable-Aktiftoko').modal('show');

              $("#id_tokoak").val(data.data.id_toko);
          })
      });

      $('#aktifkantokoaksi').validate({
              rules: {
                  status: {
                      required: true
                  },
                  tgl_aktif: {
                      required: true
                  },
                  tgl_berakhir: {
                      required: true
                  },
                  status_byr: {
                      required: true
                  }
              },       
              errorPlacement: function(label, element)
              {
                    if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

                    
              },
              submitHandler: function (form) {
                    var formData = new FormData(form);
                $('#progresssatuan').width('0%');
                $('#progresssatuan').show();

                $.ajax({
                    type: "POST",
                    url: "{{url('/admin-panel/aktiftokoaksi')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progresssatuan", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progresssatuan').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            getDataTable(status.val());
                            $('#modal-backdrop-disable-Aktiftoko').modal('hide');
                        }
                        else if(result['status'] == 'fail')
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progresssatuan").hide();
                        }
                    }
                });
                return false;
              }

       });

      $(document).on("click", ".nonaktif", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/nonaktiftoko")}}' + '/' + id, function (data) {
              
              $('#modal-backdrop-disable-NonAktiftoko').modal('show');

              $("#id_tokononak").val(data.data.id_toko);
          })
      });

      $('#nonaktifkantokoaksi').validate({
              rules: {
                  tgl_suspend: {
                      required: true
                  },
                  ket: {
                      required: true
                  }
              },       
              errorPlacement: function(label, element)
              {
                    if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

                    
              },
              submitHandler: function (form) {
                    var formData = new FormData(form);
                $('#progressnonak').width('0%');
                $('#progressnonak').show();

                $.ajax({
                    type: "POST",
                    url: "{{url('/admin-panel/nonaktiftokoaksi')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progressnonak", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progressnonak').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            getDataTable(status.val());
                            $('#modal-backdrop-disable-NonAktiftoko').modal('hide');
                        }
                        else if(result['status'] == 'fail')
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progressnonak").hide();
                        }
                    }
                });
                return false;
              }

        });

      $(document).on("click", ".aktifulang", function (){
          var id = $(this).attr('a');

          $.get('{{ url("/admin-panel/aktifulangtoko")}}' + '/' + id, function (data) {
              
              $('#modal-backdrop-disable-AktifUlangtoko').modal('show');

              $("#id_tokoakulang").val(data.data.id_toko);
          })
      });

      $('#aktifulangkantokoaksi').validate({
              rules: {
                  ket: {
                      required: true
                  }
              },       
              errorPlacement: function(label, element)
              {
                    if (element.is("select")) {
                        label.insertAfter(element.next());
                    } else if (element.is("input:radio")) {
                        label.insertAfter($('#status_div'));
                    } else if (element.is("input#picture_val")) {
                        label.insertAfter($('.file-input'));
                    }
                    else {
                        label.insertAfter(element)
                    }

                    
              },
              submitHandler: function (form) {
                    var formData = new FormData(form);
                $('#progressakulang').width('0%');
                $('#progressakulang').show();

                $.ajax({
                    type: "POST",
                    url: "{{url('/admin-panel/aktifulangtokoaksi')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progressakulang", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#progressakulang').width(percentComplete+'%');
                            }
                        }, false);

                        return xhr;
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 4000,
                        }).show();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            new Noty({
                                type: 'success',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            resetForm();
                            getDataTable(status.val());
                            $('#modal-backdrop-disable-AktifUlangtoko').modal('hide');
                        }
                        else if(result['status'] == 'fail')
                        {
                            new Noty({
                                type: 'error',
                                text: result['msg'],
                                timeout: 4000,
                                layout: 'topRight',
                                
                            }).show();

                            $("#progressakulang").hide();
                        }
                    }
                });
                return false;
              }

        });
  });

    

  </script>
@endsection


