<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="RIMS CORP">
    <meta name="keywords" content="RIMS CORP">
    <meta name="author" content="RIMS CORP">
    <title>PT.UND - INVENTORI | LOGIN</title>
    <link rel="apple-touch-icon" href="{{ asset('admins/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('logo_und_thin.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/themes/semi-dark-layout.css') }}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/app-assets/css/pages/authentication.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/noty.css') }}">
    <link rel="stylesheet" href="{{ asset('admins/noty/themes/bootstrap-v3.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    <script type="text/javascript">
        var base_path = "{{url('/admins')}}";
    </script>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
              <!-- forgot password start -->
              <section class="row flexbox-container">
                <div class="col-xl-7 col-md-9 col-10  px-0">
                    <div class="card bg-authentication mb-0">
                        <div class="row m-0">
                            <!-- left section-forgot password -->
                            <div class="col-md-6 col-12 px-0">
                                <div class="card disable-rounded-right mb-0 p-2">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="text-center mb-2">Lupa Password?</h4>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-muted text-center mb-2"><small>Masukkan email yang terdaftar</small></div>
                                        <form class="mb-2" action="{{ url('/kirimpassword') }}" method="post">
                                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                            <div class="form-group mb-2">
                                                <label class="text-bold-600" for="exampleInputEmailPhone1">Email</label>
                                                <input type="text" name="email" class="form-control" id="exampleInputEmailPhone1" placeholder="Email"></div>
                                                @if ($errors->first('email'))
                                                    <div class="badge badge-pill badge-glow badge-warning mr-1 mb-1">Kolom ini diperlukan</div>
                                                @endif
                                            <button type="submit" class="btn btn-primary glow position-relative w-100">KIRIM
                                                PASSWORD<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                        </form>
                                        <div class="text-center mb-2"><a href="{{ url('/') }}"><small>Ingat Password</small></a></div>
                                        <hr>
                                            <div class="text-center"><small class="mr-25">2021 &copy; PT. UJONG NEUBOK DALAM</small><br><span><small>Powered by<i class="bx bxs-heart pink mx-50 font-small-3"></i><a class="text-uppercase" href="https://rims.co.id" target="_blank">RIMS CORP</a></small></span></div>
                                    </div>
                                </div>
                            </div>
                            <!-- right section image -->
                            <div class="col-md-6 d-md-block d-none text-center align-self-center">
                                <img class="img-fluid" src="{{ asset('admins/app-assets/images/pages/forgot-password.png') }}" alt="branding logo" width="300">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- forgot password ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <!-- END: Page JS-->

    <script type="text/javascript">
        Noty.overrideDefaults({
                layout: 'topRight',
                theme: 'bootstrap-v3',
                animation: {
                    open: 'animated fadeInRight',
                    close: 'animated fadeOutRight'
                },
            timeout: 4000
                
            });

        function PopupCenter(url, title, w, h) {  
            // Fixes dual-screen position                         Most browsers      Firefox  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
                      
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
                      
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
            var top = ((height / 2) - (h / 2)) + dualScreenTop;  
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
          
            // Puts focus on the newWindow  
            if (window.focus) {  
                newWindow.focus();  
            }  
        }  
    </script>

    @if(Session::has('success'))
        <script type="text/javascript">
            new Noty({
                text: '<strong>Success</strong> {{Session::get('success')}}',
                layout: 'topRight',
                type: 'success',
                theme: 'success'
            }).setTimeout(4000).show();
        </script>
    @endif
    @if(Session::has('fail'))
        <script type="text/javascript">
            new Noty({
                text: '<strong>Fails</strong> {{Session::get('fail')}}',
                layout: 'topRight',
                type: 'warning'
            }).setTimeout(4000).show();
        </script>
    @endif
</body>
<!-- END: Body-->

</html>