@extends('layout.home')
@section('content')
    {!! Menu::render() !!}

    
@endsection
@section('js-scripts')

<!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->

    {!! Menu::scripts() !!}

@endsection