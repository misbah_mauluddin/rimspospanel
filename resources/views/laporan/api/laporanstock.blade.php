@extends('laporan.api.style')
@section('content')
    @php
        $colspan = 6;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.api.kop')

        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="{{ $colspan }}">
                <hr>
            </td>
        </tr>
        <tr class="heading">
            <td width="2%" rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="30%" rowspan="2" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            <td width="30%" rowspan="2" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">KATEGORI</td>
            <td width="30%" rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">QTY</td>
            <td width="30%" colspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA TERBARU</td>
        </tr>
        <tr class="heading">
            <td width="30%" style="text-align: right;vertical-align: middle;background: #334868;color: #FFF;">MODAL</td>
            <td width="30%" style="text-align: right;vertical-align: middle;background: #334868;color: #FFF;">JUAL</td>
        </tr>

        @php
            $no             = 1;
            $qtySum       = 0;
        @endphp

        @if(count($data->hasil->barang) > 0)
            @foreach($data->hasil->barang as $dtl)

                @php
                    $stock = DB::table('tbl_barang_stok')->where('id_barang',$dtl->id_barang)->first();
                    if($stock){
                        $qty = $stock->qty;
                        $modal = DB::table('tbl_hist_stok')->select('modal','harga_jual')
                                ->where('id_barang',$stock->id_barang)
                                ->orderBy('tanggal','DESC')
                                ->first();
                        if($modal){
                            if($dtl->id_kategori == 1){
                                $hrgmodal = $modal->modal;
                                $hrgharga_jual = $modal->harga_jual;
                            }else{
                                $hrgmodal = $modal->modal;
                                $hrgharga_jual = $modal->harga_satuan;
                            }
                            
                        }else{
                            $hrgmodal = "0";
                            $hrgharga_jual = "0";
                        }
                    }else{
                        $qty = "0";
                        $hrgmodal = "0";
                        $hrgharga_jual = "0";
                    }


                @endphp
               <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $dtl->nama_br }}</td>
                    <td align="left">{{ $dtl->nama_kategori }}</td>
                    <td align="center">{{ $qty }}</td>
                    <td align="right"> {{number_format($hrgmodal)}} </td>
                    <td align="right">{{ number_format($hrgharga_jual) }}</td>
                    
                </tr> 

                @php
                $qtySum += $qty;
                 @endphp
            @endforeach
            <tr class='total'>
                <td colspan='3' style="text-align: right;"><strong>TOTAL STOCK</strong></td>
                <td colspan='3' style="text-align: left;"><strong>{{$qtySum}}</strong></td>
            </tr>";
        @endif
        
        
           

    </table>
</main>
@endsection