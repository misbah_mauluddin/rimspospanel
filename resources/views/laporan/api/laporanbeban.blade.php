@extends('laporan.api.style')
@section('content')
    @php
        $colspan = 5;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.api.kop')

        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="{{ $colspan }}">
                <hr>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="{{ $colspan }}" style="text-align: left;vertical-align: middle;">TANGGAL: {{$data->filter->date}}</td>
        </tr>
        <tr class="heading">
            <td width="2%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="30%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TANGGAL</td>
            <td width="30%" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">KATEGORI</td>
            <td width="30%" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">KETERANGAN</td>
            <td width="30%" style="text-align: right;vertical-align: middle;background: #334868;color: #FFF;">NOMINAL</td>
        </tr>

        @php
            $no             = 1;
            $jmlhSum       = 0;
        @endphp

        @if(count($data->hasil->beban) > 0)
            @foreach($data->hasil->beban as $dtl)

               <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="center">{{ date("d-m-Y",strtotime($dtl->tgl)) }}</td>
                    <td align="left"> {{$dtl->kategori}} </td>
                    <td align="left"> {{$dtl->keterangan}} </td>
                    <td align="right">{{ number_format($dtl->jumlah) }}</td>
                    
                </tr> 

                @php
                $jmlhSum += $dtl->jumlah;
                 @endphp
            @endforeach
            <tr class='total'>
                <td colspan='4' style="text-align: right;"><strong>TOTAL</strong></td>
                <td style="text-align: right;"><strong>{{number_format($jmlhSum)}}</strong></td>
            </tr>";
        @endif
        
        
           

    </table>
</main>
@endsection