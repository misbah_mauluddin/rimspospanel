@extends('laporan.api.style')
@section('content')
    @php
        $colspan = 3;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.api.kop')

        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="{{ $colspan }}">
                <hr>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="{{ $colspan }}" style="text-align: left;vertical-align: middle;">TANGGAL: {{$data->filter->date}}</td>
        </tr>
        <tr class="heading">
            <td width="2%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="30%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KETERANGAN</td>
            <td width="30%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">AMOUNT</td>
        </tr>

        @php
            if($data->hasil->penjualan){
                $penjualan = $data->hasil->penjualan->total;
            }else{
                $penjualan = '0';
            }

            if($data->hasil->penjualan_trf){
                $penjualan_trf = $data->hasil->penjualan_trf->total;
            }else{
                $penjualan_trf = '0';
            }

            if($data->hasil->beban){
                $beban = $data->hasil->beban->jumlah;
            }else{
                $beban = '0';
            }

            

            if($data->hasil->modal){
                $modal = $data->hasil->modal->modal;
            }else{
                $modal = '0';
            }

            if($data->hasil->laba){
                $laba = $data->hasil->laba;
            }else{
                $laba = '0';
            }
        @endphp
        <tr class="item">
            <td align="center">1</td>
            <td align="left">Penjualan Cash</td>
            <td align="right">{{number_format($penjualan)}}</td>
        </tr>
        <tr class="item">
            <td align="center">2</td>
            <td align="left">Penjualan Transfer</td>
            <td align="right">{{number_format($penjualan_trf)}}</td>
        </tr>
        <tr class="item">
            <td align="left" colspan="2">Penjualan Total</td>
            <td style="text-align: right;">{{number_format($penjualan+$penjualan_trf)}}</td>
        </tr>

        <tr class="item">
            <td align="center">3</td>
            <td align="left">Beban</td>
            <td align="right">{{number_format($beban)}}</td>
        </tr>
        <tr class="item">
            <td align="left" colspan="2">Beban Total</td>
            <td style="text-align: right;">{{number_format($beban)}}</td>
        </tr>

        <tr class="item">
            <td align="left" colspan="2">Pendapatan Total</td>
            <td style="text-align: right;">{{number_format(($penjualan+$penjualan_trf)-$beban)}}</td>
        </tr>

        

        <tr class="item">
            <td align="center">4</td>
            <td align="left">Modal</td>
            <td align="right">{{number_format($modal)}}</td>
        </tr>

        <tr class="item">
            <td align="left" colspan="2">Laba/Rugi Total</td>
            <td style="text-align: right;">{{number_format($laba)}}</td>
        </tr>
           

    </table>
</main>
@endsection