@extends('laporan.api.style')
@section('content')
    @php
        $colspan = 6;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.api.kop')

        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="{{ $colspan }}">
                <hr>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="{{ $colspan }}" style="text-align: left;vertical-align: middle;">TANGGAL: {{$data->filter->date}}</td>
        </tr>
        <tr class="heading">
            <td width="2%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="30%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL TRANSAKSI</td>
            <td width="30%" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">KASIR</td>
            <td width="30%" style="text-align: left;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            <td width="30%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">QTY</td>
            <td width="30%" style="text-align: right;vertical-align: middle;background: #334868;color: #FFF;">TOTAL</td>
        </tr>

        @php
            $no             = 1;
            $jmlhSum       = 0;
            $qtySum       = 0;
        @endphp

        @if(count($data->hasil->penjualan) > 0)
            @foreach($data->hasil->penjualan as $dtl)

                @php
                    $getbrg = DB::table('tbl_barang')->select('nama_br')->where('id_barang',$dtl->id_barang)->first();
                    $getksr = DB::table('tbl_kasir')->select('nama_kasir')->where('id_kasir',$dtl->id_kasir)->first();
                @endphp

               <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ date("d-m-Y H:i",strtotime($dtl->tgl_penjualan)) }}</td>
                    <td align="left">{{ $getksr->nama_kasir }}</td>
                    <td align="left">{{ $getbrg->nama_br }}</td>
                    <td align="center"> {{$dtl->qty}} </td>
                    <td align="right">{{ number_format($dtl->total) }}</td>
                    
                </tr> 

                @php
                $jmlhSum += $dtl->total;
                $qtySum += $dtl->qty;
                 @endphp
            @endforeach
            <tr class='total'>
                <td colspan='4' style="text-align: right;"><strong>TOTAL</strong></td>
                <td style="text-align: center;"><strong>{{$qtySum}}</strong></td>
                <td style="text-align: right;"><strong>{{number_format($jmlhSum)}}</strong></td>
            </tr>";
        @endif
        
        
           

    </table>
</main>
@endsection