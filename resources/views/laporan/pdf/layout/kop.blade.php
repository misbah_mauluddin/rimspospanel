<tr class="top">
  <td colspan="{{ $colspan }}">
      <table width="100%" style="border: 1px solid #000000">
          <tr>
              <td rowspan="4" width="80px" style="text-align:center;vertical-align: middle;"><img style="display: block;margin:-32px;" width="100%" src="{{ asset($data->filter->aplikasi->image)}}"/></td>
              <td style="text-align:left;">{{ $data->filter->aplikasi->nama_perusahaan }}</td>
          </tr>
          <tr>
              <td style="text-align:left;">{{ $data->filter->aplikasi->nama_lainnya }}</td>
          </tr>
          <tr>
              <td style="text-align:left;">{{ $data->filter->aplikasi->alamat }}</td>
          </tr>
          <tr>
              <td style="text-align:left;">{{ $data->filter->aplikasi->email }}</td>
          </tr>
      </table>
  </td>
</tr>