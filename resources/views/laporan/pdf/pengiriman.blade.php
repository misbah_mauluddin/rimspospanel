@extends('laporan.pdf.layout.style')
@section('content')
    @php
        $colspan = 8;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.pdf.layout.kop')
        
        <tr class="top">
            @if($data->report->buat_di == 1)
            <td colspan="4">
                <table width="100%">
                    
                    <tr>
                        <td width="25%" style="text-align:left;">Kepada Yth</td>
                        <td>:</td>
                        <td width="80%" style="text-align:left;">{{ $data->filter->supplier->nama }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">Alamat</td>
                        <td>:</td>
                        <td width="90%" style="text-align:left;" >{{ $data->filter->supplier->alamat }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">Telepon</td>
                        <td>:</td>
                        <td style="text-align:left;">{{ $data->filter->supplier->hp }}</td>
                    </tr>
                </table>
            </td>
            @endif
            <td colspan="4">
                <table width="100%">
                    <tr>
                        <td width="40%" style="text-align:left;">No Pengiriman </td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->nomor_pengiriman }}</td>
                    </tr>
                    <tr>
                        <td width="40%" style="text-align:left;">Tanggal Pengiriman</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ date("m-d-Y",strtotime($data->report->tgl_pengiriman)) }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="information">
            <td colspan="8">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="8" style="text-align: left;vertical-align: middle;">Bberikut ini kami lampirkan rincian Perbaikan material yang dikirim diantaranya :</td>
        </tr>
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="20%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td>
            
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JMLH</td>
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA SATUAN</td>
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUB TOTAL</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KET</td>
        </tr>

        @php
            $no             = 1;
            $totalSum       = 0;
            $jmlhSum       = 0;
            $hrgastnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                $spbdetail = DB::table('barang_kirim_detail as a')
                    ->select('a.*')
                    ->where('a.id_barang_kirim',$data->report->id)
                    ->get();
                $pembuat = DB::table('user as a')
                ->select('a.*')
                ->where('a.id',$data->report->pembuat_id)
                ->first();
             ?>
            @foreach($spbdetail as $item)
                    <?php 

                    $brg = DB::table('barang')->where('id',$item->id_barang)->first();
                    if($brg){
                        $nmbrg = $brg->nama;
                    }else{
                        $nmbrg = "-";
                    }

                    $stn = DB::table('satuan')->where('id',$brg->id_satuan)->first();
                    if($stn){
                        $nmstn = $stn->nama;
                    }else{
                        $nmstn = "-";
                    }

                    $mrk = DB::table('merek')->where('id',$brg->id_merek)->first();
                    if($mrk){
                        $nmmrk = $mrk->nama;
                    }else{
                        $nmmrk = "-";
                    }

                    if($item->harga_satuan == ''){
                        $hrgst = "-";
                    }else{
                        $hrgst = number_format($item->harga_satuan);
                    }
                    if($item->sub_total == ''){
                        $sbtl = "-";
                    }else{
                        $sbtl = number_format($item->sub_total);
                    }

                    if($item->qty == ''){
                        $jmlh = 0;
                    }else{
                        $jmlh = $item->qty;
                    }

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $nmbrg }}</td>
                    <td align="left"> {{$nmmrk}} </td>
                    <td align="left">{{ $nmstn }}</td>
                    
                    <td align="center">{{ $jmlh }}</td>
                    <td align="right">Rp. <?php echo $hrgst; ?></td>
                    <td align="right">Rp. <?php echo $sbtl ; ?></td>
                    <td align="left">{{ $item->ket }}</td>
                    
                </tr>

                @php


                @endphp
            @endforeach

           <?php 

            if($data->report->ppn == ''){
                $ppnisi = 0;
            }else{
                $ppnisi = $data->report->ppn;
            }

            if($data->report->diskon == ''){
                $diskonisi = 0;
            }else{
                $diskonisi = $data->report->diskon;
            }

            $ppntotal = ($ppnisi/100)*$data->report->total;

             echo "<tr class='total'>
                        <td colspan='5' style='border:0px;'>NOTE :</td>
                        <td align='right'><strong>TOTAL</strong></td>
                        <td align='right' style='border-left:0px;'><strong>Rp.  " . number_format($data->report->total) . "</strong></td>
                        <td style='border:0px;'></td>
                    </tr>
                    <tr class='total'>
                        <td colspan='5' style='border:0px;'>". $data->report->ket ."</td>
                        <td align='right'><strong>PPN " . number_format($data->report->ppn) . "%</strong></td>
                        <td align='right' style='border-left:0px;'><strong>Rp.  " . number_format($ppntotal) . "</strong></td>
                        <td style='border:0px;'></td>
                    </tr>
                    <tr class='total'>
                        <td colspan='5' style='border:0px;'></td>
                        <td align='right'><strong>Diskon</strong></td>
                        <td align='right' style='border-left:0px;'><strong>Rp.  " . number_format($diskonisi) . "</strong></td>
                        <td style='border:0px;'></td>
                    </tr>
                    <tr class='total'>
                        <td colspan='5' style='border:0px;'></td>
                        <td align='right'><strong>Grand Total</strong></td>
                        <td align='right' style='border-left:0px;''><strong>Rp.  " . number_format($data->report->grand_total) . "</strong></td>
                        <td style='border:0px;'></td>
                    </tr>";
            
            ?>

        @endif
        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
            <div class="table-responsive">
              <table class="table mb-0 table-hover table-striped">
                  <thead class="thead-dark">
                      <tr>
                          <th colspan='3' style="font-size: 11px;text-align: center;">Dibuat Oleh :</th>
                          <th style="font-size: 11px;text-align: center;">Disetujui Oleh :</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td colspan='3' style="font-size: 11px;text-align: center;"><br><br>(<span style="text-decoration: underline;"> {{$pembuat->name}} </span>)<br> Adm. Gudang</td>
                          <td style="font-size: 11px;text-align: center;"><br><br>(<span style="text-decoration: underline;"> {{$data->report->penyetuju_nama}} </span>)<br> GM PT.UND</td>
                      </tr>
                  </tbody>
              </table>
            </div>
          </div>
    </table>
</main>
@endsection