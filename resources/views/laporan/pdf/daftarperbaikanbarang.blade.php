@extends('laporan.pdf.layout.style')
@section('content')

@php
    $colspan = 8;

    // if ($data->type_bayar == "SEMUA TIPE PEMBAYARAN") {
    //     $colspan++;
    // }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Nama Barang</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->nama_barang }}</td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif

                </table>
            </td>
        </tr>
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA SATUAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUB TOTAL</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KET</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR KIRIM</td>
        </tr>

        @php
            $no             = 1;
            $totalSum       = 0;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
            <tr class="item">
                <td align="center">{{ $no++ }}</td>
                <td align="left">{{ $item['barang'] }}</td>
                <td align="center">{{ $item['merek'] }}</td>
                <td align="center"> {{ $item['jmlh'] }} </td>
                <td align="right">{{ $item['harga_satuan'] }}</td>
                <td align="right">{{ $item['sub_total'] }}</td>
                <td align="left">{{ $item['ket'] }}</td>
                <td align="left">{{ $item['nomor'] }}</td>
            </tr>
            @endforeach
                   
           <?php 
            //  echo "<tr class='total'>
            //             <td colspan='".($colspan-1)."' align='right'><strong>TOTAL " . $data->status . "</strong></td>
            //             <td align='left' style='border-right:0px;'>Rp.</strong></td>
            //             <td align='right' style='border-left:0px;'><strong>" .number_format($totalSum) . "</strong></td>
            //         </tr>";
            
            ?>

        @endif
    </table>
</main>
@endsection