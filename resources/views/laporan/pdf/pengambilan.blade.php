@extends('laporan.pdf.layout.style')
@section('content')
    @php
        $colspan = 6;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.pdf.layout.kop')
        
        <tr class="top">
            <td colspan="{{ $colspan }}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Nomor Pengambilan</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->nomor_pengambilan }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal Pengambilan</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ date("m-d-Y",strtotime($data->report->tgl_pengambilan)) }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Devisi</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->devisi_nama }} / {{ $data->report->peminta_nama }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Di Buat Oleh</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->filter->admin->name }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="20%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
            {{-- <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td> --}}
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH</td>
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KET</td>
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SISA STOK</td>
        </tr>

        @php
            $no             = 1;
            $totalSum       = 0;
            $jmlhSum       = 0;
            $hrgastnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                $spbdetail = DB::table('barang_pengambilan_detail as a')
                    ->select('a.*','b.id as idbrg','b.nama')
                    ->leftJoin('barang as b','a.id_barang','=','b.id')
                    ->where('a.id_pengambilan',$data->report->id)
                    ->get();

                
             ?>
            @foreach($spbdetail as $item)
                    <?php 

                    $brg = DB::table('barang')->where('id',$item->idbrg)->first();

                    $stn = DB::table('satuan')->where('id',$brg->id_satuan)->first();
                    if($stn){
                        $nmstn = $stn->nama;
                    }else{
                        $nmstn = "-";
                    }


                    $mrk = DB::table('merek')->where('id',$brg->id_merek)->first();
                    if($mrk){
                        $nmmrk = $mrk->nama;
                    }else{
                        $nmmrk = "-";
                    }

                    if($item->qty == ''){
                        $jmlh = 0;
                    }else{
                        $jmlh = $item->qty;
                    }

                    $cekstok = DB::table('barang_stok')->where('id_barang',$item->idbrg)->first();
                    if($cekstok){
                        $stok = $cekstok->stok;
                    }else{
                        $stok = "-";
                    }

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $item->nama }}</td>
                    {{-- <td align="left">{{ $nmstn }}</td> --}}
                    <td align="left"> {{$nmmrk}} </td>
                    
                    <td align="center">{{ $jmlh. ' ' .$nmstn}}</td>
                    <td align="left">{{ $item->ket }}</td>
                    <td align="center">{{ $stok. ' ' .$nmstn }}</td>
                    
                </tr>

                @php


                @endphp
            @endforeach

           

        @endif
       
    </table>
</main>
@endsection