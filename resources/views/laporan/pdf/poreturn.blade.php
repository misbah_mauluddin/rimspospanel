@extends('laporan.pdf.layout.style')
@section('content')
    @php
        $colspan = 7;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.pdf.layout.kop')
        
        <tr class="information">
            <td colspan="7">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <hr>
            </td>
        </tr>
        <tr class="top">
            <td colspan="4">
                <table width="100%">
                    <tr>
                        <td width="25%" style="text-align:left;">Supplier</td>
                        <td>:</td>
                        <td width="80%" style="text-align:left;">{{ $data->filter->supplier->nama }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;line-height: 10px;">Alamat</td>
                        <td style="text-align:left;line-height: 10px;">:</td>
                        <td width="90%" style="text-align:left;line-height: 10px;" >{{ $data->filter->supplier->alamat }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">Telepon</td>
                        <td>:</td>
                        <td style="text-align:left;">{{ $data->filter->supplier->hp }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">Kontak</td>
                        <td>:</td>
                        <td style="text-align:left;">{{ $data->filter->supplier->kontak_person }}</td>
                    </tr>
                </table>
            </td>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Po No</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->nomor_po }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal Po</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ date("d-m-Y",strtotime($data->report->tgl_po)) }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Referensi</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->reff }}</td>
                    </tr>
                </table>
            </td>
            
        </tr>
        <tr>
            <td colspan="7">
                <hr>
            </td>
        </tr>
        <tr class="top">
            
            <td colspan="7">
                <table width="100%">

                    <?php

                        $gettgl = DB::table('po_return_detail_satuan')->where('id_po_return',$data->filter->poterimadata->id)->orderBy('id','DESC')->first();
                    ?>
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal Return</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ date("d-m-Y",strtotime($gettgl->tgl_return_satuan)) }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Penerima Barang</td>
                        <td>:</td>
                        <?php
                            $userpenerima = DB::table('user')->where('id',$data->filter->poterimadata->penerima_id)->first();
                        ?>
                        <td width="70%" style="text-align:left;">{{ $userpenerima->name }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Keterangan</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->filter->poterimadata->ket }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr class="heading">
            <td width="2%" rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td width="20%" rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH & SATUAN</td>
            <td width="15%" colspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA</td>
            
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">RETURN</td>
        </tr>
        <tr class="heading">
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td>
            <td width="15%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUB TOTAL</td>
        </tr>

        @php
            $no             = 1;
            
            $jmlhSum       = 0;
            $subtotalSUm       = 0;
            $terimaSum       = 0;
            $returnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                $podetail = DB::table('po_return_detail as a')
                            ->select('a.*','b.id_po')
                            ->leftJoin('po_return as b','a.id_po_return','=','b.id')
                            ->where('b.id_po',$data->report->id)
                            ->get();

             ?>
            @foreach($podetail as $item)
                    <?php 

                    $brg = DB::table('barang')->where('id',$item->id_barang)->first();
                    if($brg){
                        $nmbrg = $brg->nama;
                    }else{
                        $nmbrg = "-";
                    }

                    $getpodet = DB::table('po_detail')
                                ->where('id_po',$item->id_po)
                                ->where('id_barang',$item->id_barang)
                                ->first();

                    $stn = DB::table('satuan')->where('id',$getpodet->id_satuan)->first();
                    if($stn){
                        $nmstn = $stn->nama;
                    }else{
                        $nmstn = "-";
                    }

                    $mrk = DB::table('merek')->where('id',$getpodet->id_merek)->first();
                    if($mrk){
                        $nmmrk = $mrk->nama;
                    }else{
                        $nmmrk = "-";
                    }

                    if($getpodet->harga_satuan == ''){
                        $hrgst = "-";
                    }else{
                        $hrgst = number_format($getpodet->harga_satuan);
                    }
                    if($getpodet->sub_total == ''){
                        $sbtl = "-";
                    }else{
                        $sbtl = $getpodet->sub_total;
                    }

                    if($getpodet->qty == ''){
                        $jmlh = 0;
                    }else{
                        $jmlh = $getpodet->qty;
                    }

                                        
                    if($item->jmlh_brg == ''){
                        $jmlh_brg = 0;
                    }else{
                        $jmlh_brg = $item->jmlh_brg;
                    }
                    
                    

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $nmbrg }}</td>
                    <td align="left">{{ $nmmrk }} </td>
                    <td align="center">{{ $jmlh.' '.$nmstn }}</td>

                    <td align="right">Rp. <?php echo $hrgst; ?></td>
                    <td align="right">Rp. <?php echo number_format($sbtl) ; ?></td>
                    <td align="center">{{ number_format($jmlh_brg) }}</td>
                    {{--<td align="center">{{ number_format($return_brg) }}</td>--}}
                    
                </tr>

                @php

                    $jmlhSum += $jmlh;
                    $subtotalSUm += $sbtl;
                    $returnSum       += $jmlh_brg;
                @endphp
            @endforeach

           <tr class="total">
                <td colspan="3" style="text-align: right;"><strong>TOTAL</strong></td>
                <td style="text-align: center;"><strong>{{ number_format($jmlhSum) }}</strong></td>
                <td colspan="2" style="text-align: right;"><strong>Rp.  {{ number_format($subtotalSUm) }}</strong></td>
                <td style="text-align: center;"><strong>{{ number_format($returnSum) }}</strong></td>
            </tr>"

        @endif
    </table>
</main>
@endsection