@extends('laporan.pdf.layout.style')
@section('content')

@php
    // if ($data->status == "SEMUA STATUS") {
        $colspan = 7;
    // }else {
    //     $colspan = 7;
    // }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        {{-- <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Status PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;"></td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;"></td>
                    </tr>
                    @endif
                </table>
            </td>
        </tr> --}}
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR TERIMA PERBAIKAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL TERIMA</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">PERBAIKAN DI</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">PENERIMA</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
        </tr>

        @php
            $no = 1;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
                    <?php 
                    if($item->status == 0){
                        $statuss = "VERIFIKASI";
                    }elseif($item->status == 1){
                         $statuss = "DISETUJUI (PROSES KIRIM)";
                    }elseif($item->status == 2){
                         $statuss = "SELESAI";
                    }elseif($item->status == 3){
                         $statuss = "DITOLAK";
                    }elseif($item->status == 4){
                         $statuss = "PROSES TERIMA BARANG";
                    }elseif($item->status == 5){
                         $statuss = "VERIFIKASI TERIMA BARANG";
                    }

                    if($item->buat_di == 0){
                        $buatdi = "Internal";
                    }elseif($item->buat_di == 1){
                         $buatdi = "External";
                    }

                    $getadmin = DB::table('user as a')
                        ->select('a.name')
                        ->where('a.id',$item->penerima_id)
                        ->first();
                    if($getadmin){
                        $namadmin = $getadmin->name;
                    }else{
                        $namadmin = "-";
                    }

                    $getsupplier = DB::table('supplier as a')
                        ->select('a.nama')
                        ->where('a.id',$item->id_supplier)
                        ->first();
                    if($getadmin){
                        $namasupplier = $getsupplier->nama;
                    }else{
                        $namasupplier = "-";
                    }

                    if($item->jmlh == ""){
                        $jmlh = "0";
                    }else{
                        $jmlh = $item->jmlh;
                    }

                    if($item->tgl_terima == ""){
                        $tgl_terima = "-";
                    }else{
                        $tgl_terima = date("d-m-Y/H:i",strtotime($item->tgl_terima));
                    }

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="center">{{ $item->no_terima }}</td>
                    <td align="center">{{ $tgl_terima }}</td>
                    <td align="center">{{ $jmlh }}</td>
                    <td align="center">{{ $buatdi }} ({{ $namasupplier}})</small></td>
                    <td align="center">{{ $namadmin }}</td>
                    <td align="center"> {{ $statuss }} </td>
                </tr>

            @endforeach

        @endif
    </table>
</main>
@endsection