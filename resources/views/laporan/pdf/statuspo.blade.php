@extends('laporan.pdf.layout.style')
@section('content')

@php
    $colspan = 7;

    if ($data->status == "SEMUA STATUS") {
        $colspan++;
    }
    if(!$data->jenis_po){
        $colspan++;
    }
    // if ($data->type_bayar == "SEMUA TIPE PEMBAYARAN") {
    //     $colspan++;
    // }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Status PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->status }}</td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif
                    {{-- <tr>
                        <td width="30%" style="text-align:left;">Tipe Pembayaran</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->type_bayar }}</td>
                    </tr> --}}
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td rowspan="2" width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            @if (!$data->jenis_po)
                <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JENIS PO</td> 
            @endif
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR PO</td>
            <td colspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TANGGAL</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUPLIER</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DIBUAT OLEH</td>
            {{-- @if ($data->type_bayar == 'SEMUA TIPE PEMBAYARAN')
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TIPE BAYAR</td>
            @endif --}}
            @if ($data->status == 'SEMUA STATUS')
                <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
            @endif
            <td rowspan="2" colspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TOTAL BAYAR</td>
        </tr>
        <tr class="heading">
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL PO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL TERIMA</td>
        </tr>

        @php
            $no             = 1;
            $totalSum       = 0;
            // $jmlhSum       = 0;
            // $hrgastnSum       = 0;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
                    <?php 
                    // if($item->status == 0){
                    //     $statuss = "VERIFIKASI GM";
                    // }elseif($item->status == 1){
                    //      $statuss = "DISETUJUI (ON PROSES PO)";
                    // }elseif($item->status == 2){
                    //      $statuss = "SELESAI";
                    // }elseif($item->status == 3){
                    //      $statuss = "DITOLAK";
                    // }elseif($item->status == 4){
                    //      $statuss = "PROSES TERIMA BARANG";
                    // }elseif($item->status == 5){
                    //      $statuss = "VERIFIKASI TERIMA BARANG OLEH GM";
                    // }

                    // if($item->type_bayar == 0){
                    //     $typebayar = "CASH";
                    // }elseif($item->type_bayar == 1){
                    //      $typebayar = "CICIL";
                    // }

                    // $getadmin = Admin::where('id',$item->pembuat_id)->first();
                    $getadmin = DB::table('user as a')
                    ->select('a.name')
                    ->where('a.id',$item->pembuat_id)
                    ->first();
                    if($getadmin){
                        $namadmin = $getadmin->name;
                    }else{
                        $namadmin = "-";
                    }
                    ?>

                @php
                    if($item->grand_total == ''){
                        $subttl = "Rp. 0";
                    }else{
                        $subttl = "".number_format($item->grand_total);
                    }

                    if($item->tgl_terima == ''){
                        $tgl_terima = '-';
                    }else{
                        $tgl_terima = date("d-m-Y/H:i",strtotime($item->tgl_terima));
                    }

                    if($item->tgl_po == ''){
                        $tgl_po = '-';
                    }else{
                        $tgl_po = date("d-m-Y/H:i",strtotime($item->tgl_po));
                    }

                    if($item->jenis_po == 0){
                        $jnspo  = 'Pembelian(PO) Barang';                                                     
                        if($item->status == 0){
                            $sst  = 'Verifikasi';
                        }elseif($item->status == 1){
                            $sst  = 'Di Setujui (PROSES PO)';
                        }elseif($item->status == 2){
                            $sst  = 'Selesai';
                        }elseif($item->status == 3){
                            $sst  = 'Di Tolak';
                        }elseif($item->status == 4){
                            $sst  = 'Proses Terima Barang';
                        }elseif($item->status == 5){
                            $sst  = 'Verifikasi Terima Barang(GM)';
                        }
                    }elseif($item->jenis_po == 1){
                        $jnspo  = 'Perbaikan Barang';   
                        if($item->status == 0){
                            $sst  = 'Verifikasi';
                        }elseif($item->status == 1){
                            $sst  = 'Di Setujui (PROSES KIRIM BARANG)';
                        }elseif($item->status == 2){
                            $sst  = 'Selesai';
                        }elseif($item->status == 3){
                            $sst  = 'Di Tolak';
                        }elseif($item->status == 4){
                            $sst  = 'Proses Terima Barang';
                        }elseif($item->status == 5){
                            $sst  = 'Verifikasi Terima Barang(GM)';
                        }
                    }
                @endphp

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    @if (!$data->jenis_po)
                        <td align="center">{{ $jnspo }}</td>
                    @endif
                    <td align="left">{{ $item->nomor_po }}</td>
                    <td align="center">{{ $tgl_po }}</td>
                    <td align="center"> {{ $tgl_terima }} </td>
                    <td align="center">{{ $item->namasup }}</td>
                    <td align="center">{{ $namadmin }}</td>
                    {{-- @if($data->type_bayar == 'SEMUA TIPE PEMBAYARAN')
                        <td align="center">{{ $typebayar }}</td>
                    @endif --}}
                    @if($data->status == 'SEMUA STATUS')
                        <td align="center">{{ $sst }}</td>
                    @endif
                    <td width="1%" align="left" style='border-right:0px;'>Rp.</td>
                    <td align="right" style='border-left:0px;'>{{ $subttl }}</td>
                    
                </tr>

                @php
                    $totalSum += $item->grand_total;
                    // $jmlhSum += $jmlh;
                    // $hrgastnSum += $item->harga_satuan;
                    // $totalSum += 0;
                    // $jmlhSum += 0;
                    // $hrgastnSum += 0;
                @endphp
            @endforeach

           <?php 
             echo "<tr class='total'>
                        <td colspan='".($colspan-1)."' align='right'><strong>TOTAL " . $data->status . "</strong></td>
                        <td align='left' style='border-right:0px;'>Rp.</strong></td>
                        <td align='right' style='border-left:0px;'><strong>" .number_format($totalSum) . "</strong></td>
                    </tr>";
            
            ?>

        @endif
    </table>
</main>
@endsection