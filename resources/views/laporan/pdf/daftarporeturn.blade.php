@extends('laporan.pdf.layout.style')
@section('content')
    
@php
    // if ($data->status == "SEMUA STATUS") {
        $colspan = 5;
    // }else {
    //     $colspan = 7;
    // }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        {{-- <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Status PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;"></td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal PO</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;"></td>
                    </tr>
                    @endif
                </table>
            </td>
        </tr> --}}
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR PO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUPLIER</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">PENERIMA</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KETERANGAN</td>
        </tr>

        @php
            $no = 1;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
                    <?php 
                    // if($item->status == 0){
                    //     $statuss = "VERIFIKASI GM";
                    // }elseif($item->status == 1){
                    //      $statuss = "DISETUJUI (ON PROSES PO)";
                    // }elseif($item->status == 2){
                    //      $statuss = "SELESAI";
                    // }elseif($item->status == 3){
                    //      $statuss = "DITOLAK";
                    // }elseif($item->status == 4){
                    //      $statuss = "PROSES TERIMA BARANG";
                    // }elseif($item->status == 5){
                    //      $statuss = "VERIFIKASI TERIMA BARANG OLEH GM";
                    // }

                    $getadmin = DB::table('user as a')
                    ->select('a.name')
                    ->where('a.id',$item->penerima_id)
                    ->first();
                    if($getadmin){
                        $namadmin = $getadmin->name;
                    }else{
                        $namadmin = "-";
                    }
                    
                    // $brg = DB::table('barang')->where('id',$item->id)->first();
                    // if($brg){
                    //     $nmbrg = $brg->nama;
                    // // }else{
                    //     // $nmbrg = "-";
                    // // }

                    // // $stn = DB::table('satuan')->where('id',$item->id_satuan)->first();
                    // if($stn){
                    //     $nmstn = $stn->nama;
                    // }else{
                    //     $nmstn = "-";
                    // }

                    // $mrk = DB::table('merek')->where('id',$item->id_merek)->first();
                    // if($mrk){
                    //     $nmmrk = $mrk->nama;
                    // }else{
                    //     $nmmrk = "-";
                    // }

                    // if($item->nama == ''){
                    //     $hrgst = "-";
                    // }else{
                    //     $hrgst = number_format(0);
                    // }
                    // if($item->nama == ''){
                    //     $sbtl = "-";
                    // }else{
                    //     $sbtl = number_format(0);
                    // }

                    // if($item->nama == ''){
                    //     $jmlh = 0;
                    // }else{
                    //     $jmlh = $item->nama;
                    // }

                    // ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="center">{{ $item->nomor_po }}</td>
                    <td align="center">{{ $item->namasup }}</td>
                    <td align="center">{{ $namadmin }}</td>
                    <td align="center"> {{ $item->ket }} </td>
                    
                </tr>

            @endforeach

        @endif
    </table>
</main>
@endsection