@extends('laporan.pdf.layout.style')
@section('content')
    @php
        $colspan = 6;
    @endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        
        @include('laporan.pdf.layout.kop')

        <tr class="information">
            <td colspan="{{ $colspan }}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{ $colspan }}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal Pengajuan</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->tgl_pengajuan }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Nomor</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->nomor }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Devisi</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->report->devisi_nama }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Jenis SPB</td>
                        <td>:</td>
                        @if($data->report->jenis_spb == 0)
                            <td width="70%" style="text-align:left;">PEMBELIAN(PO) BARANG</td>
                        @elseif($data->report->jenis_spb == 1)
                            <td width="70%" style="text-align:left;">PERBAIKAN BARANG</td>
                        @endif
                        
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="{{ $colspan }}" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
        </tr>
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            {{-- <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td> --}}
            {{-- <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">ISI SATUAN</td> --}}
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JMLH</td>
            {{-- <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA SATUAN</td>
            <td width="9%" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SUB TOTAL</td> --}}
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KET</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
        </tr>

        @php
            $no             = 1;
            $totalSum       = 0;
            $jmlhSum       = 0;
            $hrgastnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                $spbdetail = DB::table('spb_detail as a')
                    ->select('a.*')
                    ->where('a.id_spb',$data->report->id)
                    ->get();
             ?>
            @foreach($spbdetail as $item)
                    <?php if($item->status == 0){
                        $statuss = "DISETUJUI";
                    }elseif($item->status == 1){
                         $statuss = "DITOLAK";
                    }else{
                         $statuss = "VERIFIKASI";
                    }

                    $brg = DB::table('barang')->where('id',$item->id_barang)->first();
                    if($brg){
                        $nmbrg = $brg->nama;
                    }else{
                        $nmbrg = "-";
                    }

                    $stn = DB::table('satuan')->where('id',$item->id_satuan)->first();
                    if($stn){
                        $nmstn = $stn->nama;
                    }else{
                        $nmstn = "-";
                    }

                    $mrk = DB::table('merek')->where('id',$item->id_merek)->first();
                    if($mrk){
                        $nmmrk = $mrk->nama;
                    }else{
                        $nmmrk = "-";
                    }

                    if($item->harga_satuan == ''){
                        $hrgst = "-";
                    }else{
                        $hrgst = number_format($item->harga_satuan);
                    }
                    if($item->sub_total == ''){
                        $sbtl = "-";
                    }else{
                        $sbtl = number_format($item->sub_total);
                    }

                    if($item->qty == ''){
                        $jmlh = 0;
                    }else{
                        $jmlh = $item->qty;
                    }

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $nmbrg }}</td>
                    {{-- <td align="left">{{ $nmstn }}</td> --}}
                    {{-- <td align="left">{{ $item->isi_satuan }}</td> --}}
                    <td align="left"> {{$nmmrk}} </td>
                    <td align="center">{{ $jmlh . " " . $nmstn }}</td>
                    
                    <td align="left">{{ $item->ket }}</td>
                    <td align="center">{{ $statuss }}</td>
                    
                </tr>

                @php
                    // $totalSum += $item->sub_total;
                    $jmlhSum += $jmlh;
                    // $hrgastnSum += $item->harga_satuan;
                @endphp
            @endforeach

           <?php 
             echo "<tr class='total'>
                        <td colspan='3' align='right'><strong>TOTAL</strong></td>
                        <td colspan='3' align='left'><strong>" . $jmlhSum . "</strong></td>
                    </tr>";
            
            ?>

        @endif
        
        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
            <div class="table-responsive">
              <table class="table mb-0 table-hover table-striped">
                  <thead class="thead-dark">
                      <tr>
                          <th style="text-align: center;font-size: 11px;">Diminta Oleh :</th>
                          <th style="text-align: center;font-size: 11px;">Diketahui Oleh :</th>
                          <th style="text-align: center;font-size: 11px;">Disetujui Oleh :</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td style="text-align: center;font-size: 11px;"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('/public/210617014947.png', .3)->size(100)->errorCorrection('H')->generate($data->qrcode->peminta_nama)); !!}"><br>
                            (<span style="text-decoration: underline;"> {{$data->report->peminta_nama}} </span>)
                            <br> {{$data->filter->jbtpeminta}}</td>
                          {{-- <td style="text-align: center;"><br>(<span style="text-decoration: underline;"> {{$data->report->pendamping_nama}} </span>)<br> KTU</td> --}}
                          <td style="text-align: center;font-size: 11px;"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('/public/210617014947.png', .3)->size(100)->errorCorrection('H')->generate($data->qrcode->diketahui_nama)); !!}"><br>
                            (<span style="text-decoration: underline;"> {{$data->report->diketahui_nama}} </span>)
                            <br> {{$data->filter->jbtdike}}</td>

                            <td style="text-align: center;font-size: 11px;">
                        @if($data->report->penyetuju_status == 0)
                           <br><br>
                            (<span style="text-decoration: underline;"> {{$data->report->penyetuju_nama}} </span>)
                            <br> {{$data->filter->jbtpenye}}
                        @elseif($data->report->penyetuju_status == 1)
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('/public/210617014947.png', .3)->size(100)->errorCorrection('H')->generate($data->qrcode->penyetuju_nama)); !!}"><br>
                            (<span style="text-decoration: underline;"> {{$data->report->penyetuju_nama}} </span>)
                            <br> {{$data->filter->jbtpenye}}
                        @elseif($data->report->penyetuju_status == 2)
                            <br><br>
                            (<span style="text-decoration: underline;"> {{$data->report->penyetuju_nama}} </span>)
                            <br> {{$data->filter->jbtpenye}}
                        @endif
                            </td>
                      </tr>
                  </tbody>
              </table>
            </div>
          </div>
    </table>
</main>
@endsection