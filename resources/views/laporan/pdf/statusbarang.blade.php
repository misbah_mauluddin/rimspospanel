@extends('laporan.pdf.layout.style')
@section('content')

@php
    if ($data->status == "SEMUA STATUS") {
        $colspan = 8;
    }else {
        $colspan = 7;
    }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    {{-- <tr>
                        <td width="30%" style="text-align:left;">Tanggal</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ date("Y-m-d h:i:s") }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="text-align:left;">Nomor</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;"></td>
                    </tr> --}}
                    <tr>
                        <td width="30%" style="text-align:left;">Status Barang</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->status }}</td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif

                </table>
            </td>
        </tr>
        <tr class="heading">
            <td colspan="{{$colspan}}" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
        </tr>
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JENIS</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">REKONDISI</td>
            @if ($data->status == 'SEMUA STATUS')
                <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
            @endif
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STOK BARANG</td>
        </tr>

        @php
            $no             = 1;
            // $totalSum       = 0;
            // $jmlhSum       = 0;
            // $hrgastnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                // $spbdetail = DB::table('spb_detail as a')
                //     ->select('a.*')
                //     ->where('a.id_spb',$data->report->id)
                //     ->get();
             ?>
            @foreach($data->report as $item)
                    <?php if($item->status == 0){
                        $statuss = "TIDAK AKTIF";
                    }elseif($item->status == 1){
                         $statuss = "AKTIF";
                    }elseif($item->status == 2){
                         $statuss = "DALAM PERBAIKAN";
                    }elseif($item->status == 3){
                         $statuss = "DIHAPUS";
                    }

                    //Status rekondisi barang
                    if($item->rekon == 0){
                        $rekondisi = "TIDAK BISA DIPERBAIKI";
                    }elseif($item->rekon == 1){
                        $rekondisi = "BISA DIPERBAIKI";
                    }

                    // $stok = DB::table('barang_stok as a')
                    //     ->select('a.*')
                    //     ->where('a.id_barang',$data->report->id)
                    //     ->first();

                     ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $item->nama }}</td>
                    <td align="left">{{ $item->namasatuan }}</td>
                    <td align="left"> {{$item->namamerek}} </td>
                    <td align="left">{{ $item->namajenis }}</td>
                    <td align="center">{{ $rekondisi }}</td>
                    @if($data->status == 'SEMUA STATUS')
                        <td align="center">{{ $statuss }}</td>
                    @endif
                    <td align="center">{{ $item->stok }}</td>
                    
                </tr>

            @endforeach

           <?php 
             echo "<tr class='total'>
                        <td colspan='".($colspan-1)."' align='right'><strong>TOTAL BARANG " . $data->status . "</strong></td>
                        <td align='center'><strong>" . count($data->report) . " Barang</strong></td>
                    </tr>";
            
            ?>

        @endif
    </table>
</main>
@endsection