@extends('laporan.pdf.layout.style')
@section('content')
    
@php
    $colspan = 7;
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    {{-- <tr>
                        <td width="30%" style="text-align:left;">Stok Barang</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->status }}</td>
                    </tr> --}}

                    @if ($data->date != '')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif

                </table>
            </td>
        </tr>
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NAMA BARANG</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">SATUAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JENIS</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">MEREK</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STOK BARANG</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">HARGA BARANG</td>
        </tr>

        @php
            $no             = 1;
            // $totalSum       = 0;
            // $jmlhSum       = 0;
            // $hrgastnSum       = 0;
        @endphp

        @if($data->report)

            <?php 

                // $spbdetail = DB::table('spb_detail as a')
                //     ->select('a.*')
                //     ->where('a.id_spb',$data->report->id)
                //     ->get();
             ?>
            @foreach($data->report as $item)
                    <?php 
                    $satuan = DB::table('satuan as a')
                        ->select('a.*')
                        ->where('id',$item->id_satuan)
                        ->first();
                    if($satuan){
                        $stn = $satuan->nama;
                    }else{
                        $stn = '-';
                    }

                    $jenis = DB::table('jenis as a')
                        ->select('a.*')
                        ->where('id',$item->id_jenis)
                        ->first();
                    if($jenis){
                        $jns = $jenis->nama;
                    }else{
                        $jns = '-';
                    }

                    $merek = DB::table('merek as a')
                        ->select('a.*')
                        ->where('id',$item->id_merek)
                        ->first();
                    if($merek){
                        $mrk = $merek->nama;
                    }else{
                        $mrk = '-';
                    }

                    $poterim = DB::table('po_detail as a')
                        ->select('a.*')
                        ->where('id_barang',$item->id_barang)
                        ->first();
                    // PoDetail::where('id_barang',$service->id_barang)->first();
                
                    if($poterim){
                        $htung = DB::table('po_detail as a')
                            ->select('a.*')
                            ->where('id_barang',$item->id_barang)
                            ->sum('a.sub_total');
                            // ->get();
                        // PoDetail::select(DB::raw('SUM(sub_total) as sub_total'))->where('id_barang',$service->id_barang)->first();
                        $saldo = "Rp. ".number_format($htung);
                    }else{
                        $saldo = '0';
                    }

                    ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="left">{{ $item->nama }}</td>
                    <td align="left">{{ $stn }}</td>
                    <td align="left">{{ $jns }}</td>
                    <td align="left"> {{$mrk}} </td>
                    <td align="center">{{$item->stok_akhir}}</td>
                    <td align="right">{{ $saldo }}</td>
                    
                </tr>
            @endforeach

           <?php 
             echo "<tr class='total'>
                        <td colspan='".($colspan-1)."' align='right'><strong>TOTAL BARANG</strong></td>
                        <td align='center'><strong>" . count($data->report) . " Barang</strong></td>
                    </tr>";
            
            ?>

        @endif
    </table>
</main>
@endsection