@extends('laporan.pdf.layout.style')
@section('content')

@php
    if ($data->status == "SEMUA STATUS") {
        $colspan = 8;
    }else {
        $colspan = 7;
    }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="40%" style="text-align:left;">Status Pengiriman Barang</td>
                        <td>:</td>
                        <td width="60%" style="text-align:left;">{{ $data->status }}</td>
                    </tr>

                    @if ($data->date != '')
                    <tr>
                        <td width="40%" style="text-align:left;">Tanggal Pengiriman Barang</td>
                        <td>:</td>
                        <td width="60%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td rowspan="2" width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR PENGIRIMAN</td>
            <td colspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TANGGAL</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">PERBAIKAN DI</td>
            <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DIBUAT OLEH</td>
            @if($data->status == "SEMUA STATUS")
                <td rowspan="2" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
            @endif
        </tr>
        <tr class="heading">
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL PENGIRIMAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL PENERIMAAN</td>
        </tr>

        @php
            $no = 1;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
                    <?php 
                    if($item->status == 0){
                        $statuss = "VERIFIKASI";
                    }elseif($item->status == 1){
                         $statuss = "DISETUJUI (PROSES KIRIM)";
                    }elseif($item->status == 2){
                         $statuss = "SELESAI";
                    }elseif($item->status == 3){
                         $statuss = "TIDAK DISETUJUI";
                    }elseif($item->status == 4){
                         $statuss = "PROSES TERIMA BARANG";
                    }else{
                         $statuss = "-";
                    }

                    if($item->buat_di == 0){
                        $buatdi = "Internal";
                    }elseif($item->buat_di == 1){
                         $buatdi = "External";
                    }

                    $getadmin = DB::table('user as a')
                        ->select('a.name')
                        ->where('a.id',$item->pembuat_id)
                        ->first();
                    if($getadmin){
                        $namadmin = $getadmin->name;
                    }else{
                        $namadmin = "-";
                    }

                    $getsupplier = DB::table('supplier as a')
                        ->select('a.nama')
                        ->where('a.id',$item->id_supplier)
                        ->first();
                    if($getadmin){
                        $namasupplier = $getsupplier->nama;
                    }else{
                        $namasupplier = "-";
                    }

                    // ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="center">{{ $item->nomor_pengiriman }}</td>
                    <td align="center">{{ $item->tgl_pengiriman }}</td>
                    <td align="center">{{ $item->tgl_penerimaan }}</td>
                    <td align="center">{{ $item->jmlh }}</td>
                    <td align="center">{{ $buatdi }} ({{ $namasupplier }})</td>
                    <td align="center">{{ $namadmin }}</td>
                    @if($data->status == "SEMUA STATUS")
                        <td align="center">{{ $statuss }}</td>
                    @endif
                    
                </tr>

            @endforeach

        @endif
    </table>
</main>
@endsection