@extends('laporan.pdf.layout.style')
@section('content')

@php
    if ($data->status == "SEMUA STATUS") {
        $colspan = 7;
    }else {
        $colspan = 6;
    }
@endphp

<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    <tr>
                        <td width="30%" style="text-align:left;">Status SPB</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->status }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        {{-- <tr class="heading">
            <td colspan="{{$colspan}}" style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">URAIAN BARANG</td>
        </tr> --}}
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JENIS SPB</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TANGGAL PENGAJUAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DIVISI</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DIMINTA OLEH</td>
            @if ($data->status == 'SEMUA STATUS')
                <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
            @endif
        </tr>

        @php
            $no             = 1;
        @endphp

        @if($data->report)
            @foreach($data->report as $item)
                    <?php 
                    // if($item->status == 0){
                    //     $statuss = "PROSES PERSETUJUAN";
                    // }elseif($item->status == 1){
                    //      $statuss = "DISETUJUI";
                    // }elseif($item->status == 2){
                    //      $statuss = "TIDAK DISETUJUI";
                    // }elseif($item->status == 3){
                    //      $statuss = "REVISI";
                    // }elseif($item->status == 4){
                    //      $statuss = "PROSES PO (SELESAI)";
                    // }

                    if($item->jenis_spb == 0){
                        $jnspb  = 'Pembelian(PO) Barang';
                        if($item->status == 0){
                            $sst  = 'Proses Persetujuan';
                        }elseif($item->status == 1){
                            $sst  = 'Disetujui';
                        }elseif($item->status == 2){
                            $sst  = 'Tidak Disetujui';
                        }elseif($item->status == 3){
                            $sst  = 'Revisi';
                        }elseif($item->status == 4){
                            $sst  = 'Proses PO';
                        }
                    }elseif($item->jenis_spb == 1){
                        $jnspb  = 'Perbaikan Barang';
                        if($item->status == 0){
                            $sst  = 'Proses Persetujuan';
                        }elseif($item->status == 1){
                            $sst  = 'Disetujui';
                        }elseif($item->status == 2){
                            $sst  = 'Tidak Disetujui';
                        }elseif($item->status == 3){
                            $sst  = 'Revisi';
                        }elseif($item->status == 4){
                            $sst  = 'Proses Kirim Barang';
                        }
                    }
                ?>

                <tr class="item">
                    <td align="center">{{ $no++ }}</td>
                    <td align="center">{{ $jnspb }}</td>
                    <td align="center">{{ $item->tgl_pengajuan }}</td>
                    <td align="left">{{ $item->nomor }}</td>
                    <td align="center"> {{ $item->devisi_nama }} </td>
                    <td align="center">{{ $item->peminta_nama }}</td>
                    @if($data->status == 'SEMUA STATUS')
                        <td align="center">{{ $sst }}</td>
                    @endif
                    
                </tr>
            @endforeach

           <?php 
             echo "<tr class='total'>
                        <td colspan='".($colspan-1)."' align='right'><strong>JUMLAH SPB " . $data->status . "</strong></td>
                        <td align='center'><strong>" . count($data->report) . " Surat</strong></td>
                    </tr>";
            
            ?>

        @endif
    </table>
</main>
@endsection