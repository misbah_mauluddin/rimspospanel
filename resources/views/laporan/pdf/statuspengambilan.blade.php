@extends('laporan.pdf.layout.style')
@section('content')

@php
// if ($data->status == "SEMUA STATUS") {
//     $colspan = 7;
// }else {
    $colspan = 7;
// }
@endphp
<main class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="{{$colspan}}">
                <table>
                    <tr>
                        <td>
                            {{ $data->title }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="top">
            <td colspan="{{$colspan}}">
                <table width="100%">
                    {{-- <tr>
                        <td width="30%" style="text-align:left;">Status</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->status }}</td>
                    </tr> --}}
                    @if ($data->date != ' s/d ')
                    <tr>
                        <td width="30%" style="text-align:left;">Tanggal Pengambilan</td>
                        <td>:</td>
                        <td width="70%" style="text-align:left;">{{ $data->date }}</td>
                    </tr>
                    @endif
                </table>
            </td>
        </tr> 
        
        <tr class="heading">
            <td width="2%"  style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NO</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">NOMOR PENGAMBILAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">KATEGORI</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">TGL PENGAMBILAN</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">JUMLAH</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DEVISI</td>
            <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">DIBUAT OLEH</td>
            {{-- @if ($data->status == 'SEMUA STATUS')
                <td style="text-align: center;vertical-align: middle;background: #334868;color: #FFF;">STATUS</td>
            @endif --}}
        </tr>
        @php
            $no             = 1;
            // $totalSum       = 0;
            // $jmlhSum       = 0;
            // $hrgastnSum       = 0;
        @endphp

@if($data->report)

<?php 

    // $spbdetail = DB::table('spb_detail as a')
    //     ->select('a.*')
    //     ->where('a.id_spb',$data->report->id)
    //     ->get();
 ?>
@foreach($data->report as $item)
        @php

        if($item->kategori == 0){
            $kategori = "DENGAN SPB";
        }elseif($item->kategori == 1){
             $kategori = "TANPA SPB";
        }

        $getadmin = DB::table('user')->where('id',$item->diketahui)->first();
        if($getadmin){
            $namadmin = $getadmin->name;
        }else{
            $namadmin = "-";
        }

        @endphp
    <tr class="item">
        <td align="center">{{ $no++ }}</td>
        <td align="left">{{ $item->nomor_pengambilan }}</td>
        <td align="center">{{ $kategori }}</td>
        <td align="left">{{ date("d-m-Y H:i",strtotime($item->tgl_pengambilan)) }}</td>
        <td align="center"> {{$item->jmlh}} </td>
        <td align="left">{{ $item->devisi_nama }} / {{ $item->peminta_nama }}</td>
        <td align="left">{{ $namadmin }}</td>
        {{-- @if($data->status == 'SEMUA STATUS')
            <td align="center">{{ $statuss }}</td>
        @endif --}}
        
    </tr>

@endforeach

    <?php 
        echo "<tr class='total'>
                <td colspan='".($colspan-1)."' align='right'><strong>Total Data </strong></td>
                <td align='left'><strong>" . count($data->report) . "</strong></td>
            </tr>";
    
    ?>

@endif
    </table>
</main>
@endsection