@extends('layout.home')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="breadcrumbs-top">
                    <div class="breadcrumb-wrapper d-none d-sm-block">
                        <ol class="breadcrumb  rounded-pill breadcrumb-divider">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#"> Laporan</a>
                            </li>
                            <li class="breadcrumb-item active" style="text-transform: uppercase;">{{ $data->title }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Bootstrap Select start -->
            <section class="bootstrap-select">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card border-secondary ">
                            <div class="card-header border-bottom ">
                                <h4 class="card-title">{{ $data->title_sub }} </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body mt-1 mb-2">
                                  <div class="form-body">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-xs-6">
                                            <div class="form-group">
                                                <label for="date1">Dari <small style="color: red;">*</small></label>
                                                <input type="text" id="date1" name="date1" class="form-control" placeholder="Pilih Tanggal"   value="{{ date('1 M, Y') }}">
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6">
                                            <div class="form-group">
                                                <label for="date2">Sampai <small style="color: red;">*</small></label>
                                                <input type="text" id="date2" name="date2" class="form-control" placeholder="Pilih Tanggal"   value="{{ date('d M, Y') }}">
                                                
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                       <div class="col-lg-8 col-md-8 col-xs-6">
                                            <div class="form-group">
                                                <label for="id_toko">Cari Toko <small style="color: red;">*</small></label>
                                                <select class="select2 form-control" id="id_toko" name="id_toko" style="width: 100%;">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                       <div class="col-lg-4 col-md-4 col-xs-6">
                                            <div class="form-group">
                                                <label for="status">Status <small style="color: red;">*</small></label>
                                                <select class="select2 form-control" id="status" name="status" style="width: 100%;">
                                                    <option value="all" selected>Semua Status</option>
                                                        <option value="1">Selesai</option>
                                                        <option value="2">Hutang</option>
                                                        <option value="3">Barang Keranjang</option>
                                                        <option value="4">Bayar Nanti</option>
                                                        <option value="5">Transaksi Batal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                    
                                    <div class="col-lg-12 border-bottom mb-1"></div>
                                    <div class="col-md-12">
                                        <button id="report-view" class="btn btn-icon-fixed btn-primary glow"><i class="bx bx-search"></i> Lihat Data</button>
                                        <button id="report-refresh" class="btn btn-icon-fixed btn-danger glow"><i class="bx bx-refresh"></i> Segarkan</button>
                                    </div>
                                      
                                  </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Bootstrap Select end -->
            <!-- Description -->
            <section id="dashboard">
                <div class="row">
                    
                      <div class="col-md-12 col-lg-12" id="rowdata" style="display: none;">
                          <div class="card" id="detailharga">
                              
                            
                          </div>
                      </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection
@section('js-scripts')
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('admins/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('admins/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    
    <script src="{{ asset('admins/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('admins/app-assets/js/datatables/dataTables.bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('admins/app-assets/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('admins/jquery-number/jquery.number.js') }}"></script>

    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('admins/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('admins/noty/noty.min.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/footer.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('admins/app-assets/js/helper.js') }}"></script>
    <!-- END: Theme JS-->

    <script src="{{ asset('admins/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>


@endsection
@section('js-action')
  <script type="text/javascript">
    $(function () {



      

      var table ,
            id_toko = $('#id_toko'),
            status = $('#status'),
            date1 = $('#date1'),
            date2 = $('#date2'),
            rowdata = $('#rowdata');

      $(document).on("mouseenter",".popover-hover",function(){             
          $(this).tooltip('show');
      }).on("mouseleave",".popover-hover",function(){
          $(this).tooltip('hide');
      });


        $('#date1').pickadate({
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

        $('#date2').pickadate({
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

        $("#id_toko").select2({
            placeholder: "Semua Toko",
            allowClear: true,
            quietMillis: 50,
            minimumInputLength: 1,
            delay: 250,
            "language": {
                "noResults": function () {
                    return "Tidak ada data ditemukan";
                },
                errorLoading: function () {
                    return 'Hasilnya tidak dapat dimuat';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                    return message;
                }
            },
            ajax: {
                url: "{{ url('admin-panel/gettokoglobal') }}",
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                      q: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            // if (item.id_merek == null){
                            //     var mrk = 'Belum ada merek';
                            // }else{
                            //     var mrk = item.namamerek;
                            // }
                            return {

                                text: item.nama_toko + " - Jenis Usaha: " + item.nama_jenisusaha,
                                id: item.id_toko
                            }
                            // get_satuan(item.id);
                        })
                    };
                },
                cache: true
            }
        });

        $("#status").select2({
                placeholder: "Pilih Status",
              allowClear: true,
              "language": {
                    "noResults": function () {
                        return "Tidak ada data ditemukan";
                    },
                    errorLoading: function () {
                        return 'Hasilnya tidak dapat dimuat';
                    },
                    inputTooShort: function (args) {
                        var remainingChars = args.minimum - args.input.length;

                        var message = 'Silahkan, Input ' + remainingChars + ' atau lebih karakter';

                        return message;
                    }
                },
        });

      

        function getDataPenjualanToko(id_toko,date1,date2)
          {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin-panel/history_pembayaran/data') }}",
                    data: ({_token: "{{csrf_token()}}", id_toko:id_toko,date1:date1,date2:date2}),
                    beforeSend: function () {
                        // spinner.show();
                    },
                    complete: function () {
                        // spinner.hide();
                    },
                    success: function (result) {
                        if(result['status'] == 'success')
                        {
                            $('#detailharga').html(result['data']);
                        }
                        else
                        {
                            new Noty({
                                type: 'error',
                                text: result['message'],
                                timeout: 2000,
                            }).show();
                        }
                    },
                    error: function (e) {
                        new Noty({
                            type: 'error',
                            text: 'Response error ' + e,
                            timeout: 5000
                        }).show();
                    }
                })
          }
        

      $('#report-view').on('click', function () {

            
            rowdata.fadeIn();
            getDataPenjualanToko(id_toko.val(),date1.val(),date2.val());
          
      });

      $('#report-refresh').on('click', function () {
            rowdata.fadeOut();
            getDataPenjualanToko(id_toko.val(),date1.val(),date2.val());
      });

      

      $(document).on('click', '.print-laporan', function () {
            PopupCenter('{{ url('/admin-panel/penjualantoko/print') }}?date1='+date1.val() + '&date2='+date2.val() , 'STATUS Penjualan Toko DALAM INVENTORI' ,'1200', '800');
        });


      
  });

    

  </script>
@endsection


