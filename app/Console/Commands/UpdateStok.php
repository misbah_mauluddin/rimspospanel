<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Database\QueryException;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;

class UpdateStok extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:stok';

    /**
     * The console command description.
     *
     * @var string
     */
    // protected $description = 'Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{
            $cekstok = BarangStok::orderBy('id','ASC')->get();
            if($cekstok){
                foreach($cekstok as $stokskrng){
                    $cekhistory = BarangStokHistory::where('id_barang',$stokskrng->id_barang)->orderBy('tanggal','DESC')->first();
                    if($cekhistory){

                        $newhistorystok = new BarangStokHistory();
                        $newhistorystok->tanggal = date('Y-m-d H:i:s');
                        $newhistorystok->id_barang = $stokskrng->id_barang;
                        $newhistorystok->stok_awal = $cekhistory->stok_akhir;
                        $newhistorystok->stok_masuk = '0';
                        $newhistorystok->stok_keluar = '0';
                        $newhistorystok->stok_akhir = $cekhistory->stok_akhir;

                        
                        $newhistorystok->ket = 'Update Stok Bulanan';

                        $newhistorystok->save();
                    }else{
                        $newhistorystok = new BarangStokHistory();
                        $newhistorystok->tanggal = date('Y-m-d H:i:s');
                        $newhistorystok->id_barang = $stokskrng->id_barang;
                        $newhistorystok->stok_awal = '0';
                        $newhistorystok->stok_masuk = '0';
                        $newhistorystok->stok_keluar = '0';
                        $newhistorystok->stok_akhir = '0';;

                        
                        $newhistorystok->ket = 'Update Stok Bulanan';

                        $newhistorystok->save();
                    }
                }
                \Log::info('Update Stok Bulanan Berhasil');
                
            }else{
                \Log::info('Gagal Update Stok Bulanan. Pesan : Stok Barang Sebelum nya tidak ditemukan!');
            }
            
            
        }catch (QueryException $ex){
            \Log::info("Gagal Update Stok Bulanan. Pesan : " . $ex->getMessage());
        }
    }
}
