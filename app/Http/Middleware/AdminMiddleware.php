<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(session()->get('is_active') == '1' && session()->get('is_logged') == '1'){
            // if($request->session()->get('role_id') == 1){
                return $next($request);
            // }else{
            //     return redirect('/login');
            // }
            
        }else{
            return redirect('/admin-panel/login');
        }
        
    }
}
