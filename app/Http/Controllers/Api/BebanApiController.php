<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;

use App\Http\Models\Kategori;
use App\Http\Models\KategoriBeban;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\SuccessTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\KategoriTransformer;
use App\Transformers\KategoriBebanTransformer;
use App\Transformers\BebanTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class BebanApiController extends Controller
{
   
////////BEBAN (tbl_beban)-----------------------------
    public function databeban_hariini(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $ktr = Beban::where('tgl',date("Y-m-d"))
                        ->whereRaw('id_toko = '.$request->id_toko.' AND (kategori LIKE "%'.$request->search.'%" OR tgl LIKE "%'.$request->search.'%" OR jumlah LIKE "%'.$request->search.'%")')
                    ->orderBy('tgl','DESC')->paginate(1);
              if(count($ktr) > 0){
                    // $toko = $request->id_toko;
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new BebanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);

                    // $respone =  fractal()
                    //         ->item($toko)
                    //         ->transformWith(new BebanTransformer)
                    //         ->serializeWith(new ArraySerializer)
                    //         ->toArray();
                    // return response()->json($respone, 200);
              }else{
                    $messages = 'Beban Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    public function databeban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $ktr = Beban::whereRaw('id_toko = '.$request->id_toko.' AND (kategori LIKE "%'.$request->search.'%" OR tgl LIKE "%'.$request->search.'%" OR jumlah LIKE "%'.$request->search.'%")')
                    ->orderBy('tgl','DESC')->paginate(1);
              if(count($ktr) > 0){
                    // $toko = $request->id_toko;
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new BebanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);

                    // $respone =  fractal()
                    //         ->item($toko)
                    //         ->transformWith(new BebanTransformer)
                    //         ->serializeWith(new ArraySerializer)
                    //         ->toArray();
                    // return response()->json($respone, 200);
              }else{
                    $messages = 'Beban Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_K' => 'required',
                        'id_kasir' => 'required',
                        'kategori' => 'required',
                        'keterangan' => 'required',
                        'tgl' => 'required',
                        'jumlah' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = new Beban();
                    $addktr->id_toko = $request->id_toko;
                    $addktr->id_ktr_beban = $request->id_K;
                    $addktr->id_kasir = $request->id_kasir;
                    $addktr->kategori = $request->kategori;
                    $addktr->keterangan = $request->keterangan;
                    $addktr->tgl = $request->tgl;
                    $addktr->jumlah = $request->jumlah;
                    

                    if($addktr->save()){
                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tambah kategori tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Tambah kategori tidak berhasil, silahkan coba kembali!");
        }

    }

    public function edit_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_beban' => 'required',
                        'id_k' => 'required',
                        'kategori' => 'required',
                        'keterangan' => 'required',
                        'tgl' => 'required',
                        'jumlah' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Beban::where('id_beban',$request->id_beban)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        $addktr->id_ktr_beban = $request->id_k;
                        $addktr->kategori = $request->kategori;
                        $addktr->keterangan = $request->keterangan;
                        $addktr->tgl = $request->tgl;
                        $addktr->jumlah = $request->jumlah;
                        if($addktr->save()){
                            $messages = 'Data Beban Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Edit Beban tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Edit kategori tidak berhasil, silahkan coba kembali!");
        }

    }

    public function hapus_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_beban' => 'required',
                        'id_toko' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Beban::where('id_beban',$request->id_beban)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data Beban Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus Beban tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus Beban tidak berhasil, silahkan coba kembali!");
        }

    }

}