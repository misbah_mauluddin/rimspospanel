<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\Laporan;

use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\PenjualanTokoDetailPaket;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;
use App\Http\Models\KasLaci;

use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;


use App\Transformers\ListLaporanTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\FailsTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Image;
use Mail;
use PDF;

use DateTime;
use DateInterval;
use DatePeriod;



class LaporanApiController extends Controller
{
    

    public function list_laporan(Request $request){
        try {

            
            $ktr = Laporan::where('status','1')->paginate(10);
            if(count($ktr) > 0){
                $dtl = $ktr->getCollection();                    

                $respone =  fractal()
                      ->collection($dtl, new ListLaporanTransformer, 'data')
                        ->serializeWith(new ArraySerializerV2())
                        ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                        ->addMeta(['success'=> true,
                                    'error'=> false,
                                    'message'=> 'Data Berhasil Ditampilkan!',
                                    'message_error'=> '',
                                    'catatan'=> '',
                                    ])
                        ->toArray();
                  return response()->json($respone, 200);
            }else{
                $dtl = $ktr->getCollection();        
                $respone =  fractal()
                      ->collection($dtl, new ListLaporanTransformer, 'data')
                        ->serializeWith(new ArraySerializerV2())
                        ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                        ->addMeta(['success'=> false,
                                    'error'=> true,
                                    'message'=> 'Tidak ada Data Ditemukan',
                                    'message_error'=> '',
                                    'catatan'=> '',
                                    ])
                        ->toArray();
                  return response()->json($respone, 200);
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }


////ALL REPORT
    public function LaporanUmum(Request $request)
    {
        try{
            $id_toko = $request->id_toko;
            $cektoko = Toko::where('id_toko',$id_toko)->first();
            if($cektoko){

                if( $request->date1 !='' &&  $request->date2 != ''){
                    $date1 = date("Y-m-d", strtotime($request->date1));
                    $date2 = date("Y-m-d", strtotime($request->date2));
                    $date = date("d-m-Y", strtotime($request->date1)) . ' s/d ' . date("d-m-Y", strtotime($request->date2));
                }else{
                    $date1 = $request->date1;
                    $date2 = $request->date2;
                    $date = '-';
                }

                $penjualan = PenjualanToko::select(DB::raw('SUM(total) as total'))
                            ->where('id_toko',$id_toko)
                            ->where('status',1)
                            ->whereIn('metode_bayar',['0'])
                            ->whereDate('tgl_penjualan', '>=', $date1)
                            ->whereDate('tgl_penjualan', '<=', $date2)
                            ->first();

                $penjualan_trf = PenjualanToko::select(DB::raw('SUM(total) as total'))
                            ->where('id_toko',$id_toko)
                            ->where('status',1)
                            ->whereIn('metode_bayar',['1','2'])
                            ->whereDate('tgl_penjualan', '>=', $date1)
                            ->whereDate('tgl_penjualan', '<=', $date2)
                            ->first();

                $beban = Beban::select(DB::raw('SUM(jumlah) as jumlah'))
                            ->where('id_toko',$id_toko)
                            ->whereDate('tgl', '>=', $date1)
                            ->whereDate('tgl', '<=', $date2)
                            ->first();

                // $kas = KasLaci::where('id_toko',$id_toko)->orderBy('tgl','DESC')->first();

                $modal = DB::table('tbl_penjualan_detail as p')
                        ->select(DB::raw('SUM(q.modal * p.qty) as modal'))
                        ->leftJoin('tbl_hist_stok as q','p.id_hist_stok','=','q.id_stok')
                        ->where('p.id_toko',$id_toko)
                        ->whereDate('p.tgl', '>=', $date1)
                        ->whereDate('p.tgl', '<=', $date2)
                        ->first();

                $laba = (($penjualan->total+$penjualan_trf->total)-$beban->jumlah)-$modal->modal;
                

                $data = (object) array(
                    'title'     => "LAPORAN UMUM",
                    'filter'    => (object) array(
                        'toko'    =>  $cektoko,
                        'date'    =>  $date,
                    ),
                    'hasil'    => (object) array(
                        'penjualan'    =>  $penjualan,
                        'penjualan_trf'    =>  $penjualan_trf,
                        'beban'    =>  $beban,
                        // 'kas'    =>  $kas,
                        'modal'    =>  $modal,
                        'laba'    =>  $laba,
                    ),
                );

                $pdf = PDF::loadView('laporan.api.laporanumum', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LAPORAN UMUM.pdf');
               
            }else{
                return view('errors.missing');
            }
            
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }

    public function LaporanPenjualan(Request $request)
    {
        try{
            $id_toko = $request->id_toko;
            $cektoko = Toko::where('id_toko',$id_toko)->first();
            if($cektoko){

                if( $request->date1 !='' &&  $request->date2 != ''){
                    $date1 = date("Y-m-d", strtotime($request->date1));
                    $date2 = date("Y-m-d", strtotime($request->date2));
                    $date = date("d-m-Y", strtotime($request->date1)) . ' s/d ' . date("d-m-Y", strtotime($request->date2));
                }else{
                    $date1 = $request->date1;
                    $date2 = $request->date2;
                    $date = '-';
                }

                $penjualan = DB::table('tbl_penjualan as a')->select('a.id','b.id_barang',DB::raw('SUM(b.total) as total'),DB::raw('SUM(b.qty) as qty'))
                            ->leftJoin('tbl_penjualan_detail as b','a.id','b.id_penjualan')
                            ->where('a.id_toko',$id_toko)
                            ->where('a.status',1)
                            ->whereIn('a.metode_bayar',['0','1','2'])
                            ->whereDate('a.tgl_penjualan', '>=', $date1)
                            ->whereDate('a.tgl_penjualan', '<=', $date2)
                            ->groupBy('b.id_barang')
                            ->orderBy('b.id_barang','ASC')
                            ->get();
                

                $data = (object) array(
                    'title'     => "LAPORAN PENJUALAN",
                    'filter'    => (object) array(
                        'toko'    =>  $cektoko,
                        'date'    =>  $date,
                    ),
                    'hasil'    => (object) array(
                        'penjualan'    =>  $penjualan,
                    ),
                );

                $pdf = PDF::loadView('laporan.api.laporanpenjualan', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LAPORAN PENJUALAN.pdf');
               
            }else{
                return view('errors.missing');
            }
            
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }

    public function LaporanBeban(Request $request)
    {
        try{
            $id_toko = $request->id_toko;
            $cektoko = Toko::where('id_toko',$id_toko)->first();
            if($cektoko){

                if( $request->date1 !='' &&  $request->date2 != ''){
                    $date1 = date("Y-m-d", strtotime($request->date1));
                    $date2 = date("Y-m-d", strtotime($request->date2));
                    $date = date("d-m-Y", strtotime($request->date1)) . ' s/d ' . date("d-m-Y", strtotime($request->date2));
                }else{
                    $date1 = $request->date1;
                    $date2 = $request->date2;
                    $date = '-';
                }

                $beban = Beban::where('id_toko',$id_toko)
                            ->whereDate('tgl', '>=', $date1)
                            ->whereDate('tgl', '<=', $date2)
                            ->orderBy('tgl','DESC')
                            ->get();
                

                $data = (object) array(
                    'title'     => "LAPORAN BEBAN",
                    'filter'    => (object) array(
                        'toko'    =>  $cektoko,
                        'date'    =>  $date,
                    ),
                    'hasil'    => (object) array(
                        'beban'    =>  $beban,
                    ),
                );

                $pdf = PDF::loadView('laporan.api.laporanbeban', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LAPORAN BEBAN.pdf');
               
            }else{
                return view('errors.missing');
            }
            
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }

    public function LaporanReversal(Request $request)
    {
        try{
            $id_toko = $request->id_toko;
            $cektoko = Toko::where('id_toko',$id_toko)->first();
            if($cektoko){

                if( $request->date1 !='' &&  $request->date2 != ''){
                    $date1 = date("Y-m-d", strtotime($request->date1));
                    $date2 = date("Y-m-d", strtotime($request->date2));
                    $date = date("d-m-Y", strtotime($request->date1)) . ' s/d ' . date("d-m-Y", strtotime($request->date2));
                }else{
                    $date1 = $request->date1;
                    $date2 = $request->date2;
                    $date = '-';
                }

                $penjualan = DB::table('tbl_penjualan as a')->select('a.id','a.id_kasir','a.tgl_penjualan','b.id_barang',DB::raw('SUM(b.total) as total'),DB::raw('SUM(b.qty) as qty'))
                            ->leftJoin('tbl_penjualan_detail as b','a.id','b.id_penjualan')
                            ->where('a.id_toko',$id_toko)
                            ->where('a.status',4)
                            ->whereIn('a.metode_bayar',['0','1','2'])
                            ->whereDate('a.tgl_penjualan', '>=', $date1)
                            ->whereDate('a.tgl_penjualan', '<=', $date2)
                            ->groupBy('b.id_barang')
                            ->orderBy('a.tgl_penjualan','DESC')
                            ->get();
                

                $data = (object) array(
                    'title'     => "LAPORAN REVERSAL",
                    'filter'    => (object) array(
                        'toko'    =>  $cektoko,
                        'date'    =>  $date,
                    ),
                    'hasil'    => (object) array(
                        'penjualan'    =>  $penjualan,
                    ),
                );

                $pdf = PDF::loadView('laporan.api.laporanreversal', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LAPORAN REVERSAL.pdf');
               
            }else{
                return view('errors.missing');
            }
            
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }

    public function LaporanStock(Request $request)
    {
        try{
            $id_toko = $request->id_toko;
            $cektoko = Toko::where('id_toko',$id_toko)->first();
            if($cektoko){

                // if( $request->date1 !='' &&  $request->date2 != ''){
                //     $date1 = date("Y-m-d", strtotime($request->date1));
                //     $date2 = date("Y-m-d", strtotime($request->date2));
                //     $date = date("d-m-Y", strtotime($request->date1)) . ' s/d ' . date("d-m-Y", strtotime($request->date2));
                // }else{
                //     $date1 = $request->date1;
                //     $date2 = $request->date2;
                //     $date = '-';
                // }

                $barang = DB::table('tbl_barang as a')->select('a.*','b.nama_kategori')
                            ->leftJoin('tbl_kategori as b','a.id_kategori','b.id_kategori')
                            ->where('a.id_toko',$id_toko)
                            ->where('a.status',1)
                            ->orderBy('a.id_kategori','ASC')
                            ->orderBy('a.nama_br','ASC')
                            ->get();
                

                $data = (object) array(
                    'title'     => "LAPORAN STOCK BARANG",
                    'filter'    => (object) array(
                        'toko'    =>  $cektoko,
                        // 'date'    =>  $date,
                    ),
                    'hasil'    => (object) array(
                        'barang'    =>  $barang,
                    ),
                );

                $pdf = PDF::loadView('laporan.api.laporanstock', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LAPORAN STOCK BARANG.pdf');
               
            }else{
                return view('errors.missing');
            }
            
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }
}