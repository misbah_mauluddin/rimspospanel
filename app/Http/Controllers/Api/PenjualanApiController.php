<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\Keranjang;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\PenjualanTokoDetailPaket;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\FailsTransformer;
use App\Transformers\SuccessTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;
use App\Transformers\KeranjangTransformer;
use App\Transformers\PenjualanTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class PenjualanApiController extends Controller
{

//////PENJUALAN
    public function pembayaran(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'meja' => 'required',
                        'id_kasir' => 'required',
                        'bayar' => 'required',
                        // 'kembalian' => 'required',
                        'metode_bayar' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $getkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->get();
                    

                    if(count($getkeranjang) > 0){
                    	$sumkeranjang = Keranjang::select(DB::raw('SUM(qty) as total_item, SUM(harga_brg*qty) as subtotal,SUM(harga_brg*qty)-SUM(diskon_brg) as total, SUM(diskon_brg) as diskon_total'))
                        // Keranjang::select(DB::raw('SUM(qty) as total_item, SUM(diskon_brg) as diskon_total, SUM(total) as total'))
                    				->where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->where('status','0')
                                    ->first();

                    	$addpenjualan = new PenjualanToko();
                    	$addpenjualan->id_toko = $request->id_toko; 
                    	$addpenjualan->meja = $request->meja; 
                    	$addpenjualan->id_kasir = $request->id_kasir; 
                    	$addpenjualan->bayar = $request->bayar; 
                    	$addpenjualan->kembalian = $request->bayar-$sumkeranjang->total; 
                    	$addpenjualan->tgl_penjualan = date("Y-m-d H:i:s"); 
                    	$addpenjualan->metode_bayar = $request->metode_bayar; 
                        if($request->metode_bayar == 0){
                            $addpenjualan->status = 1; 
                        }elseif($request->metode_bayar == 1){
                            $addpenjualan->status = 1; 
                        }elseif($request->metode_bayar == 2){
                            $addpenjualan->status = 1; 
                        }elseif($request->metode_bayar == 3){
                            $addpenjualan->status = 3; 
                        }
                    	

                    	$addpenjualan->total_item = $sumkeranjang->total_item; 
                    	$addpenjualan->diskon_total = $sumkeranjang->diskon_total; 
                    	$addpenjualan->sub_total = $sumkeranjang->subtotal;
                    	$addpenjualan->total = $sumkeranjang->total; 
                    	$addpenjualan->save();

                    	/* KATEGORI PRODUK (id_kategori)
                    	1. BARANG
                    	2. JASA
                    	3. PAKET (BISA ISI JASA DAN BARANG) V.1
                    	
                    	KETENTUAN PENCATATAN DETAIL PENJUALAN
						Jika kategori paket maka akan di catat ke table detail penjualan
						adalah paket nya (bukan item paket), item paket akan di catat
						dalam table detail penjualan paket
                    	*/

                        foreach ($getkeranjang as $keranjang) {
                            if($keranjang->id_kategori == 1){
                            	$update_stok = ProdukTokoStok::where('id_barang',$keranjang->id_barang)
                                                        ->where('id_toko',$keranjang->id_toko)
                                                        ->first();

                                ####///////----START---PENGURANGAN STOK-------/////////////
                                $update_stok->qty = $update_stok->qty - $keranjang->qty;
                                $update_stok->save();

                                ///////---PENGURANGAN HISTORY STOK
                                $update_histstok = HistStok::where('id_barang',$update_stok->id_barang)
                                                ->where('id_toko',$update_stok->id_toko)
                                                ->whereRaw('jlh_masuk > jlh_keluar')
                                                ->orderBy('tanggal','ASC')
                                                ->first();
                                $update_histstok->jlh_keluar = $update_histstok->jlh_keluar + $keranjang->qty;
                                $update_histstok->save();
                                ####///////----END---PENGURANGAN STOK-------/////////////


                                ///////---CATAT Detail PENJUALAN
                                $dtlpenjualan = new PenjualanTokoDetail();
                                $dtlpenjualan->id_penjualan	 	= $addpenjualan->id;
                                $dtlpenjualan->id_toko 			= $keranjang->id_toko;
                                $dtlpenjualan->meja 			= $keranjang->meja;
                                $dtlpenjualan->id_barang 		= $keranjang->id_barang;
                                $dtlpenjualan->id_kategori 		= $keranjang->id_kategori;
                                // $dtlpenjualan->id_paket = '';
                                $dtlpenjualan->id_kasir 		= $keranjang->id_kasir;
                                $dtlpenjualan->id_pekerja 		= $keranjang->id_pekerja;
                                $dtlpenjualan->nama_brg 		= $keranjang->nama_brg;
                                $dtlpenjualan->harga_brg 		= $keranjang->harga_brg;
                                $dtlpenjualan->qty 				= $keranjang->qty;
                                $dtlpenjualan->diskon_brg 		= $keranjang->diskon_brg;
                                $dtlpenjualan->total 			= $keranjang->total;
                                $dtlpenjualan->upah_pekerja 	= $keranjang->upah_pekerja;
                                $dtlpenjualan->bagian_owner 	= $keranjang->bagian_owner;
                                $dtlpenjualan->tgl 				= date("Y-m-d H:i:s");
                                $dtlpenjualan->id_hist_stok 	= $update_histstok->id_stok;
                                $dtlpenjualan->save();
                               
                                
                            }elseif($keranjang->id_kategori == 2){
                            	$dtlpenjualan = new PenjualanTokoDetail();
                                $dtlpenjualan->id_penjualan	 	= $addpenjualan->id;
                                $dtlpenjualan->id_toko 			= $keranjang->id_toko;
                                $dtlpenjualan->meja 			= $keranjang->meja;
                                $dtlpenjualan->id_barang 		= $keranjang->id_barang;
                                $dtlpenjualan->id_kategori 		= $keranjang->id_kategori;
                                // $dtlpenjualan->id_paket = '';
                                $dtlpenjualan->id_kasir 		= $keranjang->id_kasir;
                                $dtlpenjualan->id_pekerja 		= $keranjang->id_pekerja;
                                $dtlpenjualan->nama_brg 		= $keranjang->nama_brg;
                                $dtlpenjualan->harga_brg 		= $keranjang->harga_brg;
                                $dtlpenjualan->qty 				= $keranjang->qty;
                                $dtlpenjualan->diskon_brg 		= $keranjang->diskon_brg;
                                $dtlpenjualan->total 			= $keranjang->total;
                                $dtlpenjualan->upah_pekerja 	= $keranjang->upah_pekerja;
                                $dtlpenjualan->bagian_owner 	= $keranjang->bagian_owner;
                                $dtlpenjualan->tgl 				= date("Y-m-d H:i:s");

                                $dtlpenjualan->save();
                            }elseif($keranjang->id_kategori == 3){
                            	///////---CATAT Detail PENJUALAN
                                $dtlpenjualan = new PenjualanTokoDetail();
                                $dtlpenjualan->id_penjualan	 	= $addpenjualan->id;
                                $dtlpenjualan->id_toko 			= $keranjang->id_toko;
                                $dtlpenjualan->meja 			= $keranjang->meja;
                                $dtlpenjualan->id_barang 		= $keranjang->id_barang;
                                $dtlpenjualan->id_kategori 		= $keranjang->id_kategori;
                                $dtlpenjualan->id_paket 		= $keranjang->id_barang;
                                $dtlpenjualan->id_kasir 		= $keranjang->id_kasir;
                                $dtlpenjualan->id_pekerja 		= $keranjang->id_pekerja;
                                $dtlpenjualan->nama_brg 		= $keranjang->nama_brg;
                                $dtlpenjualan->harga_brg 		= $keranjang->harga_brg;
                                $dtlpenjualan->qty 				= $keranjang->qty;
                                $dtlpenjualan->diskon_brg 		= $keranjang->diskon_brg;
                                $dtlpenjualan->total 			= $keranjang->total;
                                $dtlpenjualan->upah_pekerja 	= $keranjang->upah_pekerja;
                                $dtlpenjualan->bagian_owner 	= $keranjang->bagian_owner;
                                $dtlpenjualan->tgl 				= date("Y-m-d H:i:s");
                                $dtlpenjualan->save();

                            	/* PROSES LOOPING ISI PAKET */
                            	$cekbrg_pket = ProdukPaket::where('id_paket',$keranjang->id_barang)->get();
                            	foreach ($cekbrg_pket as $rowpaket) {
                            		$cekbrg = ProdukToko::where('id_barang',$rowpaket->id_produk)->first();
                            		if($cekbrg->id_kategori == 1){
                            			/////// JIKA BARANG MAKA KURANGI STOK /////////////
                            			$update_stok = ProdukTokoStok::where('id_barang',$cekbrg->id_barang)
                                                        ->where('id_toko',$cekbrg->id_toko)
                                                        ->first();

                            			####///////----START---PENGURANGAN STOK-------/////////////
                            			$update_stok->qty = $update_stok->qty - ($rowpaket->qty*$keranjang->qty);
		                                $update_stok->save();

		                                $update_histstok = HistStok::where('id_barang',$cekbrg->id_barang)
		                                                ->where('id_toko',$cekbrg->id_toko)
		                                                ->whereRaw('jlh_masuk > jlh_keluar')
		                                                ->orderBy('tanggal','ASC')
		                                                ->first();
		                                if($update_histstok){
		                                	$update_histstok->jlh_keluar = $update_histstok->jlh_keluar + ($rowpaket->qty*$keranjang->qty);
                                    		$update_histstok->save();
		                                }
                            			####///////----END---PENGURANGAN STOK-------/////////////

                            			///////---CATAT ITEM PAKET
		                                $dtlpjlpaket = new PenjualanTokoDetailPaket();
		                                $dtlpjlpaket->id_penjualan_detail	 	= $dtlpenjualan->id;
		                                $dtlpjlpaket->id_barang 				= $cekbrg->id_barang;
		                                $dtlpjlpaket->id_kategori 				= $cekbrg->id_kategori;
		                                $dtlpjlpaket->id_pekerja 				= $dtlpenjualan->id_pekerja;
		                                $dtlpjlpaket->nama_brg 					= $cekbrg->nama_br;

		                                if($cekbrg->metode_bagihasil == 0){
		                                	// $dtlpjlpaket->harga_brg 				= 0;
		                                	// $dtlpjlpaket->upah_pekerja 				= $cekbrg->id_barang;
		                                	// $dtlpjlpaket->bagian_owner 				= $cekbrg->id_kategori;
		                                }elseif($cekbrg->metode_bagihasil == 1){
		                                	$getbagihasil = BagiHasil::where('id_paket',$rowpaket->id)->where('id_petugas',$dtlpenjualan->id_pekerja)->first();

		                                	$dtlpjlpaket->harga_brg 				= $rowpaket->harga*$dtlpenjualan->qty;
		                                	$dtlpjlpaket->upah_pekerja 				= $getbagihasil->nilai*$dtlpenjualan->qty;
		                                	$dtlpjlpaket->bagian_owner 				= $rowpaket->harga-($getbagihasil->nilai*$dtlpenjualan->qty);
		                                }

		                                $dtlpjlpaket->qty 						= $rowpaket->qty*$dtlpenjualan->qty;
		                                $dtlpjlpaket->tgl 						= date("Y-m-d H:i:s");
		                                $dtlpjlpaket->id_hist_stok  			= $update_histstok->id_stok;
		                                $dtlpjlpaket->save();
                            		}elseif($cekbrg->id_kategori == 2){
                            			//////---CATAT ITEM PAKET
		                                $dtlpjlpaket = new PenjualanTokoDetailPaket();
		                                $dtlpjlpaket->id_penjualan_detail	 	= $dtlpenjualan->id;
		                                $dtlpjlpaket->id_barang 				= $cekbrg->id_barang;
		                                $dtlpjlpaket->id_kategori 				= $cekbrg->id_kategori;
		                                $dtlpjlpaket->id_pekerja 				= $dtlpenjualan->id_pekerja;
		                                $dtlpjlpaket->nama_brg 					= $cekbrg->nama_br;

		                                if($cekbrg->metode_bagihasil == 0){
		                                	// $dtlpjlpaket->harga_brg 				= 0;
		                                	// $dtlpjlpaket->upah_pekerja 				= $cekbrg->id_barang;
		                                	// $dtlpjlpaket->bagian_owner 				= $cekbrg->id_kategori;
		                                }elseif($cekbrg->metode_bagihasil == 1){
		                                	$getbagihasil = BagiHasil::where('id_paket',$rowpaket->id)->where('id_petugas',$dtlpenjualan->id_pekerja)->first();

		                                	$dtlpjlpaket->harga_brg                 = $rowpaket->harga*$dtlpenjualan->qty;
                                            $dtlpjlpaket->upah_pekerja              = $getbagihasil->nilai*$dtlpenjualan->qty;
                                            $dtlpjlpaket->bagian_owner              = $rowpaket->harga-($getbagihasil->nilai*$dtlpenjualan->qty);
		                                }

		                                $dtlpjlpaket->qty 						= $rowpaket->qty*$dtlpenjualan->qty;
		                                $dtlpjlpaket->tgl 						= date("Y-m-d H:i:s");
		                                $dtlpjlpaket->save();
                            		}
                            	}
                            }
                            
                        }

                        // foreach ($getkeranjang as $keranjang) {
                        // 	$update_stok = ProdukTokoStok::where('id_barang',$keranjang->id_barang)
                        //                                 ->where('id_toko',$keranjang->id_toko)
                        //                                 ->first();

                        //     if($keranjang->id_kategori == 1){
                        //         ///////----START---PENGURANGAN STOK-------/////////////
                        //         if($update_stok){
                        //             $update_stok->qty = $update_stok->qty - $keranjang->qty;
                        //             if($update_stok->save()){
                        //                 $update_histstok = HistStok::where('id_barang',$update_stok->id_barang)
                        //                                 ->where('id_toko',$update_stok->id_toko)
                        //                                 ->whereRaw('jlh_masuk > jlh_keluar')
                        //                                 ->orderBy('tanggal','ASC')
                        //                                 ->first();
                        //                 if($update_histstok){
                        //                     ///////---PENGURANGAN HOSTORY STOK
                        //                     $update_histstok->jlh_keluar = $update_histstok->jlh_keluar + $keranjang->qty;
                        //                     if($update_histstok->save()){
                        //                         ///////---CATAT PENJUALAN
                        //                         $success_res = true;
                        //                         $error_res = false;
                        //                         $message_res = 'good';
                        //                         $message_error_res = 'good!';
                        //                         $code_res = 200;
                        //                     }else{
                        //                         $success_res = false;
                        //                         $error_res = true;
                        //                         $message_res = 'Gagal Melakukan Pembayaran, coba lagi!';
                        //                         $message_error_res = 'History Stok Gagal Diperbaharui!';
                        //                         $code_res = 409;
                        //                     }
                        //                 }else{
                        //                     /* Kembalikan Stok jika history stok tidak ditemukan */
                        //                     $reset_stok = ProdukTokoStok::where('id_barang',$keranjang->id_barang)
                        //                                 ->where('id_toko',$keranjang->id_toko)
                        //                                 ->first();
                        //                     $reset_stok->qty = $reset_stok->qty + $keranjang->qty;
                        //                     $reset_stok->save();

                        //                     $success_res = false;
                        //                     $error_res = true;
                        //                     $message_res = 'Gagal Melakukan Pembayaran, coba lagi!';
                        //                     $message_error_res = 'History Stok Tidak Ditemukan!';
                        //                     $code_res = 409;
                        //                 }
                        //             }else{
                        //                 $success_res = false;
                        //                 $error_res = true;
                        //                 $message_res = 'Gagal Melakukan Pembayaran, coba lagi!';
                        //                 $message_error_res = 'Stok Gagal Diperbaharui!';
                        //                 $code_res = 409;
                        //             }
                        //         }else{
                        //             $success_res = false;
                        //             $error_res = true;
                        //             $message_res = 'Gagal Melakukan Pembayaran, coba lagi!';
                        //             $message_error_res = 'Stok Tidak ditemukan!';
                        //             $code_res = 409;
                        //             break;
                        //         }
                        //         ///////----END---PENGURANGAN STOK-------/////////////
                        //     }elseif($keranjang->id_kategori == 2){

                        //     }elseif($keranjang->id_kategori == 3){

                        //     }
                        // }

                        $getkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->delete();

                        $datapenjl = PenjualanToko::where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->get();

                        $respone =  fractal()
                                ->collection($datapenjl, new PenjualanTransformer, 'data')
                                ->serializeWith(new ArraySerializerV2())
                                ->addMeta(['success'=> true,
                                            'error'=> false,
                                            'message'=> 'Pembayaran Berhasil Dilakukan!',
                                            'message_error'=> '',
                                            'catatan'=> array(
                                            				'status_dtl'=> '1 Selesai, 2 Hutang, 3 Bayar nanti, 4 transaksi batal (Reversal)',
                                            				'metode_bayar_dtl'=> '0. cash, 1. debit card, 2. kredit card, 3. bayar nanti (kasbon)',
                                            			),
                                            
                                        	])
                                ->toArray();
                        return response()->json($respone, 200);

                    }else{
                        $respone =  fractal()
                            // ->item($messages)
                            ->collection($getkeranjang, new FailsTransformer, 'data')
                            // ->transformWith(new FailsTransformer) 
                            ->serializeWith(new ArraySerializerV2())     
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Gagal Melakukan Pembayaran, Tidak ada Data di keranjang!',
                                        'message_error'=> 'Data Kosong',
                                        'catatan'=> ''
                                        
                                        ])
                            ->toArray();
                        return response()->json($respone, 409); 

                    }
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Melakukan Pembayaran, coba lagi!");
        }

    }

    public function riwayat_penjualan(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_kasir' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                // $search = $request->search;
                $kasir = $request->kasir;
                $status = $request->status;
                if( $request->daritgl !='' &&  $request->sampaitgl != ''){
                    $daritgl = date("Y-m-d", strtotime($request->daritgl));
                    $sampaitgl = date("Y-m-d", strtotime($request->sampaitgl));
                }else{
                    $daritgl = $request->daritgl;
                    $sampaitgl = $request->sampaitgl;
                }

                $getkasir = UserToko::where('id_toko',$request->id_toko)->where('id_kasir',$request->id_kasir)->first();
                if($getkasir->status == 2){
                    $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                                ->where('id_kasir',$request->id_kasir)
                                // ->whereDate('tgl_penjualan',date("2022-03-14"))
                                // ->whereBetween(DB::raw('DATE(tgl_penjualan)'), array($from_date, $to_date))
                                ->when($status!='', function ($query) use ($status){
                                    return $query->where('status', '=', $status);
                                })
                                ->when($daritgl!='', function ($query) use ($daritgl){
                                    return $query->whereDate('tgl_penjualan', '>=', $daritgl);
                                })
                                ->when($sampaitgl!='', function ($query) use ($sampaitgl){
                                    return $query->whereDate('tgl_penjualan', '<=', $sampaitgl);
                                })
                                // ->when($search!='', function ($query) use ($search){
                                //     return $query->whereRaw(' (nama_br LIKE "%'.$search.'%")');
                                // })
                                ->orderBy('tgl_penjualan','DESC')
                                ->paginate(10);
                }elseif($getkasir->status == 3){
                    $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                                ->when($status!='', function ($query) use ($status){
                                    return $query->where('status', '=', $status);
                                })
                                ->when($kasir!='', function ($query) use ($kasir){
                                    return $query->where('id_kasir', '=', $kasir);
                                })
                                ->when($daritgl!='', function ($query) use ($daritgl){
                                    return $query->whereDate('tgl_penjualan', '>=', $daritgl);
                                })
                                ->when($sampaitgl!='', function ($query) use ($sampaitgl){
                                    return $query->whereDate('tgl_penjualan', '<=', $sampaitgl);
                                })
                                ->orderBy('tgl_penjualan','DESC')
                                ->paginate(10);
                }

                if(count($penjualan) > 0){ 
                    $penjualanrow = $penjualan->getCollection();   
                    $respone =  fractal()
                            ->collection($penjualanrow, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($penjualan))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '1 Selesai, 2 Hutang, 3 Bayar nanti, 4 transaksi batal (Reversal)',
                                                        'metode_bayar_dtl'=> '0. cash, 1. debit card, 2. kredit card, 3. bayar nanti (kasbon)',
                                                    )
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $penjualanrow = $penjualan->getCollection();                    

                    $respone =  fractal()
                            ->collection($penjualanrow, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($penjualan))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Penjualan Masih Kosong!',
                                        'message_error'=> 'Data Kosong',
                                        'catatan'=> ''])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menampilkan data, coba lagi!");
        }
    }

    public function penjualan_detail(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                                ->where('id',$request->id)
                                ->get();

                if(count($penjualan) > 0){   
                    $respone =  fractal()
                            ->collection($penjualan, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '1 Selesai, 2 Hutang, 3 Bayar nanti, 4 transaksi batal (Reversal)',
                                                        'metode_bayar_dtl'=> '0. cash, 1. debit card, 2. kredit card, 3. bayar nanti (kasbon)',
                                                    )
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{                   

                    $respone =  fractal()
                            ->collection($penjualan, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Data Ditemukan!',
                                        'message_error'=> 'Data Kosong',
                                        'catatan'=> ''])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menampilkan data, coba lagi!");
        }
    }

    public function reversal_data(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_kasir' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $getkasir = UserToko::where('id_toko',$request->id_toko)->where('id_kasir',$request->id_kasir)->first();
                if($getkasir->status == 2){
                    $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                                ->where('id_kasir',$request->id_kasir)
                                ->whereDate('tgl_penjualan',date("Y-m-d"))
                                ->whereIn('status',[1, 2, 3])
                                // ->whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))
                                ->orderBy('tgl_penjualan','DESC')
                                ->paginate(10);
                }elseif($getkasir->status == 3){
                    $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                                ->whereDate('tgl_penjualan',date("Y-m-d"))
                                ->whereIn('status',[1, 2, 3])
                                ->orderBy('tgl_penjualan','DESC')
                                ->paginate(10);
                }

                if(count($penjualan) > 0){ 
                    $penjualanrow = $penjualan->getCollection();   
                    $respone =  fractal()
                            ->collection($penjualanrow, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($penjualan))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '1 Selesai, 2 Hutang, 3 Bayar nanti, 4 transaksi batal (Reversal)',
                                                        'metode_bayar_dtl'=> '0. cash, 1. debit card, 2. kredit card, 3. bayar nanti (kasbon)',
                                                    )
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $penjualanrow = $penjualan->getCollection();                    

                    $respone =  fractal()
                            ->collection($penjualanrow, new PenjualanTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($penjualan))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Penjualan Masih Kosong!',
                                        'message_error'=> 'Data Kosong',
                                        'catatan'=> ''])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menampilkan data, coba lagi!");
        }
    }

    public function reversal_aksi(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                       
                $penjualan = PenjualanToko::where('id_toko',$request->id_toko)
                            ->where('id',$request->id)
                            ->whereIn('status',[1, 2, 3])
                            ->first();
                if($penjualan){
                    $detail = PenjualanTokoDetail::where('id_penjualan',$penjualan->id)->get();
                    if(count($detail) > 0){
                        foreach ($detail as $dtl) {
                            if($dtl->id_kategori == 1){
                                ####///////----START---KEMBALIKAN STOK-------/////////////
                                $update_stok = ProdukTokoStok::where('id_barang',$dtl->id_barang)
                                                        ->where('id_toko',$dtl->id_toko)
                                                        ->first();
                                $update_stok->qty = $update_stok->qty + $dtl->qty;
                                $update_stok->save();

                                ///////---KEMBALIKAN HISTORY STOK
                                $update_histstok = HistStok::where('id_stok',$dtl->id_hist_stok)->first();
                                $update_histstok->jlh_keluar = $update_histstok->jlh_keluar - $dtl->qty;
                                $update_histstok->save();
                                ####///////----END---KEMBALIKAN STOK-------/////////////
                            }elseif($dtl->id_kategori == 3){
                                $detailpaket = PenjualanTokoDetailPaket::where('id_penjualan_detail',$dtl->id)->get();
                                foreach ($detailpaket as $dtlpaket) {
                                    if($dtlpaket->id_kategori == 1){
                                        ####///////----START---KEMBALIKAN STOK-------/////////////
                                        $update_stok = ProdukTokoStok::where('id_barang',$dtlpaket->id_barang)
                                                        ->where('id_toko',$dtl->id_toko)
                                                        ->first();

                                        $update_stok->qty = $update_stok->qty + $dtlpaket->qty;
                                        $update_stok->save();

                                        $update_histstok = HistStok::where('id_stok',$dtlpaket->id_hist_stok)->first();
                                        if($update_histstok){
                                            $update_histstok->jlh_keluar = $update_histstok->jlh_keluar - $dtlpaket->qty;
                                            $update_histstok->save();
                                        }
                                        ####///////----END---KEMBALIKAN STOK-------/////////////
                                    }  
                                }   
                            }
                        }

                        $penjualan->status = 4;
                        if($penjualan->save()){
                            $messages = 'Transaksi Berhasil Di Batalkan!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer) 
                                ->serializeWith(new ArraySerializerV2())     
                                ->toArray();
                            return response()->json($respone, 200);
                        }else{
                            $messages = 'Gagal Membatalkan Transaksi (Reversal), coba lagi -s!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new FailsTransformer) 
                                ->serializeWith(new ArraySerializerV2())     
                                ->toArray();
                            return response()->json($respone, 500);    
                        }
                    }else{
                        $messages = 'Gagal Membatalkan Transaksi (Reversal), Detail Penjualan Kosong!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new FailsTransformer) 
                            ->serializeWith(new ArraySerializerV2())     
                            ->toArray();
                        return response()->json($respone, 409); 
                    }
                }else{
                    $messages = 'Gagal Membatalkan Transaksi (Reversal), Data Penjualan Kosong!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new FailsTransformer) 
                        ->serializeWith(new ArraySerializerV2())     
                        ->toArray();
                    return response()->json($respone, 409); 
                } 
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Membatalkan Transaksi (Reversal), coba lagi!");
        }
    }
}