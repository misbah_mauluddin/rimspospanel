<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;

use App\Http\Models\Kategori;
use App\Http\Models\KategoriBeban;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\SuccessTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\KategoriTransformer;
use App\Transformers\KategoriBebanTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class KategoriApiController extends Controller
{
   
////////KATEGORI (TBL_JENIS)-----------------------------
    public function datakategori(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $ktr = Kategori::where('id_toko',$request->id_toko)->get();
              if(count($ktr) > 0){
                    $toko = $request->id_toko;
                    $respone =  fractal()
                            ->item($toko)
                            ->transformWith(new KategoriTransformer)
                            ->serializeWith(new ArraySerializer)
                            ->toArray();
                    // $respone =  fractal()
                    //     ->collection($ktr, new KategoriTransformer, 'data')
                    //     ->serializeWith(new ArraySerializerV2())
                    //     // ->paginateWith(new IlluminatePaginatorAdapter($pelangganpaging))
                    //     ->toArray();
                    return response()->json($respone, 200);
              }else{
                    $messages = 'Kategori Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_kategori(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'jenis_brg' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = new Kategori();
                    $addktr->id_toko = $request->id_toko;
                    $addktr->jenis_brg = $request->jenis_brg;
                    $addktr->status = 1;
                    

                    if($addktr->save()){
                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tambah kategori tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Tambah kategori tidak berhasil, silahkan coba kembali!");
        }

    }

    public function edit_kategori(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_jenis' => 'required',
                        'id_toko' => 'required',
                        'jenis_brg' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Kategori::where('id_jenis',$request->id_jenis)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        $addktr->jenis_brg = $request->jenis_brg;
                        if($addktr->save()){
                            $messages = 'Data Kategori Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Edit kategori tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id kategori Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Edit kategori tidak berhasil, silahkan coba kembali!");
        }

    }

    public function hapus_kategori(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_jenis' => 'required',
                        'id_toko' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Kategori::where('id_jenis',$request->id_jenis)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data Kategori Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus kategori tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id kategori Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus kategori tidak berhasil, silahkan coba kembali!");
        }

    }

////////KATEGORI BEBAN (tbl_kategoribeban)-----------------------------
    public function datakategoribeban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $ktr = KategoriBeban::where('id_toko',$request->id_toko)->get();
              if(count($ktr) > 0){
                    $toko = $request->id_toko;
                    $respone =  fractal()
                            ->item($toko)
                            ->transformWith(new KategoriBebanTransformer)
                            ->serializeWith(new ArraySerializer)
                            ->toArray();
                    return response()->json($respone, 200);
              }else{
                    $messages = 'Kategori Beban Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_kategori_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'kategori' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = new KategoriBeban();
                    $addktr->id_toko = $request->id_toko;
                    $addktr->kategori = $request->kategori;
                    

                    if($addktr->save()){
                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tambah kategori Beban tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Tambah kategori Beban tidak berhasil, silahkan coba kembali!");
        }

    }

    public function edit_kategori_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_ktr_beban' => 'required',
                        'id_toko' => 'required',
                        'kategori' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = KategoriBeban::where('id_ktr_beban',$request->id_ktr_beban)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        $addktr->kategori = $request->kategori;
                        if($addktr->save()){
                            $messages = 'Data Kategori Beban Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Edit kategori Beban tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id kategori Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Edit kategori Beban tidak berhasil, silahkan coba kembali!");
        }

    }

    public function hapus_kategori_beban(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_ktr_beban' => 'required',
                        'id_toko' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = KategoriBeban::where('id_ktr_beban',$request->id_ktr_beban)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data Kategori Beban Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus kategori Beban tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id kategori Beban / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus kategori Beban tidak berhasil, silahkan coba kembali!");
        }

    }
}