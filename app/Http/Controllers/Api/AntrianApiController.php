<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\Antrian;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\LoginTransformer;
use App\Transformers\AntrianTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\FailsTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class AntrianApiController extends Controller
{
    

////// USER KASIR DAN ADMIN
    public function antrian_pending(Request $request){
        try {

            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = Antrian::where('id_toko',$request->id_toko)->where('status', 0)->paginate(10);
                if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $dtl = $ktr->getCollection();        
                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> false,
                                        'error'=> true,
                                        'message'=> 'Tidak ada Data Ditemukan',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function antrian(Request $request){
        try {

            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = Antrian::where('id_toko',$request->id_toko)->whereIn('status', ['1','2'])->whereDate('tanggal', date("Y-m-d"))->paginate(10);
                if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $dtl = $ktr->getCollection();        
                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> false,
                                        'error'=> true,
                                        'message'=> 'Tidak ada Data Ditemukan',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function antrian_all(Request $request){
        try {

            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = Antrian::where('id_toko',$request->id_toko)->orderBy('tanggal',"DESC")->paginate(10);
                if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Data Berhasil Ditampilkan!',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $dtl = $ktr->getCollection();        
                    $respone =  fractal()
                          ->collection($dtl, new AntrianTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> false,
                                        'error'=> true,
                                        'message'=> 'Tidak ada Data Ditemukan',
                                        'message_error'=> '',
                                        'catatan'=> array(
                                                        'status_dtl'=> '0 Proses Persetujuan, 1 disetujui, 2 di tolak',
                                                    ),
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function antrianDetail(Request $request){
        try {
            
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = Antrian::where('id_toko',$request->id_toko)->where('id', $request->id)->first();
                if($ktr){               

                    $respone =  fractal()
                                ->item($ktr)
                                ->transformWith(new AntrianTransformer) 
                                ->serializeWith(new ArraySerializerV2())     
                                ->toArray();
                            return response()->json($respone, 200);
                      return response()->json($respone, 200);
                }else{
                    $messages = 'Gagal Menampilkan data, coba lagi -s!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new FailsTransformer) 
                        ->serializeWith(new ArraySerializerV2())     
                        ->toArray();
                    return response()->json($respone, 500);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function antrianUpdate(Request $request){
        try {
            
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id' => 'required',
                        'status' => 'required',
                        'id_pekerja' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = Antrian::where('id_toko',$request->id_toko)->where('id', $request->id)->first();
                if($ktr){               
                    $ktr->status = $request->status;
                    $ktr->id_pekerja = $request->id_pekerja;
                    if($request->tanggal != ''){
                        $ktr->tanggal = date("Y-m-d H:i:s",strtotime($request->tanggal));
                    }

                    
                    if($ktr->save()){
                        $messages = 'Antrian Berhasil Di update!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer) 
                            ->serializeWith(new ArraySerializerV2())     
                            ->toArray();
                        return response()->json($respone, 200);
                    }else{
                        $messages = 'Gagal Merubah data, coba lagi -s!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new FailsTransformer) 
                            ->serializeWith(new ArraySerializerV2())     
                            ->toArray();
                        return response()->json($respone, 500); 
                    }
                    
                }else{
                    $messages = 'Gagal Merubah data, coba lagi -empty data!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new FailsTransformer) 
                        ->serializeWith(new ArraySerializerV2())     
                        ->toArray();
                    return response()->json($respone, 409);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

}