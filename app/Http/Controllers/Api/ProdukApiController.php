<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;
use App\Transformers\ProdukDetailTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class ProdukApiController extends Controller
{

////// PRODUK
    public function data_produk_all(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = ProdukToko::where('id_toko',$request->id_toko)
                        ->whereRaw('status = 1 AND (nama_br LIKE "%'.$request->search.'%")')
                        ->orderBy('nama_br','ASC')
                        ->paginate(10);
                if(count($produk) > 0){
                    $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produkrow, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produkrow, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Produk Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function data_produk_bykategori(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_jenis' => 'required',

                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = ProdukToko::where('id_toko',$request->id_toko)
                        ->where('id_jenis',$request->id_jenis)
                        ->whereRaw('status = 1 AND (nama_br LIKE "%'.$request->search.'%")')
                        ->orderBy('nama_br','ASC')
                        ->paginate(10);
                if(count($produk) > 0){
                    $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produkrow, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produkrow, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Produk Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function data_produk_detail(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required',

                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = ProdukToko::where('id_toko',$request->id_toko)
                        ->where('id_barang',$request->id_barang)
                        ->first();
                if($produk){  

                    $respone =  fractal()
                        ->item($produk)
                        ->transformWith(new ProdukDetailTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200);
                }else{
                    $messages = 'Id Barang Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    public function tambah_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_user' => 'required',
                        'id_toko' => 'required',
                        'id_jenis' => 'required',
                        'id_kategori' => 'required',
                        'nama_br' => 'required',
                        'deskripsi' => 'required',
                        // 'jumlah_semua' => 'required', //STOK
                        // 'harga_satuan' => 'required', //HARGA BRG
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                    $data                               = new ProdukToko();
                    $data->id_user                      = $request->id_user;
                    $data->id_toko                = $request->id_toko;
                    $data->id_jenis                = $request->id_jenis;
                    $data->id_kategori                = $request->id_kategori;
                    $data->nama_br                = $request->nama_br;
                    $data->deskripsi                = $request->deskripsi;

                    if($request->jumlah_semua == ''){
                        if($request->id_kategori == 1){
                            $data->jumlah_semua                = 0;
                        }else{
                            $data->jumlah_semua                = 1;
                        }
                    }else{
                        $data->jumlah_semua                = $request->jumlah_semua;
                    }

                    

                    if($request->id_kategori == 3){
                        if($request->metode_bagihasil == 0){ ///BERDASARKAN PAKET BAGI HASIL
                            $data->metode_bagihasil                = $request->metode_bagihasil;
                            $data->harga_satuan                = $request->harga_satuan;
                        }elseif($request->metode_bagihasil == 1){ ///BERDASARKAN ITEM BAGI HASIL
                            $data->metode_bagihasil                = $request->metode_bagihasil;
                            $data->harga_satuan                = 0;
                        }
                    }else{
                        $data->harga_satuan                = $request->harga_satuan;
                    }
                    
                    
                    $data->tgl_masuk             = date("Y-m-d");
                    $data->tgl_update            = date("Y-m-d");
                    $data->status                = 1;
                    
                    if (Input::file('foto')) {
                      $image = $request->file('foto');
                      $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                      $destinationPath = ('uploads/produk');
                      $img = Image::make($image->getRealPath());
                      $img->resize(500, 500, function ($constraint) {
                          $constraint->aspectRatio();
                      })->save(public_path($destinationPath.'/'.$input['imagename']));

                      $image->move(public_path($destinationPath, '/'.$input['imagename']));

                      $direktori = $destinationPath.'/'.$input['imagename'];

                      $data->foto         = $direktori;
                    }


                    if($data->save()){
                        if($data->id_kategori == 1){ ///BARANG
                            $addstok = new ProdukTokoStok();
                            $addstok->id_barang = $data->id_barang;
                            $addstok->id_toko = $data->id_toko;
                            $addstok->qty = $data->jumlah_semua;
                            $addstok->harga = $data->harga_satuan;
                            $addstok->save();

                            $addstokhist = new HistStok();
                            $addstokhist->id_toko = $data->id_toko;
                            $addstokhist->id_user = $data->id_user;
                            $addstokhist->id_barang = $data->id_barang;
                            $addstokhist->nota = '-';
                            $addstokhist->modal = 0;
                            $addstokhist->harga_jual = $data->harga_satuan;
                            $addstokhist->jlh_masuk = $data->jumlah_semua;
                            $addstokhist->jlh_keluar = 0;
                            $addstokhist->tanggal = date("Y-m-d");
                            $addstokhist->save(); 

                        }
                        // elseif($data->id_kategori == 2){ ///JASA
                        //     $addstok = new ProdukTokoStok();
                        //     $addstok->id_barang = $data->id_barang;
                        //     $addstok->id_toko = $data->id_toko;
                        //     $addstok->qty = $data->jumlah_semua;
                        //     $addstok->harga = $data->harga_satuan;
                        //     $addstok->save();

                        //     $addstokhist = new HistStok();
                        //     $addstokhist->id_toko = $data->id_toko;
                        //     $addstokhist->id_user = $data->id_user;
                        //     $addstokhist->id_barang = $data->id_barang;
                        //     $addstokhist->nota = '-';
                        //     $addstokhist->modal = 0; 
                        //     $addstokhist->harga_jual = $data->harga_satuan;
                        //     $addstokhist->jlh_masuk = $data->jumlah_semua;
                        //     $addstokhist->jlh_keluar = 0;
                        //     $addstokhist->tanggal = date("Y-m-d");
                        //     $addstokhist->save(); 

                        // }elseif($data->id_kategori == 3){ ///PAKET
                        //     $addstok = new ProdukTokoStok();
                        //     $addstok->id_barang = $data->id_barang;
                        //     $addstok->id_toko = $data->id_toko;
                        //     $addstok->qty = $data->jumlah_semua;
                        //     $addstok->harga = $data->harga_satuan;
                        //     $addstok->save();

                        //     $addstokhist = new HistStok();
                        //     $addstokhist->id_toko = $data->id_toko;
                        //     $addstokhist->id_user = $data->id_user;
                        //     $addstokhist->id_barang = $data->id_barang;
                        //     $addstokhist->nota = '-';
                        //     $addstokhist->modal = 0; 
                        //     $addstokhist->harga_jual = $data->harga_satuan;
                        //     $addstokhist->jlh_masuk = $data->jumlah_semua;
                        //     $addstokhist->jlh_keluar = 0;
                        //     $addstokhist->tanggal = date("Y-m-d");
                        //     $addstokhist->save(); 
                        // }
                        

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tambah Produk tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function edit_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                        'id_jenis' => 'required',
                        'nama_br' => 'required',
                        'deskripsi' => 'required',
                        // 'harga_satuan' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                    $data                               = ProdukToko::where('id_barang',$request->id_barang)->where('id_toko',$request->id_toko)->first();
                    $data->id_jenis                = $request->id_jenis;
                    $data->nama_br                = $request->nama_br;
                    $data->deskripsi                = $request->deskripsi;
                    
                    $data->harga_satuan                = $request->harga_satuan;
                  
                    
                    $data->tgl_update            = date("Y-m-d");
                    
                    if (Input::file('foto')) {
                        if ($data->foto != "") {
                            $path = $data->foto;
                            unlink(public_path($path));
                        }
                      $image = $request->file('foto');
                      $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                      $destinationPath = ('uploads/produk');
                      $img = Image::make($image->getRealPath());
                      $img->resize(500, 500, function ($constraint) {
                          $constraint->aspectRatio();
                      })->save(public_path($destinationPath.'/'.$input['imagename']));

                      $image->move(public_path($destinationPath, '/'.$input['imagename']));

                      $direktori = $destinationPath.'/'.$input['imagename'];

                      $data->foto         = $direktori;
                    }


                    if($data->save()){
                        if($data->id_kategori == 1){
                            $addstok = ProdukTokoStok::where('id_toko',$data->id_toko)->where('id_barang',$data->id_barang)->first();
                            $addstok->harga = $data->harga_satuan;
                            $addstok->save();

                            $addstokhist = HistStok::where('id_toko',$data->id_toko)->where('id_barang',$data->id_barang)->orderBy('id_stok','DESC')->first();
                            $addstokhist->harga_jual = $data->harga_satuan;
                            $addstokhist->save();    
                        }
                                                                

                        $messages = 'Data Berhasil Diperbaharui';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Edit Produk tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function hapus_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = ProdukToko::where('id_barang', $request->id_barang)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        $addktr->status = 2;
                        if($addktr->save()){
                            $messages = 'Data Produk Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus Produk tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id Produk / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus Produk tidak berhasil, silahkan coba kembali!");
        }

    }


}