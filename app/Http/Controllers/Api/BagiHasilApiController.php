<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class BagiHasilApiController extends Controller
{


/////BAGI HASIL
    public function data_bagihasil_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $cekbrg = ProdukToko::where('id_barang',$request->id_barang)->first();
                if($cekbrg->id_kategori == 3){
                    if($cekbrg->metode_bagihasil == 0){
                        $produk = BagiHasil::where('id_barang',$request->id_barang)->get();
                    }elseif($cekbrg->metode_bagihasil == 1){
                        $getpkt = ProdukPaket::where('id_paket',$request->id_barang)->first();
                        $produk = BagiHasil::where('id_paket',$getpkt->id)->get();
                    }
                    
                }else{
                    $produk = BagiHasil::where('id_barang',$request->id_barang)->get();
                }
                
                if(count($produk) > 0){               

                    $respone =  fractal()
                          ->collection($produk, new BagihasilTransformer(), 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{               

                    $respone =  fractal()
                          ->collection($produk, new BagihasilTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Data Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_bagihasil_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                        'id_pekerja' => 'required',
                        'metode' => 'required', ////Metode Bagi hasil
                        'bagihasil' => 'required', 
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekpaket = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                    if($cekpaket){
                        $messages = 'Pekerja Sudah Terdaftar';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{
                            $cekbrg = ProdukToko::where('id_barang',$request->id_barang)->first();
                            if($cekbrg->id_kategori == 1){
                                
                                $getstok = ProdukTokoStok::where('id_barang',$request->id_barang)->first();
                                if($getstok){
                                    $hrga = $getstok->harga;
                                }else{
                                    $hrga = '0';
                                }

                                $addbgihasil = new BagiHasil();
                                $addbgihasil->id_barang = $request->id_barang;
                                $addbgihasil->id_petugas = $request->id_pekerja;
                                $addbgihasil->metode = $request->metode;

                                if($request->metode == 'persen'){
                                    $hitung = ($request->bagihasil/100)*$hrga;
                                    $addbgihasil->nilai = $hitung;
                                }elseif($request->metode == 'nominal'){
                                    $addbgihasil->nilai = $request->bagihasil;
                                }

                                $addbgihasil->save();     

                            }else{
                                // $getpkt = ProdukPaket::where('id_paket',$request->id_barang)->first();
                                
                                $hrga = $cekbrg->harga_satuan;

                                $addbgihasil = new BagiHasil();
                                $addbgihasil->id_barang = $request->id_barang;
                                $addbgihasil->id_petugas = $request->id_pekerja;
                                $addbgihasil->metode = $request->metode;

                                if($request->metode == 'persen'){
                                    $hitung = ($request->bagihasil/100)*$hrga;
                                    $addbgihasil->nilai = $hitung;
                                }elseif($request->metode == 'nominal'){
                                    $addbgihasil->nilai = $request->bagihasil;
                                }
                                
                                $addbgihasil->save();

                            }

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                            
                    }
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function edit_bagihasil_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_bagihasil' => 'required',
                        'id_barang' => 'required',
                        'id_pekerja' => 'required',
                        'metode' => 'required', ////Metode Bagi hasil
                        'bagihasil' => 'required', 
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                        
                        $cekbrg = ProdukToko::where('id_barang',$request->id_barang)->first();
                        if($cekbrg->id_kategori == 1){
                            $getstok = ProdukTokoStok::where('id_barang',$request->id_barang)->first();
                            if($getstok){
                                $hrga = $getstok->harga;
                            }else{
                                $hrga = '0';
                            }
                        }else{
                            $hrga = $cekbrg->harga_satuan;
                        }
                        

                        $addbgihasil = BagiHasil::where('id',$request->id_bagihasil)
                                        ->where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                        if($addbgihasil){

                            // $addbgihasil->id_barang = $request->id_barang;
                            // $addbgihasil->id_petugas = $request->id_pekerja;
                            $addbgihasil->metode = $request->metode;
                            if($request->metode == 'persen'){
                                $hitung = ($request->bagihasil/100)*$hrga;
                                $addbgihasil->nilai = $hitung;
                            }elseif($request->metode == 'nominal'){
                                $addbgihasil->nilai = $request->bagihasil;
                            }

                            $addbgihasil->save();

                            $messages = 'Data Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200);
                        }else{
                            $messages = 'Id bagi hasil/ id barang  Tidak Ditemukan!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer) 
                                ->serializeWith(new ArraySerializer)     
                                ->toArray();
                            return response()->json($respone, 401); 
                        }
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function hapus_bagihasil_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_bagihasil' => 'required',
                        'id_barang' => 'required',
                        'id_pekerja' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                        
                        $addbgihasil = BagiHasil::where('id',$request->id_bagihasil)
                                        ->where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->delete();

                        $messages = 'Data Berhasil Dihapus';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                            
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menghapus data, coba lagi!");
        }

    }

}