<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\FailsTransformer;
use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\LoginTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class LoginApiController extends Controller
{
    private function cleannohp($nomor){
        $hp = '';
        if(substr($nomor,0,1) === '0'){
            $hp = $nomor;
        } elseif(substr($nomor,0,1) === '+'){
            if(substr($nomor,0,3) === '+62'){
                $hp = substr_replace($nomor,'0',0,3);
            } else{
                $hp = substr_replace($nomor,'0',0,1);
            }
        }
        return $hp;
    }

    public function Login(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'nomor_hp' => 'required',
                        'pass' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ceklogin = UserToko::where('nomor_hp',$this->cleannohp($request->nomor_hp))->where('pass',md5($request->pass))->first();
                if($ceklogin){
                    $ceksttstoko = Toko::where('id_toko',$ceklogin->id_toko)->first();
                    if($ceksttstoko->status == '0'){
                        $messages = 'Toko Anda Diblokir, Silahkan Menghubungi RIMS!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)      
                            ->serializeWith(new ArraySerializer)
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{
                        if($ceklogin->status == 4){
                            $messages = 'Anda Tidak memiliki akses login, Silahkan Menghubungi Admin!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new FailsTransformer)      
                                ->serializeWith(new ArraySerializer)
                                ->toArray();
                            return response()->json($respone, 409); 
                        }else{
                            if($ceklogin->stts_user == '0'){
                                $messages = 'Akun Anda Diblokir, Silahkan Menghubungi RIMS!';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new FailsTransformer)      
                                    ->serializeWith(new ArraySerializer)
                                    ->toArray();
                                return response()->json($respone, 409); 
                            }else{
                                $kasir = $ceklogin->id_kasir;

                                $respone =  fractal()
                                    ->item($kasir)
                                    ->transformWith(new LoginTransformer)
                                    ->serializeWith(new ArraySerializer)
                                    ->toArray();
                                return response()->json($respone, 200);
                            }
                        }
                        
                    }
                }else{
                    $messages = 'Nomor Hp atau Password salah!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    
}