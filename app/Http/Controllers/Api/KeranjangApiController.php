<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\Keranjang;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;
use App\Transformers\KeranjangTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class KeranjangApiController extends Controller
{

//////KERANJANG
    public function data_keranjang(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'meja' => 'required',
                        'id_kasir' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = Keranjang::where('id_toko',$request->id_toko)
                        ->where('id_kasir',$request->id_kasir)
                        ->where('meja',$request->meja)
                        ->where('status','0')
                        ->orderBy('tgl','DESC')
                        ->get();
                if(count($produk) > 0){                    
                    $sumkeranjang = Keranjang::select(DB::raw('SUM(qty) as total_item, SUM(harga_brg*qty) as subtotal,SUM(harga_brg*qty)-SUM(diskon_brg) as total'))
                                    ->where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->where('status','0')
                                    ->first();
                    $respone =  fractal()
                          ->collection($produk, new KeranjangTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['subtotal'=> intval($sumkeranjang->subtotal),
                                        'total'=> intval($sumkeranjang->total),
                                        'success'=> true,
                                        'error'=> false
                                        ])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $respone =  fractal()
                          ->collection($produk, new KeranjangTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Keranjang Masih Kosong!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_keranjang_old(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'meja' => 'required',
                        'id_kasir' => 'required',
                        'id_barang' => 'required',
                        'id_kategori' => 'required',
                        'id_pekerja' => 'required', //BARBER MAN (KANG PANGKAS)
                        'diskon_brg' => 'required',
                        'qty' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    /* METODE BAGI HASIL BERDASARKAN KATEGORI
                    1. Barang   ( Bagi hasil Biasa ) ex: 60%-40%
                    2. Jasa     ( Bagi hasil Biasa ) ex: 60%-40%
                    3. Paket    ( ada 2 tipe bagi hasil untuk paket )
                        3.0 Berdasarkan Paket (0)
                        3.1 Berdasarkan Isi Item Paket (1)
                    */
                    $getstok = ProdukTokoStok::where('id_toko',$request->id_toko)->where('id_barang',$request->id_barang)->first();
                    if($request->id_kategori == 1){
                        
                        if($getstok){
                            $stk = $getstok->qty;
                            $hrga = $getstok->harga;

                            if($getstok->qty == 0){
                                $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new ErorrTransformer) 
                                    ->serializeWith(new ArraySerializer)     
                                    ->toArray();
                                return response()->json($respone, 409); 
                            }elseif($getstok->qty < 0){
                                $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new ErorrTransformer) 
                                    ->serializeWith(new ArraySerializer)     
                                    ->toArray();
                                return response()->json($respone, 409); 
                            }else{
                                $getbrg = ProdukToko::where('id_barang',$request->id_barang)->first();
                                ####///////----START---CEK HISTORY STOK-------/////////////
                                $cek_histstok = HistStok::where('id_barang',$getbrg->id_barang)
                                                            ->where('id_toko',$getbrg->id_toko)
                                                            ->whereRaw('jlh_masuk > jlh_keluar')
                                                            ->orderBy('tanggal','ASC')
                                                            ->first();
                                if($cek_histstok){
                                    $cekkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                                    ->where('id_barang',$request->id_barang)
                                                    ->where('id_pekerja',$request->id_pekerja)
                                                    // ->where('diskon_brg',$request->diskon_brg)
                                                    ->where('meja',$request->meja)
                                                    ->first();

                                    if($cekkeranjang){
                                        // if($request->diskon_brg == ''){
                                        //     $dskon = '0';
                                        // }else{
                                        //     $dskon = $request->diskon_brg; 
                                        // }
                                        $dskon = $cekkeranjang->diskon_brg+$request->diskon_brg;
                                        $cekkeranjang->diskon_brg = $dskon;
                                        $tentukanharga = $hrga*($cekkeranjang->qty+$request->qty)-$dskon;
                                        
                                        ##///////----START---ATUR BAGI HASIL-------/////////////
                                        
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                            
                                        $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                        $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));

                                        /////BARANG BAGI HASIL SEKALI AJA
                                        // $cekkeranjang->upah_pekerja = $getbagihasil->nilai;
                                        // $cekkeranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                            
                                        
                                        ####///////----END---ATUR BAGI HASIL-------/////////////

                                        $cekkeranjang->qty = $cekkeranjang->qty+$request->qty;
                                        $cekkeranjang->total =  $tentukanharga;
                                        $cekkeranjang->tgl = date("Y-m-d H:i:s");
                                        $cekkeranjang->save();

                                    }else{
                                        $keranjang = new Keranjang();
                                        $keranjang->id_toko     = $request->id_toko;
                                        $keranjang->meja        = $request->meja;
                                        $keranjang->id_barang   = $request->id_barang;
                                        $keranjang->id_kategori = $request->id_kategori;
                                        $keranjang->id_kasir    = $request->id_kasir;
                                        $keranjang->id_pekerja  = $request->id_pekerja;  
                                        
                                        $keranjang->nama_brg = $getbrg->nama_br;
                                        $keranjang->harga_brg = $hrga;

                                        if($request->diskon_brg == ''){
                                            $dskon = '0'; 
                                        }else{
                                            $dskon = $request->diskon_brg; 
                                        }
                                        $keranjang->diskon_brg = $dskon; 
                                        
                                        
                                        ####///////----START---ATUR BAGI HASIL-------/////////////
                                        if($request->qty > 1){
                                            $tentukanharga = ($hrga*$request->qty)-$dskon;
                                            
                                            $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();

                                            $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                            $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);

                                            /////BARANG BAGI HASIL SEKALI AJA
                                            // $keranjang->upah_pekerja = $getbagihasil->nilai;
                                            // $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                                
                                            
                                        }else{
                                            $tentukanharga = $hrga-$dskon;
                                            $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                                
                                            $keranjang->upah_pekerja = $getbagihasil->nilai;
                                            $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                            
                                        }
                                        ####///////----END---ATUR BAGI HASIL-------/////////////
                                        

                                        $keranjang->qty = $request->qty;
                                        $keranjang->total = $tentukanharga;
                                        $keranjang->status = 0;
                                        $keranjang->tgl = date("Y-m-d H:i:s");

                                        $keranjang->save();
                                    }

                                    $messages = 'Data Berhasil Ditambah';
                                    $respone =  fractal()
                                        ->item($messages)
                                        ->transformWith(new SuccessTransformer)
                                        ->serializeWith(new ArraySerializer)        
                                        ->toArray();
                                    return response()->json($respone, 200); 
                                }else{
                                    $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                    $respone =  fractal()
                                        ->item($messages)
                                        ->transformWith(new ErorrTransformer) 
                                        ->serializeWith(new ArraySerializer)     
                                        ->toArray();
                                    return response()->json($respone, 409);
                                }  
                                ####///////----END---CEK HISTORY STOK-------/////////////
                                
                            }
                        }else{
                            $messages = 'Tidak Ada Stok Ditemukan!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer) 
                                ->serializeWith(new ArraySerializer)     
                                ->toArray();
                            return response()->json($respone, 401); 
                        }
                    }else{
                        $getbrg = ProdukToko::where('id_barang',$request->id_barang)->first();

                        ####///////----START---CEK STOK-------/////////////
                        if(empty($getstok)){
                            $addstok = new ProdukTokoStok();
                            $addstok->id_barang = $getbrg->id_barang;
                            $addstok->id_toko = $getbrg->id_toko;
                            $addstok->qty = $getbrg->jumlah_semua;
                            $addstok->harga = $getbrg->harga_satuan;
                            $addstok->save();
                        }
                        ####///////----START---CEK STOK-------/////////////

                        $stk = $addstok->qty;
                        $hrga = $addstok->harga;

                        
                        ####///////----START---CEK HISTORY STOK-------/////////////
                        $cek_histstok = HistStok::where('id_barang',$getbrg->id_barang)
                                                    ->where('id_toko',$getbrg->id_toko)
                                                    ->whereRaw('jlh_masuk > jlh_keluar')
                                                    ->orderBy('tanggal','ASC')
                                                    ->first();
                        if(empty($cek_histstok)){
                            $addstokhist = new HistStok();
                            $addstokhist->id_toko = $getbrg->id_toko;
                            $addstokhist->id_user = $getbrg->id_user;
                            $addstokhist->id_barang = $getbrg->id_barang;
                            $addstokhist->nota = '-';
                            $addstokhist->modal = 0;
                            $addstokhist->harga_jual = $getbrg->harga_satuan;
                            $addstokhist->jlh_masuk = $getbrg->jumlah_semua;
                            $addstokhist->jlh_keluar = 0;
                            $addstokhist->tanggal = date("Y-m-d");
                            $addstokhist->save(); 
                        } 
                        ####///////----END---CEK HISTORY STOK-------/////////////
                        $cekkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                        ->where('id_barang',$request->id_barang)
                                        ->where('id_pekerja',$request->id_pekerja)
                                        // ->where('diskon_brg',$request->diskon_brg)
                                        ->where('meja',$request->meja)
                                        ->first();

                        if($cekkeranjang){
                            // if($request->diskon_brg == ''){
                            //     $dskon = '0';
                            // }else{
                            //     $dskon = $request->diskon_brg; 
                            // }
                            $dskon = $cekkeranjang->diskon_brg+$request->diskon_brg;
                            $cekkeranjang->diskon_brg = $dskon;
                            $tentukanharga = $hrga*($cekkeranjang->qty+$request->qty)-$dskon;
                            
                            ####///////----START---ATUR BAGI HASIL-------/////////////
                            if($getbrg->id_kategori == 3){
                                if($getbrg->metode_bagihasil ==  0){
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                    $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                    $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));
                                }elseif($getbrg->metode_bagihasil ==  1){

                                    $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                    $bghsl = 0;
                                    foreach ($produkpaket as $prdkpket) {
                                        $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                        $bghsl += $getbagihasil->nilai;
                                    }
                                    $cekkeranjang->upah_pekerja = $bghsl*($cekkeranjang->qty+$request->qty);
                                    $cekkeranjang->bagian_owner = $tentukanharga-($bghsl*($cekkeranjang->qty+$request->qty));
                                }
                            }elseif($getbrg->id_kategori == 2){
                                $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));
                            }
                            elseif($getbrg->id_kategori == 1){
                                $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));

                                /////BARANG BAGI HASIL SEKALI AJA
                                // $cekkeranjang->upah_pekerja = $getbagihasil->nilai;
                                // $cekkeranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                
                            } 
                            ####///////----END---ATUR BAGI HASIL-------/////////////

                            $cekkeranjang->qty = $cekkeranjang->qty+$request->qty;
                            $cekkeranjang->total =  $tentukanharga;
                            $cekkeranjang->tgl = date("Y-m-d H:i:s");
                            $cekkeranjang->save();

                        }else{
                            $keranjang = new Keranjang();
                            $keranjang->id_toko     = $request->id_toko;
                            $keranjang->meja        = $request->meja;
                            $keranjang->id_barang   = $request->id_barang;
                            $keranjang->id_kategori = $request->id_kategori;
                            $keranjang->id_kasir    = $request->id_kasir;
                            $keranjang->id_pekerja  = $request->id_pekerja;  
                            
                            $keranjang->nama_brg = $getbrg->nama_br;
                            $keranjang->harga_brg = $hrga;

                            if($request->diskon_brg == ''){
                                $dskon = '0'; 
                            }else{
                                $dskon = $request->diskon_brg; 
                            }
                            $keranjang->diskon_brg = $dskon; 
                            
                            
                            ####///////----START---ATUR BAGI HASIL-------/////////////
                            if($request->qty > 1){
                                $tentukanharga = ($hrga*$request->qty)-$dskon;
                                if($getbrg->id_kategori == 3){
                                    if($getbrg->metode_bagihasil ==  0){
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                        $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                        $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);
                                    }elseif($getbrg->metode_bagihasil ==  1){
                                        $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                        $bghsl = 0;
                                        foreach ($produkpaket as $prdkpket) {
                                            $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                            $bghsl += $getbagihasil->nilai;
                                        }
                                        $keranjang->upah_pekerja = $bghsl*$request->qty;
                                        $keranjang->bagian_owner = $tentukanharga-($bghsl*$request->qty);
                                    }
                                }elseif($getbrg->id_kategori == 2){
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                    $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                    $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);
                                }elseif($getbrg->id_kategori == 1){ 
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();

                                    $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                    $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);

                                    /////BARANG BAGI HASIL SEKALI AJA
                                    // $keranjang->upah_pekerja = $getbagihasil->nilai;
                                    // $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                    
                                } 
                            }else{
                                $tentukanharga = $hrga-$dskon;
                                if($getbrg->id_kategori == 3){
                                    if($getbrg->metode_bagihasil ==  0){////PAKET BAGI HASIL PER PAKET
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                        $keranjang->upah_pekerja = $getbagihasil->nilai;
                                        $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                    }elseif($getbrg->metode_bagihasil ==  1){ ////PAKET BAGI HASIL PER ITEM
                                        $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                        $bghsl = 0;
                                        foreach ($produkpaket as $prdkpket) {
                                            $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                            $bghsl += $getbagihasil->nilai;
                                        }
                                        // return $bghsl;
                                        $keranjang->upah_pekerja = $bghsl;
                                        $keranjang->bagian_owner = $tentukanharga-$bghsl;
                                    }
                                }else{
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                    $keranjang->upah_pekerja = $getbagihasil->nilai;
                                    $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                } 
                            }
                            ####///////----END---ATUR BAGI HASIL-------/////////////
                            

                            $keranjang->qty = $request->qty;
                            $keranjang->total = $tentukanharga;
                            $keranjang->status = 0;
                            $keranjang->tgl = date("Y-m-d H:i:s");

                            $keranjang->save();
                        }

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function tambah_keranjang(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'meja' => 'required',
                        'id_kasir' => 'required',
                        'id_barang' => 'required',
                        'id_kategori' => 'required',
                        'id_pekerja' => 'required', //BARBER MAN (KANG PANGKAS)
                        'diskon_brg' => 'required',
                        'qty' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    /* METODE BAGI HASIL BERDASARKAN KATEGORI
                    1. Barang   ( Bagi hasil Biasa ) ex: 60%-40%
                    2. Jasa     ( Bagi hasil Biasa ) ex: 60%-40%
                    3. Paket    ( ada 2 tipe bagi hasil untuk paket )
                        3.0 Berdasarkan Paket (0)
                        3.1 Berdasarkan Isi Item Paket (1)
                    */
                    $getstok = ProdukTokoStok::where('id_toko',$request->id_toko)->where('id_barang',$request->id_barang)->first();
                    if($request->id_kategori == 1){
                        
                        if($getstok){
                            $stk = $getstok->qty;
                            $hrga = $getstok->harga;

                            if($getstok->qty == 0){
                                $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new ErorrTransformer) 
                                    ->serializeWith(new ArraySerializer)     
                                    ->toArray();
                                return response()->json($respone, 409); 
                            }elseif($getstok->qty < 0){
                                $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new ErorrTransformer) 
                                    ->serializeWith(new ArraySerializer)     
                                    ->toArray();
                                return response()->json($respone, 409); 
                            }else{
                                $getbrg = ProdukToko::where('id_barang',$request->id_barang)->first();
                                ####///////----START---CEK HISTORY STOK-------/////////////
                                $cek_histstok = HistStok::where('id_barang',$getbrg->id_barang)
                                                            ->where('id_toko',$getbrg->id_toko)
                                                            ->whereRaw('jlh_masuk > jlh_keluar')
                                                            ->orderBy('tanggal','ASC')
                                                            ->first();
                                if($cek_histstok){
                                    $cekkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                                    ->where('id_barang',$request->id_barang)
                                                    ->where('id_pekerja',$request->id_pekerja)
                                                    // ->where('diskon_brg',$request->diskon_brg)
                                                    ->where('meja',$request->meja)
                                                    ->first();

                                    if($cekkeranjang){
                                        // if($request->diskon_brg == ''){
                                        //     $dskon = '0';
                                        // }else{
                                        //     $dskon = $request->diskon_brg; 
                                        // }
                                        $dskon = $cekkeranjang->diskon_brg+$request->diskon_brg;
                                        $cekkeranjang->diskon_brg = $dskon;
                                        $tentukanharga = $hrga*($cekkeranjang->qty+$request->qty)-$dskon;
                                        
                                        ##///////----START---ATUR BAGI HASIL-------/////////////
                                        
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                            
                                        $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                        $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));

                                        /////BARANG BAGI HASIL SEKALI AJA
                                        // $cekkeranjang->upah_pekerja = $getbagihasil->nilai;
                                        // $cekkeranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                            
                                        
                                        ####///////----END---ATUR BAGI HASIL-------/////////////

                                        $cekkeranjang->qty = $cekkeranjang->qty+$request->qty;
                                        $cekkeranjang->total =  $tentukanharga;
                                        $cekkeranjang->tgl = date("Y-m-d H:i:s");
                                        $cekkeranjang->save();

                                    }else{
                                        $keranjang = new Keranjang();
                                        $keranjang->id_toko     = $request->id_toko;
                                        $keranjang->meja        = $request->meja;
                                        $keranjang->id_barang   = $request->id_barang;
                                        $keranjang->id_kategori = $request->id_kategori;
                                        $keranjang->id_kasir    = $request->id_kasir;
                                        $keranjang->id_pekerja  = $request->id_pekerja;  
                                        
                                        $keranjang->nama_brg = $getbrg->nama_br;
                                        $keranjang->harga_brg = $hrga;

                                        if($request->diskon_brg == ''){
                                            $dskon = '0'; 
                                        }else{
                                            $dskon = $request->diskon_brg; 
                                        }
                                        $keranjang->diskon_brg = $dskon; 
                                        
                                        
                                        ####///////----START---ATUR BAGI HASIL-------/////////////
                                        if($request->qty > 1){
                                            $tentukanharga = ($hrga*$request->qty)-$dskon;
                                            
                                            $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();

                                            $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                            $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);

                                            /////BARANG BAGI HASIL SEKALI AJA
                                            // $keranjang->upah_pekerja = $getbagihasil->nilai;
                                            // $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                                
                                            
                                        }else{
                                            $tentukanharga = $hrga-$dskon;
                                            $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                                
                                            $keranjang->upah_pekerja = $getbagihasil->nilai;
                                            $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                            
                                        }
                                        ####///////----END---ATUR BAGI HASIL-------/////////////
                                        

                                        $keranjang->qty = $request->qty;
                                        $keranjang->total = $tentukanharga;
                                        $keranjang->status = 0;
                                        $keranjang->tgl = date("Y-m-d H:i:s");

                                        $keranjang->save();
                                    }

                                    $messages = 'Data Berhasil Ditambah';
                                    $respone =  fractal()
                                        ->item($messages)
                                        ->transformWith(new SuccessTransformer)
                                        ->serializeWith(new ArraySerializer)        
                                        ->toArray();
                                    return response()->json($respone, 200); 
                                }else{
                                    $messages = 'Stok Sudah Habis!, Silahkan Tambahkan Stok Terlebih dahulu';
                                    $respone =  fractal()
                                        ->item($messages)
                                        ->transformWith(new ErorrTransformer) 
                                        ->serializeWith(new ArraySerializer)     
                                        ->toArray();
                                    return response()->json($respone, 409);
                                }  
                                ####///////----END---CEK HISTORY STOK-------/////////////
                                
                            }
                        }else{
                            $messages = 'Tidak Ada Stok Ditemukan!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer) 
                                ->serializeWith(new ArraySerializer)     
                                ->toArray();
                            return response()->json($respone, 401); 
                        }
                    }else{
                        $getbrg = ProdukToko::where('id_barang',$request->id_barang)->first();

                        $hrga = $getbrg->harga_satuan;
                        
                        $cekkeranjang = Keranjang::where('id_toko',$request->id_toko)
                                        ->where('id_barang',$request->id_barang)
                                        ->where('id_pekerja',$request->id_pekerja)
                                        // ->where('diskon_brg',$request->diskon_brg)
                                        ->where('meja',$request->meja)
                                        ->first();

                        if($cekkeranjang){
                            // if($request->diskon_brg == ''){
                            //     $dskon = '0';
                            // }else{
                            //     $dskon = $request->diskon_brg; 
                            // }
                            $dskon = $cekkeranjang->diskon_brg+$request->diskon_brg;
                            $cekkeranjang->diskon_brg = $dskon;
                            $tentukanharga = $hrga*($cekkeranjang->qty+$request->qty)-$dskon;
                            
                            ####///////----START---ATUR BAGI HASIL-------/////////////
                            if($getbrg->id_kategori == 3){
                                if($getbrg->metode_bagihasil ==  0){
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                    $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                    $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));
                                }elseif($getbrg->metode_bagihasil ==  1){

                                    $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                    $bghsl = 0;
                                    foreach ($produkpaket as $prdkpket) {
                                        $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                        $bghsl += $getbagihasil->nilai;
                                    }
                                    $cekkeranjang->upah_pekerja = $bghsl*($cekkeranjang->qty+$request->qty);
                                    $cekkeranjang->bagian_owner = $tentukanharga-($bghsl*($cekkeranjang->qty+$request->qty));
                                }
                            }elseif($getbrg->id_kategori == 2){
                                $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));
                            }
                            elseif($getbrg->id_kategori == 1){
                                $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                    
                                $cekkeranjang->upah_pekerja = $getbagihasil->nilai*($cekkeranjang->qty+$request->qty);
                                $cekkeranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*($cekkeranjang->qty+$request->qty));

                                /////BARANG BAGI HASIL SEKALI AJA
                                // $cekkeranjang->upah_pekerja = $getbagihasil->nilai;
                                // $cekkeranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                
                            } 
                            ####///////----END---ATUR BAGI HASIL-------/////////////

                            $cekkeranjang->qty = $cekkeranjang->qty+$request->qty;
                            $cekkeranjang->total =  $tentukanharga;
                            $cekkeranjang->tgl = date("Y-m-d H:i:s");
                            $cekkeranjang->save();

                        }else{
                            $keranjang = new Keranjang();
                            $keranjang->id_toko     = $request->id_toko;
                            $keranjang->meja        = $request->meja;
                            $keranjang->id_barang   = $request->id_barang;
                            $keranjang->id_kategori = $request->id_kategori;
                            $keranjang->id_kasir    = $request->id_kasir;
                            $keranjang->id_pekerja  = $request->id_pekerja;  
                            
                            $keranjang->nama_brg = $getbrg->nama_br;
                            $keranjang->harga_brg = $hrga;

                            if($request->diskon_brg == ''){
                                $dskon = '0'; 
                            }else{
                                $dskon = $request->diskon_brg; 
                            }
                            $keranjang->diskon_brg = $dskon; 
                            
                            
                            ####///////----START---ATUR BAGI HASIL-------/////////////
                            if($request->qty > 1){
                                $tentukanharga = ($hrga*$request->qty)-$dskon;
                                if($getbrg->id_kategori == 3){
                                    if($getbrg->metode_bagihasil ==  0){
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                        $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                        $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);
                                    }elseif($getbrg->metode_bagihasil ==  1){
                                        $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                        $bghsl = 0;
                                        foreach ($produkpaket as $prdkpket) {
                                            $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                            $bghsl += $getbagihasil->nilai;
                                        }
                                        $keranjang->upah_pekerja = $bghsl*$request->qty;
                                        $keranjang->bagian_owner = $tentukanharga-($bghsl*$request->qty);
                                    }
                                }elseif($getbrg->id_kategori == 2){
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                    $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                    $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);
                                }elseif($getbrg->id_kategori == 1){ 
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();

                                    $keranjang->upah_pekerja = $getbagihasil->nilai*$request->qty;
                                    $keranjang->bagian_owner = $tentukanharga-($getbagihasil->nilai*$request->qty);

                                    /////BARANG BAGI HASIL SEKALI AJA
                                    // $keranjang->upah_pekerja = $getbagihasil->nilai;
                                    // $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                    
                                } 
                            }else{
                                $tentukanharga = $hrga-$dskon;
                                if($getbrg->id_kategori == 3){
                                    if($getbrg->metode_bagihasil ==  0){////PAKET BAGI HASIL PER PAKET
                                        $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                        $keranjang->upah_pekerja = $getbagihasil->nilai;
                                        $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                    }elseif($getbrg->metode_bagihasil ==  1){ ////PAKET BAGI HASIL PER ITEM
                                        $produkpaket = ProdukPaket::where('id_paket',$request->id_barang)->get();
                                        $bghsl = 0;
                                        foreach ($produkpaket as $prdkpket) {
                                            $getbagihasil = BagiHasil::where('id_paket',$prdkpket->id)->where('id_petugas',$request->id_pekerja)->first();
                                            $bghsl += $getbagihasil->nilai;
                                        }
                                        // return $bghsl;
                                        $keranjang->upah_pekerja = $bghsl;
                                        $keranjang->bagian_owner = $tentukanharga-$bghsl;
                                    }
                                }else{
                                    $getbagihasil = BagiHasil::where('id_barang',$request->id_barang)->where('id_petugas',$request->id_pekerja)->first();
                                        
                                    $keranjang->upah_pekerja = $getbagihasil->nilai;
                                    $keranjang->bagian_owner = $tentukanharga-$getbagihasil->nilai;
                                } 
                            }
                            ####///////----END---ATUR BAGI HASIL-------/////////////
                            

                            $keranjang->qty = $request->qty;
                            $keranjang->total = $tentukanharga;
                            $keranjang->status = 0;
                            $keranjang->tgl = date("Y-m-d H:i:s");

                            $keranjang->save();
                        }

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function hapus_item_keranjang(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id' => 'required',
                        'id_toko' => 'required',
                        'meja' => 'required',
                        'id_kasir' => 'required',
                        'id_barang' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Keranjang::where('id',$request->id)
                                    ->where('id_toko',$request->id_toko)
                                    ->where('meja',$request->meja)
                                    ->where('id_kasir',$request->id_kasir)
                                    ->where('id_barang',$request->id_barang)
                                    ->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data Item Keranjang Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus Item Keranjang tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Hapus Item Keranjang tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus Item Keranjang tidak berhasil, silahkan coba kembali!");
        }

    }


}