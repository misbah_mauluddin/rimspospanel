<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use App\Http\Models\Admin;
use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;

use App\Http\Models\Diskon;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\SuccessTransformer;

use App\Transformers\DiskonTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class DiskonApiController extends Controller
{
   
////////DISKON-----------------------------
    public function data_diskon(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $ktr = Diskon::where('id_toko',$request->id_toko)
                    ->orderBy('diskon','ASC')->paginate(10);
              if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new DiskonTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
              }else{
                    $messages = 'Tidak Ada Data Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function tambah_diskon(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'diskon' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = new Diskon();
                    $addktr->id_toko = $request->id_toko;
                    $addktr->diskon = $request->diskon;
                    $addktr->tgl = date("Y-m-d");
                    

                    if($addktr->save()){
                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tambah Diskon tidak berhasil, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Tambah Diskon tidak berhasil, silahkan coba kembali!");
        }

    }

    public function edit_diskon(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_diskon' => 'required',
                        'id_toko' => 'required',
                        'diskon' => 'required',
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Diskon::where('id_diskon',$request->id_diskon)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        $addktr->diskon = $request->diskon;
                        if($addktr->save()){
                            $messages = 'Data Diskon Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Edit Diskon tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id Diskon / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    

                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Edit Diskon tidak berhasil, silahkan coba kembali!");
        }

    }

    public function hapus_diskon(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_diskon' => 'required',
                        'id_toko' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = Diskon::where('id_diskon',$request->id_diskon)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data Diskon Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus Diskon tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id Diskon / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus Diskon tidak berhasil, silahkan coba kembali!");
        }

    }

}