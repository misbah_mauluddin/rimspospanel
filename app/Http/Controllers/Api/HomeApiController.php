<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;
use App\Http\Models\KasLaci;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\HomeTransformer;
use App\Transformers\VersionTransformer;
use App\Transformers\PendapatanHarianTransformer;
use App\Transformers\SuccessTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class HomeApiController extends Controller
{
   
    public function loadkonten(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $toko = $request->id_toko;
              $respone =  fractal()
                    ->item($toko)
                    ->transformWith(new HomeTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();
              return response()->json($respone, 200);
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    public function loadversi(Request $request){
        try {
           
                
              $versi = Versi::first();
              $respone =  fractal()
                    ->item($versi)
                    ->transformWith(new VersionTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();
              return response()->json($respone, 200);
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function open_store(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $kas = new KasLaci();
                    $kas->id_toko = $request->id_toko;
                    $kas->kas = $request->kas;
                    $kas->tgl = date("Y-m-d");
                    $kas->save();

                    $messages = 'Kas Telah dimasukkan';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200);
                }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
                }
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function daily_report(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            ), array(
                        'id_toko.required' => 'Missing Parameter Value!'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $penjualan = PenjualanToko::select(DB::raw('SUM(total) as total'),'id_toko')
                            ->where('id_toko',$request->id_toko)
                            ->where('status',1)
                            ->whereIn('metode_bayar',['0','1','2'])
                            ->whereDate('tgl_penjualan', '>=', date("Y-m-d 00:00:00"))
                            ->whereDate('tgl_penjualan', '<=', date("Y-m-d 23:59:59"))
                            ->first();

                    $respone =  fractal()
                            ->item($penjualan)
                            ->transformWith(new PendapatanHarianTransformer)
                            ->serializeWith(new ArraySerializer)
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
                }
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function edit_store(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'nama_toko' => 'required',
                        'alamat' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                   $cektoko->nama_toko = $request->nama_toko;
                   $cektoko->alamat = $request->alamat;

                   $cektoko->save();

                   $messages = 'Data Berhasil Diperbaharui';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200); 
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function edit_store_logo(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'logo' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                   if (Input::file('logo')) {
                      $image = $request->file('logo');
                      $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                      $destinationPath = ('uploads/toko');
                      $img = Image::make($image->getRealPath());
                      $img->resize(500, 500, function ($constraint) {
                          $constraint->aspectRatio();
                      })->save(public_path($destinationPath.'/'.$input['imagename']));

                      $image->move(public_path($destinationPath, '/'.$input['imagename']));

                      $direktori = $destinationPath.'/'.$input['imagename'];

                      $cektoko->logo         = $direktori;
                    }

                   $cektoko->save();

                   $messages = 'Data Berhasil Diperbaharui';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200); 
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }
}