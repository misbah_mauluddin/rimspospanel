<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\LoginTransformer;
use App\Transformers\UserTokoTransformer;
use App\Transformers\SuccessTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class UserTokoApiController extends Controller
{
    

////// USER KASIR DAN ADMIN
    public function datakasir(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = UserToko::where('id_toko',$request->id_toko)->whereIn('status', ['2', '3'])->paginate(10);
                if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new UserTokoTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $messages = 'Data User Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    public function tambahkasir(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_user' => 'required',
                        'id_toko' => 'required',
                        'nama_kasir' => 'required',
                        'nomor_hp' => 'required',
                        'pass' => 'required',
                        'status' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $dataFound = UserToko::where('nomor_hp', $request->nomor_hp)->first();
                    if($dataFound){
                        $messages = 'Nomor Handphone Sudah Terdaftar Pada Sistem';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{

                        $data                               = new UserToko();
                        $data->id_user                      = $request->id_user;
                        $data->id_toko                = $request->id_toko;
                        $data->id_jenis_usaha                    = $cektoko->id_jenisusaha;
                        $data->nama_toko                       = $cektoko->nama_toko;
                        $data->nama_kasir                         = $request->nama_kasir;
                        $data->nomor_hp                          = $this->cleannohp($request->nomor_hp);
                        $data->pass                       = md5($request->pass);
                        $data->status                        = $request->status;
                        $data->stts_user                        = 1;
                        

                        if($data->save()){
                            $messages = 'Data Berhasil Ditambah';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Tambah User tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function editkasir(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_kasir' => 'required',
                        'nama_kasir' => 'required',
                        'nomor_hp' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekidkasir = UserToko::where('id_kasir', $request->id_kasir)->first();
                    if($cekidkasir){
                        $dataFound = UserToko::where('id_kasir','<>', $request->id_kasir)->where('nomor_hp', $request->nomor_hp)->first();
                        if($dataFound){
                            $messages = 'Nomor Handphone Sudah Terdaftar Pada Sistem';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }else{

                            $data        = UserToko::where('id_kasir', $request->id_kasir)->where('id_toko',$request->id_toko)->first();
                            $data->nama_kasir      = $request->nama_kasir;
                            $data->nomor_hp        = $this->cleannohp($request->nomor_hp);
                            

                            if($data->save()){
                                $messages = 'Data Berhasil Diperbaharui';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new SuccessTransformer)
                                    ->serializeWith(new ArraySerializer)        
                                    ->toArray();
                                return response()->json($respone, 200); 
                            }else{
                                $messages = 'Edit User tidak berhasil, silahkan coba kembali!';
                                $respone =  fractal()
                                    ->item($messages)
                                    ->transformWith(new ErorrTransformer)    
                                    ->serializeWith(new ArraySerializer)  
                                    ->toArray();
                                return response()->json($respone, 401);
                            }
                        }
                    }else{
                        $messages = 'Id User Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function gantipasskasir(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_kasir' => 'required',
                        'pass' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekidkasir = UserToko::where('id_kasir', $request->id_kasir)->first();
                    if($cekidkasir){
                        $data        = UserToko::where('id_toko',$request->id_toko)->where('id_kasir', $request->id_kasir)->first();
                        $data->pass      = md5($request->pass);
                        

                        if($data->save()){
                            $messages = 'Data Berhasil Diperbaharui';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Edit Pass User tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id User Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function hapuskasir(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_kasir' => 'required',
                        'id_toko' => 'required'
                            )
            );


            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
              $cektoko = Toko::where('id_toko',$request->id_toko)->first();
              if($cektoko){
                    
                    $addktr = UserToko::where('id_kasir', $request->id_kasir)->where('id_toko',$request->id_toko)->first();
                    if($addktr){
                        if($addktr->delete()){
                            $messages = 'Data User Berhasil Dihapus';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Hapus User tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }else{
                        $messages = 'Id User / Id Toko tidak sesuai, silahkan coba kembali!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }
                    
              }else{
                    $messages = 'Id Toko Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer) 
                        ->serializeWith(new ArraySerializer)     
                        ->toArray();
                    return response()->json($respone, 401); 
              }
              
            }

        } catch (QueryException $ex) {
            throw new HttpException(500, "Hapus User tidak berhasil, silahkan coba kembali!");
        }

    }

////// PEKERJA
    public function dataPekerja(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $ktr = UserToko::where('id_toko',$request->id_toko)->where('status', '4')->paginate(10);
                if(count($ktr) > 0){
                    $dtl = $ktr->getCollection();                    

                    $respone =  fractal()
                          ->collection($dtl, new UserTokoTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->paginateWith(new IlluminatePaginatorAdapter($ktr))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $messages = 'Data Pekerja Tidak Ditemukan!';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new ErorrTransformer)      
                        ->serializeWith(new ArraySerializer)
                        ->toArray();
                    return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }
    
    public function tambahpekerja(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_user' => 'required',
                        'id_toko' => 'required',
                        'nama_kasir' => 'required',
                        'nomor_hp' => 'required',
                        // 'pass' => 'required',
                        // 'status' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $dataFound = UserToko::where('nomor_hp', $request->nomor_hp)->first();
                    if($dataFound){
                        $messages = 'Nomor Handphone Sudah Terdaftar Pada Sistem';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer)    
                            ->serializeWith(new ArraySerializer)  
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{

                        $data                               = new UserToko();
                        $data->id_user                      = $request->id_user;
                        $data->id_toko                = $request->id_toko;
                        $data->id_jenis_usaha                    = $cektoko->id_jenisusaha;
                        $data->nama_toko                       = $cektoko->nama_toko;
                        $data->nama_kasir                         = $request->nama_kasir;
                        $data->nomor_hp                          = $this->cleannohp($request->nomor_hp);
                        $data->pass                       = '0';
                        $data->status                        = 4;
                        $data->stts_user                        = 1;
                        

                        if($data->save()){
                            $messages = 'Data Berhasil Ditambah';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new SuccessTransformer)
                                ->serializeWith(new ArraySerializer)        
                                ->toArray();
                            return response()->json($respone, 200); 
                        }else{
                            $messages = 'Tambah Pekerja tidak berhasil, silahkan coba kembali!';
                            $respone =  fractal()
                                ->item($messages)
                                ->transformWith(new ErorrTransformer)    
                                ->serializeWith(new ArraySerializer)  
                                ->toArray();
                            return response()->json($respone, 401);
                        }
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }
    
    public function cleannohp($nomor){
        $hp = '';
        if(substr($nomor,0,1) === '0'){
            $hp = $nomor;
        } elseif(substr($nomor,0,1) === '+'){
            if(substr($nomor,0,3) === '+62'){
                $hp = substr_replace($nomor,'0',0,3);
            } else{
                $hp = substr_replace($nomor,'0',0,1);
            }
        }
        return $hp;
    }
}