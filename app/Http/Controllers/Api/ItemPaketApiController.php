<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;
use App\Transformers\FailsTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class ItemPaketApiController extends Controller
{

//////ITEM PAKET
    public function data_produk_brgjasa(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = ProdukToko::whereRaw('id_toko = '.$request->id_toko.' AND status = 1 AND (id_kategori = 1 OR id_kategori = 2)')
                        ->orderBy('nama_br','ASC')
                        ->get();
                if(count($produk) > 0){                    

                    $respone =  fractal()
                          ->collection($produk, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    $respone =  fractal()
                          ->collection($produk, new ProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Produk Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

    public function data_itempaket(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                $produk = ProdukToko::where('id_toko',$request->id_toko)
                        ->where('id_barang',$request->id_barang)
                        ->get();
                if(count($produk) > 0){               

                    $respone =  fractal()
                          ->collection($produk, new ProdukPaketTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{               

                    $respone =  fractal()
                          ->collection($produk, new ProdukPaketTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Produk Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal menampilkan data, coba lagi!");
        }

    }

//////BAGI HASIL BY PAKET
    public function tambah_itempaket_bypaket(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_paket' => 'required',
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekpaket = ProdukPaket::where('id_paket',$request->id_paket)->where('id_produk',$request->id_barang)->first();
                    if($cekpaket){
                        $messages = 'Item Barang Sudah Di masukkan';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{
                        $data                               = new ProdukPaket();
                        $data->id_paket                      = $request->id_paket;
                        $data->id_produk                = $request->id_barang;
                        if($request->qty == ''){
                            $data->qty                  = 1;
                        }else{
                            $data->qty                  = $request->qty;
                        }
                        $data->save();
                            

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }
                    
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function edit_itempaket_bypaket(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_paket' => 'required',
                        'id_toko' => 'required',
                        'id_detailpaket' => 'required',
                        'id_barang' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                    $data        = ProdukPaket::where('id',$request->id_detailpaket)
                                    ->where('id_paket',$request->id_paket)->where('id_produk',$request->id_barang)->first();
                    $data->id_paket                      = $request->id_paket;
                    $data->id_produk                = $request->id_barang;
                    if($request->qty == ''){
                        $data->qty                  = 1;
                    }else{
                        $data->qty                  = $request->qty;
                    }
                    $data->save();
                        

                    $messages = 'Data Berhasil Diperbaharui';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200); 
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    public function hapus_itempaket_bypaket(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        // 'id_paket' => 'required',
                        'id_toko' => 'required',
                        'id_detailpaket' => 'required',
                        // 'id_barang' => 'required',
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                    $data        = ProdukPaket::where('id',$request->id_detailpaket)->first();
                    
                    if($data->delete()){

                        $update_hargabrg = ProdukToko::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                        $update_hargabrg->harga_satuan = $update_hargabrg->harga_satuan-$data->harga;
                        $update_hargabrg->save();
                        if($update_hargabrg->metode_bagihasil == 0){ //berdasarkan paket
                            $bghasil = BagiHasil::where('id_paket',$data->id)->delete();
                        }elseif($update_hargabrg->metode_bagihasil == 1){ //berdasarkan ITEM
                            $bghasil = BagiHasil::where('id_barang',$data->id_produk)->delete();
                        }

                        $messages = 'Data Berhasil Diperbaharui';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Gagal Menghapus Item Paket, coba lagi!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new FailsTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 409); 
                    }
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }

    
//////BAGI HASIL BY ITEM
    public function tambah_itempaket_byitem(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_paket' => 'required',
                        'id_barang' => 'required',
                        'harga' => 'required',
                        // 'id_pekerja' => 'required',
                        // 'metode' => 'required', ////Metode Bagi hasil
                        'bagihasil' => 'required', 
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekpaket = ProdukPaket::where('id_paket',$request->id_paket)->where('id_produk',$request->id_barang)->first();
                    if($cekpaket){
                        $messages = 'Item Barang Sudah Di masukkan';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401);
                    }else{
                        $data                               = new ProdukPaket();
                        $data->id_paket                      = $request->id_paket;
                        $data->id_produk                = $request->id_barang;
                        if($request->qty == ''){
                            $data->qty                  = 1;
                        }else{
                            $data->qty                  = $request->qty;
                        }
                        $data->harga                  = $request->harga;
                        $data->save();

                        foreach ($request->bagihasil as $dt ) {
                            $addbgihasil = new BagiHasil();
                            $addbgihasil->id_paket = $data->id;
                            // $addbgihasil->id_barang = $request->id_barang;
                            $addbgihasil->id_petugas = $dt['id_pekerja'];
                            $addbgihasil->metode = $dt['metode'];

                            if($dt['metode'] == 'persen'){
                                $hitung = ($dt['bagihasil']/100)*$data->harga;
                                $addbgihasil->nilai = $hitung;
                            }elseif($dt['metode'] == 'nominal'){
                                $addbgihasil->nilai = $dt['bagihasil'];
                            }
                            
                            $addbgihasil->save();
                        }

                        // $updthrgastok = ProdukTokoStok::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                        // $updthrgastok->harga = $updthrgastok->harga+$request->harga;
                        // $updthrgastok->save();
                        
                        // $updatehiststok = HistStok::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                        // $updatehiststok->harga_jual = $updthrgastok->harga;
                        // $updatehiststok->save();

                        $update_hargabrg = ProdukToko::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                        $update_hargabrg->harga_satuan = $update_hargabrg->harga_satuan+$request->harga;
                        $update_hargabrg->save();

                        $messages = 'Data Berhasil Ditambah';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                            
                    }
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah data, coba lagi!");
        }

    }

    public function edit_itempaket_byitem(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_paket' => 'required',
                        'id_detailpaket' => 'required',
                        'id_barang' => 'required',
                        'harga' => 'required',
                        'bagihasil' => 'required', 
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    
                    $data     = ProdukPaket::where('id',$request->id_detailpaket)
                                ->where('id_paket',$request->id_paket)->where('id_produk',$request->id_barang)->first();

                    // $updthrgastok = ProdukTokoStok::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                    // $updthrgastok->harga = $updthrgastok->harga - $data->harga + $request->harga;
                    // $updthrgastok->save();

                    // $updatehiststok = HistStok::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                    // $updatehiststok->harga_jual = $updthrgastok->harga;
                    // $updatehiststok->save();

                    $update_hargabrg = ProdukToko::where('id_barang',$data->id_paket)->where('id_toko',$request->id_toko)->first();
                    $update_hargabrg->harga_satuan = $update_hargabrg->harga_satuan - $data->harga + $request->harga;
                    $update_hargabrg->save();

                    
                    if($request->qty == ''){
                        $data->qty                  = 1;
                    }else{
                        $data->qty                  = $request->qty;
                    }
                    $data->harga                  = $request->harga;

                    $data->save();

                    foreach ($request->bagihasil as $dt ) {
                        $addbgihasil = BagiHasil::where('id_paket',$data->id)
                                        // ->where('id_barang',$data->id_barang)
                                        ->where('id_petugas',$dt['id_pekerja'])
                                        ->first();
                        if($addbgihasil){
                            $addbgihasil->metode = $dt['metode'];
                            if($dt['metode'] == 'persen'){
                                $hitung = ($dt['bagihasil']/100)*$data->harga;
                                $addbgihasil->nilai = $hitung;
                            }elseif($dt['metode'] == 'nominal'){
                                $addbgihasil->nilai = $dt['bagihasil'];
                            }
                            $addbgihasil->save();
                        }else{
                            $addbgihasilnew = new BagiHasil();
                            $addbgihasilnew->id_paket = $data->id;
                            // $addbgihasilnew->id_barang = $request->id_barang;
                            $addbgihasilnew->id_petugas = $dt['id_pekerja'];
                            $addbgihasilnew->metode = $dt['metode'];
                            
                            if($dt['metode'] == 'persen'){
                                $hitung = ($dt['bagihasil']/100)*$data->harga;
                                $addbgihasilnew->nilai = $hitung;
                            }elseif($dt['metode'] == 'nominal'){
                                $addbgihasilnew->nilai = $dt['bagihasil'];
                            }
                            $addbgihasilnew->save();   
                        }
                        
                    }
                    
                    
                    

                    $messages = 'Data Berhasil Diperbaharui';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200);
                    
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah data, coba lagi!");
        }

    }



}