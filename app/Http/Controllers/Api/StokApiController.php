<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;


use App\Http\Models\Konten;
use App\Http\Models\Versi;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\HistStok;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\ProdukTokoStokPengurangan;
use App\Http\Models\ProdukPaket;
use App\Http\Models\BagiHasil;



use App\Transformers\ErorValidasiTransformer;
use App\Transformers\ErorrTransformer;

use App\Transformers\DistrictsTransformer;
use App\Transformers\VillagesTransformer;


use App\Transformers\ProdukTransformer;
use App\Transformers\SuccessTransformer;
use App\Transformers\ModalProdukTransformer;
use App\Transformers\ProdukPaketTransformer;
use App\Transformers\BagihasilTransformer;


use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\ArraySerializerV2;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



use Helperss;
use Image;
use Mail;

use QrCode;
use Storage;

use DateTime;
use DateInterval;
use DatePeriod;



class StokApiController extends Controller
{

////// STOK
    public function tambah_stok_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_user' => 'required',
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                        'qty' => 'required',
                        'modal' => 'required',
                        'harga_jual' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekproduk = ProdukToko::where('id_barang',$request->id_barang)->first();

                    $addstok = ProdukTokoStok::where('id_barang',$request->id_barang)->first();
                    if($addstok){
                        ///////TAMBAH STOK
                        $addstok->qty = $addstok->qty+$request->qty;
                        $addstok->harga = $request->harga_jual;
                        $addstok->save();

                        /////// UPDATE HARGA DI TBL BRG
                        $cekproduk->jumlah_semua = $addstok->qty;
                        $cekproduk->harga_satuan = $request->harga_jual;
                        $cekproduk->save();

                        //////TAMBAH HISTORI STOK
                        $addstokhist = new HistStok();
                        $addstokhist->id_toko = $cekproduk->id_toko;
                        $addstokhist->id_user = $cekproduk->id_user;
                        $addstokhist->id_barang = $cekproduk->id_barang;
                        $addstokhist->nota = $request->nota;
                        $addstokhist->modal = $request->modal;
                        $addstokhist->harga_jual = $request->harga_jual;
                        $addstokhist->jlh_masuk = $request->qty;
                        $addstokhist->jlh_keluar = 0;
                        $addstokhist->tanggal = date("Y-m-d");
                        $addstokhist->save(); 
                    }else{
                        ///////TAMBAH STOK
                        $addstok = new ProdukTokoStok();
                        $addstok->id_barang = $cekproduk->id_barang;
                        $addstok->id_toko = $cekproduk->id_toko;
                        $addstok->qty = $request->qty;
                        $addstok->harga = $request->harga_jual;
                        $addstok->save();

                        /////// UPDATE HARGA DI TBL BRG
                        $cekproduk->jumlah_semua = $addstok->qty;
                        $cekproduk->harga_satuan = $request->harga_jual;
                        $cekproduk->save();

                        //////TAMBAH HISTORI STOK
                        $addstokhist = new HistStok();
                        $addstokhist->id_toko = $cekproduk->id_toko;
                        $addstokhist->id_user = $cekproduk->id_user;
                        $addstokhist->id_barang = $cekproduk->id_barang;
                        $addstokhist->nota = $request->nota;
                        $addstokhist->modal = $request->modal;
                        $addstokhist->harga_jual = $request->harga_jual;
                        $addstokhist->jlh_masuk = $request->qty;
                        $addstokhist->jlh_keluar = 0;
                        $addstokhist->tanggal = date("Y-m-d");
                        $addstokhist->save(); 
                    }
                    

                        
                        

                    $messages = 'Stok Berhasil Ditambah';
                    $respone =  fractal()
                        ->item($messages)
                        ->transformWith(new SuccessTransformer)
                        ->serializeWith(new ArraySerializer)        
                        ->toArray();
                    return response()->json($respone, 200); 
                
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Menambah Stok, coba lagi!");
        }

    }

    public function getmodal_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $produk = HistStok::where('id_toko',$request->id_toko)
                        ->where('id_barang',$request->id_barang)
                        ->where('jlh_masuk','>', 'jlh_keluar')
                        ->orderBy('id_toko','ASC')
                        ->get();
                if(count($produk) > 0){
                    // $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produk, new ModalProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            // ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,'error'=> false])
                            ->toArray();
                      return response()->json($respone, 200);
                }else{
                    // $produkrow = $produk->getCollection();                    

                    $respone =  fractal()
                          ->collection($produk, new ModalProdukTransformer, 'data')
                            ->serializeWith(new ArraySerializerV2())
                            // ->paginateWith(new IlluminatePaginatorAdapter($produk))
                            ->addMeta(['success'=> true,
                                        'error'=> false,
                                        'message'=> 'Tidak Ada Stok Ditemukan!'])
                            ->toArray();
                      return response()->json($respone, 200);
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Tidak Ada Stok Ditemukan, coba lagi!");
        }

    }

    public function edit_modal_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_user' => 'required',
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                        'id_stok' => 'required',
                        'modal' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekproduk = ProdukToko::where('id_barang',$request->id_barang)->first();

                    $addstok = ProdukTokoStok::where('id_barang',$request->id_barang)->first();
                    if($addstok){
                        $gethiststok = HistStok::where('id_stok',$request->id_stok)
                                        ->where('id_toko',$request->id_toko)
                                        ->where('id_barang',$request->id_barang)
                                        ->first();
                        $gethiststok->id_user = $request->id_user;
                        $gethiststok->modal = $request->modal;
                        $gethiststok->save();

                        $messages = 'Modal Berhasil Diperbaharui';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tidak Ada Stok Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Merubah Modal, coba lagi!");
        }

    }

    public function penyesuaian_stok_produk(Request $request){
        try {
            $validator = Validator::make($request->all(),
                             array(
                        'id_toko' => 'required',
                        'id_barang' => 'required',
                        'qty' => 'required'
                            )
            );
            if ($validator->fails()) {
                // $error_messages = $validator->messages()->all();
                $error_messages = 'Missing Parameter Value!';
                $response = fractal()
                    ->item($error_messages)
                    ->transformWith(new ErorValidasiTransformer)
                    ->serializeWith(new ArraySerializer)
                    ->toArray();

                return response()->json($response, 422);

            }else{
                
                $cektoko = Toko::where('id_toko',$request->id_toko)->first();
                if($cektoko){
                    $cekproduk = ProdukToko::where('id_barang',$request->id_barang)->first();

                    $addstok = ProdukTokoStok::where('id_barang',$request->id_barang)->first();
                    if($addstok){
                        $addstok->qty = $addstok->qty-$request->qty;
                        $addstok->save();

                        $gethiststok = HistStok::where('id_toko',$request->id_toko)
                                        ->where('id_barang',$request->id_barang)
                                        ->where('jlh_masuk','>', 'jlh_keluar')
                                        ->orderBy('id_toko','ASC')
                                        ->first();
                        $gethiststok->jlh_keluar = $gethiststok->jlh_keluar+$request->qty;
                        $gethiststok->save();

                        $pengurangan = new ProdukTokoStokPengurangan();
                        $pengurangan->tanggal = date("Y-m-d");
                        $pengurangan->id_toko = $request->id_toko;
                        $pengurangan->id_barang = $request->id_barang;
                        $pengurangan->id_hist_stok = $gethiststok->id_stok;
                        $pengurangan->qty = $request->qty;
                        $pengurangan->alasan = $request->alasan;
                        $pengurangan->save();



                        $messages = 'Stok Berhasil Disesuaikan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new SuccessTransformer)
                            ->serializeWith(new ArraySerializer)        
                            ->toArray();
                        return response()->json($respone, 200); 
                    }else{
                        $messages = 'Tidak Ada Stok Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                    }
                        
                }else{
                        $messages = 'Id Toko Tidak Ditemukan!';
                        $respone =  fractal()
                            ->item($messages)
                            ->transformWith(new ErorrTransformer) 
                            ->serializeWith(new ArraySerializer)     
                            ->toArray();
                        return response()->json($respone, 401); 
                }
              
            }
            

        } catch (QueryException $ex) {
            throw new HttpException(500, "Gagal Mengurangi Stok, coba lagi!");
        }

    }

}