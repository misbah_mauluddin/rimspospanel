<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;
use DateTime;
use Session;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\Antrian;



class AntrianController extends Controller
{

    public function Antrian(Request $request)
    {
        $data = (object)array(
            'title'     => 'Antrian',
            'title_sub'     => 'Antrian',
            'halaman'     => 'data',
            'select' => (object)array(
                    'toko' => Toko::whereIn('status',['1','2'])->get()
            ) 
        );
        return view('antrian',compact('data'))->with(["page" => "Antrian"]);
    }

    public function get_barberman(Request $request)
    {
        $id = $request->a;

        $regencies = UserToko::where('id_toko',$id)->where('status',4)->orderBy('nama_kasir','ASC')->get();
        $reg_data = array();
        try {
            $reg_data['status'] = 'SUCCESS';
            foreach ($regencies as $reg)
            {
                $reg_data['data'][] = array(
                    'a' => $reg->id_kasir,
                    'b' => $reg->nama_kasir,
                );
            }

            
        } catch (QueryException $ex) {
            $reg_data['status'] = 'FAILED';
            $reg_data['msg'] = 'Data Baberman tidak ditemukan ' . $ex->getMessage();
        }

        echo json_encode($reg_data);
    }

    public function AntrianAdd(Request $request)
    {
        if ($request->isMethod('POST')) {
            try {

                $cekantrian = Antrian::where('no_hp',$request->no_hp)->where('tanggal',date("Y-m-d H:i",strtotime($request->tanggal)))->first();
                if($cekantrian){
                    $this->response['status'] = 'fail';
                    $this->response['msg'] = 'Antrian atas  <br> nama: <strong>'.$request->nama.'</strong> <br> Tanggal: <strong>'.date("d-m-Y",strtotime($request->tanggal)).'</strong> <br> sudah diajukan! silahkan masukkan tanggal yang berbeda';
                }else{
                    $sponsor = new Antrian();
                    $sponsor->id_toko = $request->id_toko;
                    $sponsor->id_pekerja = $request->id_pekerja;
                    $sponsor->nama = $request->nama;
                    $sponsor->no_hp = $request->no_hp;
                    $sponsor->email = $request->email;
                    $sponsor->tanggal = date("Y-m-d H:i:s",strtotime($request->tanggal));
                    $sponsor->status = 0;

                    $sponsor->save();

                    $this->response['status'] = 'success';
                    $this->response['pemesan'] = encrypt($sponsor->id);
                    $this->response['msg'] = 'Antrian Berhasil di ajukan atas nama <strong>'.$request->nama.'</strong>';
                }
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function AntrianPage(Request $request,$nomor)
    {
        try {
            $data = Antrian::where('id', decrypt($nomor))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Antrian ',$nomor,
                    'title_sub'     => 'Antrian ',$nomor,
                    'halaman'     => 'detail',
                    'select'    => (object) array(
                        'toko' => Toko::where('id_toko',$data->id_toko)->first(),
                        'pekerja' => UserToko::where('id_kasir',$data->id_pekerja)->first(),
                    ),
                    'form'      => $data
                );
                return view('antrianpage',compact('data'))->with(["page" => "Antrian ".$nomor]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }
}
