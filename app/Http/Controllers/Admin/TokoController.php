<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;
use DateTime;
use Session;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\Admin;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



class TokoController extends Controller
{


///////GET DATA GLOBAL---------------------------------------
    public function GetTokoGlobal(Request $request)
    {
        
            try{

                $q = $request->q;
                $item = DB::table('tbl_toko as a')
                        ->select('a.*','b.nama_jenisusaha as nama_jenisusaha')
                        ->leftJoin('tbl_jenis_usaha as b','a.id_jenisusaha','=','b.id_jenisusaha')
                        ->where('a.nama_toko', 'LIKE', '%'. $q . '%')
                        ->orWhere('b.nama_jenisusaha', 'LIKE', '%'. $q . '%')
                        ->orderBy('a.nama_toko','ASC')
                       ->get();

                return response()->json($item);
            }catch (QueryException $ex){
                $item = "Gagal ambil data " . $ex->getMessage();

                return response()->json($item);
            }
        
        
    }


////METODE BAYAR
    public function metodeBayar(Request $request){
        $data = (object)array(
            'title'     => 'METODE BAYAR',
            'halaman'     => 'input'
        );
        return view('master.metodebayar',compact('data'))->with(["page" => "Metode Bayar"]);
    }

    public function dataMetodeBayar(Request $request)
    {

        $columns = array(
            1 => 'id',
            2 => 'nama'
        );

        $totalData = MetodeBayar::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('metode_bayar')
                ->select('id', 'nama')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('metode_bayar')
                ->select('id', 'nama')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('nama', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('metode_bayar')
                ->select('id', 'nama')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('nama', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['nama'] = $service->nama;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }
    
    public function TambahMetodeBayarAksi(Request $request)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = MetodeBayar::where('nama', $request->metodebayar)->first();
                if($dataExists)
                {
                    return back()->with("fail", "Metode Bayar <strong>" . $request->metodebayar . "</strong> sudah terdata")->withInput();
                }

                $data               = new MetodeBayar();
                $data->nama         = $request->metodebayar;

                $data->save();

                return redirect('/admin-panel/metodebayar')->with('success', 'Berhasil tambah data Metode Bayar <strong>' . $data->nama . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal tambah data Metode Bayar " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function EditMetodeBayarView(Request $request,$id)
    {
        try {
            $data = MetodeBayar::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Metode Bayar',
                    'title_sub'     => 'Metode Bayar',
                    'halaman'     => 'update',
                    'form'      => $data
                );
                return view('master.metodebayar', compact('data'))->with(["page" => "Metode Bayar"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditMetodeBayarAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = MetodeBayar::where('nama', $request->metodebayar)->where('id', '<>', decrypt($id))->first();
                if($dataExists)
                {
                    return back()->with("fail", "Metode Bayar <strong>" . $request->metodebayar . "</strong> sudah terdaftar")->withInput();
                }

                $data               = MetodeBayar::where('id', decrypt($id))->first();
                $data->nama         = $request->metodebayar;

                $data->save();

                return redirect('/admin-panel/metodebayar')->with('success', 'Berhasil ubah data Metode Bayar <strong>' . $data->nama . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah data Metode Bayar " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }
    
    public function MetodeBayarDelete(Request $request)
    {
        try{
            $data = MetodeBayar::where('id', $request->id)->first();
            if($data)
            {
              
                $data->delete();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Hapus Metode Bayar <strong>'. $data->nama .'</strong>';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Metode Bayar';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

////TOKO-----------------------------------------------
    
    public function Toko(Request $request){
        $data = (object)array(
            'title'     => 'Toko',
            'title_sub'     => 'Toko',
            'halaman'     => 'data',
            'select'    => (object) array(
                'jenis'  => JenisUsaha::orderBy('nama_jenisusaha', 'ASC')->get(),
                'id_metodebyr'  => MetodeBayar::orderBy('nama', 'ASC')->get()
            )
        );
        return view('toko.toko',compact('data'))->with(["page" => "Toko"]);
    }

    public function DataToko(Request $request)
    {
        $status = $request->status;
        if( $request->date1 !='' &&  $request->date2 != ''){
            $date1 = date("Y-m-d", strtotime($request->date1));
            $date2 = date("Y-m-d", strtotime($request->date2));
        }else{
            $date1 = $request->date1;
            $date2 = $request->date2;
        }
        
        $columns = array(
            0 => 'id_toko',
            1 => 'nama_toko',
            2 => 'alamat',
            3 => 'nama_jenisusaha',
            4 => 'tgl_aktif',
            5 => 'status_byr',
            6 => 'status'
        );

        if($status == "all"){
            $totalData = Toko::when($date1!='', function ($query) use ($date1){
                                    return $query->whereDate('tgl', '>=', $date1);
                                })
                                ->when($date2!='', function ($query) use ($date2){
                                    return $query->whereDate('tgl', '<=', $date2);
                                })
                                ->count();
        }else{
            $totalData = Toko::where('status', $status)
                                ->when($date1!='', function ($query) use ($date1){
                                    return $query->whereDate('tgl', '>=', $date1);
                                })
                                ->when($date2!='', function ($query) use ($date2){
                                    return $query->whereDate('tgl', '<=', $date2);
                                })
                                ->count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                ->when($date1!='', function ($query) use ($date1){
                    return $query->whereDate('a.tgl', '>=', $date1);
                })
                ->when($date2!='', function ($query) use ($date2){
                    return $query->whereDate('a.tgl', '<=', $date2);
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                ->when($date1!='', function ($query) use ($date1){
                    return $query->whereDate('a.tgl', '>=', $date1);
                })
                ->when($date2!='', function ($query) use ($date2){
                    return $query->whereDate('a.tgl', '<=', $date2);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_aktif','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_berakhir','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_aktif','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_berakhir','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->logo == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->logo);
                }

                if($service->tgl_aktif == ""){
                    $tgl_aktif = "-";
                    $date1 = '0';
                }else{
                    $tgl_aktif = date("d-m-Y",strtotime($service->tgl_aktif));
                    $date1 = date("Y-m-d",strtotime($service->tgl_aktif));
                }

                if($service->tgl_berakhir == ""){
                    $tgl_berakhir = "-";
                    $date2 = '0';
                }else{
                    $tgl_berakhir = date("d-m-Y",strtotime($service->tgl_berakhir));
                    $date2 = date("Y-m-d",strtotime($service->tgl_berakhir));
                }

                if($service->status == 0){
                    if($service->is_suspend == 1){
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Suspend</div>';
                    }elseif($service->is_suspend == 0){
                        $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                    }
                }elseif($service->status == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                }elseif($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Proses Aktifasi</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Di Hapus</div>';
                }else{
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                }

                if($service->status_byr == 0){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-warning">Belum Bayar</div>';
                }elseif($service->status_byr == 1){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-secondary">Lunas</div>';
                }elseif($service->status_byr == 2){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-info">Cicil</div>';
                }else{
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                }


                if($date1 == '0' && $date2 == '0'){
                    $diffd = '0 Hari';
                    $diffm = '0 Bln';
                    $diff = '0 Thn';
                }else{

                    // $fdate = date("Y-m-d");
                    // $tdate = $date2;
                    // $datetime1 = new DateTime($fdate);
                    // $datetime2 = new DateTime($tdate);
                    // $interval = $datetime1->diff($datetime2);
                    // $diff = $interval->format('%a').' Hari '.$interval->format('%m').' Bln ' .$interval->format('%y').' Thn';

                    $date1 = date("Y-m-d");
                    $date2 = $date2;

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);

                    $day1 = date('d', $ts1);
                    $day2 = date('d', $ts2);

                    $diff = (($year2 - $year1) * 12);
                    $diffm = (($year2 - $year1) * 12) + ($month2 - $month1);
                    $diffd = (($year2 - $year1) * 12) + ($day2 - $day1).' Hari';

                    if($diffd < 0){
                        $diffd = '0 Hari';
                    }else{
                        $diffd = (($year2 - $year1) * 12)  + ($day2 - $day1).' Hari ';
                    }

                    if($diffm < 0){
                        $diffm = '0 Bln';
                    }else{
                        $diffm = (($year2 - $year1) * 12)  + ($month2 - $month1).' Bln ';
                    }

                    if($diff < 0){
                        $diff = '0 Thn';
                    }else{
                        $diff = (($year2 - $year1) * 12) .' Thn ';
                    }
                }
                

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id_toko;
                $nestedData['nama'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> <a href="#" class="view_photo" a="'.$service->id_toko.'">'.$service->nama_toko.'</a></span><br>
                                            <span class="text-muted"><small> No Hp :'.$service->nohp.'</small></span><br>
                                            <span class="text-muted"><small> Email :'.$service->email.'</small></span>
                                        </div>
                                    </div>';
                $nestedData['alamat'] = $service->alamat;
                $nestedData['namajenis'] = $service->namajenis;
                $nestedData['tgl'] = '<div class="d-flex align-items-center">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> '.$diffd.' '.$diffm.' '.$diff.'</span><br>
                                            <span class="text-muted"><small>'.$tgl_aktif.' s/d '.$tgl_berakhir.'</small> </span><br>
                                            
                                        </div>
                                    </div>';
                $nestedData['status_byr'] = $sstbyr;
                $nestedData['status'] = $sst;
                $nestedData['statusbar'] = $service->status;
                $nestedData['is_suspend'] = $service->is_suspend;
                $nestedData['id_hist_suspend'] = $service->id_hist_suspend;
                $nestedData['namabar'] = $service->nama_toko;
                $nestedData['url'] = encrypt($service->id_toko);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function TokoImageView($id)
    {
        $sumber = Toko::find($id);


        return response()->json(['data' => $sumber]);
    }


    public function TambahTokoView(Request $request)
    {
        $data = (object)array(
            'title'     => 'Toko',
            'title_sub'     => 'Toko',
            'halaman'     => 'input',
            'select'    => (object) array(
                'jenis'  => JenisUsaha::orderBy('nama_jenisusaha', 'ASC')->get(),
                'id_harga_custom'  => HargaCustom::orderBy('nama', 'ASC')->get(),
                'id_metodebyr'  => MetodeBayar::orderBy('nama', 'ASC')->get()
            )
        );
        return view('toko.toko',compact('data'))->with(["page" => "Toko"]);
    }
    
    public function TambahTokoAksi(Request $request)
    {
        if($request->isMethod("POST"))
        {
            try{

                $getses = Session::get('idadmin');
                $getadmin = Admin::where('id',$getses)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "";
                }

                if($request->tgl_aktif == ""){
                    $tgl_aktif = NULL;
                }else{
                    $tgl_aktif = date("Y-m-d H:i:s",strtotime($request->tgl_aktif));
                }

                if($request->tgl_berakhir == ""){
                    $tgl_berakhir = NULL;
                }else{
                    $tgl_berakhir = date("Y-m-d H:i:s",strtotime($request->tgl_berakhir));
                }

                $data                               = new Toko();
                $data->id_user                      = $idadmin;
                $data->id_jenisusaha                = $request->jenis;
                $data->nama_toko                    = $request->nama_toko;
                $data->alamat                       = $request->alamat;
                $data->nohp                         = $request->nohp;
                $data->tgl                          = date('Y-m-d');
                $data->status                       = $request->status;
                $data->tgl_aktif                    = $tgl_aktif;
                $data->tgl_berakhir                 = $tgl_berakhir;
                $data->status_byr                   = $request->status_byr;
                $data->id_metodebyr                 = $request->id_metodebyr;
                $data->email                        = $request->email;
                $data->id_harga_custom                        = $request->id_harga_custom;
                $data->is_suspend                        = 0;
                // $data->id_hist_suspend                        = $request->id_hist_suspend;

                if (Input::file('image')) {
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/toko');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->logo         = $direktori;
                }

                $data->save();


                return redirect('/admin-panel/toko')->with('success', 'Berhasil tambah Toko <strong>' . $data->nama_toko . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal tambah Toko " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function EditTokoView(Request $request,$id)
    {
        try {
            $data = Toko::where('id_toko', decrypt($id))->first();
            if($data)
            {
                if($data->tgl_aktif == ""){
                    $tgl_aktif = "";
                }else{
                    $tgl_aktif = date("d M, Y",strtotime($data->tgl_aktif));
                }

                if($data->tgl_berakhir == ""){
                    $tgl_berakhir = "";
                }else{
                    $tgl_berakhir = date("d M, Y",strtotime($data->tgl_berakhir));
                }

                $data = (object)array(
                    'title'     => 'Toko',
                    'title_sub'     => 'Toko',
                    'halaman'     => 'update',
                    'select'    => (object) array(
                        'jenis'  => JenisUsaha::orderBy('nama_jenisusaha', 'ASC')->get(),
                        'id_harga_custom'  => HargaCustom::orderBy('nama', 'ASC')->get(),
                        'id_metodebyr'  => MetodeBayar::orderBy('nama', 'ASC')->get()
                    ),
                    'cektgl' => (object) array(
                        'tgl_aktif' => $tgl_aktif,
                        'tgl_berakhir' => $tgl_berakhir,
                    ),
                    'form'      => $data
                );
                return view('toko.toko', compact('data'))->with(["page" => "Toko"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditTokoAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{

                $getses = Session::get('idadmin');
                $getadmin = Admin::where('id',$getses)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "7";
                }

                // if($request->tgl_aktif == ""){
                //     $tgl_aktif = NULL;
                // }else{
                //     $tgl_aktif = date("Y-m-d H:i:s",strtotime($request->tgl_aktif));
                // }

                // if($request->tgl_berakhir == ""){
                //     $tgl_berakhir = NULL;
                // }else{
                //     $tgl_berakhir = date("Y-m-d H:i:s",strtotime($request->tgl_berakhir));
                // }

                $data               = Toko::where('id_toko', decrypt($id))->first();
                $data->id_user                      = $idadmin;
                $data->id_jenisusaha                = $request->jenis;
                $data->nama_toko                    = $request->nama_toko;
                $data->alamat                       = $request->alamat;
                $data->nohp                         = $request->nohp;
                $data->tgl                          = date('Y-m-d');
                $data->email                        = $request->email;
                // $data->status                       = $request->status;
                // $data->tgl_aktif                    = $tgl_aktif;
                // $data->tgl_berakhir                 = $tgl_berakhir;
                // $data->status_byr                   = $request->status_byr;
                // $data->id_metodebyr                 = $request->id_metodebyr;
                
                $data->id_harga_custom                        = $request->id_harga_custom;
                // $data->is_suspend                        = 0;

                if (Input::file('image')) {
                    if ($data->logo != "") {
                        $path = $data->logo;
                        unlink(public_path($path));
                    }
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/toko');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->logo         = $direktori;
                }

                $data->save();

                // if($data->save()){
                //     $addstok = BarangStok::where('id_barang',$data->id)->first();
                //     $addstok->isi_satuan = $data->isi_satuan;
                //     $addstok->save();
                // }

                return redirect('/admin-panel/toko')->with('success', 'Berhasil ubah Toko <strong>' . $data->nama_toko . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah Toko " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }
    
    public function TokoDelete(Request $request)
    {
        try{
            $data = Toko::where('id_toko', $request->id)->first();
            if($data)
            {
                if ($data->logo != "") {
                    $path = $data->logo;
                    unlink(public_path($path));
                }

                
                
                // $data->status = 3;
                // $data->save(); 
                $data->delete();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Toko <strong>'. $data->nama_toko .'</strong> Berhasil Di hapus';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function TokoAktif(Request $request,$id)
    {
        $sumber = Toko::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function TokoAktifAksi(Request $request)
    {
        try{
            $data = Toko::where('id_toko', $request->id_tokoak)->first();
            if($data)
            {
                if($request->tgl_aktif == ""){
                    $date1 = '0';
                }else{
                    $date1 = date("Y-m-d",strtotime($request->tgl_aktif));
                }

                if($request->tgl_berakhir == ""){
                    $date2 = '0';
                }else{
                    $date2 = date("Y-m-d",strtotime($request->tgl_berakhir));
                }

                if($date1 == '0' && $date2 == '0'){
                    $diff = 0;

                    $this->response['status'] = 'fail';
                    $this->response['msg'] = 'Tgl Aktif dan Tgl Berakhir Blm Di Masukkan';
                }else{
                    
                    $data->status                       = $request->status;
                    $data->tgl_aktif                    = date('Y-m-d H:i:s',strtotime($request->tgl_aktif));
                    $data->tgl_berakhir                 = date('Y-m-d H:i:s',strtotime($request->tgl_berakhir));
                    $data->status_byr                   = $request->status_byr;
                    $data->id_metodebyr                 = $request->id_metodebyr;


                    if($data->save()){

                        // DB::connection()->enableQueryLog();
                        // $queries = DB::getQueryLog();
                        // return dd($queries);

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);

                        $year1 = date('Y', $ts1);
                        $year2 = date('Y', $ts2);

                        $month1 = date('m', $ts1);
                        $month2 = date('m', $ts2);

                        $day1 = date('d', $ts1);
                        $day2 = date('d', $ts2);

                        $diffm = (($year2 - $year1) * 12) + ($month2 - $month1);

                        $gethrg = HargaCustom::where('id',$data->id_harga_custom)->first();
                        $calhrg = $gethrg->harga*$diffm;
                        
                        $gotghcreate = DB::select('call create_tagihan_toko("'.$calhrg.'", "'.$data->id_toko.'")');
                        if($gotghcreate){
                            ///////CREATE HISTORY SEWA
                            $crthstsewa = new HistSewaToko();
                            $crthstsewa->tanggal = date("Y-m-d H:i:s");
                            $crthstsewa->id_toko = $data->id_toko;
                            $crthstsewa->stts = $data->status;
                            $crthstsewa->ket = "Sewa App Selama ".$diffm." Bulan";
                            $crthstsewa->save();

                            $this->response['status'] = 'success';
                            $this->response['msg'] = 'Toko <strong>'. $data->nama_toko .'</strong> Berhasil Di Aktifkan';
                        }else{
                            $datatokoreset = Toko::where('id_toko', $request->id_tokoak)->first();
                            $datatokoreset->status                       = 0;
                            $datatokoreset->tgl_aktif                    = NULL;
                            $datatokoreset->tgl_berakhir                 = NULL;
                            $datatokoreset->status_byr                   = NULL;
                            $datatokoreset->id_metodebyr                 = NULL;
                            $datatokoreset->save();

                            $this->response['status'] = 'fail';
                            $this->response['msg'] = 'Create  Tagihan Toko <strong>'. $datatokoreset->nama_toko .'</strong> Gagal';
                        }
                    }else{
                            $this->response['status'] = 'fail';
                            $this->response['msg'] = 'Tambah Toko <strong>'. $data->nama_toko .'</strong> Gagal';
                        }

                    
                }
                
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function TokoNonAktif(Request $request,$id)
    {
        $sumber = Toko::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function TokoNonAktifAksi(Request $request)
    {
        try{
            $data = Toko::where('id_toko', $request->id_tokononak)->first();
            if($data)
            {
                    ////CREATE HISTORY SUSPEND
                    $crthstsewa = new HistSuspendToko();
                    $crthstsewa->tgl_suspend = date('Y-m-d H:i:s',strtotime($request->tgl_suspend));
                    $crthstsewa->id_toko = $data->id_toko;
                    $crthstsewa->ket = $request->ket;
                    $crthstsewa->save();


                    $data->status                       = 0;
                    $data->is_suspend                   = 1;
                    $data->id_hist_suspend                 = $crthstsewa->id;


                    if($data->save()){
                        

                        ///////CREATE HISTORY SEWA
                        $crthstsewa = new HistSewaToko();
                        $crthstsewa->tanggal = date("Y-m-d H:i:s");
                        $crthstsewa->id_toko = $data->id_toko;
                        $crthstsewa->stts = 4;
                        $crthstsewa->ket = "Suspend/NonAktifkan App. Ket : ".$request->ket;
                        $crthstsewa->save();
                      
                    } 

                    $this->response['status'] = 'success';
                    $this->response['msg'] = 'Toko <strong>'. $data->nama_toko .'</strong> Berhasil Di Suspend/NonAktifkan';
                
                
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function TokoAktifUlang(Request $request,$id)
    {
        $sumber = Toko::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function TokoAktifUlangAksi(Request $request)
    {
        try{
            $data = Toko::where('id_toko', $request->id_tokoakulang)->first();
            if($data)
            {
                    $data->status                       = 1;
                    $data->is_suspend                   = 0;
                    $data->id_hist_suspend                 = NULL;


                    if($data->save()){
                        

                        ///////CREATE HISTORY SEWA
                        $crthstsewa = new HistSewaToko();
                        $crthstsewa->tanggal = date("Y-m-d H:i:s");
                        $crthstsewa->id_toko = $data->id_toko;
                        $crthstsewa->stts = 1;
                        $crthstsewa->ket = "Aktifasi Kembali App. Ket : ".$request->ket;
                        $crthstsewa->save();
                      
                    } 

                    $this->response['status'] = 'success';
                    $this->response['msg'] = 'Toko <strong>'. $data->nama_toko .'</strong> Berhasil Di Aktifkan Kembali';
                
                
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function DataUserTokoDetaildata(Request $request)
    {
        $id_toko = $request->id_toko;
        
        
        $columns = array(
            0 => 'id_kasir',
            1 => 'nama_kasir',
            2 => 'id_toko',
            3 => 'id_jenis_usaha',
            4 => 'nama_toko',
            5 => 'nomor_hp',
            6 => 'status'
        );

        $totalData = UserToko::where('id_toko', $id_toko)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->where('a.id_toko', '=', $id_toko)
                
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->where('a.id_toko', '=', $id_toko)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.nama_kasir','LIKE',"%{$search}%")
                            ->orWhere('a.nomor_hp','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->where('a.id_toko', '=', $id_toko)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.nama_kasir','LIKE',"%{$search}%")
                            ->orWhere('a.nomor_hp','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->logo == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->logo);
                }

                if($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Kasir</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Admin</div>';
                }elseif($service->status == 4){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Pekerja</div>';
                }else{
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak Ada Status</div>';
                }
                

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id_kasir;
                $nestedData['nama'] = $service->nama_kasir;
                $nestedData['namatoko'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> '.$service->nama_toko.'</span><br>
                                        </div>
                                    </div>';
                $nestedData['namajenis'] = $service->namajenis;
                $nestedData['nomor_hp'] = $service->nomor_hp;
                $nestedData['status'] = $sst;
                $nestedData['namabar'] = $service->nama_kasir;
                $nestedData['url'] = encrypt($service->id_kasir);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function TokoView(Request $request,$id)
    {
        try {
            $data = Toko::where('id_toko', decrypt($id))->first();
            if($data)
            {
                $getadmin = Admin::where('id',$data->id_user)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "7";
                }

                $data = (object)array(
                    'title'     => 'Toko',
                    'title_sub'     => 'Toko',
                    'halaman'     => 'detail',
                    'select'    => (object) array(
                        'jenis'  => JenisUsaha::where('id_jenisusaha', $data->id_jenisusaha)->first(),
                        'id_metodebyr'  => MetodeBayar::where('id', $data->id_metodebyr)->first(),
                        'id_harga_custom'  => HargaCustom::where('id', $data->id_harga_custom)->first(),
                        'idadmin'  => $idadmin
                    ),
                    'form'      => $data
                );
                return view('toko.toko', compact('data'))->with(["page" => "Toko"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    
////USERTOKO-----------------------------------------------
    
    public function UserToko(Request $request){
        $data = (object)array(
            'title'     => 'User Toko',
            'title_sub'     => 'User Toko',
            'halaman'     => 'data',
            'select'    => (object) array(
                'toko'  => Toko::orderBy('nama_toko', 'ASC')->get()
            )
        );
        return view('toko.usertoko',compact('data'))->with(["page" => "User Toko"]);
    }

    public function DataUserToko(Request $request)
    {
        $status = $request->status;
        
        
        $columns = array(
            0 => 'id_kasir',
            1 => 'nama_kasir',
            2 => 'id_toko',
            3 => 'id_jenis_usaha',
            4 => 'nama_toko',
            5 => 'nomor_hp',
            6 => 'status'
        );

        if($status == "all"){
            $totalData = UserToko::count();
        }else{
            $totalData = UserToko::where('id_toko', $status)->count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.id_toko', '=', $status);
                })
                
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.id_toko', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.nama_kasir','LIKE',"%{$search}%")
                            ->orWhere('a.nomor_hp','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tbl_kasir as a')
                ->select('a.*','b.nama_jenisusaha as namajenis','c.logo')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenis_usaha', 'b.id_jenisusaha')
                ->leftJoin('tbl_toko as c', 'a.id_toko', 'c.id_toko')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.id_toko', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.nama_kasir','LIKE',"%{$search}%")
                            ->orWhere('a.nomor_hp','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->logo == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->logo);
                }

                if($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Kasir</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Admin</div>';
                }elseif($service->status == 4){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Pekerja</div>';
                }else{
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak Ada Status</div>';
                }
                

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id_kasir;
                $nestedData['nama'] = $service->nama_kasir;
                $nestedData['namatoko'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> <a href="#" class="view_photo" a="'.encrypt($service->id_toko).'">'.$service->nama_toko.'</a></span><br>
                                        </div>
                                    </div>';
                $nestedData['namajenis'] = $service->namajenis;
                $nestedData['nomor_hp'] = $service->nomor_hp;
                $nestedData['status'] = $sst;
                $nestedData['namabar'] = $service->nama_kasir;
                $nestedData['url'] = encrypt($service->id_kasir);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function UserTokoImageView($id)
    {
        $sumber = Toko::find($id);


        return response()->json(['data' => $sumber]);
    }


    public function TambahUserTokoView(Request $request)
    {
        $data = (object)array(
            'title'     => 'User Toko',
            'title_sub'     => 'User Toko',
            'halaman'     => 'input',
            'select'    => (object) array(
                'id_toko'  => Toko::orderBy('nama_toko', 'ASC')->get()
            )
        );
        return view('toko.usertoko',compact('data'))->with(["page" => "User Toko"]);
    }
    
    public function TambahUserTokoAksi(Request $request)
    {
        if($request->isMethod("POST"))
        {
            try{

                $getses = Session::get('idadmin');
                $getadmin = Admin::where('id',$getses)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "";
                }

                $dataFound = Toko::where('id_toko', $request->id_toko)->first();
                if($dataFound)
                {
                    $data                               = new UserToko();
                    $data->id_user                      = $idadmin;
                    $data->id_toko                = $request->id_toko;
                    $data->id_jenis_usaha                    = $dataFound->id_jenisusaha;
                    $data->nama_toko                       = $dataFound->nama_toko;
                    $data->nama_kasir                         = $request->nama_kasir;
                    $data->nomor_hp                          = $request->nomor_hp;
                    $data->pass                       = md5($request->pass);
                    $data->status                        = $request->status;
                    $data->stts_user                        = 1;
                    $data->save();


                    return redirect('/admin-panel/usertoko')->with('success', 'Berhasil tambah User Toko <strong>' . $data->nama_toko . '</strong>');
                }else{
                    return back()->with("fail", "Toko Tidak Ditemukan")->withInput();
                }
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal tambah User Toko " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function EditUserTokoView(Request $request,$id)
    {
        try {
            $data = UserToko::where('id_kasir', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'User Toko',
                    'title_sub'     => 'User Toko',
                    'halaman'     => 'update',
                    'select'    => (object) array(
                        'id_toko'  => Toko::orderBy('nama_toko', 'ASC')->get()
                    ),
                    'form'      => $data
                );
                return view('toko.usertoko', compact('data'))->with(["page" => "User Toko"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditUserTokoAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{

                $getses = Session::get('idadmin');
                $getadmin = Admin::where('id',$getses)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "";
                }

                $dataFound = Toko::where('id_toko', $request->id_toko)->first();
                if($dataFound)
                {
                    $data               = UserToko::where('id_kasir', decrypt($id))->first();
                    $data->id_user                      = $idadmin;
                    $data->id_toko                = $request->id_toko;
                    $data->id_jenis_usaha                    = $dataFound->id_jenisusaha;
                    $data->nama_toko                       = $dataFound->nama_toko;
                    $data->nama_kasir                         = $request->nama_kasir;
                    $data->nomor_hp                          = $request->nomor_hp;
                    // $data->pass                       = md5($request->pass);
                    $data->status                        = $request->status;

                    $data->save();


                    return redirect('/admin-panel/usertoko')->with('success', 'Berhasil Edit User Toko <strong>' . $data->nama_toko . '</strong>');
                }else{
                    return back()->with("fail", "Toko Tidak Ditemukan")->withInput();
                }
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah User Toko " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }
    
    public function UserTokoDelete(Request $request)
    {
        try{
            $data = UserToko::where('id_kasir', $request->id)->first();
            if($data)
            {
                $data->delete();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'User Toko <strong>'. $data->nama_kasir .'</strong> Berhasil Di hapus';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data User Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function UserTokoView(Request $request,$id)
    {
        try {
            $data = UserToko::where('id_kasir', decrypt($id))->first();
            if($data)
            {
                $getadmin = Admin::where('id',$data->id_user)->first();
                if($getadmin){
                    $idadmin = $getadmin->id;
                }else{
                    $idadmin = "7";
                }

                $data = (object)array(
                    'title'     => 'User Toko',
                    'title_sub'     => 'User Toko',
                    'halaman'     => 'detail',
                    'select'    => (object) array(
                        'toko'  => Toko::where('id_toko', $data->id_toko)->first(),
                        'jenis'  => JenisUsaha::where('id_jenisusaha', $data->id_jenis_usaha)->first(),
                        'idadmin'  => $idadmin
                    ),
                    'form'      => $data
                );
                return view('toko.usertoko', compact('data'))->with(["page" => "User Toko"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditUserTokoPwView(Request $request,$id)
    {
        try {
            $data = UserToko::where('id_kasir', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Password User',
                    'title_sub'     => 'Password User',
                    'halaman'     => 'password',
                    'form'      => $data
                );
                return view('toko.usertoko', compact('data'))->with(["page" => "User Toko"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditUserTokoPwAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{

                $data                       = UserToko::where('id_kasir', decrypt($id))->first();
                $data->pass = md5($request->password);

                $data->save();

                return redirect('/admin-panel/usertoko')->with('success', 'Berhasil ubah password User <strong>' . $data->nama_kasir . '</strong> <br> Dari Toko <strong>'.$data->nama_toko.'</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah password User " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

///PENJUALAN----------------------------------------------------------
    public function PenjualanToko(Request $request)
    {
        $data = (object)array(
            'title'     => 'Penjualan(POS)',
            'title_sub'     => 'Penjualan(POS)',
            'halaman'     => 'data'
        );
        return view('toko.penjualan',compact('data'))->with(["page" => "Penjualan(POS)"]);
    }

    public function DataPenjualanToko(Request $request)
    {
        
        try{
            $date1 = $request->date1;
            $date2 = $request->date2;

            if($request->date1 != 'all' && $request->date2 != 'all')
            {
                $date1 = date('Y-m-d', strtotime($request->date1));
                $date2 = date('Y-m-d', strtotime($request->date2));
            }

            $id_toko         = $request->id_toko;
            $status         = $request->status;

            $datas = DB::table('tbl_penjualan as a')
                    ->select('a.*','b.nama_toko','b.logo','c.nama_kasir')
                    ->leftJoin('tbl_toko as b','a.id_toko','=','b.id_toko')
                    ->leftJoin('tbl_kasir as c','a.id_kasir','=','c.id_kasir')
                    ->where('a.id_toko',$id_toko)
                    ->when($status!='all', function ($query) use ($date1){
                        return $query->where('a.status', $status);
                    })
                    ->when($date1!='all', function ($query) use ($date1){
                        return $query->whereDate('a.tgl_penjualan', '>=', $date1);
                    })
                    ->when($date2!='all', function ($query) use ($date2){
                        return $query->whereDate('a.tgl_penjualan', '<=', $date2);
                    })
                ->get();

            if($datas)
            {
                $report_data    = '';
                $no             = 1;

                $nmtko = Toko::where('id_toko',$id_toko)->first();
                if($nmtko){
                    $namatoko = $nmtko->nama_toko;
                }else{
                    $namatoko = "";
                }
                
                $report_data .= '
                                <div class="card-header">
                                    <h4 class="card-title">Data Penjualan(POS) - '.$namatoko.' </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th width="30%">Tanggal / Waktu</th>
                                            <th>Id Penjualan</th>
                                            <th>Kasir</th>
                                            <th >Harga</th>
                                            <th >Bayar</th>
                                            <th>Kembalian</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap;">';
                foreach ($datas as $data)
                {
                  
                    if($data->status == 1){
                        $sst  = '<div class="badge badge-pill badge-glow badge-success">Selesai</div>';
                    }elseif($data->status == 2){
                        $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Hutang</div>';
                    }elseif($data->status == 3){
                        $sst  = '<div class="badge badge-pill badge-glow badge-info">Barang Keranjang</div>';
                    }elseif($data->status == 4){
                        $sst  = '<div class="badge badge-pill badge-glow badge-warning">Bayar Nanti</div>';
                    }elseif($data->status == 5){
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Transaksi Batal</div>';
                    }else{
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak Ada Status</div>';
                    }

                    if($data->harga != ""){
                        $harga = number_format($data->harga);
                    }else{
                        $harga = '0';
                    }

                    if($data->bayar != ""){
                        $bayar = number_format($data->bayar);
                    }else{
                        $bayar = '0';
                    }

                    if($data->kembalian != ""){
                        $kembalian = number_format($data->kembalian);
                    }else{
                        $kembalian = '0';
                    }
                    
                    $report_data .= '<tr>
                                        <td align="center">' . $no++ . '</td>
                                        <td>' . date('d M, Y / H:i', strtotime($data->tgl_penjualan)) . '</td>
                                        <td> <a href="javascript:void(0);" id="viewtrsda" a="'.$data->id_penjualan.'">' . $data->id_penjualan . '</a></td>
                                        <td>' . $data->nama_kasir . '</td>
                                        <td align="center">' . $harga . '</td>
                                        <td align="center">' . $bayar . '</td>
                                        <td align="center">' . $kembalian . '</td>
                                        <td align="center">' . $sst . '</td>
                                    </tr>';

                    
                    
                        // $saldosknrg   += $data->stok_masuk-$data->stok_keluar;
                }

                

                $report_data .= '</tbody>
                            </table>
                        </div>
                        </div>';

                $response['status']     = 'success';
                $response['message']    = 'Data Laporan berhasil ditampilkan';
                $response['data']       = $report_data;
            }
            else
            {
                $response['status']     = 'fail';
                $response['message']    = 'Data Laporan tidak tersedia!';
            }

        } catch (QueryException $ex) {
            $response['status']     = 'fail';
            $response['message']    = 'Gagal memuat data laporan, coba lagi!'. $ex->getMessage();
        }
        
        

        return response()->json($response);
    }

    public function PenjualanTokoDetail($id)
    {
        $sumber = PenjualanToko::where('id_penjualan',$id)->first();


        return response()->json(['data' => $sumber]);
    }

    public function PenjualanTokoDetailAksi(Request $request)
    {
        $id_penjualan = $request->id_penjualan;
        $id_toko = $request->id_toko;

        if( $request->date1 !='' &&  $request->date2 != ''){
            $date1 = date("Y-m-d", strtotime($request->date1));
            $date2 = date("Y-m-d", strtotime($request->date2));
        }else{
            $date1 = $request->date1;
            $date2 = $request->date2;
        }
        
        $columns = array(
            0 => 'id_keranjang',
            1 => 'tgl',   
            2 => 'id_barang',   
            3 => 'id_kategori',
            4 => 'id_kasir',
            5 => 'id_pekerja',
            6 => 'harga_br',
            7 => 'upah_pekerja',
            8 => 'bagian_owner',
            9 => 'qty',
            10 => 'diskon_br',
            11 => 'total',
            12 => 'status',
        );

        
            $totalData = PenjualanTokoDetail::where('id_penjualan',  $id_penjualan)
                                ->where('id_toko',  $id_toko)
                                ->when($date1!='', function ($query) use ($date1){
                                    return $query->whereDate('tgl', '>=', $date1);
                                })
                                ->when($date2!='', function ($query) use ($date2){
                                    return $query->whereDate('tgl', '<=', $date2);
                                })
                                ->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tbl_keranjang as a')
                ->select('a.*','b.nama_br as nama_brg','c.nama_kategori as nama_ktg')
                ->leftJoin('tbl_barang as b', 'a.id_barang', 'b.id_barang')
                ->leftJoin('tbl_kategori as c', 'a.id_kategori', 'c.id_kategori')
                ->where('a.id_penjualan',  $id_penjualan)
                ->where('a.id_toko',  $id_toko)
                ->when($date1!='', function ($query) use ($date1){
                    return $query->whereDate('a.tgl', '>=', $date1);
                })
                ->when($date2!='', function ($query) use ($date2){
                    return $query->whereDate('a.tgl', '<=', $date2);
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tbl_keranjang as a')
                ->select('a.*','b.nama_br as nama_brg','c.nama_kategori as nama_ktg')
                ->leftJoin('tbl_barang as b', 'a.id_barang', 'b.id_barang')
                ->leftJoin('tbl_kategori as c', 'a.id_kategori', 'c.id_kategori')
                ->where('a.id_penjualan',  $id_penjualan)
                ->where('a.id_toko',  $id_toko)
                ->when($date1!='', function ($query) use ($date1){
                    return $query->whereDate('a.tgl', '>=', $date1);
                })
                ->when($date2!='', function ($query) use ($date2){
                    return $query->whereDate('a.tgl', '<=', $date2);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('b.nama_br','LIKE',"%{$search}%")
                            ->orWhere('a.tgl','LIKE',"%{$search}%")
                            ->orWhere('c.nama_kategori','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tbl_keranjang as a')
                ->select('a.*','b.nama_br as nama_brg','c.nama_kategori as nama_ktg')
                ->leftJoin('tbl_barang as b', 'a.id_barang', 'b.id_barang')
                ->leftJoin('tbl_kategori as c', 'a.id_kategori', 'c.id_kategori')
                ->where('a.id_penjualan',  $id_penjualan)
                ->where('a.id_toko',  $id_toko)
                ->when($date1!='', function ($query) use ($date1){
                    return $query->whereDate('a.tgl', '>=', $date1);
                })
                ->when($date2!='', function ($query) use ($date2){
                    return $query->whereDate('a.tgl', '<=', $date2);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('b.nama_br','LIKE',"%{$search}%")
                            ->orWhere('a.tgl','LIKE',"%{$search}%")
                            ->orWhere('c.nama_kategori','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {

                if($service->tgl == ""){
                    $tgl = "-";
                }else{
                    $tgl = date("d M, Y",strtotime($service->tgl));
                }

                if($service->status == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-success">Selesai</div>';
                }elseif($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Hutang</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Barang Keranjang</div>';
                }elseif($service->status == 4){
                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Bayar Nanti</div>';
                }elseif($service->status == 5){
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Transaksi Batal</div>';
                }else{
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Tidak Ada Status</div>';
                }

                $ksrd = UserToko::where('id_kasir',$service->id_kasir)->first();
                if($ksrd){
                    $ksrrow = $ksrd->nama_kasir;
                }else{
                    $ksrrow = '-';
                }

                $pkrd = UserToko::where('id_kasir',$service->id_pekerja)->first();
                if($pkrd){
                    $pkrrow = $pkrd->nama_kasir;
                }else{
                    $pkrrow = '-';
                }
                

                if($service->harga_br){
                    $hrg = number_format($service->harga_br);
                }else{
                    $hrg = '0';
                }

                if($service->upah_pekerja){
                    $uph = number_format($service->upah_pekerja);
                }else{
                    $uph = '0';
                }

                if($service->bagian_owner){
                    $bgn = number_format($service->bagian_owner);
                }else{
                    $bgn = '0';
                }

                if($service->diskon_br){
                    $dskn = number_format($service->diskon_br);
                }else{
                    $dskn = '0';
                }

                if($service->total){
                    $ttl = number_format($service->total);
                }else{
                    $ttl = '0';
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id_keranjang;
                $nestedData['tgl'] = $tgl;
                $nestedData['nama_brg'] = $service->nama_brg;
                $nestedData['nama_ktg'] = $service->nama_ktg;
                $nestedData['ksrrow'] = $ksrrow;
                $nestedData['pkrrow'] = $pkrrow;
                $nestedData['harga_br'] = $hrg;
                $nestedData['upah_pekerja'] = $uph;
                $nestedData['bagian_owner'] = $bgn;
                $nestedData['qty'] = $service->qty;
                $nestedData['diskon_br'] = $dskn;
                $nestedData['total'] = $ttl;
                $nestedData['status'] = $sst;
                $nestedData['url'] = encrypt($service->id_keranjang);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

///PRODUK----------------------------------------------------------
    public function ProdukToko(Request $request)
    {
        $data = (object)array(
            'title'     => 'Produk',
            'title_sub'     => 'Produk',
            'halaman'     => 'data'
        );
        return view('toko.produk',compact('data'))->with(["page" => "Produk"]);
    }

    public function DataProdukToko(Request $request)
    {
        
        try{
            

            $id_toko         = $request->id_toko;

            $datas = DB::table('tbl_barang as a')
                    ->select('a.*','b.jenis_brg','c.nama_kategori')
                    ->leftJoin('tbl_jenis as b','a.id_jenis','=','b.id_jenis')
                    ->leftJoin('tbl_kategori as c','a.id_kategori','=','c.id_kategori')
                    ->where('a.id_toko',$id_toko)
                ->get();

            if($datas)
            {
                $report_data    = '';
                $no             = 1;

                $nmtko = Toko::where('id_toko',$id_toko)->first();
                if($nmtko){
                    $namatoko = $nmtko->nama_toko;
                }else{
                    $namatoko = "";
                }
                
                $report_data .= '
                                <div class="card-header">
                                    <h4 class="card-title">Data Produk - '.$namatoko.' </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5%" rowspan="2">No.</th>
                                            <th rowspan="2">Nama Barang</th>
                                            <th rowspan="2">Jenis</th>
                                            <th rowspan="2">Kategori</th>
                                            <th rowspan="2">Stok</th>
                                            <th rowspan="2">Harga Satuan</th>
                                            <th  colspan="2">Tanggal / Waktu</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">Tgl Masuk</th>
                                            <th rowspan="2">Tgl Update</th>
                                        </tr>
                                        </thead>
                                        <tbody >';
                foreach ($datas as $data)
                {
                  

                    if($data->harga_satuan != ""){
                        $harga_satuan = number_format($data->harga_satuan);
                    }else{
                        $harga_satuan = '0';
                    }

                    if($data->tgl_masuk == ""){
                        $tgl_masuk = "-";
                    }else{
                        $tgl_masuk = date("d M, Y",strtotime($data->tgl_masuk));
                    }

                    if($data->tgl_update == ""){
                        $tgl_update = "-";
                    }else{
                        $tgl_update = date("d M, Y",strtotime($data->tgl_update));
                    }

                    if($data->foto == ""){
                        $image = url('placeholder.png');
                    }else{
                        $image = url($data->foto);
                    }
                    
                    $report_data .= '<tr>
                                        <td align="center">' . $no++ . '</td>
                                        <td style="white-space: nowrap;"> <div class="d-flex align-items-center">
                                                <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                                <div class="ml-1">
                                                    <span class="text-bold-500"> <a href="#" class="view_photo" a="'.$data->id_barang.'">'.$data->nama_br.'</a></span><br>
                                                    <span class="text-muted"><small> Ket :'.$data->deskripsi.'</small></span>
                                                </div>
                                            </div></td>
                                        <td>' . $data->jenis_brg . '</td>
                                        <td>' . $data->nama_kategori . '</td>
                                        <td>' . $data->jumlah_semua . '</td>
                                        <td>' . $harga_satuan . '</td>
                                        <td style="white-space: nowrap;">' . $tgl_masuk . '</td>
                                        <td style="white-space: nowrap;">' . $tgl_update . '</td>
                                    </tr>';

                    
                    
                        // $saldosknrg   += $data->stok_masuk-$data->stok_keluar;
                }

                

                $report_data .= '</tbody>
                            </table>
                        </div>
                        </div>';

                $response['status']     = 'success';
                $response['message']    = 'Data Laporan berhasil ditampilkan';
                $response['data']       = $report_data;
            }
            else
            {
                $response['status']     = 'fail';
                $response['message']    = 'Data Laporan tidak tersedia!';
            }

        } catch (QueryException $ex) {
            $response['status']     = 'fail';
            $response['message']    = 'Gagal memuat data laporan, coba lagi!'. $ex->getMessage();
        }
        
        

        return response()->json($response);
    }

    public function ProdukTokoImageView($id)
    {
        $sumber = ProdukToko::find($id);


        return response()->json(['data' => $sumber]);
    }

}
