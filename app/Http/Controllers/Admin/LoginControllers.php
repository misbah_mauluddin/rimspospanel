<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Http\Models\Admin;
use App\Http\Models\Token;
use App\Http\Models\Aplikasi;


class LoginControllers extends Controller
{

    public function getHash($password){
        $salt       = sha1(rand());
        $salt       = substr($salt, 0, 10);
        $encrypted  = password_hash($password.$salt, PASSWORD_DEFAULT);
        $hash       = array("salt" => $salt, "encrypted" => $encrypted);

        return $hash;
    }

    public function verifyHash($password, $hash){
        return password_verify($password, $hash);
    }

    ///-------------------------------------------------------------
    public function index(Request $request){
  	  	if(session()->get('is_active') == '1' && session()->get('is_logged') == '1'){
          
            return redirect('/admin-panel/dashboard');
            
        }else{
            return redirect('/admin-panel/login');
        }
  	}

  	public function logout(Request $request){

	      Session::flush();
        Session::save();
	      
	      return redirect('/admin-panel/login');

	  }

	  public function login(Request $request){
  	  	
  	   if(session()->get('is_active') == '1' && session()->get('is_logged') == '1'){
          
            return redirect('/admin-panel/dashboard');
            
        }else{
            return view('login');
        }
  	}

    public function emails(Request $request){
        
       return view('emails.email_notif');
    }

    public function resetpassword(Request $request){
        
       return view('resetpassword.resetpassword');
    }

    public function kirimPasswordBaru(Request $request)
    {
      $validator = Validator::make($request->all(),   
        
            array(
                    'email' => 'required',
                 ),

            array(
                    'email.required' => 'username tidak boleh kosong',         
                  )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi)
        if($validator->fails()){

            $eror = $validator->messages()->all();

            return redirect('/resetpassword')->withInput($request->input())->withErrors($validator);
        
        }else {

          $email = $request->input('email');
          $password = '12345678';

          // $senderEmail = 'rims@gmail.com';
          // $senderName = 'UND Inventory';
          // $subject = 'Permintaan Reset Password';

          $getUserData = Admin::where('email', '=', $email)->orWhere('username', $email)->first();
          $getAppData = Aplikasi::first();

          if(!$getUserData){
            return back()->with("fail", "Email/Username tidak terdaftar!")->withInput();
          }else{
            $hash = $this->getHash($password);
            $updatePass = Admin::where('email', '=', $email)->orWhere('username', $email)
                            ->update(['password' => $hash['encrypted'], 'salt'=> $hash['salt']]);

            if(!$updatePass){
              return back()->with("fail", "Gagal Mereset Password")->withInput();
            }

            $data = array(
              'name' => $getUserData->name, 
              'password' => $password,
              'logo' => 'https://und.rims.co.id/simpeg_und/assets/img/logoweb/logo_und_thin.png',
              'link_web' => 'https://und.rims.co.id/',
              'nama_perusahaan' => $getAppData->nama_perusahaan,
              'nama_aplikasi' => $getAppData->nama_aplikasi,
              'alamat_perusahaan' => $getAppData->alamat
            );
      
            // echo $getAppData->image;
            Mail::send('resetpassword.emailtemplate', $data, function($mail) use($email)
            {
              $mail->to($email, 'no-reply')
                    ->subject('Permintaan Reset Password');
              $mail->from('rims@gmail.com', 'Inventori PT. UJUNG NEUBOK DALAM');
            });
      
            if(Mail::failures()){
              // return "Gagal mengirim email";
              return back()->with('fail', 'Gagal mengirim email');
            }else{
              // return "Email berhasil dikirim!";
              return back()->with('success', 'Password Berhasil Dikirim ke Email Anda');
            }
          }
        }
    }

    public function resetpasswordaksi(Request $request){
        
        $validator = Validator::make($request->all(),   
    
        array(
                
                'email' => 'required'
             ),

        array(
                           
                'email.required' => 'email tidak boleh kosong',
            )
        );

        
        if($validator->fails()){

            $eror = $validator->messages()->all();

            return redirect('/reset-password')->withInput($request->input())->withErrors($validator);
        
        }else {

            $admin = Admin::where('email',$request->email)->first();

            if($admin){
              $token = Token::where('email',$request->email)->first();
              $email = $token->email;
              $tokenemail = $token->token;

              $url = url('/reset-password');
              $link = $url.'/'.$tokenemail;

              $message_body = array('emailreset' => $link, 'nama' => $admin->nama);
              $subject = "SIMAUN - Sistem Manajemen ASN Untuk Negeri - Reset Password";
      
              Mail::send('emails.email_admin', array('mail_body' => $message_body), function ($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                });

              return redirect('/reset-password')->with('success','Email Berhasil Dikirimkan');
            }else{
              return redirect('/reset-password')->with('fail','Username Atau Email Tidak Ditemukan!.');
            }
        }
       
    }

    public function resetpasswordtoken(Request $request,$token){
        
      $cek = Token::where('token',$token)->first();
      if($cek){

        return view('resetpasswordnew',compact('cek'));
      }else{
        return view('resetpasswordnot');
      }
    }

    public function resetpasswordtokenaksi(Request $request,$token){
        
        $validator = Validator::make($request->all(),   
    
        array(
                
                'password' => 'required',
                'password2' => 'required'
             ),

        array(
                           
                'password.required' => 'password tidak boleh kosong',
                'password2.required' => 'password2 tidak boleh kosong'
            )
        );

        
        if($validator->fails()){

            $eror = $validator->messages()->all();

            return redirect('reset-password/'.$token)->withInput($request->input())->withErrors($validator);
        
        }else {

            $cektoken = Token::where('token',$token)->first();
            if($request->password2 != $request->password){
                return redirect('reset-password/'.$token)->with('fail','Konfirmasi Password Tidak Sama!')->withInput($request->input());
            }else{

              $admin = Admin::where('email',$cektoken->email)->first();

              $hash               = $this->getHash($request->password);
              $encrypted_password = $hash['encrypted'];
              $salt               = $hash['salt'];

              $admin->password = $encrypted_password;
              $admin->salt = $salt;

              $admin->save();


              $cektoken = Token::where('token',$token)->first();
              $cektoken->delete();
              return redirect('/admin-panel/login')->with('success','Password Berhasil Diganti!');
            }
        }
    }


    public function register(Request $request){
  		return view('registrasi');
  	}


  	public function registerverify(Request $request){
        
        $validator = Validator::make($request->all(),   
    
        array(
                
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
                'password1' => 'required',
                'password2' => 'required'
             ),

        array(
                
                'name.required' => 'name tidak boleh kosong',             
                'email.required' => 'email tidak boleh kosong',             
                'username.required' => 'username tidak boleh kosong',
                'password1.required' => 'password1 tidak boleh kosong',
                'password2.required' => 'password2 tidak boleh kosong'
            )
        );

        
        if($validator->fails()){

            $eror = $validator->messages()->all();

            return redirect('/admin-panel/register')->withInput($request->input())->withErrors($validator);
        
        }else {

            $admin = Admin::where('username',$request->username)->first();

            if($admin){
              return redirect('/admin-panel/register')->with('fail','Username Sudah digunakan')->withInput($request->input());
            }else{
            	if($request->password2 != $request->password1){
            		return redirect('/admin-panel/register')->with('fail','Konfirmasi Password Tidak Sama!')->withInput($request->input());
            	}else{
            		$hash               = $this->getHash($request->password1);
		        	$encrypted_password = $hash['encrypted'];
		        	$salt               = $hash['salt'];
	            	
	              $admin = new Admin;

	              $admin->name = $request->name;
	              $admin->email = $request->email;
	              $admin->username = $request->username;
	              $admin->password = $encrypted_password;
	              $admin->salt = $salt;
	              $admin->role_id = 2;
	              $admin->is_active = 1;

	              $admin->save();

	              $token = new Token;

	              $token->email = $request->email;
	              $token->token = encrypt($request->name.$request->email);

	              $token->save();

	              return redirect('/admin-panel/login')->with('success','Anda Berhasil Mendaftar! Silahkan Login.');
            	}
            }
        }
       
    }

    public function loginverify(Request $request){
    	$validator = Validator::make($request->all(),   
        
            array(
                    
                    'username' => 'required',
                    'password' => 'required',
                 ),

            array(
                    
                    'username.required' => 'username tidak boleh kosong',              
                    'password.required' => 'Password tidak boleh kosong',
                  )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi)
        if($validator->fails()){

            $eror = $validator->messages()->all();

            return redirect('/admin-panel/login')->withInput($request->input())->withErrors($validator);
        
        }else {

            $username = $request->username;
            $password = $request->password;

            $admin = Admin::where('username',$username)->orWhere('email', $username)->first();

            if($admin){
                if($admin->is_active == 0){
                  return redirect('/admin-panel/login')->with('fail','Akun Anda Tidak Aktif!');
                }else{
                  $db_encrypted_password = $admin->password;
                  $salt = $admin->salt;

                  //Pengecekan Password
                  $hasil_pass = $this->verifyHash($password.$salt,$db_encrypted_password);

                  if($hasil_pass) {
                   $take = Admin::where('username',$username)->orWhere('email', $username)->first();
                        Session::put('name', $take->name);
                         Session::put('email', $take->email);
                         Session::put('id', $take->id);
                         Session::put('idadmin', $take->id);
                         Session::put('username', $take->username);
                         Session::put('role_id', $take->role_id);
                         Session::put('is_active', $take->is_active);
                         Session::put('is_logged', '1');
                          Session::save();
                         return redirect('/admin-panel/dashboard');
                         
                  }else {
                    return redirect('/admin-panel/login')->with('fail','Password anda Salah!');
                  }
                }
            }else {
	      		   return redirect('/admin-panel/login?error=1')->with('fail','Username Atau Email Tidak Ditemukan!');
            }
        }
    }

}
