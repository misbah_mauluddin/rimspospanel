<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use Helperss;

use App\Http\Models\Admin;
use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;
use App\Http\Models\AppVersion;
use App\Http\Models\Role;

use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;


class HomeController extends Controller
{

    public function getHash($password){
        $salt       = sha1(rand());
        $salt       = substr($salt, 0, 10);
        $encrypted  = password_hash($password.$salt, PASSWORD_DEFAULT);
        $hash       = array("salt" => $salt, "encrypted" => $encrypted);

        return $hash;
    }

    public function verifyHash($password, $hash){
        return password_verify($password, $hash);
    }


    public function index(Request $request){
        return redirect('/admin-panel/dashboard');
    }

    public function dashboard(Request $request){

        if(session()->get('is_active') == '1' && session()->get('is_logged') == '1'){
          
            $data = (object)array(
                'total_customer' => Toko::where('status',1)->count(),
                'total_customer_byr' => DB::table('tbl_toko as a')
                                ->select(DB::Raw('COUNT(b.id_toko) as ttl'))
                                ->leftJoin('tgh_toko as b', 'a.id_toko', 'b.id_toko')
                                ->where('a.status',1)
                                ->where('b.status',1)
                                ->whereMonth('b.periode',date('m'))->first(),
                'total_customer_blmbyr' => DB::table('tbl_toko as a')
                                ->select(DB::Raw('COUNT(b.id_toko) as ttl'))
                                ->leftJoin('tgh_toko as b', 'a.id_toko', 'b.id_toko')
                                ->where('a.status',1)
                                ->where('b.status',0)
                                ->whereMonth('b.periode',date('m'))->first(),
                'total_customer_blmaktif' => Toko::where('status','!=',1)->count(),
                'title_sub' => "Surat Permintaan Barang (SPB)",

            );
          
            // return view('dashboard')->with(["page" => "Dashboard"]);
            return view('dashboard', compact('data'))->with(["page" => "Dashboard"]);
            
        }else{
            return redirect('/admin-panel/dashboard');
           
        }
        
        
    }


    public function get_reg(Request $request)
    {
        $id = $request->a;

        $regencies = Regencies::where('province_id',$id)->orderBy('name','ASC')->get();
        $reg_data = array();
        try {
            $reg_data['status'] = 'SUCCESS';
            foreach ($regencies as $reg)
            {
                $reg_data['data'][] = array(
                    'a' => $reg->id,
                    'b' => $reg->name,
                );
            }

            
        } catch (QueryException $ex) {
            $reg_data['status'] = 'FAILED';
            $reg_data['msg'] = 'Data provinsi tidak ditemukan ' . $ex->getMessage();
        }

        echo json_encode($reg_data);
    }

    public function get_dist(Request $request)
    {
        $id = $request->a;

        $districts = Districts::where('regency_id',$id)->orderBy('name','ASC')->get();
        $dist_data = array();
        try {
          if(!empty($id)){
            if(!empty($districts)){
                $dist_data['status'] = 'SUCCESS';
                foreach ($districts as $dist)
                {
                    $dist_data['data'][] = array(
                        'a' => $dist->id,
                        'b' => $dist->name,
                    );
                }
              }else{
                $dist_data['status'] = 'FAILED';
                $dist_data['msg'] = 'Data tidak ditemukan ';
              }
          }else{
            $dist_data['status'] = 'FAILED';
            $dist_data['msg'] = 'Data tidak ditemukan';
          }
            
            

            
        } catch (QueryException $ex) {
            $dist_data['status'] = 'FAILED';
            $dist_data['msg'] = 'Data Pusat tidak ditemukan ' . $ex->getMessage();
        }

        echo json_encode($dist_data);
    }

    public function get_reg_edit(Request $request)
    {
        $id = $request->a;
        $idreg = $request->b;

        $regenciesedit = Regencies::where('id',$idreg)->orderBy('name','ASC')->first();
        $regencies = Regencies::orderBy('name','ASC')->get();
        $reg_data = array();
        try {
            $reg_data['status'] = 'SUCCESS';
            foreach ($regencies as $reg)
            {
                $reg_data['data'][] = array(
                    'a' => $reg->id,
                    'b' => $reg->name,
                    'aa' => $regenciesedit->id,
                    'bb' => $regenciesedit->name,
                );
            }

            
        } catch (QueryException $ex) {
            $reg_data['status'] = 'FAILED';
            $reg_data['msg'] = 'Data provinsi tidak ditemukan ' . $ex->getMessage();
        }

        echo json_encode($reg_data);
    }

    public function get_dist_edit(Request $request)
    {
        $id = $request->a;
        $iddua = $request->b;
        // $idketiga = $request->aa;

        $districtsedit = Districts::where('regency_id',$id)->where('id',$iddua)->orderBy('name','ASC')->first();
        if($districtsedit){
            $iiddata = $districtsedit->id;
            $namdata = $districtsedit->name;
        }else{
            $iiddata = "-";
            $namdata = "-";
        }
        $districts = Districts::where('regency_id',$id)->orderBy('name','ASC')->get();
        $dist_data = array();
        try {
          if(!empty($id)){
            if(!empty($districts)){
                $dist_data['status'] = 'SUCCESS';
                foreach ($districts as $dist)
                {
                  
                    $dist_data['data'][] = array(
                        'a' => $dist->id,
                        'b' => $dist->name,
                        'aa' => $iiddata,
                        'bb' => $namdata,
                    );
                }
              }else{
                $dist_data['status'] = 'FAILED';
                $dist_data['msg'] = 'Data tidak ditemukan ';
              }
          }else{
            $dist_data['status'] = 'FAILED';
            $dist_data['msg'] = 'Data tidak ditemukan';
          }
            
            

            
        } catch (QueryException $ex) {
            $dist_data['status'] = 'FAILED';
            $dist_data['msg'] = 'Data Pusat tidak ditemukan ' . $ex->getMessage();
        }

        echo json_encode($dist_data);
    }
    
    
    ////// Manajemen User
    public function ManajemenUser(Request $request){
        $data = (object)array(
            'title'     => 'User',
            'title_sub'     => ' User',
            'halaman'     => 'data'
        );
        return view('role.datauser',compact('data'))->with(["page" => "Data User"]);
    }

    public function DataUser(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'name',
            3 => 'role_id',
            4 => 'is_active',
        );

        $totalData = Admin::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user as a')
            ->select('a.id', 'a.name', 'a.email', 'a.username', 'a.image', 'a.role_id', 'a.is_active', 'a.created_at', 'b.role as rolenama')
                ->leftJoin('user_role as b', 'a.role_id', '=', 'b.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user as a')
                ->select('a.*')
                ->where(function ($query) use ($search){
                        $query->orWhere('a.id','LIKE',"%{$search}%")
                            ->orWhere('a.username','LIKE',"%{$search}%")
                            ->orWhere('a.name','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('user as a')
                ->select('a.*')
                ->where(function ($query) use ($search){
                        $query->orWhere('a.id','LIKE',"%{$search}%")
                            ->orWhere('a.username','LIKE',"%{$search}%")
                            ->orWhere('a.name','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->image == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->image);
                }

                if($service->is_active == 0){
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Nonaktif</div>';
                }elseif($service->is_active == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-success">Aktif</div>';
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['username'] = $service->username;
                $nestedData['nama'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        </span>
                                        <div class="ml-1">
                                            <span class="text-bold-500"> <a href="#" class="view_photo" a="'.$service->id.'">'.$service->name.'</a></span><br>
                                            <span class="text-muted"><small> role : '.$service->rolenama.'</small></span>
                                        </div>
                                    </div>';
                $nestedData['email'] = $service->email;
                $nestedData['status'] = $sst;
                $nestedData['namabar'] = $service->name;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function UserImageView($id)
    {
        $sumber = Admin::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function EditUserView(Request $request,$id)
    {
        try {
            $data = Admin::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'User',
                    'title_sub'     => 'User',
                    'halaman'     => 'update',
                    'select'    => (object) array(
                        'role'  => Role::orderBy('role', 'ASC')->get(),
                        // 'jenis'  => Jenis::orderBy('nama', 'ASC')->get(),
                        // 'merek'  => Merek::orderBy('nama', 'ASC')->get()
                    ),
                    'form'      => $data
                );
                return view('role.datauser', compact('data'))->with(["page" => "Data User"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function TambahUserView(Request $request)
    {
        $data = (object)array(
            'title'     => 'User',
            'title_sub'     => 'User',
            'halaman'     => 'input',
            'select'    => (object) array(
                'role'  => Role::orderBy('role', 'ASC')->get(),
                // 'jenis'  => Jenis::orderBy('nama', 'ASC')->get(),
                // 'merek'  => Merek::orderBy('nama', 'ASC')->get()
            ),
        );
        return view('role.datauser',compact('data'))->with(["page" => "Data User"]);
    }

    public function EditPasswordView(Request $request,$id)
    {
        try {
            $data = Admin::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Password',
                    'title_sub'     => 'Password',
                    'halaman'     => 'password',
                    'select'    => (object) array(
                        'role'  => Role::orderBy('role', 'ASC')->get(),
                        // 'jenis'  => Jenis::orderBy('nama', 'ASC')->get(),
                        // 'merek'  => Merek::orderBy('nama', 'ASC')->get()
                    ),
                    'form'      => $data
                );
                return view('role.datauser', compact('data'))->with(["page" => "Data User"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function UserView(Request $request,$id)
    {
        try {
            $data = Admin::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'User',
                    'title_sub'     => 'User',
                    'halaman'     => 'detail',
                    'select'    => (object) array(
                        'role'  => Role::where('id', $data->role_id)->first()
                    ),
                    'form'      => $data
                );
                return view('role.datauser', compact('data'))->with(["page" => "Data User"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }
    
    public function TambahUserAksi(Request $request)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = Admin::where('username', $request->username)->first();
                if($dataExists)
                {
                    return back()->with("fail", "Username <strong>" . $request->username . "</strong> sudah terpakai")->withInput();
                }

                $dataEmailExists = Admin::where('email', $request->email)->first();
                if($dataEmailExists)
                {
                    return back()->with("fail", "Email <strong>" . $request->email . "</strong> sudah terpakai")->withInput();
                }

                $data                       = new Admin();
                $data->name                 = $request->nama;
                $data->username             = $request->username;
                $data->email                = $request->email;
                $data->role_id              = $request->role;
                $data->is_active            = $request->status;

                $hash               = $this->getHash($request->password);
                $encrypted_password = $hash['encrypted'];
                $salt               = $hash['salt'];

                $data->password = $encrypted_password;
                $data->salt = $salt;

                if (Input::file('image')) {
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/user');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/manajement/user')->with('success', 'Berhasil tambah User <strong>' . $data->name . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal tambah User " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function EditUserAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = Admin::where('username', $request->username)->where('id', '<>', decrypt($id))->first();
                if($dataExists)
                {
                    return back()->with("fail", "Username <strong>" . $request->username . "</strong> sudah terpakai")->withInput();
                }

                $dataEmailExists = Admin::where('email', $request->email)->where('id', '<>', decrypt($id))->first();
                if($dataEmailExists)
                {
                    return back()->with("fail", "Email <strong>" . $request->email . "</strong> sudah terpakai")->withInput();
                }

                $data                       = Admin::where('id', decrypt($id))->first();
                $data->name                 = $request->nama;
                $data->username             = $request->username;
                $data->email                = $request->email;
                $data->role_id              = $request->role;
                $data->is_active            = $request->status;

                if (Input::file('image')) {
                    if ($data->image != "") {
                        $path = $data->image;
                        unlink(public_path($path));
                    }
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/user');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/manajement/user')->with('success', 'Berhasil ubah User <strong>' . $data->name . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah User " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function UserDelete(Request $request)
    {
        try{
            $data = Admin::where('id', $request->id)->first();
            if($data)
            {
                if ($data->image != "") {
                    $path = $data->image;
                    unlink(public_path($path));
                }
                $data->delete();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Hapus User <strong>'. $data->name .'</strong>';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data User';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function EditPasswordAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{
                $hash               = $this->getHash($request->password);
                $encrypted_password = $hash['encrypted'];
                $salt               = $hash['salt'];

                $data                       = Admin::where('id', decrypt($id))->first();
                $data->password = $encrypted_password;
                $data->salt = $salt;

                $data->save();

                return redirect('/admin-panel/manajement/user')->with('success', 'Berhasil ubah password User <strong>' . $data->name . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah password User " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }


    //////////////// User Profile
    public function UserProfile(Request $request){

        try {
            $data = Admin::where('id', session()->get('idadmin'))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'My Profile',
                    'title_sub' => ' My Profile',
                    'halaman'   => 'data',
                    'form'      => $data
                );
                return view('role.myprofile',compact('data'))->with(["page" => "My Profile"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }

    }

    public function EditMyProfile(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = Admin::where('username', $request->username)->where('id', '<>', decrypt($id))->first();
                if($dataExists)
                {
                    return back()->with("fail", "Username <strong>" . $request->username . "</strong> sudah terpakai")->withInput();
                }

                $dataEmailExists = Admin::where('email', $request->email)->where('id', '<>', decrypt($id))->first();
                if($dataEmailExists)
                {
                    return back()->with("fail", "Email <strong>" . $request->email . "</strong> sudah terpakai")->withInput();
                }

                $data                       = Admin::where('id', decrypt($id))->first();
                $data->name                 = $request->nama;
                $data->username             = $request->username;
                $data->email                = $request->email;

                if (Input::file('image')) {
                    if ($data->image != "") {
                        $path = $data->image;
                        unlink(public_path($path));
                    }
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/user');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/user/profile')->with('success', 'Data Anda Berhasil Diubah</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah Data " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function ChangeMyPassword(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            // $hash               = $this->getHash($request->password_lama);
            // $encrypted_password = $hash['encrypted'];
            // print_r($encrypted_password);
            // exit;
            $admin = Admin::where('id', decrypt($id))->first();
            
            try{
                $db_encrypted_password = $admin->password;
                $salt = $admin->salt;

                $hasil_pass = $this->verifyHash(($request->password_lama).$salt,$db_encrypted_password);
                if(!$hasil_pass)
                {
                    return back()->with("fail", "Password Lama <strong>Tidak Cocok</strong>")->withInput();
                }

                if($request->password == $request->password_lama){
                    return back()->with("fail", "Password Baru dan Password Lama <strong>Tidak Boleh Sama</strong>")->withInput();
                }

                $hash               = $this->getHash($request->password);
                $encrypted_password = $hash['encrypted'];
                $salt               = $hash['salt'];

                $data                       = Admin::where('id', decrypt($id))->first();
                $data->password = $encrypted_password;
                $data->salt = $salt;

                $data->save();

                return redirect('/admin-panel/user/profile')->with('success', 'Password Anda Berhasil Diubah</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah Password " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }
}
