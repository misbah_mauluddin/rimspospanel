<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;

use App\Http\Models\Satuan;
use App\Http\Models\Jenis;
use App\Http\Models\Merek;
use App\Http\Models\Barang;
use App\Http\Models\Supplier;

use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Districts;
use App\Http\Models\PoTerima;
use App\Http\Models\Admin;
use App\Http\Models\Aplikasi;

class SupplierController extends Controller
{


////SUPPLIER-----------------------------------------------
    
    public function Supplier(Request $request){
        $data = (object)array(
            'title'     => 'Supplier',
            'title_sub'     => 'Supplier',
            'halaman'     => 'data'
        );
        return view('supplier.supplier',compact('data'))->with(["page" => "Partner"]);
    }

    public function DataSupplier(Request $request)
    {

        $status = $request->status;
        $columns = array(
            0 => 'id',
            1 => 'nama',
            2 => 'alamat',
            3 => 'regency_id',
            4 => 'telepon',
            5 => 'hp',
            6 => 'status'
        );
        if($status == "all"){
            $totalData = Supplier::count();
        }else{
            $totalData = Supplier::where('status', $status)->count();
        }
        

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('supplier as a')
                ->select('a.*','b.name as namareg')
                ->leftJoin('regencies as b', 'a.regency_id', 'b.id')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('supplier as a')
                ->select('a.*','b.name as namareg')
                ->leftJoin('regencies as b', 'a.regency_id', 'b.id')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.id','LIKE',"%{$search}%")
                            ->orWhere('a.nama','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('b.name','LIKE',"%{$search}%")
                            ->orWhere('a.kontak_person','LIKE',"%{$search}%")
                            ->orWhere('a.telepon','LIKE',"%{$search}%")
                            ->orWhere('a.hp','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('supplier as a')
                ->select('a.*','b.name as namareg')
                ->leftJoin('regencies as b', 'a.regency_id', 'b.id')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.id','LIKE',"%{$search}%")
                            ->orWhere('a.nama','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('b.name','LIKE',"%{$search}%")
                            ->orWhere('a.kontak_person','LIKE',"%{$search}%")
                            ->orWhere('a.telepon','LIKE',"%{$search}%")
                            ->orWhere('a.hp','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->image == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->image);
                }

                if($service->status == 0){
                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                }elseif($service->status == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['nama'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        </span>
                                        <div class="ml-1">
                                            <span class="text-bold-500"> <a href="#" class="view_photo" a="'.$service->id.'">'.$service->nama.'</a></span><br>
                                            <span class="text-muted"><small> Person :'.$service->kontak_person.'</small></span>
                                        </div>
                                    </div>';
                $nestedData['alamat'] = $service->alamat;
                $nestedData['namareg'] = $service->namareg;
                $nestedData['telepon'] = $service->telepon;
                $nestedData['hp'] = $service->hp;
                $nestedData['status'] = $sst;
                $nestedData['namasup'] = $service->nama;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function SupplierImageView($id)
    {
        $sumber = Supplier::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function TambahSupplierView(Request $request)
    {
        $data = (object)array(
            'title'     => 'Supplier',
            'title_sub'     => 'Supplier',
            'halaman'     => 'input',
            'select'    => (object) array(
                'regency'  => Regencies::orderBy('name', 'ASC')->get()
            )
        );
        return view('supplier.supplier',compact('data'))->with(["page" => "Partner"]);
    }
    
    public function TambahSupplierAksi(Request $request)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = Supplier::where('nama', $request->nama)->first();
                if($dataExists)
                {
                    return back()->with("fail", "Supplier Dengan Nama <strong>" . $request->nama . "</strong> sudah terdata")->withInput();
                }

                $data                       = new Supplier();
                $data->nama            = $request->nama;
                $data->kontak_person             = $request->kontak_person;
                $data->telepon             = $request->telepon;
                $data->hp                 = $request->hp;
                $data->email                 = $request->email;
                $data->website           = $request->website;
                $data->alamat                = $request->alamat;
                $data->alamat_dua                  = $request->alamat_dua;
                $data->ket                  = $request->ket;
                $data->status                = $request->status;
                $data->regency_id               = $request->regency_id;
                $data->district_id               = $request->district_id;

                if (Input::file('image')) {
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/supplier');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/supplier')->with('success', 'Berhasil tambah Supplier <strong>' . $data->nama . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal tambah Supplier " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }

    public function EditSupplierView(Request $request,$id)
    {
        try {
            $data = Supplier::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Supplier',
                    'title_sub'     => 'Supplier',
                    'halaman'     => 'update',
                    'select'    => (object) array(
                        'regency'  => Regencies::orderBy('name', 'ASC')->get()
                    ),
                    'form'      => $data
                );
                return view('supplier.supplier', compact('data'))->with(["page" => "Partner"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function EditSupplierAksi(Request $request,$id)
    {
        if($request->isMethod("POST"))
        {
            try{
                $dataExists = Supplier::where('nama', $request->nama)->where('id', '<>', decrypt($id))->first();
                if($dataExists)
                {
                    return back()->with("fail", "Supplier Dengan nama <strong>" . $request->nama . "</strong> sudah terdata")->withInput();
                }

                $data               = Supplier::where('id', decrypt($id))->first();
                $data->nama            = $request->nama;
                $data->kontak_person             = $request->kontak_person;
                $data->telepon             = $request->telepon;
                $data->hp                 = $request->hp;
                $data->email                 = $request->email;
                $data->website           = $request->website;
                $data->alamat                = $request->alamat;
                $data->alamat_dua                  = $request->alamat_dua;
                $data->ket                  = $request->ket;
                $data->status                = $request->status;
                $data->regency_id               = $request->regency_id;
                $data->district_id               = $request->district_id;

                if (Input::file('image')) {
                    if ($data->image != "") {
                        $path = $data->image;
                        unlink(public_path($path));
                    }
                  $image = $request->file('image');
                  $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
                  $destinationPath = ('uploads/supplier');
                  $img = Image::make($image->getRealPath());
                  $img->resize(500, 500, function ($constraint) {
                      $constraint->aspectRatio();
                  })->save(public_path($destinationPath.'/'.$input['imagename']));

                  $image->move(public_path($destinationPath, '/'.$input['imagename']));

                  $direktori = $destinationPath.'/'.$input['imagename'];

                  $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/supplier')->with('success', 'Berhasil ubah Supplier <strong>' . $data->nama . '</strong>');
            }catch (QueryException $ex){
                return back()->with("fail", "Gagal ubah Supplier " . $ex->getMessage())->withInput();
            }
        }
        else
        {
            return abort(405);
        }
    }
    
    public function SupplierDelete(Request $request)
    {
        try{
            $data = Supplier::where('id', $request->id)->first();
            if($data)
            {
                if ($data->image != "") {
                    $path = $data->image;
                    unlink(public_path($path));
                }
                $data->delete();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Hapus Supplier <strong>'. $data->nama .'</strong>';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Supplier';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function SupplierView(Request $request,$id)
    {
        try {
            $data = Supplier::where('id', decrypt($id))->first();
            if($data)
            {
                $data = (object)array(
                    'title'     => 'Supplier',
                    'title_sub'     => 'Supplier',
                    'halaman'     => 'detail',
                    'select'    => (object) array(
                        'regency'  => Regencies::where('id',$data->regency_id)->orderBy('name', 'ASC')->first(),
                        'district'  => Districts::where('id',$data->district_id)->orderBy('name', 'ASC')->first()
                    ),
                    'form'      => $data
                );
                return view('supplier.supplier', compact('data'))->with(["page" => "Partner"]);
            }
            else
            {
                return abort(404);
            }
        } catch (QueryException $ex) {
            return abort(500);
        }
    }

    public function RiwayatPembelianSupplierView(Request $request)
    {

        $id = $request->id;

        $columns = array(
            0 => 'id',
            1 => 'nomor_po',
            2 => 'tgl_po',
            3 => 'tgl_terima',
            4 => 'id_supplier',
            5 => 'penerima_id',
            6 => 'ket'
        );

        $totalData = PoTerima::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('po_terima as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('po_terima as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.penerima_id','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_terima','LIKE',"%{$search}%")
                            ->orWhere('b.nama','LIKE',"%{$search}%")
                            ->orWhere('c.nomor_po','LIKE',"%{$search}%")
                            ->orWhere('c.tgl_po','LIKE',"%{$search}%")
                            ->orWhere('c.id_supplier','LIKE',"%{$search}%")
                            ->orWhere('c.pembuat_id','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('po_terima as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.penerima_id','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_terima','LIKE',"%{$search}%")
                            ->orWhere('b.nama','LIKE',"%{$search}%")
                            ->orWhere('c.nomor_po','LIKE',"%{$search}%")
                            ->orWhere('c.tgl_po','LIKE',"%{$search}%")
                            ->orWhere('c.id_supplier','LIKE',"%{$search}%")
                            ->orWhere('c.pembuat_id','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->tgl_po == ""){
                    $tgl_po = "-";
                }else{
                    $tgl_po = date("d-m-Y/H:i",strtotime($service->tgl_po));
                }

                if($service->tgl_terima == ""){
                    $tgl_terima = "-";
                }else{
                    $tgl_terima = date("d-m-Y/H:i",strtotime($service->tgl_terima));
                }

                $getadmin = Admin::where('id',$service->penerima_id)->first();
                if($getadmin){
                    $namadmin = $getadmin->name;
                }else{
                    $namadmin = "-";
                }

                if($service->status == 0){
                    $sst  = '<div class="badge badge-pill badge-glow badge-primary">Verifikasi</div>';
                }elseif($service->status == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Di Setujui (PROSES PO)</div>';
                }elseif($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-success">Selesai</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Di Tolak</div>';
                }elseif($service->status == 4){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Proses Terima Barang</div>';
                }elseif($service->status == 5){
                    $sst  = '<div class="badge badge-pill badge-glow badge-warning">Verifikasi Terima Barang(GM)</div>';
                }


                $nestedData['no'] = $no++;
                $nestedData['id_po'] = encrypt($service->id_po);
                $nestedData['id'] = $service->id;
                $nestedData['nomor'] = $service->nomor_po;
                $nestedData['tgl_po'] = $tgl_po;
                $nestedData['tgl_terima'] = $tgl_terima;
                $nestedData['supplier'] = $service->namasup;
                $nestedData['penerima'] = $namadmin;
                $nestedData['status'] = $sst;
                $nestedData['status_code'] = $service->status;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function RiwayatReturnBarangView(Request $request)
    {
        $id = $request->id;
        $columns = array(
            0 => 'id',
            1 => 'nomor_po',
            2 => 'id_supplier',
            3 => 'penerima_id',
            4 => 'ket'
        );

        $totalData = DB::table('po_return as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('po_return as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('po_return as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.penerima_id','LIKE',"%{$search}%")
                            ->orWhere('a.ket','LIKE',"%{$search}%")
                            ->orWhere('b.nama','LIKE',"%{$search}%")
                            ->orWhere('c.nomor_po','LIKE',"%{$search}%")
                            ->orWhere('c.id_supplier','LIKE',"%{$search}%")
                            ->orWhere('c.pembuat_id','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('po_terima as a')
                ->select('a.*','b.nama as namasup','c.nomor_po','c.tgl_po','c.id_supplier','c.status')
                ->leftJoin('po as c','a.id_po','=','c.id')
                ->leftJoin('supplier as b','c.id_supplier','=','b.id')
                ->where('b.id',$id)
                ->where(function ($query) use ($search){
                        $query->orWhere('a.penerima_id','LIKE',"%{$search}%")
                            ->orWhere('a.ket','LIKE',"%{$search}%")
                            ->orWhere('b.nama','LIKE',"%{$search}%")
                            ->orWhere('c.nomor_po','LIKE',"%{$search}%")
                            ->orWhere('c.id_supplier','LIKE',"%{$search}%")
                            ->orWhere('c.pembuat_id','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                

                $getadmin = Admin::where('id',$service->penerima_id)->first();
                if($getadmin){
                    $namadmin = $getadmin->name;
                }else{
                    $namadmin = "-";
                }



                $nestedData['no'] = $no++;
                $nestedData['id_po'] = encrypt($service->id_po);
                $nestedData['id'] = $service->id;
                $nestedData['nomor'] = $service->nomor_po;
                $nestedData['supplier'] = $service->namasup;
                $nestedData['penerima'] = $namadmin;
                $nestedData['ket'] = $service->ket;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function LaporanDataSupplierPrint(Request $request)
    {
        try{
            $columns = array(
                0 => 'id',
                1 => 'kode',
                2 => 'nama',
                3 => 'id_satuan',
                4 => 'id_jenis',
                5 => 'id_merek',
                6 => 'status'
            );

            $det_status = array(
                "all" => "SEMUA STATUS",
                0 => "TIDAK AKTIF",
                1 => "AKTIF"
            );

            // $limit = $request->input('length');
            // $start = $request->input('start');
            $status = $request->status;


            // $datas = DB::table('barang as a')
            //     ->select('a.*','b.nama as namasatuan','c.nama as namajenis','d.nama as namamerek')
            //     ->leftJoin('satuan as b', 'a.id_satuan', 'b.id')
            //     ->leftJoin('jenis as c', 'a.id_jenis', 'c.id')
            //     ->leftJoin('merek as d', 'a.id_merek', 'd.id')
            //     ->where('a.status', $status)
            //     // ->offset($start)
            //     // ->limit($limit)
            //     ->orderBy('nama','ASC')
            //     ->get();

            $datas = DB::table('supplier as a')
                ->select('a.*','b.name as kota')
                ->leftJoin('regencies as b', 'a.regency_id', 'b.id')
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status', '=', $status);
                })
                // ->where(function ($query) use ($search){
                //         $query->orWhere('a.id','LIKE',"%{$search}%")
                //             ->orWhere('a.nama','LIKE',"%{$search}%")
                //             ->orWhere('a.alamat','LIKE',"%{$search}%")
                //             ->orWhere('b.name','LIKE',"%{$search}%")
                //             ->orWhere('a.kontak_person','LIKE',"%{$search}%")
                //             ->orWhere('a.telepon','LIKE',"%{$search}%")
                //             ->orWhere('a.hp','LIKE',"%{$search}%");
                //     })
                // ->offset($start)
                // ->limit($limit)
                ->orderBy('id', 'asc')
                ->get();

            
            if($datas){
                $data = (object) array(
                    'title'     => 'LIST DATA SUPPLIER',
                    'filter'    => (object) array(
                        'aplikasi'    =>  Aplikasi::first()

                    ),
                    'status'    => $det_status[$status],
                    'report'    => $datas
                );

                $pdf = PDF::loadView('supplier.pdf.datasupplier', compact('data'))->setPaper('potrait', 'landscape');
                return $pdf->stream('LIST DATA SUPPLIER.pdf');
            }else{
                return abort(404);
            }
            
        }
        catch (QueryException $ex)
        {
            return abort(500);
        }
    }
}
