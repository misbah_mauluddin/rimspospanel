<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Session;
use Storage;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;
use DateTime;

use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\Admin;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



class LaporanController extends Controller
{
    
///////HISTORY PEMBAYARAN-------------------------------

    public function HistPembayaran(Request $request)
    {
        $data = (object)array(
            'title'     => 'History Pembayaran',
            'title_sub'     => 'History Pembayaran',
            'halaman'     => 'data'
        );
        return view('laporan.historytgh',compact('data'))->with(["page" => "History Pembayaran"]);
    }

    public function DataHistPembayaran(Request $request)
    {
        
        try{
            $date1 = $request->date1;
            $date2 = $request->date2;

            if($request->date1 != 'all' && $request->date2 != 'all')
            {
                $date1 = date('Y-m-d', strtotime($request->date1));
                $date2 = date('Y-m-d', strtotime($request->date2));
            }

            $id_toko         = $request->id_toko;

            $datas = DB::table('hist_tagihan_toko as a')
                    ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                    ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                    // ->where('a.jns','D')
                    ->when($id_toko!='', function ($query) use ($id_toko){
                        return $query->where('a.id_toko', $id_toko);
                    })
                    ->when($date1!='all', function ($query) use ($date1){
                        return $query->whereDate('a.tanggal', '>=', $date1);
                    })
                    ->when($date2!='all', function ($query) use ($date2){
                        return $query->whereDate('a.tanggal', '<=', $date2);
                    })
                    ->orderBy('a.tanggal')
                ->get();

            if($datas)
            {
                $report_data    = '';
                $no             = 1;

                if($id_toko == ""){
                    $namatoko = "Semua Toko";
                }else{
                   $nmtko = Toko::where('id_toko',$id_toko)->first();
                    if($nmtko){
                        $namatoko = $nmtko->nama_toko;
                    }else{
                        $namatoko = "";
                    } 
                }
                
                
                $report_data .= '
                                <div class="card-header">
                                    <h4 class="card-title">Data History Pembayaran - '.$namatoko.' </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th width="30%">Tanggal / Waktu</th>
                                            <th>Nama Toko</th>
                                            <th>Jumlah</th>
                                            <th >Ket</th>
                                        </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap;">';
                foreach ($datas as $data)
                {
                  
                  

                    if($data->saldo != ""){
                        $saldo = number_format($data->saldo);
                    }else{
                        $saldo = '0';
                    }

                    if($data->logo == ""){
                        $image = url('placeholder.png');
                    }else{
                        $image = url($data->logo);
                    }

                    $getjns = JenisUsaha::where('id_jenisusaha',$data->id_jenisusaha)->first();
                    if($getjns){
                        $jnsu = $getjns->nama_jenisusaha;
                    }else{
                        $jnsu = '-';
                    }
                    
                    $report_data .= '<tr>
                                        <td align="center">' . $no++ . '</td>
                                        <td>' . date('d M, Y / H:i', strtotime($data->tanggal)) . '</td>
                                        <td><div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                                <div class="ml-1">
                                                    <span class="text-bold-500"> '.$data->nama_toko.'</span><br>
                                                    <span class="text-muted"><small> Jns Usaha :'.$jnsu.'</small></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td align="center">' . $saldo . '</td>
                                        <td align="left">' . $data->ket . '</td>
                                    </tr>';

                    
                    
                        // $saldosknrg   += $data->stok_masuk-$data->stok_keluar;
                }

                

                $report_data .= '</tbody>
                            </table>
                        </div>
                        </div>';

                $response['status']     = 'success';
                $response['message']    = 'Data Laporan berhasil ditampilkan';
                $response['data']       = $report_data;
            }
            else
            {
                $response['status']     = 'fail';
                $response['message']    = 'Data Laporan tidak tersedia!';
            }

        } catch (QueryException $ex) {
            $response['status']     = 'fail';
            $response['message']    = 'Gagal memuat data laporan, coba lagi!'. $ex->getMessage();
        }
        
        

        return response()->json($response);
    }

///////HISTORY SEWA APP-------------------------------

    public function HistSewa(Request $request)
    {
        $data = (object)array(
            'title'     => 'History Sewa App',
            'title_sub'     => 'History Sewa App',
            'halaman'     => 'data'
        );
        return view('laporan.historysewa',compact('data'))->with(["page" => "History Sewa App"]);
    }

    public function DataHistSewa(Request $request)
    {
        
        try{
            $date1 = $request->date1;
            $date2 = $request->date2;

            if($request->date1 != 'all' && $request->date2 != 'all')
            {
                $date1 = date('Y-m-d', strtotime($request->date1));
                $date2 = date('Y-m-d', strtotime($request->date2));
            }

            $id_toko         = $request->id_toko;

            $datas = DB::table('hist_sewa_toko as a')
                    ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                    ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                    ->when($id_toko!='', function ($query) use ($id_toko){
                        return $query->where('a.id_toko', $id_toko);
                    })
                    ->when($date1!='all', function ($query) use ($date1){
                        return $query->whereDate('a.tanggal', '>=', $date1);
                    })
                    ->when($date2!='all', function ($query) use ($date2){
                        return $query->whereDate('a.tanggal', '<=', $date2);
                    })
                    ->orderBy('a.tanggal')
                ->get();

            if($datas)
            {
                $report_data    = '';
                $no             = 1;

                if($id_toko == ""){
                    $namatoko = "Semua Toko";
                }else{
                   $nmtko = Toko::where('id_toko',$id_toko)->first();
                    if($nmtko){
                        $namatoko = $nmtko->nama_toko;
                    }else{
                        $namatoko = "";
                    } 
                }

                
                
                
                $report_data .= '
                                <div class="card-header">
                                    <h4 class="card-title">Data History Sewa - '.$namatoko.' </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th width="30%">Tanggal / Waktu</th>
                                            <th>Nama Toko</th>
                                            <th>Status</th>
                                            <th >Ket</th>
                                        </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap;">';
                foreach ($datas as $data)
                {
                  
                  

                    if($data->logo == ""){
                        $image = url('placeholder.png');
                    }else{
                        $image = url($data->logo);
                    }

                    $getjns = JenisUsaha::where('id_jenisusaha',$data->id_jenisusaha)->first();
                    if($getjns){
                        $jnsu = $getjns->nama_jenisusaha;
                    }else{
                        $jnsu = '-';
                    }

                    if($data->stts == 0){
                        $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                    }elseif($data->stts == 1){
                        $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                    }elseif($data->stts == 2){
                        $sst  = '<div class="badge badge-pill badge-glow badge-info">Proses Aktifasi</div>';
                    }elseif($data->stts == 3){
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Di Hapus</div>';
                    }elseif($data->stts == 4){
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Suspend</div>';
                    }
                    
                    $report_data .= '<tr>
                                        <td align="center">' . $no++ . '</td>
                                        <td>' . date('d M, Y / H:i', strtotime($data->tanggal)) . '</td>
                                        <td><div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                                <div class="ml-1">
                                                    <span class="text-bold-500"> '.$data->nama_toko.'</span><br>
                                                    <span class="text-muted"><small> Jns Usaha :'.$jnsu.'</small></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td align="center">' . $sst . '</td>
                                        <td align="left">' . $data->ket . '</td>
                                    </tr>';

                    
                    
                        // $saldosknrg   += $data->stok_masuk-$data->stok_keluar;
                }

                

                $report_data .= '</tbody>
                            </table>
                        </div>
                        </div>';

                $response['status']     = 'success';
                $response['message']    = 'Data Laporan berhasil ditampilkan';
                $response['data']       = $report_data;
            }
            else
            {
                $response['status']     = 'fail';
                $response['message']    = 'Data Laporan tidak tersedia!';
            }

        } catch (QueryException $ex) {
            $response['status']     = 'fail';
            $response['message']    = 'Gagal memuat data laporan, coba lagi!'. $ex->getMessage();
        }
        
        

        return response()->json($response);
    }


///////HISTORY PEMUTUSAN APP-------------------------------

    public function HistSuspend(Request $request)
    {
        $data = (object)array(
            'title'     => 'History Pemutusan App',
            'title_sub'     => 'History Pemutusan App',
            'halaman'     => 'data'
        );
        return view('laporan.historysuspend',compact('data'))->with(["page" => "History Pemutusan App"]);
    }

    public function DataHistSuspend(Request $request)
    {
        
        try{
            $date1 = $request->date1;
            $date2 = $request->date2;

            if($request->date1 != 'all' && $request->date2 != 'all')
            {
                $date1 = date('Y-m-d', strtotime($request->date1));
                $date2 = date('Y-m-d', strtotime($request->date2));
            }

            $id_toko         = $request->id_toko;

            $datas = DB::table('hist_suspend_toko as a')
                    ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                    ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                    ->when($id_toko!='', function ($query) use ($id_toko){
                        return $query->where('a.id_toko', $id_toko);
                    })
                    ->when($date1!='all', function ($query) use ($date1){
                        return $query->whereDate('a.created_at', '>=', $date1);
                    })
                    ->when($date2!='all', function ($query) use ($date2){
                        return $query->whereDate('a.created_at', '<=', $date2);
                    })
                    ->orderBy('a.created_at')
                ->get();

            if($datas)
            {
                $report_data    = '';
                $no             = 1;

                if($id_toko == ""){
                    $namatoko = "Semua Toko";
                }else{
                   $nmtko = Toko::where('id_toko',$id_toko)->first();
                    if($nmtko){
                        $namatoko = $nmtko->nama_toko;
                    }else{
                        $namatoko = "";
                    } 
                }

                
                
                
                $report_data .= '
                                <div class="card-header">
                                    <h4 class="card-title">Data Pemutusan Sewa App - '.$namatoko.' </h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0 d-flex align-items-center">
                                            <li class="ml-2">
                                            <a href="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table id="sponsor" class="table nowrap table-bordered table-hover table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th width="30%">Tanggal / Waktu</th>
                                            <th>Nama Toko</th>
                                            <th >Ket</th>
                                        </tr>
                                        </thead>
                                        <tbody style="white-space: nowrap;">';
                foreach ($datas as $data)
                {
                  
                  

                    if($data->logo == ""){
                        $image = url('placeholder.png');
                    }else{
                        $image = url($data->logo);
                    }

                    $getjns = JenisUsaha::where('id_jenisusaha',$data->id_jenisusaha)->first();
                    if($getjns){
                        $jnsu = $getjns->nama_jenisusaha;
                    }else{
                        $jnsu = '-';
                    }
                    
                    $report_data .= '<tr>
                                        <td align="center">' . $no++ . '</td>
                                        <td>' . date('d M, Y', strtotime($data->tgl_suspend)) . '</td>
                                        <td><div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                                <div class="ml-1">
                                                    <span class="text-bold-500"> '.$data->nama_toko.'</span><br>
                                                    <span class="text-muted"><small> Jns Usaha :'.$jnsu.'</small></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td align="left">' . $data->ket . '</td>
                                    </tr>';

                    
                    
                        // $saldosknrg   += $data->stok_masuk-$data->stok_keluar;
                }

                

                $report_data .= '</tbody>
                            </table>
                        </div>
                        </div>';

                $response['status']     = 'success';
                $response['message']    = 'Data Laporan berhasil ditampilkan';
                $response['data']       = $report_data;
            }
            else
            {
                $response['status']     = 'fail';
                $response['message']    = 'Data Laporan tidak tersedia!';
            }

        } catch (QueryException $ex) {
            $response['status']     = 'fail';
            $response['message']    = 'Gagal memuat data laporan, coba lagi!'. $ex->getMessage();
        }
        
        

        return response()->json($response);
    }

}