<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use DateTime;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Models\Admin;
use App\Http\Models\Role;
use App\Http\Models\Aksesmenu;
use App\Http\Models\Submenu;
use App\Http\Models\TitleMenu;
use App\Http\Models\Menu;
use App\Http\Models\Token;
use App\Http\Models\Icons;

use App\Http\Models\Districts;
use App\Http\Models\Provinces;
use App\Http\Models\Regencies;
use App\Http\Models\Villages;

use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;

use App\Http\Models\Aplikasi;
use Illuminate\Database\QueryException;
class WebController extends Controller
{
    public function getHash($password){
        $salt       = sha1(rand());
        $salt       = substr($salt, 0, 10);
        $encrypted  = password_hash($password.$salt, PASSWORD_DEFAULT);
        $hash       = array("salt" => $salt, "encrypted" => $encrypted);

        return $hash;
    }

    public function verifyHash($password, $hash){
        return password_verify($password, $hash);
    }

    public function menuviews(Request $request){
        return view('menu.menu')->with(["page" => "Menu Management"]);
    }

    //////////////////////////// ROLE

    public function role()
    {
        return view('role.role')->with(["page" => "Role Akses Menu"]);
    }

    public function data_role(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'role'
        );

        $totalData = Role::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user_role')
                ->select('id', 'role')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user_role')
                ->select('id', 'role')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('role', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('user_role')
                ->select('id', 'role')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('role', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['role'] = $service->role;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function tambahroleaksi(Request $request)
    {
        if ($request->isMethod('POST')) {
            $sponsor = new Role();
            $sponsor->role = $request->judul;

            try {
                $sponsor->save();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Simpan Role';
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function editroleview($id)
    {
        $sumber = Role::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function roledelete(Request $request)
    {
        $id = $request->a;

        $sponsor = Role::find($id);


        try {
            $sponsor->delete();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Berhasil Hapus Role';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Role ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function editroleaksi(Request $request)
    {
        $id = $request->idbr;

        $data = Role::where('id', $id)->firstOrFail();

        if ($data) {
            $data->role = $request->juduledit;

            $data->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Berhasil Di Ubah';

        } else {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data tidak ditemukan!';
        }

        return response($this->response);
    }

    public function editroleakses(Request $request,$id)
    {

        // $menuuu = TitleMenu::orderBy('id','ASC')->get();
        $menuuu = DB::table('admin_menu_items')->orderBy('sort','ASC')->get();
        $role = Role::where('id',$id)->first();
         if ($request->ajax()) {
       
          return view('role.datarole', array('menuuu' => $menuuu,'role' => $role))->render();
        }

        // return response()->json(['data' => $sumber]);
    }

    public function editroleaksesaksi(Request $request)
    {
        $menu_id = $request->menuId;
        $role_id = $request->roleId;

        $akses = DB::table('user_access_menu')->where('role_id',$role_id)->where('menu_id',$menu_id)->first();

        if (empty($akses)) {
            $data = new Aksesmenu;
            $data->role_id = $role_id;
            $data->menu_id = $menu_id;

            $data->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Role Access Berhasil Di Ubah';

        } else {
            $aksess = DB::table('user_access_menu')->where('role_id',$role_id)->where('menu_id',$menu_id)->delete();
            
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data Role Access Berhasil Di Ubah';
        }

        return response($this->response);
    }

    //////////////////////////////////// MENU

    public function menu()
    {
        
        return view('menu.menu')->with(["page" => "Menu Management"]);
    }

    public function data_menu(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'id_titile_menu',
            2 => 'menu',
            3 => 'icon'
        );

        $totalData = DB::table('user_menu as a')
                ->select('a.id', 'a.menu','a.icon','b.menu as titlemenu')
                ->leftJoin('user_title_menu as b','a.id_titile_menu','=','b.id')
                ->where('a.id','!=','1')
                ->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user_menu as a')
                ->select('a.id', 'a.menu','a.icon','b.menu as titlemenu')
                ->leftJoin('user_title_menu as b','a.id_titile_menu','=','b.id')
                ->where('a.id','!=','1')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user_menu as a')
                ->select('a.id', 'a.menu','a.icon','b.menu as titlemenu')
                ->leftJoin('user_title_menu as b','a.id_titile_menu','=','b.id')
                ->where('a.id','!=','1')
                ->where('a.id', 'LIKE', "%{$search}%")
                ->orWhere('a.menu', 'LIKE', "%{$search}%")
                ->orWhere('b.menu', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('user_menu as a')
                ->select('a.id', 'a.menu','a.icon','b.menu as titlemenu')
                ->leftJoin('user_title_menu as b','a.id_titile_menu','=','b.id')
                ->where('a.id','!=','1')
                ->where('a.id', 'LIKE', "%{$search}%")
                ->orWhere('a.menu', 'LIKE', "%{$search}%")
                ->orWhere('b.menu', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {

               
                $iconn = "<i class='menu-livicon' style='visibility: visible; width: 20px;' data-icon='$service->icon'></i>";
                        
                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['titlemenu'] = $service->titlemenu;
                $nestedData['menu'] = $service->menu;
                $nestedData['icon'] = $iconn;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function tambahmenuaksi(Request $request)
    {
        if ($request->isMethod('POST')) {
            $sponsor = new Menu();
            $sponsor->id_titile_menu = $request->id_titile_menu;
            $sponsor->menu = $request->judul;
            $sponsor->icon = $request->icon;

            try {
                $sponsor->save();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Simpan Menu';
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function editmenuview($id)
    {
        $sumber = Menu::find($id);

        $menuuu = TitleMenu::where('id', $sumber->id_titile_menu)->first();
        $menuall = TitleMenu::where('id','!=','1')->get();

        $sbb = view('role.carititlemenu', compact('sumber', 'menuuu', 'menuall'))->render();
        $icc = view('role.cariicon', compact('sumber', 'menuuu', 'menuall'))->render();


        return response()->json(['data' => $sumber,'sbb' => $sbb,'icc' => $icc]);
    }

    public function menudelete(Request $request)
    {
        $id = $request->a;

        $sponsor = Menu::find($id);


        try {
            // $sub = Submenu::where('menu_id',$id)->delete();
            // $sub = Aksesmenu::where('menu_id',$id)->delete();
            $sponsor->delete();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Berhasil Hapus Menu';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Menu ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function editmenuaksi(Request $request)
    {
        $id = $request->idbr;

        $data = Menu::where('id', $id)->firstOrFail();

        if ($data) {
            $data->menu = $request->juduledit;
            $data->id_titile_menu = $request->id_titile_menuedit;
            $data->icon = $request->iconedit;

            $data->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Berhasil Di Ubah';

        } else {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data tidak ditemukan!';
        }

        return response($this->response);
    }

    //////////////////////////////////// TITLE MENU

    public function data_menutitle(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'menu'
        );

        $totalData = TitleMenu::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        // $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user_title_menu')
                ->select('id', 'menu')
                ->where('id','!=','1')
                // ->offset($start)
                ->limit($limit)
                ->orderBy('menu', 'ASC')
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user_title_menu')
                ->select('id', 'menu')
                ->where('id','!=','1')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('menu', 'LIKE', "%{$search}%")
                // ->offset($start)
                ->limit($limit)
                ->orderBy('menu', 'ASC')
                ->get();

            $totalFiltered = DB::table('user_title_menu')
                ->select('id', 'menu')
                ->where('id','!=','1')
                ->where('id', 'LIKE', "%{$search}%")
                ->orWhere('menu', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['menu'] = $service->menu;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function tambahmenutitleaksi(Request $request)
    {
        if ($request->isMethod('POST')) {
            $sponsor = new TitleMenu();
            $sponsor->menu = $request->judultitle;

            try {
                $sponsor->save();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Simpan TitleMenu';
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function editmenutitleview($id)
    {
        $sumber = TitleMenu::find($id);


        return response()->json(['data' => $sumber]);
    }

    public function menutitledelete(Request $request)
    {
        $id = $request->a;

        $sponsor = TitleMenu::find($id);
        $cek =  Menu::where('id_titile_menu',$id)->first();

        try {
            $sub1 = Aksesmenu::where('menu_id',$id)->delete();
            if($sub1){
                $sub2 = Submenu::where('menu_id',$cek->id)->delete();
                if($sub2){
                   $sub3 = Menu::where('id_titile_menu',$id)->delete(); 
                }
                
            }
            
            
            $sponsor->delete();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Berhasil Hapus TitleMenu';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus TitleMenu ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function editmenutitleaksi(Request $request)
    {
        $id = $request->idbrtitle;

        $data = TitleMenu::where('id', $id)->first();

        if ($data) {
            $data->menu = $request->juduledittitle;

            $data->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Berhasil Di Ubah';

        } else {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data tidak ditemukan!';
        }

        return response($this->response);
    }

    //////////////////////////////////// SUB MENU

    public function submenu()
    {
        $menuuu = Menu::where('id','!=','1')->get();
        $submenu = DB::table('user_sub_menu as a')
                ->select('a.*','b.menu')
                ->leftJoin('user_menu as b', 'a.menu_id', '=', 'b.id')
                ->orderBy('b.menu','ASC')
                ->get();
                // $icon = Icons::all();
        return view('role.submenu',compact('menuuu','submenu'))->with(["page" => "Submenu Management"]);
    }

    public function data_submenu(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'menu_id',
            2 => 'title',
            3 => 'url',
            4 => 'icon',
            5 => 'is_active'
        );

        $totalData = Submenu::where('is_active',1)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        // $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user_sub_menu as a')
                ->select('a.*','b.menu')
                ->leftJoin('user_menu as b', 'a.menu_id', '=', 'b.id')
                // ->offset($start)
                ->limit($limit)
                ->orderBy('a.menu_id', 'ASC')
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user_sub_menu as a')
                ->select('a.*','b.menu')
                ->leftJoin('user_menu as b', 'a.menu_id', '=', 'b.id')
                ->where('a.id', 'LIKE', "%{$search}%")
                ->where('a.title', 'LIKE', "%{$search}%")
                ->orWhere('b.menu', 'LIKE', "%{$search}%")
                // ->offset($start)
                ->limit($limit)
                ->orderBy('a.menu_id', 'ASC')
                ->get();

            $totalFiltered = DB::table('user_sub_menu as a')
                ->select('a.*','b.menu')
                ->leftJoin('user_menu as b', 'a.menu_id', '=', 'b.id')
                ->where('a.id', 'LIKE', "%{$search}%")
                ->where('a.title', 'LIKE', "%{$search}%")
                ->orWhere('a.menu_id', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->is_active != 1){
                    $je = "<button class='btn btn-danger glow popover-hover activee btn-sm' a='$service->id' b='$service->title' data-container='body' data-toggle='tooltip' data-placement='left' data-content='Active Menu'><i class='bx bxs-x-circle'></i> Tidak Aktif</button>";
                    
                }else{
                    $je = "<button class='btn btn-primary glow popover-hover shutt btn-sm' a='$service->id' b='$service->title' data-container='body' data-toggle='tooltip' data-placement='left' data-content='Inactive Menu'><i class='bx bx-check-double'></i> Aktif</button>";
                    
                }

                $iconn = '<button type="button" class="btn btn-icon rounded-circle btn-light-primary" style="padding-bottom: 12px;padding-left: 11px;">
                    <div class="fonticon-wrap"><i class="menu-livicon" data-options="name: '.$service->icon.'.svg; size: 20px; style: solid;"></i>
                    </div>
                </button>';



                if($service->menu_id != 1){
                    $menuuuu = $service->menu;
                    
                }else{
                    $menuuuu = 'Single Menu';
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['menu'] = $menuuuu;
                $nestedData['title'] = $service->title;
                $nestedData['icon'] = $iconn;
                $nestedData['is_active'] = $je;
                $nestedData['url'] = $service->url;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function tambahsubmenuaksi(Request $request)
    {
        if ($request->isMethod('POST')) {
            $sponsor = new Submenu();
            $sponsor->menu_id = $request->menu_id;
            $sponsor->title = $request->title;
            $sponsor->url = $request->url;
            $sponsor->icon = $request->icon;
            if($request->is_active == ""){
                $sponsor->is_active = 0;
            }else{
               $sponsor->is_active = $request->is_active; 
            }


            try {
                $sponsor->save();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Simpan Sub Menu';
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function editsubmenuview($id)
    {
        $sumber = Submenu::find($id);

        $menuuu = Menu::where('id', $sumber->menu_id)->first();
        $menuall = Menu::where('id','!=','1')->get();

        $sbb = view('role.carimenu', compact('sumber', 'menuuu', 'menuall'))->render();
        $icc = view('role.cariicon', compact('sumber', 'menuuu', 'menuall'))->render();


        return response()->json(['data' => $sumber,'sbb' => $sbb,'icc' => $icc]);
    }

    public function submenudelete(Request $request)
    {
        $id = $request->a;

        $sponsor = Submenu::find($id);

        try {
            
            $sponsor->delete();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Berhasil Hapus SubMenu';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus SubMenu ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function editsubmenuaksi(Request $request)
    {
        $id = $request->idbr;

        $data = Submenu::where('id', $id)->firstOrFail();

        if ($data) {
            $data->menu_id = $request->menu_idedit;
            $data->title = $request->titleedit;
            $data->url = $request->urledit;
            $data->icon = $request->iconedit;

            $data->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Berhasil Di Ubah';

        } else {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data tidak ditemukan!';
        }

        return response($this->response);
    }

    public function aktifsubmenu(Request $request)
    {
        $id = $request->a;

        $sponsor = Submenu::find($id);

        try {
            
            $sponsor->is_active = 1;
            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Sub Menu Berhasil Di Aktifkan!';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal Aktifkan SubMenu ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function nonaktifsubmenu(Request $request)
    {
        $id = $request->a;

        $sponsor = Submenu::find($id);

        try {
            
            $sponsor->is_active = 0;
            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Sub Menu Berhasil Di NonAktifkan!';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal NonAktifkan SubMenu ' . $ex->getMessage();
        }

        return response($this->response);
    }

    //////////////////////// MANAJEMENT USER

    public function manajementuser()
    {
        $reg = Regencies::get();
        $roleak = Role::where('role','!=','Super Admin')->get();
        return view('datauser.datauser',compact('roleak','reg'))->with(["page" => "Data User"]);
    }

    public function data_manajementuser(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'username',
            4 => 'role_id',
            5 => 'is_active',
            6 => 'created_at'
        );

        $totalData = Admin::where('role_id','!=','3')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('user as a')
                ->select('a.id', 'a.name', 'a.email', 'a.username', 'a.image', 'a.role_id', 'a.is_active', 'a.created_at','b.role as rolenama')
                ->leftJoin('user_role as b', 'a.role_id', '=', 'b.id')
                ->where('a.role_id','!=','3')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $services = DB::table('user as a')
                ->select('a.id', 'a.name', 'a.email', 'a.username', 'a.image', 'a.role_id', 'a.is_active', 'a.created_at','b.role as rolenama')
                ->leftJoin('user_role as b', 'a.role_id', '=', 'b.id')
                ->where('a.role_id','!=','3')
                ->where('a.name', 'LIKE', "%{$search}%")
                ->where('a.username', 'LIKE', "%{$search}%")
                ->where('a.email', 'LIKE', "%{$search}%")
                ->orWhere('b.role', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('user as a')
                ->select('a.id', 'a.name', 'a.email', 'a.username', 'a.image', 'a.role_id', 'a.is_active', 'a.created_at','b.role as rolenama')
                ->leftJoin('user_role as b', 'a.role_id', '=', 'b.id')
                ->where('a.role_id','!=','3')
                ->where('a.name', 'LIKE', "%{$search}%")
                ->where('a.username', 'LIKE', "%{$search}%")
                ->where('a.email', 'LIKE', "%{$search}%")
                ->orWhere('b.role', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->is_active != 1){
                    $je = "<button class='btn btn-danger btn-shadowed popover-hover activee btn-xs' a='$service->id' b='$service->name' data-container='body' data-toggle='tooltip' data-placement='left' data-content='Active Menu'><i class='bx bxs-x-circle'></i> Tidak Aktif</button>";
                    
                }else{
                    $je = "<button class='btn btn-info btn-shadowed popover-hover shutt btn-xs' a='$service->id' b='$service->name' data-container='body' data-toggle='tooltip' data-placement='left' data-content='Inactive Menu'><i class='bx bx-check-double'></i> Aktif</button>";
                    
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['name'] = $service->name;
                $nestedData['email'] = $service->email;
                $nestedData['username'] = $service->username;
                $nestedData['role_id'] = $service->rolenama;
                $nestedData['is_active'] = $je;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function tambahmanajementuseraksi(Request $request)
    {
        if ($request->isMethod('POST')) {
            $sponsor = new Admin();
            $sponsor->name = $request->name;
            $sponsor->email = $request->email;
            $sponsor->username = $request->username;
            $sponsor->role_id = $request->role_id;
            $sponsor->regency_id = $request->regency_id;

          $hash               = $this->getHash($request->password);
          $encrypted_password = $hash['encrypted'];
          $salt               = $hash['salt'];


            $sponsor->password = $encrypted_password;
            $sponsor->salt = $salt;

            if($request->is_active == ""){
                $sponsor->is_active = 0;
            }else{
               $sponsor->is_active = $request->is_active; 
            }

            if (Input::file('image')) {
              $image = $request->file('image');
              $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
              $destinationPath = ('uploads/profile');
              $img = Image::make($image->getRealPath());
              $img->resize(200, 200, function ($constraint) {
                  $constraint->aspectRatio();
              })->save(public_path($destinationPath.'/'.$input['imagename']));

              $image->move(public_path($destinationPath, '/'.$input['imagename']));

              $direktori = $destinationPath.'/'.$input['imagename'];
              $sponsor->image = $direktori;
            }



            try {
                $sponsor->save();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Berhasil Simpan Data User';
            } catch (QueryException $ex) {
                $this->response['status'] = 'fail';
                $this->response['msg'] = $ex->getMessage();
            }

        }
        return response($this->response);
    }

    public function editmanajementuserview($id)
    {
        $sumber = Admin::find($id);

        $rolee = Role::where('id', $sumber->role_id)->first();
        $roleeall = Role::where('role','!=','Super Admin')->get();

        $sbb = view('datauser.carirole', compact('sumber', 'rolee', 'roleeall'))->render();


        return response()->json(['data' => $sumber,'sbb' => $sbb]);
    }

    public function editmanajementuseraksi(Request $request)
    {
        $id = $request->idbr;

        $sponsor = Admin::where('id', $id)->firstOrFail();

        if ($sponsor) {
            $sponsor->name = $request->nameedit;
            $sponsor->email = $request->emailedit;
            $sponsor->username = $request->usernameedit;
            $sponsor->role_id = $request->role_idedit;
            $sponsor->regency_id = $request->regency_idedit;
            

            if (Input::file('imageedit')) {
                if ($sponsor->image != "") {
                    $path = $sponsor->image;
                    unlink(public_path($path));
                }
              $image = $request->file('imageedit');
              $input['imagename'] =  date('ymdhis').'.'.$image->getClientOriginalExtension();
              $destinationPath = ('uploads/profile');
              $img = Image::make($image->getRealPath());
              $img->resize(200, 200, function ($constraint) {
                  $constraint->aspectRatio();
              })->save(public_path($destinationPath.'/'.$input['imagename']));

              $image->move(public_path($destinationPath, '/'.$input['imagename']));

              $direktori = $destinationPath.'/'.$input['imagename'];
              $sponsor->image = $direktori;
            }

            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data Berhasil Di Ubah';

        } else {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Data tidak ditemukan!';
        }

        return response($this->response);
    }

    public function editpassowrdmanajementuserview($id)
    {
        $sumber = Admin::find($id);

        return response()->json(['data' => $sumber]);
    }

    public function editpassowrdmanajementuseraksi(Request $request)
    {
        $id = $request->idbrpw;

        $sponsor = Admin::find($id);

        try {
            
              $hash               = $this->getHash($request->passwordpw);
              $encrypted_password = $hash['encrypted'];
              $salt               = $hash['salt'];


            $sponsor->password = $encrypted_password;
            $sponsor->salt = $salt;


            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Password Berhasil Di Ganti';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Password Gagal Di Ganti ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function manajementuserdelete(Request $request)
    {
        $id = $request->a;

        $sponsor = Admin::find($id);

        try {
            if ($sponsor->image != "") {
                    $path = $sponsor->image;
                    unlink(public_path($path));
                }
            
            $sponsor->delete();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Berhasil Hapus Data User';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data User ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function aktifmanajementuser(Request $request)
    {
        $id = $request->a;

        $sponsor = Admin::find($id);

        try {
            
            $sponsor->is_active = 1;
            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data User Berhasil Di Aktifkan!';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal Aktifkan Data User ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function nonaktifmanajementuser(Request $request)
    {
        $id = $request->a;

        $sponsor = Admin::find($id);

        try {
            
            $sponsor->is_active = 0;
            $sponsor->save();

            $this->response['status'] = 'success';
            $this->response['msg'] = 'Data User Berhasil Di NonAktifkan!';
        } catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal NonAktifkan Data User ' . $ex->getMessage();
        }

        return response($this->response);
    }

    // PENGATURAN --> APLIKASI ------

    public function Aplikasi(Request $request)
    {
        $aplikasi = Aplikasi::first();
        $data = (object)array(
            'title' => 'Aplikasi',
            'title_sub' => 'Aplikasi',
            'form' => $aplikasi
        );
        return view('konfigurasi.aplikasi', compact('data'))->with(["page" => "Aplikasi"]);
    }

    public function UpdateAplikasiAksi(Request $request, $id)
    {
        if ($request->isMethod("POST")) {
            try {
                $dataExists = Aplikasi::where('nama_perusahaan', $request->nama_perusahaan)->where('id', '<>', decrypt($id))->first();
                if ($dataExists) {
                    return back()->with("fail", "Perusahaan Dengan Nama <strong>" . $request->nama_perusahaan . "<strong> Sudah Ada")->withInput();
                }

                $data = Aplikasi::where('id', decrypt($id))->first();
                $data->nama_aplikasi = $request->nama_aplikasi;
                $data->nama_perusahaan = $request->nama_perusahaan;
                $data->nama_lainnya = $request->nama_lainnya;
                $data->no_telepon = $request->no_telepon;
                $data->no_hp = $request->no_hp;
                $data->email = $request->email;
                $data->fax = $request->fax;
                $data->kodepos = $request->kodepos;
                $data->alamat = $request->alamat;
                $data->facebook = $request->facebook;
                $data->twitter = $request->twitter;
                $data->youtube = $request->youtube;
                $data->instagram = $request->instagram;
                $data->linkedin = $request->linkedin;

                if (Input::file('image')) {
                    if ($data->image != "") {
                        $path = $data->image;
                        unlink(public_path($path));
                    }
                    $image = $request->file('image');
                    $input['imagename'] =  date('ymdhis') . '.' . $image->getClientOriginalExtension();
                    $destinationPath = ('uploads/aplikasi/logo');
                    $img = Image::make($image->getRealPath());
                    $img->resize(500, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(public_path($destinationPath . '/' . $input['imagename']));

                    $image->move(public_path($destinationPath, '/' . $input['imagename']));

                    $direktori = $destinationPath . '/' . $input['imagename'];

                    $data->image         = $direktori;
                }

                $data->save();

                return redirect('/admin-panel/aplikasi')->with('success', 'Berhasil Update Identitas Aplikasi');
            } catch (QueryException $ex) {
                // return back()->with("fail", "Gagal Update Data Aplikasi" . $ex->getMessage())->withInput();
                var_dump($data);
            }
        } else {
            return abort(405);
        }
    }
}
