<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;
use DateTime;
use Session;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\Admin;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



class TagihanController extends Controller
{


///TAGIHAN TOKO----------------------------------------------------------
    public function Tagihan(Request $request)
    {
        $data = (object)array(
            'title'     => 'Tagihan',
            'title_sub'     => 'Tagihan',
            'halaman'     => 'data'
        );
        return view('tagihan.tagihan',compact('data'))->with(["page" => "Tagihan"]);
    }

    public function DataTagihan(Request $request)
    {
        $id_toko = $request->id_toko;
        
        $columns = array(
            0 => 'id_toko',
            1 => 'nama_toko',
            2 => 'paket',
            3 => 'periode',
            4 => 'jmlh',
            5 => 'status',
            6 => 'ket'
        );

        
        $totalData = TagihanToko::where('id_toko',$id_toko)->count();
        

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tgh_toko as a')
                ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                ->where('a.id_toko',$id_toko)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tgh_toko as a')
                ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                ->where('a.id_toko',$id_toko)
                ->where(function ($query) use ($search){
                        $query->orWhere('b.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('b.nohp','LIKE',"%{$search}%")
                            ->orWhere('a.period','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tgh_toko as a')
                ->select('a.*','b.nama_toko','b.nohp','b.logo','b.id_jenisusaha','b.id_harga_custom')
                ->leftJoin('tbl_toko as b', 'a.id_toko', 'b.id_toko')
                ->where('a.id_toko',$id_toko)
                ->where(function ($query) use ($search){
                        $query->orWhere('b.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('b.nohp','LIKE',"%{$search}%")
                            ->orWhere('a.period','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->logo == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->logo);
                }

                

                if($service->status == 0){
                    $sst  = '<button class="btn btn-sm btn-icon-fixed btn-danger glow popover-hover terimatgh" a="'.$service->id.'" b="'.$service->nama_toko.'" c="'.date("M, Y",strtotime($service->periode)).'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Klik Untuk Terima Tagihan"><i class="bx bx-wifi-off"></i> Belum Bayar</button>';

                }elseif($service->status == 1){
                    $sst  = '<button class="btn btn-sm btn-icon-fixed btn-secondary glow popover-hover tolaktgh" a="'.$service->id.'" b="'.$service->nama_toko.'" c="'.date("M, Y",strtotime($service->periode)).'" data-container="body" data-toggle="tooltip" data-placement="left" data-title="Klik Untuk Menolak Tagihan"><i class="bx bx-wifi-off"></i> Lunas</button>';
                }

                $getjns = JenisUsaha::where('id_jenisusaha',$service->id_jenisusaha)->first();
                if($getjns){
                    $jnsu = $getjns->nama_jenisusaha;
                }else{
                    $jnsu = '-';
                }
                
                $getpket = HargaCustom::where('id',$service->id_harga_custom)->first();
                if($getpket){
                    if($getpket->id == 1){
                        $pkt  = '<div class="badge badge-pill badge-glow badge-secondary">Standar <br> Rp '.number_format($getpket->harga).'</div>';
                    }elseif($getpket->id == 2){
                         $pkt  = '<div class="badge badge-pill badge-glow badge-info">Promo <br> Rp '.number_format($getpket->harga).'</div>';
                    }elseif($getpket->id == 3){
                         $pkt  = '<div class="badge badge-pill badge-glow badge-success">Premium <br> Rp '.number_format($getpket->harga).'</div>';
                    }else{
                        $pkt  = '-';
                    }
                }else{
                    $pkt  = '-';
                }

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id;
                $nestedData['nama'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> '.$service->nama_toko.'</span><br>
                                            <span class="text-muted"><small> No Hp :'.$service->nohp.'</small></span><br>
                                            <span class="text-muted"><small> Jns Usaha :'.$jnsu.'</small></span>
                                        </div>
                                    </div>';
                $nestedData['paket'] = $pkt;
                $nestedData['periode'] = date("M, Y",strtotime($service->periode));
                $nestedData['jmlh'] = number_format($service->jmlh);
                $nestedData['status'] = $sst;
                $nestedData['ket'] = $service->ket;
                $nestedData['url'] = encrypt($service->id);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }


    public function TagihanTerima(Request $request)
    {
        try{
            $data = TagihanToko::where('id', $request->id)->first();
            if($data)
            {
                
                $data->status = 1;
                if($data->save()){
                    $histtgh = new HistTagihanToko();
                    $histtgh->tanggal = date("Y-m-d H:i:s");
                    $histtgh->id_toko = $data->id_toko;
                    $histtgh->saldo = $data->jmlh;
                    $histtgh->jns = "D";
                    $histtgh->ket = "Pembayaran Tagihan Untuk Bulan ".date("M, Y",strtotime($data->periode));
                    $histtgh->save();
                } 

                $getnamatoko = Toko::where('id_toko',$data->id_toko)->first();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Pembayaran Tagihan Untuk Toko <strong>'. $getnamatoko->nama_toko .'</strong><br> Periode <strong>'. date("M, Y",strtotime($data->periode)) .' <br> Berhasil Di Terima</strong>';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

    public function TagihanTolak(Request $request)
    {
        try{
            $data = TagihanToko::where('id', $request->id)->first();
            if($data)
            {
                
                $data->status = 0;
                if($data->save()){
                    $histtgh = new HistTagihanToko();
                    $histtgh->tanggal = date("Y-m-d H:i:s");
                    $histtgh->id_toko = $data->id_toko;
                    $histtgh->saldo = $data->jmlh;
                    $histtgh->jns = "K";
                    $histtgh->ket = "Penolakan Tagihan Untuk Bulan ".date("M, Y",strtotime($data->periode));
                    $histtgh->save();
                } 

                $getnamatoko = Toko::where('id_toko',$data->id_toko)->first();

                $this->response['status'] = 'success';
                $this->response['msg'] = 'Pembayaran Tagihan Untuk Toko <strong>'. $getnamatoko->nama_toko .'</strong><br> Periode <strong>'. date("M, Y",strtotime($data->periode)) .' <br> Berhasil Di Tolak</strong>';
            }
            else
            {
                $this->response['status'] = 'fail';
                $this->response['msg'] = 'Tidak ditemukan data Toko';
            }
        }catch (QueryException $ex) {
            $this->response['status'] = 'fail';
            $this->response['msg'] = 'Gagal hapus Data ' . $ex->getMessage();
        }

        return response($this->response);
    }

}
