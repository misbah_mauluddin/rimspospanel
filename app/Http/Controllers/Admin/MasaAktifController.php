<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use DB;
use Validator;
use Response;
use Hash;
use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

use Helperss;
use DateTime;
use Session;


use App\Http\Models\Toko;
use App\Http\Models\UserToko;
use App\Http\Models\JenisUsaha;
use App\Http\Models\HargaCustom;
use App\Http\Models\TagihanToko;
use App\Http\Models\HistSewaToko;
use App\Http\Models\HistTagihanToko;
use App\Http\Models\HistSuspendToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\MetodeBayar;
use App\Http\Models\ProdukToko;
use App\Http\Models\Admin;
use App\Http\Models\BarangStok;
use App\Http\Models\BarangStokHistory;



class MasaAktifController extends Controller
{


///MASA AKTIF TOKO----------------------------------------------------------
    public function MasaAktif(Request $request)
    {
        $data = (object)array(
            'title'     => 'Masa Aktif Customer',
            'title_sub'     => 'Masa Aktif Customer',
            'halaman'     => 'data'
        );
        return view('toko.masaaktif',compact('data'))->with(["page" => "Masa Aktif Customer"]);
    }

    public function DataMasaAktif(Request $request)
    {
        $id_toko = $request->id_toko;
        $status = $request->status;
        
        $columns = array(
            0 => 'id_toko',
            1 => 'nama_toko',
            2 => 'alamat',
            3 => 'nama_jenisusaha',
            4 => 'tgl_aktif',
            5 => 'status'
        );

        if($status == "all"){
            $totalData = Toko::where('id_toko',$id_toko)->count();
        }else{
            $totalData = Toko::where('id_toko',$id_toko)->where('status_byr', $status)->count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $services = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->where('id_toko',$id_toko)
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status_byr', '=', $status);
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();            
        } else {
            $search = $request->input('search.value');

            $services = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->where('id_toko',$id_toko)
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status_byr', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_aktif','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_berakhir','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = DB::table('tbl_toko as a')
                ->select('a.*','b.nama_jenisusaha as namajenis')
                ->leftJoin('tbl_jenis_usaha as b', 'a.id_jenisusaha', 'b.id_jenisusaha')
                ->where('id_toko',$id_toko)
                ->when($status!='all', function ($query) use ($status){
                    return $query->where('a.status_byr', '=', $status);
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('a.nama_toko','LIKE',"%{$search}%")
                            ->orWhere('a.alamat','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_aktif','LIKE',"%{$search}%")
                            ->orWhere('a.tgl_berakhir','LIKE',"%{$search}%")
                            ->orWhere('b.nama_jenisusaha','LIKE',"%{$search}%");
                    })
                ->count();
        }

        $data = array();
        if (!empty($services)) {

            $no = 1;
            foreach ($services as $service) {


                if($service->logo == ""){
                    $image = url('placeholder.png');
                }else{
                    $image = url($service->logo);
                }

                if($service->tgl_aktif == ""){
                    $tgl_aktif = "-";
                    $date1 = '0';
                }else{
                    $tgl_aktif = date("d-m-Y",strtotime($service->tgl_aktif));
                    $date1 = date("Y-m-d",strtotime($service->tgl_aktif));
                }

                if($service->tgl_berakhir == ""){
                    $tgl_berakhir = "-";
                    $date2 = '0';
                }else{
                    $tgl_berakhir = date("d-m-Y",strtotime($service->tgl_berakhir));
                    $date2 = date("Y-m-d",strtotime($service->tgl_berakhir));
                }

                if($service->status == 0){
                    if($service->is_suspend == 1){
                        $sst  = '<div class="badge badge-pill badge-glow badge-danger">Suspend</div>';
                    }elseif($service->is_suspend == 0){
                        $sst  = '<div class="badge badge-pill badge-glow badge-warning">Tidak Aktif</div>';
                    }
                }elseif($service->status == 1){
                    $sst  = '<div class="badge badge-pill badge-glow badge-secondary">Aktif</div>';
                }elseif($service->status == 2){
                    $sst  = '<div class="badge badge-pill badge-glow badge-info">Proses Aktifasi</div>';
                }elseif($service->status == 3){
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Di Hapus</div>';
                }else{
                    $sst  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                }

                if($service->status_byr == 0){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-warning">Belum Bayar</div>';
                }elseif($service->status_byr == 1){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-secondary">Lunas</div>';
                }elseif($service->status_byr == 2){
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-info">Cicil</div>';
                }else{
                    $sstbyr  = '<div class="badge badge-pill badge-glow badge-danger">Belum Ada Status</div>';
                }


                if($date1 == '0' && $date2 == '0'){
                    $diffd = '0 Hari';
                    $diffm = '0 Bln';
                    $diff = '0 Thn';
                }else{

                    // $fdate = date("Y-m-d");
                    // $tdate = $date2;
                    // $datetime1 = new DateTime($fdate);
                    // $datetime2 = new DateTime($tdate);
                    // $interval = $datetime1->diff($datetime2);
                    // $diff = $interval->format('%a').' Hari '.$interval->format('%m').' Bln ' .$interval->format('%y').' Thn';

                    $date1 = date("Y-m-d");
                    $date2 = $date2;

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);

                    $day1 = date('d', $ts1);
                    $day2 = date('d', $ts2);

                    $diff = (($year2 - $year1) * 12);
                    $diffm = (($year2 - $year1) * 12) + ($month2 - $month1);
                    $diffd = (($year2 - $year1) * 12)  + ($day2 - $day1).' Hari';

                    if($diffd < 0){
                        $diffd = '0 Hari';
                    }else{
                        $diffd = (($year2 - $year1) * 12)  + ($day2 - $day1).' Hari ';
                    }

                    if($diffm < 0){
                        $diffm = '0 Bln';
                    }else{
                        $diffm = (($year2 - $year1) * 12)  + ($month2 - $month1).' Bln ';
                    }

                    if($diff < 0){
                        $diff = '0 Thn';
                    }else{
                        $diff = (($year2 - $year1) * 12) .' Thn ';
                    }
                }
                

                $nestedData['no'] = $no++;
                $nestedData['id'] = $service->id_toko;
                $nestedData['nama'] ='<div class="d-flex align-items-center">
                                            <img class="round" src="' . $image . '" alt="avatar" height="35" width="35">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> <a href="#" class="view_photo" a="'.$service->id_toko.'">'.$service->nama_toko.'</a></span><br>
                                            <span class="text-muted"><small> No Hp :'.$service->nohp.'</small></span><br>
                                            <span class="text-muted"><small> Email :'.$service->email.'</small></span>
                                        </div>
                                    </div>';
                $nestedData['alamat'] = $service->alamat;
                $nestedData['namajenis'] = $service->namajenis;
                $nestedData['tgl'] = '<div class="d-flex align-items-center">
                                        <div class="ml-1">
                                            <span class="text-bold-500"> '.$diffd.' '.$diffm.' '.$diff.'</span><br>
                                            <span class="text-muted"><small>'.$tgl_aktif.' s/d '.$tgl_berakhir.'</small> </span><br>
                                            
                                        </div>
                                    </div>';
                $nestedData['status_byr'] = $sstbyr;
                $nestedData['status'] = $sst;
                $nestedData['statusbar'] = $service->status;
                $nestedData['is_suspend'] = $service->is_suspend;
                $nestedData['id_hist_suspend'] = $service->id_hist_suspend;
                $nestedData['namabar'] = $service->nama_toko;
                $nestedData['url'] = encrypt($service->id_toko);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function ProdukTokoImageView($id)
    {
        $sumber = ProdukToko::find($id);


        return response()->json(['data' => $sumber]);
    }

}
