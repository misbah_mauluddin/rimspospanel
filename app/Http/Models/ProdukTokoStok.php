<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukTokoStok extends Model
{
    protected $table = 'tbl_barang_stok';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
