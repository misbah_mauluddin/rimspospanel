<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'tbl_keranjang';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
