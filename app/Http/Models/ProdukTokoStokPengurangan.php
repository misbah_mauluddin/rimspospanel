<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukTokoStokPengurangan extends Model
{
    protected $table = 'tbl_barang_stok_pengurangan';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
