<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanTokoDetailPaket extends Model
{
    protected $table = 'tbl_penjualan_detail_paket';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
