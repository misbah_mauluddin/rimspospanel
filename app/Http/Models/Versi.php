<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Versi extends Model
{
    protected $table = 'tbl_versi';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
