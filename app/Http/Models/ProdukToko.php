<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukToko extends Model
{
    protected $table = 'tbl_barang';
    public $timestamps = false;
    protected $primaryKey = 'id_barang';
}
