<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriBeban extends Model
{
    protected $table = 'tbl_kategoribeban';
    public $timestamps = false;
    protected $primaryKey = 'id_ktr_beban';
}
