<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanToko extends Model
{
    protected $table = 'tbl_penjualan';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
