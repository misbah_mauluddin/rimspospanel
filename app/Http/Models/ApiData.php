<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ApiData extends Model
{
    protected $table = 'api_data';
    public $timestamps = false;
}
