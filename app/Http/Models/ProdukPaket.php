<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukPaket extends Model
{
    protected $table = 'tbl_paket';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
