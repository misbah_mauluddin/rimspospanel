<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class HistSewaToko extends Model
{
    protected $table = 'hist_sewa_toko';
    public $timestamps = false;
}
