<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class HistStok extends Model
{
    protected $table = 'tbl_hist_stok';
    public $timestamps = false;
    protected $primaryKey = 'id_stok';
}
