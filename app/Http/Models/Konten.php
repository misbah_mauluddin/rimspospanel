<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Konten extends Model
{
    protected $table = 'tbl_konten';
    public $timestamps = false;
    protected $primaryKey = 'id_konten';
}
