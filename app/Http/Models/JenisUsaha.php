<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class JenisUsaha extends Model
{
    protected $table = 'tbl_jenis_usaha';
    public $timestamps = false;
    protected $primaryKey = 'id_jenisusaha';
}
