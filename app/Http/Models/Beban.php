<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Beban extends Model
{
    protected $table = 'tbl_beban';
    public $timestamps = false;
    protected $primaryKey = 'id_beban';
}
