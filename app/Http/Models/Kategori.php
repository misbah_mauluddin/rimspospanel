<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'tbl_jenis';
    public $timestamps = false;
    protected $primaryKey = 'id_jenis';
}
