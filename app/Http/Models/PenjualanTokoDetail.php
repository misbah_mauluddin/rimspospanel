<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanTokoDetail extends Model
{
    protected $table = 'tbl_penjualan_detail';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
