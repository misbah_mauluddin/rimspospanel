<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserToko extends Model
{
    protected $table = 'tbl_kasir';
    public $timestamps = false;
    protected $primaryKey = 'id_kasir';
}
