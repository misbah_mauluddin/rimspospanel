<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class HistSuspendToko extends Model
{
    protected $table = 'hist_suspend_toko';
}
