<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    protected $table = 'tbl_diskon';
    public $timestamps = false;
    protected $primaryKey = 'id_diskon';
}
