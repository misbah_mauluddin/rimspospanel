<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BagiHasil extends Model
{
    protected $table = 'tbl_bagi_hasil';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
