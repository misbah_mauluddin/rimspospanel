<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class KasLaci extends Model
{
    protected $table = 'kas_laci';
    public $timestamps = false;
}
