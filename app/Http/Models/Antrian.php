<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = 'antrian';
    public $timestamps = false;
}
