<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Aplikasi extends Model
{
    protected $table = 'aplikasi';
    public $timestamps = false;
}
