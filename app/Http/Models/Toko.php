<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $table = 'tbl_toko';
    public $timestamps = false;
    protected $primaryKey = 'id_toko';
}
