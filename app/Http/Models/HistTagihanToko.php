<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class HistTagihanToko extends Model
{
    protected $table = 'hist_tagihan_toko';
    public $timestamps = false;
}
