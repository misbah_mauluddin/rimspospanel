<?php

namespace App\Traits;

trait TransformerParams {

    private $params;

    public function addParam() {
        $args = func_get_args();
        if(is_array($args[0]))
        {
            $this->params = $args[0];
        } else {
            $this->params[$args[0]] = $args[1];
        }
    }
}