<?php

namespace App\Transformers;

use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class LoginTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($kasir)
    {
        
        $ksr = UserToko::where('id_kasir',$kasir)->first();

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["id_kasir"] = $ksr->id_kasir;
        $respon["id_toko"]  = $ksr->id_toko;
        $respon["nama_toko"]= $ksr->nama_toko;
        $respon["id_jenis_usaha"]   = $ksr->id_jenis_usaha;
        $respon["nama"]     = $ksr->nama_kasir;
        $respon["nomor_hp"] = $ksr->nomor_hp;
        $respon["status"]   = $ksr->status;

        return $respon;
    }
}