<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class ErorValidasiTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($error_messages)
    {
        return [
            'success'              => false,
            'error'           => true,
            'error_code'           => 422,
            'message'       => $error_messages,
            

        ];
    }
}
