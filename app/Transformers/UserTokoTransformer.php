<?php

namespace App\Transformers;

use App\Http\Models\Beban;

use League\Fractal\TransformerAbstract;

use DB;

class UserTokoTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
       
        $respon["id_kasir"] = $dtl->id_kasir;
        $respon["id_user"] = $dtl->id_user;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_jenis_usaha"] = $dtl->id_jenis_usaha;
        $respon["nama_toko"] = $dtl->nama_toko;
        $respon["nama_kasir"] = $dtl->nama_kasir;
        $respon["nomor_hp"] = $dtl->nomor_hp;
        $respon["status"] = $dtl->status;

        return $respon;
    }
    
}