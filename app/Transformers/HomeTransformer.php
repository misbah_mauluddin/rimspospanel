<?php

namespace App\Transformers;

use App\Http\Models\Konten;
use App\Http\Models\Versi;
use App\Http\Models\Toko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\Fitur;
use App\Http\Models\FiturToko;

use League\Fractal\TransformerAbstract;

use DB;

class HomeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($toko)
    {
        $cektoko = Toko::where('id_toko',$toko)->first();

        if($cektoko){
            $idtoko = $cektoko->id_toko;
            $toko = $cektoko;
            $cekpendapatan = PenjualanToko::select(DB::Raw('SUM(total) as harga'))
                                        ->where('id_toko',$idtoko)
                                        ->whereDate('tgl_penjualan',date('Y-m-d'))
                                        ->where('status', 1)
                                        ->first();

            $cekbiaya = Beban::select(DB::Raw('SUM(jumlah) as jumlah'))
                                        ->where('id_toko',$idtoko)
                                        ->whereDate('tgl',date('Y-m-d'))
                                        ->first();

            if($cekpendapatan->harga){
                $pendapatan = $cekpendapatan->harga;
            }else{
                $pendapatan = 0; 
            }

            
            if($cekbiaya->jumlah){
                $biaya = $cekbiaya->jumlah;
            }else{
                $biaya = 0; 
            }

            

        }else{
            $idtoko = [];
            $toko = [];
            $pendapatan = 0; 
            $biaya = 0; 
        }


        $isikonten = array();
        $getknten = Konten::get();

        foreach ($getknten as $ds){
            $ds->photo = url('/').'/'.$ds->foto;
            unset($ds->foto);
            array_push($isikonten,$ds);
        }

        $versi = Versi::get();

        $getfitur = DB::table('fitur_toko as a')
                        // ->select('b.nama','a.id_fitur','a.id_toko','a.is_active')
                        ->select('b.nama','a.is_active')
                        ->leftJoin('fitur as b','a.id_fitur','=','b.id')
                        ->where('a.id_toko',$idtoko)
                        ->get();
        
        $konten[] = array("toko" => array(
                                        "id_toko" => $cektoko->id_toko,
                                        "id_user" => $cektoko->id_user,
                                        "id_jenisusaha" => $cektoko->id_jenisusaha,
                                        "nama_toko" => $cektoko->nama_toko,
                                        "alamat" => $cektoko->alamat,
                                        "nohp" => $cektoko->nohp,
                                        "email" => $cektoko->email,
                                        "tgl" => $cektoko->tgl,
                                        "logo" => url('/').'/'.$cektoko->logo,
                                        "status" => $cektoko->tgl,
                                        "tgl_aktif" => $cektoko->tgl_aktif,
                                        "tgl_berakhir" => $cektoko->tgl_berakhir,
                                        "is_suspend" => $cektoko->is_suspend,
                                        "id_hist_suspend" => $cektoko->id_hist_suspend,
                                        "status_byr" => $cektoko->status_byr,
                                        "id_metodebyr" => $cektoko->id_metodebyr,
                                        "kode_aktifasi" => $cektoko->kode_aktifasi,
                                        "id_harga_custom" => $cektoko->id_harga_custom,
                                    ),
                         "fitur" => $getfitur, 
                         "konten" => $isikonten, 
                        "pendapatan" => $pendapatan, 
                        "biaya" => $biaya, 
                        "version" => $versi);

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["message"] = "";
        $respon["data"] = $konten;

        return $respon;
    }
}