<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class DevisiTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        if($data['error'] == true){
            return [
                    "data" =>   [
                            array(
                                'success'               => false,
                                'message'           => "DATA TIDAK DITEMUKAN, SILAHKAN INPUT ULANG!",
                            )

                     ]
                    
                    
            ];
        }else{
            return [
                       
                    'success'               => true,
                    'error'           => $data['error'],
                    'data'           => $data['data']              
                            
                    
            ];
        }
    }
}
