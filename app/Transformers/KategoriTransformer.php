<?php

namespace App\Transformers;

use App\Http\Models\Kategori;

use League\Fractal\TransformerAbstract;

use DB;

class KategoriTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($toko)
    {
        
        $ktr = Kategori::where('id_toko',$toko)->where('status',1)->orderBy('jenis_brg','ASC')->get();

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["data"] = $ktr;

        return $respon;
    }
}