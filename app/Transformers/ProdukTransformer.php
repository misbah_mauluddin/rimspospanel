<?php

namespace App\Transformers;

use App\Http\Models\ProdukPaket;
use App\Http\Models\ProdukToko;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\BagiHasil;
use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class ProdukTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        if($dtl->id_kategori == 1){
            $getstok = ProdukTokoStok::where('id_barang',$dtl->id_barang)->first();
            if($getstok){
                $stk = $getstok->qty;
                $hrga = $getstok->harga;
            }else{
                $stk = '0';
                $hrga = '0';
            }
        }else{
            $stk = $dtl->jumlah_semua;
            $hrga = $dtl->harga_satuan;
        }

        if($dtl->foto != ''){
            $foto = url('/').'/'.$dtl->foto;
        }else{
            $foto = '';
        }
        $respon["id_barang"] = $dtl->id_barang;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_user"] = $dtl->id_user;
        $respon["id_jenis"] = $dtl->id_jenis;
        $respon["id_kategori"] = $dtl->id_kategori;
        $respon["nama_br"] = $dtl->nama_br;
        $respon["deskripsi"] = $dtl->deskripsi;
        $respon["jumlah_semua"] = $stk;
        $respon["harga_satuan"] = $hrga;
        $respon["tgl_masuk"] = date("d-m-Y",strtotime($dtl->tgl_masuk));
        $respon["tgl_update"] = date("d-m-Y",strtotime($dtl->tgl_update));
        $respon["foto"] = $foto;
        $respon["status"] = $dtl->status;

        

        if($dtl->id_kategori == 3){
            $respon["metode_bagihasil"] = $dtl->metode_bagihasil;
            if($dtl->metode_bagihasil == 0){
                $respon["metode_bagihasil_pen"] = 'Bagi hasil berdasarkan paket';
            }elseif($dtl->metode_bagihasil == 1){
                $respon["metode_bagihasil_pen"] = 'Bagi hasil berdasarkan item paket';
            }else{
                $respon["metode_bagihasil_pen"] = '';
            }
            
            $dtlpaket = ProdukPaket::where('id_paket',$dtl->id_barang)->get();
            if(count($dtlpaket) > 0){
                foreach ($dtlpaket as $rowpaket) {
                $getnmbrg = ProdukToko::where('id_barang',$rowpaket->id_produk)->first();
                if($getnmbrg){
                    $idbrg = $getnmbrg->id_barang;
                    $nmbrg = $getnmbrg->nama_br;
                }else{
                    $idbrg = "-";
                    $nmbrg = "-";
                }
                    if($dtl->metode_bagihasil == 0){ ///BERDASARKAN PAKET BGI HASIL
                    
                        $respon["detail_paket"][] = array(
                                                    "id_detailpaket" => $rowpaket->id,
                                                    "id_barang" => $idbrg,
                                                    "nama_br" => $nmbrg,
                                                    "qty" => $rowpaket->qty,
                                                    "harga" => $rowpaket->harga,
                                                ); 
                                                
                        
                    }elseif($dtl->metode_bagihasil == 1){ /// BERDASARKAN ITEM BAGI HASIL
                        $bghasil = BagiHasil::where('id_paket',$rowpaket->id)->get();
                        $bagihasil = array();
                        

                        if(count($bghasil) > 0){
                            foreach ($bghasil as $rowpbg) {
                                $getnmksr = UserToko::where('id_kasir',$rowpbg->id_petugas)->first();    
                                $bagihasil[] = array(
                                                    "id_bagihasil" => $rowpbg->id, ///id bagi hasil
                                                    "id_paket" => $rowpbg->id_paket,
                                                    "id_barang" => $rowpbg->id_barang,
                                                    "id_pekerja" => $rowpbg->id_petugas,
                                                    "nama_kasir" => $getnmksr->nama_kasir,
                                                    "metode" => $rowpbg->metode,
                                                    "bagihasil" => $rowpbg->nilai
                                                );
                                                                                   
                            }
                        }

                        $respon["detail_paket"][] = array(
                                                            "id_detailpaket" => $rowpaket->id,
                                                            "id_barang" => $idbrg,
                                                            "nama_br" => $nmbrg,
                                                            "qty" => $rowpaket->qty,
                                                            "harga" => $rowpaket->harga,
                                                            "bagihasil" => $bagihasil,
                                                        ); 
                    }
                }
            }
            
            if($dtl->metode_bagihasil == 0){
                $bghasil = BagiHasil::where('id_barang',$dtl->id_barang)->get();
                        
                if(count($bghasil) > 0){ 
                    foreach ($bghasil as $rowpbg) {
                        $getnmksr = UserToko::where('id_kasir',$rowpbg->id_petugas)->first();
                              
                        $respon["bagihasil"][] = array(
                                                        "id_bagihasil" => $rowpbg->id, ///id bagi hasil
                                                        "id_paket" => $rowpbg->id_paket,
                                                        "id_barang" => $rowpbg->id_barang,
                                                        "id_pekerja" => $rowpbg->id_petugas,
                                                        "nama_kasir" => $getnmksr->nama_kasir,
                                                        "metode" => $rowpbg->metode,
                                                        "bagihasil" => $rowpbg->nilai
                                                            
                                                );       
                    }
                }
            }

            
        }else{
           $bghasil = BagiHasil::where('id_barang',$dtl->id_barang)->get(); 
           if(count($bghasil) > 0){
               foreach ($bghasil as $rowpbg) {
                   $getnmksr = UserToko::where('id_kasir',$rowpbg->id_petugas)->first();
                    $respon["bagihasil"][] = array(
                                                "id_bagihasil" => $rowpbg->id, ///id bagi hasil
                                                "id_paket" => $rowpbg->id_paket,
                                                "id_barang" => $rowpbg->id_barang,
                                                "id_pekerja" => $rowpbg->id_petugas,
                                                "nama_kasir" => $getnmksr->nama_kasir,
                                                "metode" => $rowpbg->metode,
                                                "bagihasil" => $rowpbg->nilai
                                            
                                            );    
               }
           }
           
                  
        }

        return $respon;
    }
    
}