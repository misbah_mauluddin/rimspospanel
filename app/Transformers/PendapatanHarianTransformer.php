<?php

namespace App\Transformers;

use App\Http\Models\Konten;
use App\Http\Models\Versi;
use App\Http\Models\Toko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\Fitur;
use App\Http\Models\FiturToko;
use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class PendapatanHarianTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($penjualanoko)
    {
        $owner = DB::table('tbl_penjualan as a')->select(DB::raw('SUM(b.bagian_owner) as owner'))
                            ->leftJoin('tbl_penjualan_detail as b','a.id','=','b.id_penjualan')
                            ->where('a.id_toko',$penjualanoko->id_toko)
                            ->where('a.status',1)
                            ->whereIn('a.metode_bayar',['0','1','2'])
                            ->whereDate('a.tgl_penjualan', '>=', date("Y-m-d 00:00:00"))
                            ->whereDate('a.tgl_penjualan', '<=', date("Y-m-d 23:59:59"))
                            ->first(); 

        $pekerja = DB::table('tbl_penjualan as a')->select(DB::raw('SUM(b.upah_pekerja) as pekerja'))
                ->leftJoin('tbl_penjualan_detail as b','a.id','=','b.id_penjualan')
                ->where('a.id_toko',$penjualanoko->id_toko)
                ->where('a.status',1)
                ->whereIn('a.metode_bayar',['0','1','2'])
                ->whereDate('a.tgl_penjualan', '>=', date("Y-m-d 00:00:00"))
                ->whereDate('a.tgl_penjualan', '<=', date("Y-m-d 23:59:59"))
                ->first(); 

        if($penjualanoko->total){
            $npendapatan = $penjualanoko->total;
        }else{
            $npendapatan = '0';
        }

        if($owner){
            $nowner = $owner->owner;
        }else{
            $nowner = '0';
        }

        if($pekerja){
            $npekerja = $pekerja->pekerja;
        }else{
            $npekerja = '0';
        }

        $pekerjadtl = DB::table('tbl_penjualan as a')->select(DB::raw('SUM(b.upah_pekerja) as pekerja'),'b.id_pekerja')
                ->leftJoin('tbl_penjualan_detail as b','a.id','=','b.id_penjualan')
                ->where('a.id_toko',$penjualanoko->id_toko)
                ->where('a.status',1)
                ->whereIn('a.metode_bayar',['0','1','2'])
                ->whereDate('a.tgl_penjualan', '>=', date("Y-m-d 00:00:00"))
                ->whereDate('a.tgl_penjualan', '<=', date("Y-m-d 23:59:59"))
                ->groupBy('b.id_pekerja')
                ->get(); 

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["pendapatan"] = number_format($npendapatan);
        $respon["owner"] = number_format($nowner);
        $respon["pekerja"] = number_format($npekerja);

        if(count($pekerjadtl) > 0){
            foreach($pekerjadtl as $dtl){
                $getpeekrja = UserToko::where('id_kasir',$dtl->id_pekerja)->first();
                if($getpeekrja){
                    $namapkr = $getpeekrja->nama_kasir; 
                }else{
                    $namapkr = "-";
                }
                $respon["detail_pekerja"][] = array(
                                                "id_pekerja" => $dtl->id_pekerja,
                                                "nama_pekerja" => $namapkr,
                                                "upah_pekerja" => number_format($dtl->pekerja),
                                            );
            }
        }
        

        return $respon;
    }
}