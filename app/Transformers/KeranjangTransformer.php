<?php

namespace App\Transformers;

use App\Http\Models\ProdukPaket;
use App\Http\Models\ProdukToko;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\BagiHasil;
use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class KeranjangTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
        $getnmksr = UserToko::where('id_kasir',$dtl->id_kasir)->first();
        if($getnmksr){
            $nama_kasir = $getnmksr->nama_kasir;
        }else{
            $nama_kasir = "-";
        }

        $getnmpekr = UserToko::where('id_kasir',$dtl->id_pekerja)->first();
        if($getnmpekr){
            $nama_pekerja = $getnmpekr->nama_kasir;
        }else{
            $nama_pekerja = "-";
        }

        $respon["id"] = $dtl->id;
        $respon["meja"] = $dtl->meja;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_barang"] = $dtl->id_barang;
        $respon["id_kategori"] = $dtl->id_kategori;
        $respon["id_kasir"] = $dtl->id_kasir;
        $respon["nama_kasir"] = $nama_kasir;
        $respon["id_pekerja"] = $dtl->id_pekerja;
        $respon["nama_pekerja"] = $nama_pekerja;

        $respon["nama_brg"] = $dtl->nama_brg;
        $respon["harga_brg"] = $dtl->harga_brg;
        $respon["diskon_brg"] = $dtl->diskon_brg;
        $respon["upah_pekerja"] = $dtl->upah_pekerja;
        $respon["bagian_owner"] = $dtl->bagian_owner;
        $respon["qty"] = $dtl->qty;
        $respon["total"] = $dtl->total;
        $respon["status"] = $dtl->status;
        $respon["tgl"] = date("d-m-Y",strtotime($dtl->tgl));


        

        if($dtl->id_kategori == 3){
            
            $dtlpaket = ProdukPaket::where('id_paket',$dtl->id_barang)->get();
            if(count($dtlpaket) > 0){
                foreach ($dtlpaket as $rowpaket) {
                $getnmbrg = ProdukToko::where('id_barang',$rowpaket->id_produk)->first();
                if($getnmbrg){
                    $idbrg = $getnmbrg->id_barang;
                    $nmbrg = $getnmbrg->nama_br;
                }else{
                    $idbrg = "-";
                    $nmbrg = "-";
                }
                    
                        
                $respon["detail_paket"][] = array(
                                        "id_detailpaket" => $rowpaket->id,
                                        "id_barang" => $idbrg,
                                        "nama_br" => $nmbrg,
                                        "qty" => $rowpaket->qty,
                                        "harga" => $rowpaket->harga,
                                    ); 
                        
                    
                }
            } 
        }

        return $respon;
    }
    
}