<?php

namespace App\Transformers;

use App\Http\Models\ProdukPaket;
use App\Http\Models\ProdukToko;
use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class BagihasilTransformer extends TransformerAbstract
{


    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {


        $getnmksr = UserToko::where('id_kasir',$dtl->id_petugas)->first();
        if($getnmksr){
            $ksr = $getnmksr->nama_kasir;
        }else{
            $ksr = '-';
        }


        


        $respon["id_bagihasil"] = $dtl->id;
        $respon["id_paket"] = $dtl->id_paket;
        $respon["id_barang"] = $dtl->id_barang;
        $respon["id_pekerja"] = $dtl->id_petugas;
        $respon["nama_kasir"] = $ksr;
        $respon["metode"] = $dtl->metode;
        $respon["bagihasil"] = $dtl->nilai;

        return $respon;
    }
    
}