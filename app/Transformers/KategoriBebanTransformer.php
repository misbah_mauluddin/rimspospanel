<?php

namespace App\Transformers;

use App\Http\Models\KategoriBeban;

use League\Fractal\TransformerAbstract;

use DB;

class KategoriBebanTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($toko)
    {
        
        $ktr = KategoriBeban::where('id_toko',$toko)->get();

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["data"] = $ktr;

        return $respon;
    }
}