<?php

namespace App\Transformers;

use App\Http\Models\ProdukPaket;
use App\Http\Models\ProdukToko;

use League\Fractal\TransformerAbstract;

use DB;

class ModalProdukTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
        
       
        $respon["id_stok"] = $dtl->id_stok;
        $respon["id_user"] = $dtl->id_user;
        $respon["id_barang"] = $dtl->id_barang;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["tanggal"] = date("d-m-Y",strtotime($dtl->tanggal));
        $respon["nota"] = $dtl->nota;
        $respon["stok"] = $dtl->jlh_masuk-$dtl->jlh_keluar;
        $respon["modal"] = $dtl->modal;

       

        return $respon;
    }
    
}