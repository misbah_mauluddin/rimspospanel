<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class FailsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($messages)
    {
        return [
            'success'              => false,
            'error'                => true,
            'error_code'           => 409,
            'message'       => $messages
            
        ];
    }
}
