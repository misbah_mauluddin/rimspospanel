<?php

namespace App\Transformers;

use App\Http\Models\Beban;

use League\Fractal\TransformerAbstract;

use DB;

class BebanTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
        // $ktr = Beban::where('id_toko',$toko)->get();

        // $respon["success"] = true;
        // $respon["error"] = false;
        $respon["id_beban"] = $dtl->id_beban;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_ktr_beban"] = $dtl->id_ktr_beban;
        $respon["id_kasir"] = $dtl->id_kasir;
        $respon["kategori"] = $dtl->kategori;
        $respon["keterangan"] = $dtl->keterangan;
        $respon["tgl"] = date('d-m-Y',strtotime($dtl->tgl));
        $respon["jumlah"] = $dtl->jumlah;

        return $respon;
    }
    
}