<?php

namespace App\Transformers;

use App\Http\Models\Konten;
use App\Http\Models\Versi;
use App\Http\Models\Toko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\Beban;
use App\Http\Models\Fitur;
use App\Http\Models\FiturToko;

use League\Fractal\TransformerAbstract;

use DB;

class VersionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($versi)
    {

        $respon["success"] = true;
        $respon["error"] = false;
        $respon["message"] = "";
        $respon["id"] = $versi->id;
        $respon["app_version"] = $versi->app_version;
        $respon["link"] = $versi->link;
        $respon["comment"] = $versi->comment;

        return $respon;
    }
}