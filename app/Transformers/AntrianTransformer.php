<?php

namespace App\Transformers;

use App\Http\Models\UserToko;

use League\Fractal\TransformerAbstract;

use DB;

class AntrianTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        $getnmksr = UserToko::where('id_kasir',$dtl->id_pekerja)->first();
        if($getnmksr){
            $ksr = $getnmksr->nama_kasir;
        }else{
            $ksr = '-';
        }
        
        $respon["id"] = $dtl->id;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_pekerja"] = $dtl->id_pekerja;
        $respon["nama_kasir"] = $ksr;
        $respon["nama_pemesan"] = $dtl->nama;
        $respon["no_hp"] = $dtl->no_hp;
        $respon["email"] = $dtl->email;
        $respon["tanggal"] = date("d-m-Y, H:i",strtotime($dtl->tanggal)) ." WIB";
        $respon["status"] = $dtl->status;

        return $respon;
    }
    
}