<?php

namespace App\Transformers;

use App\Http\Models\Beban;

use League\Fractal\TransformerAbstract;

use DB;

class DiskonTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
        $respon["id_diskon"] = $dtl->id_diskon;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["diskon"] = $dtl->diskon;
        $respon["tgl"] = date('d-m-Y',strtotime($dtl->tgl));

        return $respon;
    }
    
}