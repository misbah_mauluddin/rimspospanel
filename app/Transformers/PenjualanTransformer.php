<?php

namespace App\Transformers;

use App\Http\Models\ProdukPaket;
use App\Http\Models\ProdukToko;
use App\Http\Models\ProdukTokoStok;
use App\Http\Models\BagiHasil;
use App\Http\Models\UserToko;
use App\Http\Models\PenjualanToko;
use App\Http\Models\PenjualanTokoDetail;
use App\Http\Models\PenjualanTokoDetailPaket;

use League\Fractal\TransformerAbstract;

use DB;

class PenjualanTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($dtl)
    {
        
        $getnmksr = UserToko::where('id_kasir',$dtl->id_kasir)->first();
        if($getnmksr){
            $nama_kasir = $getnmksr->nama_kasir;
        }else{
            $nama_kasir = "-";
        }

        

        $respon["id"] = $dtl->id;
        $respon["meja"] = $dtl->meja;
        $respon["id_toko"] = $dtl->id_toko;
        $respon["id_kasir"] = $dtl->id_kasir;
        $respon["nama_kasir"] = $nama_kasir;
        $respon["total_item"] = $dtl->total_item;
        $respon["diskon_total"] = $dtl->diskon_total;
        $respon["sub_total"] = $dtl->sub_total;
        $respon["total"] = $dtl->total;
        $respon["bayar"] = $dtl->bayar;
        $respon["kembalian"] = $dtl->kembalian;
        $respon["tgl_penjualan"] = date("d-m-Y / H:i",strtotime($dtl->tgl_penjualan))." WIB";
        $respon["metode_bayar"] = $dtl->metode_bayar;
        // $respon["metode_bayar_dtl"] = '0. cash, 1. debit card, 2. kredit card, 3. bayar nanti (kasbon)';
        $respon["status"] = $dtl->status;
        // $respon["status_dtl"] = '1 Selesai, 2 Hutang, 3 Bayar nanti, 4 transaksi batal (Reversal)';

        $detailpnjl = PenjualanTokoDetail::where('id_penjualan',$dtl->id)->orderBy('nama_brg','ASC')->get();
        $detail_paket = array();
        foreach ($detailpnjl as $dtlpenjl) {
            $getnmpekr = UserToko::where('id_kasir',$dtlpenjl->id_pekerja)->first();
            if($getnmpekr){
                $nama_pekerja = $getnmpekr->nama_kasir;
            }else{
                $nama_pekerja = "-";
            }
            

            if($dtlpenjl->id_kategori == 3){
                $dtlpaket = PenjualanTokoDetailPaket::where('id_penjualan_detail',$dtlpenjl->id)->orderBy('nama_brg','ASC')->get();
                if(count($dtlpaket) > 0){
                    foreach ($dtlpaket as $paket) {
                        $detail_paket[] = array(
                                        "id_barang"         => $paket->id_barang,
                                        "id_kategori"       => $paket->id_kategori,
                                        "nama_brg"          => $paket->nama_brg,
                                        "harga_brg"         => $paket->harga_brg,
                                        "qty"               => $paket->qty,
                                        // "upah_pekerja"      => $paket->upah_pekerja,
                                        // "bagian_owner"      => $paket->bagian_owner,
                                        // "tgl"               => date("d-m-Y / H:i",strtotime($paket->tgl)),
                                    );
                    }
                }
            }

            $respon["detail_item"][] = array(
                                "id_penjualan"  => $dtlpenjl->id_penjualan,
                                "id_toko"       => $dtlpenjl->id_toko,
                                "meja"          => $dtlpenjl->meja,
                                "id_barang"     => $dtlpenjl->id_barang,
                                "id_kategori"   => $dtlpenjl->id_kategori,
                                "id_paket"      => $dtlpenjl->id_paket,
                                "id_pekerja"    => $dtlpenjl->id_pekerja,
                                "nama_pekerja"  => $nama_pekerja,
                                "nama_brg"      => $dtlpenjl->nama_brg,
                                "harga_brg"     => $dtlpenjl->harga_brg,
                                "qty"           => $dtlpenjl->qty,
                                "diskon_brg"    => $dtlpenjl->diskon_brg,
                                "total"         => $dtlpenjl->total,
                                // "upah_pekerja"  => $dtlpenjl->upah_pekerja,
                                // "bagian_owner"  => $dtlpenjl->bagian_owner,
                                "tgl"           => date("d-m-Y / H:i",strtotime($dtlpenjl->tgl))." WIB",
                                "detail_paket"  => $detail_paket,
                            ); 
            
        }
        
        return $respon;
    }
    
}