<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            "*",
        ], function ($view){
            // $role = UserRole::where('user_id', session()->get('user_id'))->first()->toJson();
            // $role = json_decode($role);
            $menu = Menus::first();
            $public_menu = $menu->items;

            $view->with(compact('public_menu'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
