jQuery.validator.setDefaults({
	    messages: {},
	    groups: {},
	    rules: {},
	    errorClass: "is-invalid",
	    pendingClass: "pending",
	    validClass: "is-valid",
	    errorElement: "label",
	    focusCleanup: false,
	    focusInvalid: true,
	    errorContainer: $( [] ),
	    errorLabelContainer: $( [] ),
	    onsubmit: true,
	    ignore: ":hidden",
	    ignoreTitle: false,
	    onfocusin: function( element ) {
	        this.lastActive = element;

	        // Hide error label and remove error class on focus if enabled
	        if ( this.settings.focusCleanup ) {
	            if ( this.settings.unhighlight ) {
	                this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
	            }
	            this.hideThese( this.errorsFor( element ) );
	        }
	    },
	    onfocusout: function( element ) {
	        if ( !this.checkable( element ) && ( element.name in this.submitted || !this.optional( element ) ) ) {
	            this.element( element );
	        }
	    },
	    onkeyup: function( element, event ) {

	        // Avoid revalidate the field when pressing one of the following keys
	        // Shift       => 16
	        // Ctrl        => 17
	        // Alt         => 18
	        // Caps lock   => 20
	        // End         => 35
	        // Home        => 36
	        // Left arrow  => 37
	        // Up arrow    => 38
	        // Right arrow => 39
	        // Down arrow  => 40
	        // Insert      => 45
	        // Num lock    => 144
	        // AltGr key   => 225
	        var excludedKeys = [
	            16, 17, 18, 20, 35, 36, 37,
	            38, 39, 40, 45, 144, 225
	        ];

	        if ( event.which === 9 && this.elementValue( element ) === "" || $.inArray( event.keyCode, excludedKeys ) !== -1 ) {
	            return;
	        } else if ( element.name in this.submitted || element.name in this.invalid ) {
	            this.element( element );
	        }
	    },
	    onclick: function( element ) {

	        // Click on selects, radiobuttons and checkboxes
	        if ( element.name in this.submitted ) {
	            this.element( element );

	            // Or option elements, check parent select in that case
	        } else if ( element.parentNode.name in this.submitted ) {
	            this.element( element.parentNode );
	        }
	    },
	    highlight: function( element, errorClass, validClass ) {
	        if ( element.type === "radio" ) {
	            this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
	        } else {
	            $( element ).addClass( errorClass ).removeClass( validClass );
	        }
	    },
	    unhighlight: function( element, errorClass, validClass ) {
	        if ( element.type === "radio" ) {
	            this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
	        } else {
	            $( element ).removeClass( errorClass ).addClass( validClass );
	        }
	    }
	});